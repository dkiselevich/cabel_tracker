require 'test_helper'

class ServiceUserGroupsControllerTest < ActionController::TestCase
  setup do
    @service_user_group = service_user_groups(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:service_user_groups)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create service_user_group" do
    assert_difference('ServiceUserGroup.count') do
      post :create, :service_user_group => @service_user_group.attributes
    end

    assert_redirected_to service_user_group_path(assigns(:service_user_group))
  end

  test "should show service_user_group" do
    get :show, :id => @service_user_group.to_param
    assert_response :success
  end

  test "should get edit" do
    get :edit, :id => @service_user_group.to_param
    assert_response :success
  end

  test "should update service_user_group" do
    put :update, :id => @service_user_group.to_param, :service_user_group => @service_user_group.attributes
    assert_redirected_to service_user_group_path(assigns(:service_user_group))
  end

  test "should destroy service_user_group" do
    assert_difference('ServiceUserGroup.count', -1) do
      delete :destroy, :id => @service_user_group.to_param
    end

    assert_redirected_to service_user_groups_path
  end
end
