module JobsHelper
	def status_name(status)
		return "zu disponieren" if (status == 0)
		return "disponiert" if (status == 1)
		return "wartend" if (status == 2)
		return "abgeschlossen" if (status == 3)
		return "technisch abgeschlossen" if (status == 4)
		return "storniert" if (status == 5)
	end
end
