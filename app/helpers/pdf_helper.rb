module PdfHelper
require 'RMagick'
include Magick
require 'aws/s3'

	def self.create_signature(signature_string, uid)
=begin	
		img = Magick::ImageList.new
		img.new_image(800, 200)

		line = Magick::Draw.new
		line.stroke('black')
		line.fill('none')
		line.stroke_width(5)

	
		arr = [] #[{x: 20, y:30},{x: 40, y:30},{x: 50, y:40},{x: 50, y:50}]

		if !signature_string.blank? then
			parts = signature_string.split(";")
			xs = parts[0].split(",")
			ys = parts[1].split(",")
			drags = parts[2].split(",")

			i = 0
			while ( i < xs.length) do
				arr.push({x: xs[i], y: ys[i], d: drags[i]})	
				i += 1
			end
		
			last = arr[0]
			arr.each do |c|
				if c[:d] == "1" then
					if (!c[:x].blank? && !c[:y].blank?) then
						
						line.line(last[:x],last[:y],  c[:x],c[:y])
						line.draw(img)
						last = c
					end
				else
					last = c
				end
			end
		end

		img.format = 'gif'
		local_file = "#{Rails.root.to_s}/tmp/signature_#{uid.to_s}.gif"

		begin
			Dir::mkdir("#{Rails.root.to_s}/tmp") #sometimes it seems to get deleted on heroku..?
		rescue
		end
		img.write(local_file)

		bucket = S3_CONFIG[::Rails.env]["bucket"]+"/signatures"
		mime_type = "image/gif"

		AWS::S3::Base.establish_connection!(
		  :access_key_id     => S3_CONFIG[::Rails.env]["access_key_id"],
		  :secret_access_key => S3_CONFIG[::Rails.env]["secret_access_key"]
		)

		base_name = File.basename(local_file)

		puts "Uploading #{local_file} as '#{base_name}' to '#{bucket}'"

		AWS::S3::S3Object.store(
		  base_name,
		  File.open(local_file),
		  bucket,
		  :content_type => mime_type,
		  :access => :public_read 
		)

		puts "Uploaded!"
=end
	end
end