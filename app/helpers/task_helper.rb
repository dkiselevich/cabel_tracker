module TaskHelper

	def self.send_test_email()
		AppointmentsMailer.test_mail().deliver
	end

	def self.send_pdf_with_appointments_for_tomorrow_to_all_technicians(client)
		User.service.assigned_to_company(client).each do |t|
			begin
				upcoming_appointments = Appointment.where(:user_id => t.id).where(:day => Date.tomorrow)
				if upcoming_appointments.count > 0
				  AppointmentsMailer.upcoming_appointments(t, upcoming_appointments.first.scheduling_user, upcoming_appointments).deliver
				else
				  puts "User has no appointments " 
				end
			rescue Exception => ex
				puts "Fehler bei Techniker #{t.id} (technician has no appointments)"
				puts ex
				#AdminMailer.report_exception(ex, "Error when send_pdf_with_appointments_for_tomorrow_to_all_technicians for technician with id: #{t.id}").deliver
			end
		end
		Admin::LogsController.create("System sent pdf with appointments for tomorrow to all technicians for "+client.name) 
	end

	def self.send_emails_with_blank_completion_pdf_for_each_appointment_tomorrow_to_all_technicians(client)
		puts "Emails pro Termin mit Auftragsabschlussformular an alle Techniker" 
		upcoming_appointments = Appointment.where(:day => Date.tomorrow)
		upcoming_appointments.each do |a|
			if (a.job.client == client) then
				begin
					puts "#{a.user.full_name}: #{a.short_description}"
					AppointmentsMailer.single_appointment(a.user, a.scheduling_user, a).deliver
				rescue Exception => ex
					puts "Fehler bei appointment #{a.id}"
					puts ex
					AdminMailer.report_exception(ex, "Error when sending PDF form for appointment with id: #{a.id}").deliver
				end
			end
		end	

		upcoming_service_appointments = ServiceAppointment.where(:day => Date.tomorrow)
		upcoming_service_appointments.each do |a|
			if (a.service_job.client == client) then
				begin
					puts "#{a.user.full_name}: #{a.short_description}"
					AppointmentsMailer.single_service_appointment(a.user, a.scheduling_user, a).deliver
				rescue Exception => ex
					puts "Fehler bei service_appointment #{a.id}"
					puts ex
					AdminMailer.report_exception(ex, "Error when sending PDF form for service_appointment with id: #{a.id}").deliver
				end
			end
		end
		
		Admin::LogsController.create("System sent emails with blank completion pdf for each appointment tomorrow to all technicians for "+client.name) 
	end


	def self.send_emails_with_blank_completion_pdf_for_each_appointment_today_to_all_technicians(client)
		puts "Emails pro Termin mit Auftragsabschlussformular an alle Techniker" 
		upcoming_appointments = Appointment.where(:day => Date.today)
		upcoming_appointments.each do |a|
			if (a.job.client == client) then
				begin
					puts "#{a.user.full_name}: #{a.short_description}"
					AppointmentsMailer.single_appointment(a.user, a.scheduling_user, a).deliver
				rescue Exception => ex
					puts "Fehler bei appointment #{a.id}"
					puts ex
					AdminMailer.report_exception(ex, "Error when sending PDF form for appointment with id: #{a.id}").deliver
				end
			end
		end	

		upcoming_service_appointments = ServiceAppointment.where(:day => Date.today)
		upcoming_service_appointments.each do |a|
			if (a.service_job.client == client) then
				begin
					puts "#{a.user.full_name}: #{a.short_description}"
					AppointmentsMailer.single_service_appointment(a.user, a.scheduling_user, a).deliver
				rescue Exception => ex
					puts "Fehler bei service_appointment #{a.id}"
					puts ex
					AdminMailer.report_exception(ex, "Error when sending PDF form for service_appointment with id: #{a.id}").deliver
				end
			end
		end
	end
end
