module AppointmentsHelper
	def display_state(state)
		case state
		when 0
			"offen"
		when 1
			"erledigt"
		else 
			"?"
		end
	end
end
