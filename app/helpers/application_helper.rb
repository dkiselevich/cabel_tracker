# encoding: utf-8
module ApplicationHelper
	def status_short(status)
		return "ZDE" if (status == 0)
		return "DI" if (status == 1)
		return "WK" if (status == 2)
		return "AG" if (status == 3)
		return "TA" if (status == 4)
		return "OM" if (status == 5)
	end

	def get_route
		#return controller.class.name.split("::").first.downcase
		request.env['PATH_INFO'].split("/").first(3).join("/")
	end

	def current_view
		view = controller.class.name.split("::").first.downcase 
		return "none" if view == "basecontroller"
		return "errors" if view == "errorscontroller"

	    return view if (["management","scheduling","scheduling_leader","service","accounting","admin","test", "purchase"].include?(view))
	    return cookies[:last_view] unless cookies[:last_view].blank?
	    return "errors"
	end

	def get_current_user_view_options
		options = Array.new
		options << ["-", ""] if (["none","errors"].include?(current_view))
		options << ["Geschäftsführung", "management"] if (current_user.is_boss)
		options << ["Disponierung", "scheduling"] if (current_user.is_controller)
		options << ["Disponierungsleitung", "scheduling_leader"] if (current_user.is_controller_leader)
		options << ["Aussendienst", "service"] if (current_user.is_technician)
		options << ["Einkaufsleitung", "purchase"] if (current_user.is_purchase_manager)
		options << ["Administration", "admin"] if (current_user.is_admin)
		return options
	end

	def sortable(column, title = nil)
		title ||= column.titlelize
		css_class = column == sort_column ? "current #{sort_direction}" : nil
		direction = column == sort_column && sort_direction == "asc" ? "desc" : "asc"
		link_to title, params.merge(:sort => column, :direction => direction, :page => nil), {:class => css_class}
	end

	def sort_link_custom(builder_name, column_name, title)
		search_param = builder_name+"[s]"
	
		current_order = nil
		if params[builder_name.to_sym] then
			if params[builder_name.to_sym][:s] then
				if params[builder_name.to_sym][:s].match(column_name.to_s) then
					current_order = params[builder_name.to_sym][:s].split(" ")[1]
				end
			end
		end

		new_order = "asc"
		new_order = "desc" if current_order == "asc"
#		link_to [ERB::Util.h(title), order_indicator_for(current_order)].compact.join('&nbsp;').html_safe,request.url+"?&"+search_param+"="+column_name.to_s+"+"+new_order

#		require "CGI" #TODO: WUERG! unescaping the url encoding of the whole link so that squeel/ransack can get the sort param
		raw CGI::unescape link_to [ERB::Util.h(title), order_indicator_for(current_order)].compact.join('&nbsp;').html_safe, {:controller => params[:controller], :action => params[:action], search_param => column_name.to_s+"+"+new_order}
	end

	def order_indicator_for(order)
	    if order == 'asc'
	      '&#9650;'
	    elsif order == 'desc'
	      '&#9660;'
	    else
	      nil
	    end
	end

	def display_active_state(state)
		if (state) then
			"ja" 
		else
			"nein"
		end
	end

	def simple_field(field, optional_class = "")
		return ("<span class='simple-field #{optional_class}'>"+field+'</span>').html_safe
	end
	def simple_boolean_select(form, record, attribute, label, disabled = false, required = false, include_blank = '-', no_radio_buttons = false)
		return simple_select(form, record, attribute, label, [["ja", true],["nein",false]], disabled, required, include_blank, (no_radio_buttons ? "" : "radiobuttons") )
	end
	
	def simple_select(form, record, attribute, label, options, disabled = false, required = false, include_blank = '-', extra_classes = '')
		label ||= attribute.to_s.titleize
		l = form.label(attribute, label, :class => "label"+(required ? " required" : ""))
		s = form.select(attribute, options_for_select(options, record[attribute]), {:include_blank => include_blank}, :class => "select #{extra_classes}", :disabled => disabled)
		return simple_field l+s
	end

	def simple_date_select(form, record, attribute, label, options, disabled = false, required = false)
		label ||= attribute.to_s.titleize
		l = form.label(attribute, label, :class => "label"+(required ? " required" : ""))
		s = form.date_select(attribute, :use_month_names => german_month_names, :include_blank => '-', :class => 'date_select', :disabled => disabled)
		return simple_field l+s,"date_select"
	end

	def simple_time_select(form, record, attribute, label, options, disabled = false, required = false)
		label ||= attribute.to_s.titleize
		l = form.label(attribute, label, :class => "label"+(required ? " required" : ""))
		s = form.time_select(attribute , :class => 'time_select', :minute_step => 15, :disabled => disabled)
		return simple_field l+s,"time_select"
	end

	def simple_text_field(form, record, attribute, label, disabled = false, required = false)
		label ||= attribute.to_s.titleize
		l = form.label(attribute, label, :class => "label"+(required ? " required" : ""))
		tf = form.text_field(attribute, :class => "text_field", :disabled => disabled)
		return simple_field(l+tf)
	end
	
	 def simple_text_area(form, record, attribute, label, disabled = false, required = false)
    label ||= attribute.to_s.titleize
    l = form.label(attribute, label, :class => "label"+(required ? " required" : ""))
    tf = form.text_area(attribute, :class => "text_area", :disabled => disabled, :style => "width: 400px; height: 80px;")
    return simple_field(l+tf)
  end

	def simple_number_field(form, record, attribute, label, disabled = false, required = false)
		label ||= attribute.to_s.titleize
		l = form.label(attribute, label, :class => "label"+(required ? " required" : ""))
		tf = form.number_field(attribute, :class => "text_field", :disabled => disabled)
		return simple_field(l+tf)
	end

	def simple_check_box(form, record, attribute, label, disabled = false, required = false)
		label ||= attribute.to_s.titleize
		l = form.label(attribute, label, :class => "label"+(required ? " required" : ""))
		cb = form.check_box(attribute, :class => "check_box", :disabled => disabled)
		return simple_field l+cb
	end

=begin
	def simple_photo_field(form, record, attribute, label, disabled = false, required = false)
		label ||= attribute.to_s.titleize
		l = form.label(attribute, label, :class => "label"+(required ? " required" : ""))
		p "----------------- "+record.attributes()[:bvt_photo_file_name].to_s
		if (record[attribute]) then 
			return "asdfsd"
		end

		pf = form.file_field(attribute, :class => "photo_field", :disabled => disabled)
		return simple_field(l+pf)
	end
=end

	def german_month_names
		[
			"Januar",
			"Februar",
			"März",
			"April",
			"Mai",
			"Juni",
			"Juli",
			"August",
			"September",
			"Oktober",
			"November",
			"Dezember"
		]
	end

	def link_to_phone(number)
		link_to number.to_s, "tel:"+number.to_s, "data-role" => "", :rel => "external"
	end
	def link_to_email(email)
		link_to email, "mail:"+email, "data-role" => "", :rel => "external"
	end

	def number_format(number)
		number_with_precision(number, :precision => 2, :separator => ',', :delimiter => '.') if (!number.blank?)
	end

	def bool_format(b)
		b ? "Ja" : "Nein"
	end

	def date_format(d)
		d.strftime("%d.%m.%Y") if d
	end

	def time_format(t)
		t.strftime("%H:%M:%S") if t
	end

	def currency_format(m)
		number_to_currency(m, :precision => 2, :unit => "&euro;", :separator => ",", :delimiter => ".")
	end

	def time_format_short(t)
		t.strftime("%H:%M") if t
	end

	def to_js_array(ruby_array)
		"[#{ruby_array.join(',')}]"
	end

end
