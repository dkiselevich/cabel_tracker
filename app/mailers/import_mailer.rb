# encoding: utf-8
class ImportMailer < ActionMailer::Base
  	helper :application
 	default :from => "cabletracker@3kabel.de"
  
  def report_imports(new_jobs, existing_jobs, blocked_jobs, scheduling_user)
    @new_jobs = new_jobs
    @existing_jobs = existing_jobs
    @blocked_jobs = blocked_jobs
    bcc = nil
    #bcc = scheduling_user.scheduling_user_group.bcc_email_receiver if scheduling_user.scheduling_user_group
		mail(:to => scheduling_user.email,  :from => "cabletracker@3kabel.de",
         :subject => "CT: Aufträge importiert - #{Date.today.strftime("%d.%m.%Y")}", :bcc => bcc)    
  end

  def report_service_job_imports(new_jobs, existing_jobs, blocked_jobs, scheduling_user)
    @new_jobs = new_jobs
    @existing_jobs = existing_jobs
    @blocked_jobs = blocked_jobs
    bcc = nil
    #bcc = scheduling_user.scheduling_user_group.bcc_email_receiver if scheduling_user.scheduling_user_group
		mail(:to => scheduling_user.email,  :from => "cabletracker@3kabel.de",
         :subject => "CT: Aufträge importiert - #{Date.today.strftime("%d.%m.%Y")}", :bcc => bcc)    
  end
end
