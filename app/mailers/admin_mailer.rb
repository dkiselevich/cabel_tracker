# encoding: utf-8
class AdminMailer < ActionMailer::Base
	helper :application
	default :from => "cabletracker@3kabel.de"
	
	def report_exception(ex, message = "Exception aufgetreten")
		@ex = ex
		@message = message
		#mail(:to => "frank.duffner@gmail.com",  :from => "cabletracker@3kabel.de",
		mail(:to => "christian@arkavis.com",  :from => "cabletracker@3kabel.de",
			:subject => "CT: Exception aufgetreten - #{Date.today.strftime("%d.%m.%Y")}")    
	end
end
