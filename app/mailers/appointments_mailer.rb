# encoding: utf-8
class AppointmentsMailer < ActionMailer::Base
	helper :application
	default :from => "cabletracker@3kabel.de"

	def fix_filename(s)
		s = s.gsub("ö","oe")
		s = s.gsub("ü","ue")
		s = s.gsub("ä","ae")
		s = s.gsub("Ö","Oe")
		s = s.gsub("Ü","ue")
		s = s.gsub("Ä","Ae")
		s = s.gsub("ß","ss")
		s = s.gsub(" ","-")
		s = s.gsub("/","-")
		return s
	end

	def upcoming_appointments(service_user, scheduling_user, appointments)
		@service_user = service_user
		@scheduling_user = scheduling_user
		@appointments = appointments

    #create pdf in tmp
    file_name = fix_filename("Termine #{service_user.full_name} #{Date.today.strftime("%d.%m.%Y")}.pdf")
    file_path = "#{Rails.root.to_s}/tmp/#{file_name}"
    html = render_to_string(:service_user => "service_user", :appointments => "appointments", :template => 'appointments_mailer/upcoming_appointments.html.erb')
    kit = PDFKit.new(html)
    begin
		Dir::mkdir("#{Rails.root.to_s}/tmp") #sometimes it seems to get deleted on heroku..?
	rescue
	end
	file = kit.to_file(file_path)

	#attach pdf and send
	attachments[file_name] = File.read(file_path)

	bcc = nil
	bcc = @scheduling_user.scheduling_user_group.bcc_email_receiver if @scheduling_user.scheduling_user_group
	email = "no-reply@cabletracker.de"
	email = @scheduling_user.scheduling_user_group.sender_email if @scheduling_user.scheduling_user_group
	mail(:to => @service_user.email,  :from => email,
		:subject => "CT: Kommende Termine - #{Date.today.strftime("%d.%m.%Y")}", :cc => bcc)    
end

def tomorrow_appointments_forms(service_user, scheduling_user, appointments)
	puts "new tomorrow_appointments_forms!!!!!!!!!!!!"
	appointments.each do |appointment|
		single_appointment(service_user, scheduling_user, appointment)
	end
end

def tomorrow_appointments_forms_org(service_user, scheduling_user, appointments)
	
puts "1"
	@service_user = service_user
	@scheduling_user = scheduling_user
	@appointments = appointments
	
	begin
		Dir::mkdir("#{Rails.root.to_s}/tmp") #sometimes it seems to get deleted on heroku..?
	rescue
	end
puts "2"
	appointment =  @appointments.each do |appointment|
	    puts "3"
			#create fake completion for use with erb renderer
	    @completion = Completion.new
	    @completion.appointment = appointment
	    @completion.job_type = appointment.job.job_type
	    @completion.network_type = appointment.job.network_type
	    @completion.vorverstaerker_id = 0
	    @completion.technician = appointment.user
			@empty_form = true #special flag

	    #create pdf in tmp
	    file_name = fix_filename("#{appointment.short_for_mail}.pdf")
	    file_path = "#{Rails.root.to_s}/tmp/#{file_name}"
	    html = render_to_string(:empty_form => "empty_form", :completion => "completion", :template => 'completions/pdf_v4.html.erb')
	    kit = PDFKit.new(html)

	    file = kit.to_file(file_path)

		#attach pdf
		attachments[file_name] = File.read(file_path)
	end
puts "4"

	bcc = nil
	bcc = @scheduling_user.scheduling_user_group.bcc_email_receiver if @scheduling_user.scheduling_user_group

	email = "no-reply@cabletracker.de"
	email = @scheduling_user.scheduling_user_group.sender_email if @scheduling_user.scheduling_user_group
	mail(:to => @service_user.email,  :from => email,
		:subject => "CT: Kommende Termine - #{Date.today.strftime("%d.%m.%Y")}", :cc => bcc)  
	puts "5"

end

def single_appointment(service_user, scheduling_user, appointment)
	#client = Client.find(3) #TODO
	client = Client.find(Client.last) #TODO
	#OtHelper.update_modem_type_for_job_from_ot(client, scheduling_user, appointment.job)

	@service_user = service_user
	@scheduling_user = scheduling_user
	@appointment = appointment

	puts "preparing mail for appointment #{appointment.id}..."


    #create fake completion for use with erb renderer
    @completion = Completion.new
    @completion.appointment = @appointment
    @completion.job_type = @appointment.job.job_type
    @completion.network_type = @appointment.job.network_type
    @completion.vorverstaerker_id = 0
    @completion.technician = @appointment.user
		@empty_form = true #special flag

    #create pdf in tmp
    file_name = fix_filename("#{@appointment.short_for_mail}.pdf")
    file_path = "#{Rails.root.to_s}/tmp/#{file_name}"
    html = render_to_string(:empty_form => "empty_form", :completion => "completion", :template => 'completions/pdf_v4.html.erb')
    kit = PDFKit.new(html)
    begin
			Dir::mkdir("#{Rails.root.to_s}/tmp") #sometimes it seems to get deleted on heroku..?
		rescue
	end
	file = kit.to_file(file_path)

	#attach pdf and send
	attachments[file_name] = File.read(file_path)
	
	bcc = nil
	bcc = @scheduling_user.scheduling_user_group.bcc_email_receiver if @scheduling_user.scheduling_user_group
	
	puts "sending now to #{@service_user.email}"
	#mail(:to => @service_user.email, :from => @scheduling_user.scheduling_user_group.sender_email,
	mail(:to => @service_user.email, :from => "cabletracker@3kabel.de",
		:subject => "CT: #{@appointment.short_for_mail}", :cc => bcc)  
   
	puts "done."
end




def single_service_appointment(service_user, scheduling_user, service_appointment)
	@service_user = service_user
	@scheduling_user = scheduling_user
	@service_appointment = service_appointment

	puts "preparing mail for service_appointment #{service_appointment.id}..."
    #create fake completion for use with erb renderer
    @service_completion = ServiceCompletion.new
    @service_completion.service_appointment = @service_appointment
    @service_completion.service_appointment = @service_appointment
    @service_completion.technician = @service_appointment.user
		@empty_form = true #special flag

    #create pdf in tmp
    file_name = fix_filename("#{@service_appointment.short_for_mail}.pdf")
    file_path = "#{Rails.root.to_s}/tmp/#{file_name}"
    html = render_to_string(:empty_form => "empty_form", :service_completion => "service_completion", :template => 'service_completions/pdf.html.erb')
    kit = PDFKit.new(html)
    begin
			Dir::mkdir("#{Rails.root.to_s}/tmp") #sometimes it seems to get deleted on heroku..?
		rescue
	end
	file = kit.to_file(file_path)

	#attach pdf and send
	attachments[file_name] = File.read(file_path)
	
	bcc = nil
	bcc = @scheduling_user.scheduling_user_group.bcc_email_receiver if @scheduling_user.scheduling_user_group
	
	puts "sending now to #{@service_user.email}"
	mail(:to => @service_user.email, :from =>"info@cabletracker.de",
		:subject => "CT: #{@service_appointment.short_for_mail}", :cc => bcc)      
	puts "done."
end


# Test Mail in der Konsole absenden: AppointmentsMailer.test_mail.deliver
def test_mail()
	puts "sending test mail..."
  #recipient = "frank.duffner@gmail.com"
  recipient = "christian@arkavis.com"
  mail(:to => recipient, :from => "cabletracker@3kabel.de",
		:subject => "CT: TEST")      
	puts "done."
	puts smtp_settings
end




end
