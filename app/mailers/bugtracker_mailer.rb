class BugtrackerMailer < ActionMailer::Base
  default from: "christian@arkavis.com"

  def new_bug(bug)
		@bug = bug
		if Rails.env.production?
			mail(:to => "frank@arkavis.com", :subject => "Neue Bug-Meldung")
		else
			mail(:to => "christian@arkavis.com", :subject => "Neue Bug-Meldung")
		end
	end

	def bug_solved(bug)
		@bug = bug
		mail(:to => @bug.email, :subject => "Ihr gemeldeter Bug wurde behoben")
	end
end
