// Place your application-specific JavaScript functions and classes here
// This file is automatically included by javascript_include_tag :defaults


//= require jquery
//= require jquery_ujs
//= require customSelect.jquery.js
//= require highcharts.js
//= require jquery.quicksearch.js
//= require jquery.tablesorter.js
//= require jquery.autocomplete.min.js
//= require jquery.easing-1.3.pack.js
//= require jquery.fancybox-1.3.4.pack.js
//= require jquery.filestyle
//= require jquery.mousewheel-3.0.4.pack.js
//= require jquery.query.js
//= require jquery.watermark.min
//= require underscore-min.js
//= require search
//= require bugtrackers