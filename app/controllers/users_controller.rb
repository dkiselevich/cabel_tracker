class UsersController < ApplicationController
  before_filter :get_job_count_by_status, :if => Proc.new { |controller| ["scheduling","scheduling_leader"].include?(current_view) } 

  # GET /users
  # GET /users.xml
  def index
    @users = User.paginate :per_page => Settings.DEFAULT_ITEMS_PER_TABLE, :page => params[:page]
    
    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @users }
    end
  end

  # GET /users/1
  # GET /users/1.xml
  def show
    @user = User.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @user }
    end
  end

  # GET /users/new
  # GET /users/new.xml
  def new
    @user = User.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @user }
    end
  end

  # GET /users/1/edit
  def edit
    @user = User.find(params[:id])
    redirect_to "/no_right" if @user != current_user
  end

  # POST /users
  # POST /users.xml
  def create
    @user = User.new(params[:user])

    respond_to do |format|
      if @user.save
        Admin::LogsController.create(current_user.vorname+" "+current_user.nachname+" created user "+@user.vorname+" "+@user.nachname, @user.id, "user") 
        format.html { redirect_to(@user, :notice => 'User was successfully created.') }
        format.xml  { render :xml => @user, :status => :created, :location => @user }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @user.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /users/1
  # PUT /users/1.xml
  def update
    @user = User.find(params[:id])

    respond_to do |format|
      if @user.update_attributes(params[:user])
        Admin::LogsController.create(current_user.vorname+" "+current_user.nachname+" updated user "+@user.vorname+" "+@user.nachname, @user.id, "user") 
        @user.delay.create_signature(@user.signature, "technician_"+@user.id.to_s)   

        expire_action :action => :show
        flash[:notice] = 'Benutzerdaten wurden gespeichert.'
        format.html { render :action => "edit" }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @user.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /users/1
  # DELETE /users/1.xml
  def destroy
    @user = User.find(params[:id])
    Admin::LogsController.create(current_user.vorname+" "+current_user.nachname+" deleted user "+@user.vorname+" "+@user.nachname) 
    @user.destroy

    expire_action :action => :show
    respond_to do |format|
      format.html { redirect_to(users_url) }
      format.xml  { head :ok }
    end
  end
end
