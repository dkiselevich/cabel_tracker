class Android::TokensController < ApplicationController
    skip_before_filter :prepare_for_mobile
    skip_before_filter :authenticate_user!
    skip_before_filter :check_rights_for_view
    skip_before_filter :verify_authenticity_token
    respond_to :json
    
    
  def create
    email = params[:email]
    password = params[:password]
    if request.format != :json
      render :status=>400, :json=>{:message=>"The request must be json"}
      return
    end
 
    if email.nil? or password.nil?
       render :status=>400, :json=>{:message=>"The request must contain the user email and password."}
       return
    end
 
    @user=User.find_by_email(email.downcase)
 
    if @user.nil?
      render :status=>403, :json=>{:message=>"Invalid email or passoword."}
      return
    end

    # http://rdoc.info/github/plataformatec/devise/master/Devise/Models/TokenAuthenticatable
    @user.ensure_authentication_token!
 
    if not @user.valid_password?(password)
      render :status=>403, :json=>{:message=>"Invalid email or password."}
    else
      render :status=>200, :json=>{:token=>@user.authentication_token}
    end
  end
 
  def destroy
    @user=User.find_by_authentication_token(params[:id])
    if @user.nil?
      logger.info("Token not found.")
      render :status=>404, :json=>{:message=>"Invalid token."}
    else
      @user.reset_authentication_token!
      render :status=>200, :json=>{:token=>params[:id]}
    end
  end
 
end