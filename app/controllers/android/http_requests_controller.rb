class Android::HttpRequestsController < ApplicationController
  skip_before_filter :prepare_for_mobile
  skip_before_filter :authenticate_user!
  skip_before_filter :check_rights_for_view

  

  def getTermine
    if !params[:auth_token].nil?
      token = params[:auth_token]
      user = User.find_by_authentication_token(token)

      if user
        appointments = Appointment.where(:user_id => user.id).order("day ASC").limit(10)
        service_appointments = ServiceAppointment.where(:user_id => user.id).order("day ASC").limit(10)
        
        result = Hash.new
        result["appointment"] = Array.new
        counter = 0    
        appointments.each do |a|       
          result["appointment"][counter] = Hash.new
          # Termindetails
          result["appointment"][counter][:appointment_id] = a.id  
          result["appointment"][counter][:day] = a.day
          result["appointment"][counter][:start_time] = a.start_time.strftime("%H:%M")
          result["appointment"][counter][:end_time] = a.end_time.strftime("%H:%M")
          result["appointment"][counter][:high_priority] = a.high_priority
          result["appointment"][counter][:bemerkungen] = a.bemerkungen      
          
          # Auftragsdaten
          result["appointment"][counter][:status] = a.job.status
          result["appointment"][counter][:auftragsart] = a.job.auftragsart_id
          result["appointment"][counter][:workorder] = a.job.workorder
          result["appointment"][counter][:kundennummer] = a.job.kunde_kundennummer  
          result["appointment"][counter][:ot_auftragsnummer] = a.job.ot_auftragsnummer 
          result["appointment"][counter][:ot_id] = a.job.ot_id
          result["appointment"][counter][:modemtyp] = a.job.modemtyp_id
          
          # Kunde
          result["appointment"][counter][:kunde_vorname] = a.job.kunde_vorname
          result["appointment"][counter][:kunde_nachname] = a.job.kunde_nachname 
          result["appointment"][counter][:kunde_plz] = a.job.kunde_plz
          result["appointment"][counter][:kunde_ort] = a.job.kunde_ort
          result["appointment"][counter][:kunde_strasse] = a.job.kunde_strasse
          result["appointment"][counter][:kunde_hausnummer] = a.job.kunde_hausnummer
          result["appointment"][counter][:neue_anschlussnummer] = a.job.telefon_neu
          result["appointment"][counter][:kunde_telefon_privat] = a.job.kunde_telefon_privat 
          result["appointment"][counter][:kunde_telefon_mobil] = a.job.kunde_telefon_mobil
          result["appointment"][counter][:kunde_telefon_geschaeftlich] = a.job.kunde_telefon_geschaeftlich
          result["appointment"][counter][:kunde_email] = a.job.kunde_email
          result["appointment"][counter][:kunde_is_eigentuemer] = a.job.kunde_is_eigentuemer
          
          # Eigentümer
          result["appointment"][counter][:eigentuemer_vorname] = a.job.kunde_vorname
          result["appointment"][counter][:eigentuemer_nachname] = a.job.kunde_nachname 
          result["appointment"][counter][:eigentuemer_plz] = a.job.eigentuemer_plz
          result["appointment"][counter][:eigentuemer_ort] = a.job.eigentuemer_ort
          result["appointment"][counter][:eigentuemer_strasse] = a.job.eigentuemer_strasse
          result["appointment"][counter][:eigentuemer_hausnummer] = a.job.eigentuemer_hausnummer
          result["appointment"][counter][:eigentuemer_telefon_1] = a.job.eigentuemer_telefon_1
          result["appointment"][counter][:eigentuemer_telefon_2] = a.job.eigentuemer_telefon_2
          result["appointment"][counter][:eigentuemer_email] = a.job.eigentuemer_email
          
          # Objekt
          result["appointment"][counter][:objektart] = a.job.objektart_id
          
          counter += 1          
        end
        result["service_appointment"] = Array.new 
        counter = 0
        service_appointments.each do |sa|
          result["service_appointment"][counter] = Hash.new
          # Termindetails
          result["service_appointment"][counter][:appointment_id] = sa.id  
          result["service_appointment"][counter][:day] = sa.day
          result["service_appointment"][counter][:start_time] = sa.start_time.strftime("%H:%M")
          result["service_appointment"][counter][:end_time] = sa.end_time.strftime("%H:%M")
          result["service_appointment"][counter][:high_priority] = sa.high_priority
          result["service_appointment"][counter][:bemerkungen] = sa.bemerkungen      
          
          # Auftragsdaten
          result["service_appointment"][counter][:status] = sa.job.status
          result["service_appointment"][counter][:auftragsklasse] = sa.job.auftragsklasse
          #result["service_appointment"][counter][:workorder] = sa.job.workorder
          result["service_appointment"][counter][:kundennummer] = sa.job.kunde_kundennummer
          result["service_appointment"][counter][:ot_auftragsnummer] = sa.job.ot_auftragsnummer 
          result["service_appointment"][counter][:ot_id] = sa.job.ot_id
          #result["service_appointment"][counter][:modemtyp] = sa.job.modemtyp_id
          
          # Kunde
          result["service_appointment"][counter][:kunde_vorname] = sa.job.kunde_vorname
          result["service_appointment"][counter][:kunde_nachname] = sa.job.kunde_nachname 
          result["service_appointment"][counter][:kunde_plz] = sa.job.kunde_plz
          result["service_appointment"][counter][:kunde_ort] = sa.job.kunde_ort
          result["service_appointment"][counter][:kunde_strasse] = sa.job.kunde_strasse
          result["service_appointment"][counter][:kunde_hausnummer] = sa.job.kunde_hausnummer
          #result["service_appointment"][counter][:neue_anschlussnummer] = sa.job.telefon_neu
          result["service_appointment"][counter][:kunde_telefon_privat] = sa.job.kunde_telefon_privat 
          result["service_appointment"][counter][:kunde_telefon_mobil] = sa.job.kunde_telefon_mobil
          result["service_appointment"][counter][:kunde_telefon_geschaeftlich] = sa.job.kunde_telefon_geschaeftlich
          result["service_appointment"][counter][:kunde_email] = sa.job.kunde_email
          #result["service_appointment"][counter][:kunde_is_eigentuemer] = sa.job.kunde_is_eigentuemer
          
          # Eigentümer
          # result["service_appointment"][counter][:eigentuemer_vorname] = sa.job.kunde_nachname 
          # result["service_appointment"][counter][:eigentuemer_nachname] = sa.job.kunde_nachname 
          # result["service_appointment"][counter][:eigentuemer_plz] = sa.job.eigentuemer_plz
          # result["service_appointment"][counter][:eigentuemer_ort] = sa.job.eigentuemer_ort
          # result["service_appointment"][counter][:eigentuemer_strasse] = sa.job.eigentuemer_strasse
          # result["service_appointment"][counter][:eigentuemer_hausnummer] = sa.job.eigentuemer_hausnummer
          # result["service_appointment"][counter][:eigentuemer_telefon_1] = sa.job.eigentuemer_telefon_1
          # result["service_appointment"][counter][:eigentuemer_telefon_2] = sa.job.eigentuemer_telefon_2
          # result["service_appointment"][counter][:eigentuemer_email] = sa.job.eigentuemer_email
          
          # Objekt
          #result["service_appointment"][counter][:objektart] = sa.job.objektart_id


          counter += 1          
        end
        render :status => 200, :json => result        
      else
        render :status=>400, :json=>{:message=>"Authentication token invalid"}
      end
      
    else
      result = Hash.new
      result[:message] = "Zugriff verweigert"
      render :status => 400, :json => result
    end  
    
  end
  
  def postAuftragsabschluss
    if !params[:auth_token].nil?
      token = params[:auth_token]
      user = User.find_by_authentication_token(token)
      
      if user
        completion = Completion.new(params[:completion])
        completion.technician = user
        completion.appointment = Appointment.belonging_to_user_client(user).find(params[:completion][:appointment_id])
        puts "completion appointment: "+completion.appointment.id.to_s
        if completion.save
          completion.appointment.job.status = 3 #abgeschlossen
          completion.appointment.job.save
          puts "completion saved"
          render :status=>200, :json=>{:message=>"Auftragsabschlussformular erfolgreich gespeichert"}
        else
          render :status=>400, :json=>{:message=>"Speichern fehlgeschlagen"}
        end
        
      else
        render :status=>400, :json=>{:message=>"Authentication token invalid"}
      end
      
    else
      render :status=>400, :json=>{:message=>"Authentication missing"}
    end   
  end
  
  def index
    
    render :inline => "index"
  end
  
  def show
    
    render :inline => "show"
  end
  


end
