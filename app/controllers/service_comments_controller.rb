# encoding: utf-8
class ServiceCommentsController < InheritedResources::Base
  actions :index, :new, :create, :show, :edit, :update

  def index
    @service_comments = ServiceComment.where(:service_job_id => params[:service_job_id])
    render "service_comments/index", :layout => false
  end

  def new
    @service_comment = ServiceComment.new
    @service_comment.service_job_id = params[:service_job_id]
    render :layout => "pop_up"
  end

  def create
    @service_comment = ServiceComment.new(params[:service_comment])
    @service_comment.user = current_user
    @service_comment.service_job = ServiceJob.find(params[:service_job_id])
    @service_comment.save
    render :layout => false
  end
end
