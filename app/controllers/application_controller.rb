class ApplicationController < ActionController::Base
  before_filter :prepare_for_mobile
  before_filter :authenticate_user!, :unless => proc { |c| c.devise_controller? }
  before_filter :check_rights_for_view, :unless => proc { |c| c.devise_controller? || params[:action] == "no_right"}
  protect_from_forgery
  layout :specify_layout

  unless Rails.application.config.consider_all_requests_local
    rescue_from Exception, :with => :render_error
    rescue_from ActiveRecord::RecordNotFound, :with => :render_not_found
    rescue_from ActionController::RoutingError, :with => :render_not_found
    rescue_from ActionController::UnknownController, :with => :render_not_found
    rescue_from ActionController::UnknownAction, :with => :render_not_found
  end
# ExceptionNotifier::Notifier
#      .exception_notification(request.env, exception)
#      .deliver

  private

    def format_report(report)
      respond_to do |format|
          format.pdf { send_data(report.render_pdf, :filename => "#{report.name}.pdf", :type => 'application/pdf', :disposition => 'inline') }
          format.html { render :inline => report.render_html, :layout => params[:layout] }
          format.csv { send_data(report.render_csv, :filename => "#{report.name}.csv") }
      end
    end

    def render_error(exception)
      #ExceptionNotifier::Notifier.exception_notification(request.env, exception).deliver
      render "general/error", :layout => false
    end

    def render_not_found(exception)
      #ExceptionNotifier::Notifier.exception_notification(request.env, exception).deliver
      render "general/not_found", :layout => false
    end

    def specify_layout
      if devise_controller? && resource_name == :user && (params[:action] == 'new')
        false
      else
        "application"
      end
    end

    #def get_namespace
    #  self.class.to_s.split("::").first
    #end

    def check_rights_for_view

      return redirect_to "/no_right?view=service" if !current_user.active

      case current_view
        when "service"
          redirect_to "/no_right?view=service" if !current_user.is_technician
        when "scheduling"
          redirect_to "/no_right?view=scheduling" if !current_user.is_controller
        when "scheduling_leader"
          redirect_to "/no_right?view=scheduling_leader" if !current_user.is_controller_leader
        when "admin"
          redirect_to "/no_right?view=admin" if !current_user.is_admin
        when "management"
          redirect_to "/no_right?view=management" if !current_user.is_boss
        when "accounting"
          redirect_to "/no_right?view=accounting" if !current_user.is_accountant
      end
    end

    def current_view
      view = self.class.name.split("::").first.downcase
      view = 'scheduling_leader' if view == "schedulingleader"
      return view if (["management","scheduling","scheduling_leader","service","accounting","admin"].include?(view))
      return cookies[:last_view] unless cookies[:last_view].blank?
      return "error"
    end

    def get_job_count_by_status
      is_client_filter = "client_id in (#{current_user.clients.collect(&:id).join(',')})"
      @job_count_by_status = {
        "ZDE" => Job.count(:conditions => "status = 0 and #{is_client_filter}"),
        "DI" => Job.count(:conditions => "status = 1 and #{is_client_filter}"),
        "WK" => Job.count(:conditions => "status = 2 and #{is_client_filter}"),
        "AG" => Job.count(:conditions => "status = 3 and #{is_client_filter}"),
        "TA" => Job.count(:conditions => "status = 4 and #{is_client_filter}"),
        "OM" => Job.count(:conditions => "status = 5 and #{is_client_filter}"),
      }
      @service_job_count_by_status = {
        "ZDE" => ServiceJob.count(:conditions => "status = 0 and #{is_client_filter}"),
        "DI" => ServiceJob.count(:conditions => "status = 1 and #{is_client_filter}"),
        "WK" => ServiceJob.count(:conditions => "status = 2 and #{is_client_filter}"),
        "AG" => ServiceJob.count(:conditions => "status = 3 and #{is_client_filter}"),
        "TA" => ServiceJob.count(:conditions => "status = 4 and #{is_client_filter}"),
        "OM" => ServiceJob.count(:conditions => "status = 5 and #{is_client_filter}"),
      }
    end

private
  def mobile_device?
    if session[:mobile_param]
      return true
    end
  end
  helper_method :mobile_device?

  def prepare_for_mobile
    if params[:mobile]
      session[:mobile_param] = true
    else
      session[:mobile_param] = false
    end
    #request.format = :mobile if mobile_device?
  end

  def get_quotes_for_user(user)

    ea_completions = Completion.joins(:job_type).where(:technician => user, "job_types.shortname" => "EA").length
    fa_completions = Completion.joins(:job_type).where(:technician => user, "job_types.shortname" => "FA").length

    ea_cancellations = Cancellation.joins(:appointment => [ :job => [:job_type]]).where(:user => user, "job_types.shortname" => "EA").length
    fa_cancellations = Cancellation.joins(:appointment => [ :job => [:job_type]]).where(:user => user, "job_types.shortname" => "FA").length

    all_ea_appointments = Appointment.joins(:job => [ :job_type]).where(:user => user, "job_types.shortname" => "EA").length
    all_fa_appointments = Appointment.joins(:job => [ :job_type]).where(:user => user, "job_types.shortname" => "FA").length

    @quotes = Hash.new
    @quotes[:succesfull] =
    [
      ea_completions,
      fa_completions,
      0
    ]
    @quotes[:all] =
    [
      ea_completions+ea_cancellations,
      fa_completions+fa_cancellations,
      0
    ]

    @ea_success_quote = ((@quotes[:all][0] == 0) ? 0 : ((@quotes[:succesfull][0].to_f / @quotes[:all][0].to_f) *100)).to_i.to_s+"%"
    @fa_success_quote = ((@quotes[:all][1] == 0) ? 0 : ((@quotes[:succesfull][1].to_f / @quotes[:all][1].to_f) *100)).to_i.to_s+"%"
    @other_success_quote = ((@quotes[:all][2] == 0) ? 0 : ((@quotes[:succesfull][2].to_f / @quotes[:all][2].to_f) *100)).to_i.to_s+"%"
  end
end

class ActionDispatch::Request
  def local?
    false
  end
end