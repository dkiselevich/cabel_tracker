# encoding: utf-8
class ServiceJobsController < InheritedResources::Base
  #caches_action :index , :cache_path => Proc.new { |controller| controller.params.to_s + current_user.client_names }

  def all
    @search = ServiceJob.search(params[:q])
    @service_jobs = @search.result.belonging_to_user_client(current_user).paginate :per_page => Settings.DEFAULT_ITEMS_PER_TABLE, :page => params[:page]
    #@service_jobs = ServiceJob.ot_auftragsnummer_like(params[:ot_auftragsnummer]).workorder_like(params[:workorder]).mandant_id_like(params[:mandant_id]).kunde_plz_like(params[:kunde_plz]).kunde_ort_like(params[:kunde_ort]).kunde_strasse_like(params[:kunde_strasse]).kunde_hausnummer_like(params[:kunde_hausnummer]).kunde_nachname_like(params[:kunde_nachname]).kunde_vorname_like(params[:kunde_vorname]).kunde_plz_like(params[:kunde_kundennummer]).eigentuemer_nachname_like(params[:eigentuemer_nachname]).eigentuemer_vorname_like(params[:eigentuemer_vorname]).order(sort_column + " " + sort_direction).paginate :per_page => Settings.DEFAULT_ITEMS_PER_TABLE, :page => params[:page]
    #@service_jobs = ServiceJob.where("ot_auftragsnummer LIKE '%#{params[:ot_auftragsnummer]}%'").where("kunde_plz LIKE '%#{params[:kunde_plz]}%'").where("kunde_ort LIKE '%#{params[:kunde_ort]}%'").where("kunde_strasse LIKE '%#{params[:kunde_strasse]}%'").where("kunde_hausnummer LIKE '%#{params[:kunde_hausnummer]}%'").where("kunde_nachname LIKE '%#{params[:kunde_nachname]}%'").where("kunde_vorname LIKE '%#{params[:kunde_vorname]}%'").where("kunde_kundennummer LIKE '%#{params[:kunde_kundennummer]}%'").where("eigentuemer_nachname LIKE '%#{params[:eigentuemer_nachname]}%'").where("eigentuemer_vorname LIKE '%#{params[:eigentuemer_vorname]}%'").order(sort_column + " " + sort_direction).paginate :per_page => Settings.DEFAULT_ITEMS_PER_TABLE, :page => params[:page]
    #@service_jobs = ServiceJob.order(sort_column + " " + sort_direction).paginate :per_page => Settings.DEFAULT_ITEMS_PER_TABLE, :page => params[:page]

    render :action => "index", :layout => "application"
  end

  def index
    if params[:q] && params[:q][:s] && params[:q][:s].include?("kunde_string")
      @search = ServiceJob.search(params[:q])
      @service_jobs = ServiceJob.belonging_to_user_client(current_user).belonging_to_scheduling_user_group(current_user).order(params[:q][:s].sub("kunde_string", "kunde_plz")).order(params[:q][:s].sub("kunde_string", "kunde_ort")).order(params[:q][:s].sub("kunde_string", "kunde_strasse")).order(params[:q][:s].sub("kunde_string", "kunde_hausnummer")).paginate :per_page => Settings.DEFAULT_ITEMS_PER_TABLE, :page => params[:page]
    else
      @search = ServiceJob.search(params[:q])
      @service_jobs = @search.result.belonging_to_user_client(current_user).belonging_to_scheduling_user_group(current_user).paginate :per_page => Settings.DEFAULT_ITEMS_PER_TABLE, :page => params[:page]
    end

    render :layout => "application"
  end

  def show
    @service_job = ServiceJob.find(params[:id])
    #@other_service_jobs_in_object = get_other_jobs_in_object(@service_job)
    #fresh_when(:etag => @service_job, :last_modified => @service_job.updated_at.utc, :public => false) unless (Rails.env == "development")
  end

  def edit
    @service_job = ServiceJob.find(params[:id])
    #@other_service_jobs_in_object = get_other_service_jobs_in_object(@service_job)
    #fresh_when(:etag => @service_job, :last_modified => @service_job.updated_at.utc, :public => false) unless (Rails.env == "development")
  end

  def create
    @service_job = ServiceJob.new(params[:service_job])

    if @service_job.save
      redirect_to([current_view, :service_jobs], :notice => 'Auftrag erstellt: '+self.class.helpers.link_to(@service_job.short_description, [current_view, @service_job]))
    else
      render :action => "new"
    end
  end

  def update
    @service_job = ServiceJob.find(params[:id])
    if @service_job.update_attributes(params[:service_job])
      redirect_to([current_view, @service_job], :notice => 'Auftrag upgedated: '+ self.class.helpers.link_to(@service_job.short_description, url_for([current_view, @service_job])))
    else
      render :action => "edit"
    end
  end

  def destroy
    @service_job = ServiceJob.find(params[:id])
    @service_job.destroy
    redirect_to([current_view, :service_jobs], :notice => 'Auftrag gelöscht.')
  end

  def close
    @service_job = ServiceJob.find(params[:id])
    @service_job.status = 4
    #@service_job.closed = true
    @service_job.save
    redirect_to :controller => "jobs", :action => "closed"
  end


  private

    def get_other_service_jobs_in_object(service_job)
      ServiceJob.where(:kunde_plz => service_job.kunde_plz).where(:kunde_ort => service_job.kunde_ort).where(:kunde_strasse => service_job.kunde_strasse).where(:kunde_hausnummer => service_job.kunde_hausnummer).where("id != "+service_job.id.to_s)
    end


end
