class StreetNumbersController < ApplicationController
  def index
	q = params[:q] || ""
  	street = params[:street] || ""
  	@street_numbers = Job.all.collect do |j| 
	  	if ((j.kunde_hausnummer.downcase.starts_with?(q.downcase)) && (j.kunde_strasse == street)) then
			j.kunde_hausnummer
		end
	end.uniq.compact.sort

    render :layout => false  end
end
