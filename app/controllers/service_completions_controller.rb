class ServiceCompletionsController < InheritedResources::Base
  #caches_action :show, :edit

  def index
    @service_completions = ServiceCompletion.paginate :per_page => Settings.DEFAULT_ITEMS_PER_TABLE, :page => params[:page]
    render "service_completions/index"
  end

  def new
    @service_completion = ServiceCompletion.new
    @service_completion.service_appointment = ServiceAppointment.find(params[:service_appointment_id])
    @service_completion.order_number = @service_completion.service_appointment.service_job.ot_auftragsnummer
    @service_completion.customer_number = @service_completion.service_appointment.service_job.kunde_kundennummer

    @service_completion.customer_first_name = @service_completion.service_appointment.service_job.kunde_vorname
    @service_completion.customer_last_name = @service_completion.service_appointment.service_job.kunde_nachname
    @service_completion.customer_street_name = @service_completion.service_appointment.service_job.kunde_strasse
    @service_completion.customer_house_number = @service_completion.service_appointment.service_job.kunde_hausnummer
    @service_completion.customer_city = @service_completion.service_appointment.service_job.kunde_ort
    @service_completion.customer_postal_code = @service_completion.service_appointment.service_job.kunde_plz

    @service_completion.date = @service_completion.service_appointment.day
    @service_completion.start_time = @service_completion.service_appointment.start_time
    @service_completion.end_time = @service_completion.service_appointment.end_time

    @service_completion.call_out_cost_per_unit = Settings.CALL_OUT_COST_PER_UNIT
    @service_completion.service_time_cost_per_unit = Settings.SERVICE_TIME_COST_PER_UNIT
    @service_completion.additional_cost_per_unit = Settings.ADDITIONAL_COST_PER_UNIT


    #@service_completion.network_type = @service_completion.service_appointment.service_job.network_type
    #@service_completion.wohneinheiten = @service_completion.service_appointment.service_job.wohneinheiten
#    Settings.MAX_ASSETS_UPLOAD.times { @completion.completion_photos.build }
    render "service_completions/new"
  end


  def show
    @service_completion = ServiceCompletion.belonging_to_user_client(current_user).find(params[:id])
    render "service_completions/show"
  end

  def pdf
    @service_completion = ServiceCompletion.belonging_to_user_client(current_user).find(params[:id])

    file_name = "#{@service_completion.job.kunde_plz} #{@service_completion.job.kunde_ort} #{@service_completion.job.kunde_strasse} #{@service_completion.job.kunde_hausnummer}"
    html = render_to_string "service_completions/pdf", :layout => false, :format => "pdf"
    pdf = PDFKit.new(html).to_pdf
    send_data(pdf, :filename => "SERVICE-#{file_name}.pdf", :type => 'application/pdf', :disposition => 'inline')
  end

  def edit
    @service_completion = ServiceCompletion.belonging_to_user_client(current_user).find(params[:id])
    #Settings.MAX_ASSETS_UPLOAD.times { @service_completion.service_photos.build }
    #fresh_when(:etag => @service_completion, :last_modified => @service_completion.updated_at.utc, :public => false) unless (Rails.env == "development")
  end
  
  def create
    @service_completion = ServiceCompletion.new(params[:service_completion])
 #   Settings.MAX_ASSETS_UPLOAD.times { @service_completion.service_photos.build }
    @service_completion.technician = current_user
    @service_completion.service_appointment = ServiceAppointment.belonging_to_user_client(current_user).find(params[:service_completion][:service_appointment_id])
    if @service_completion.save
      @service_completion.delay.create_signature(@service_completion.signature, @service_completion.service_job.ot_auftragsnummer)   
      @service_completion.service_appointment.service_job.status = 3 #abgeschlossen
      @service_completion.service_appointment.service_job.save
      redirect_to(params[:fromPath] || [current_view, :services], :notice => 'Auftragsabschluss erstellt: '+self.class.helpers.link_to(@service_completion.service_job.ot_auftragsnummer, url_for([current_view, @service_completion]))) 
    else
      render "service_completions/new"
    end
  end

  def update
    @service_completion = ServiceCompletion.belonging_to_user_client(current_user).find(params[:id])
    #Settings.MAX_ASSETS_UPLOAD.times { @service_completion.service_photos.build }
    @service_completion.service_appointment = ServiceAppointment.belonging_to_user_client(current_user).find(params[:service_completion][:service_appointment_id])
    
    if @service_completion.update_attributes(params[:service_completion])
    #  @service_completion.messwerte_photo_file_name = "#{params[:service][:messwerte_photo_file_name]}"
    #  @service_completion.save
      expire_action :action => :show
      
      @service_completion.delay.create_signature(@service_completion.signature, @service_completion.service_job.ot_auftragsnummer)   
      @service_completion.service_appointment.service_job.status = 3 #technisch abgeschlossen
      @service_completion.service_appointment.service_job.save
      redirect_to(params[:fromPath] || [current_view, :services], :notice => 'Auftragsabschluss upgedatet: '+self.class.helpers.link_to(@service_completion.service_job.ot_auftragsnummer, url_for([current_view, @service_completion]))) 
    else
      render "service_completions/edit"
    end
  end

  def destroy
    @service_completion = ServiceCompletion.belonging_to_user_client(current_user).find(params[:id])
    @service_completion.destroy
    expire_action :action => :show
    redirect_to(params[:fromPath] || [current_view, :services], :notice => 'Auftragsabschluss wurde geloescht.')
  end

  def get_zip
    headers['Cache-Control'] = 'no-cache'  
    c = ServiceCompletion.belonging_to_user_client(current_user).find(params[:id])
    text = render_to_string(:template => "service_completions/pdf", :locals => {:service => c}, :layout => false)
    filename = c.create_zip(text)
    send_data(File.open(Rails.root.to_s+"/tmp/"+filename, "rb+").read, :type => 'application/zip', :disposition => 'attachment', :filename => filename+".zip")
    File.delete Rails.root.to_s+"/tmp/"+filename
  end
end


