# encoding: utf-8
class TransferPointsController < InheritedResources::Base
  #caches_action :show, :edit

  def index
    @transfer_points = TransferPoint.paginate :per_page => Settings.DEFAULT_ITEMS_PER_TABLE, :page => params[:page]
    render "transfer_points/index"
  end

  def show
    @transfer_point = TransferPoint.find(params[:id])
    render "transfer_points/show"
  end
  
  def edit
    @transfer_point = TransferPoint.find(params[:id])
    render "transfer_points/edit"
  end

  def new
    @transfer_point = TransferPoint.new
    render "transfer_points/new"
  end
  
  def create
    @transfer_point = TransferPoint.new(params[:transfer_point])
    if @transfer_point.save
      redirect_to(params[:fromPath] || [current_view, :transfer_points], :notice => 'ÜP erstellt: '+self.class.helpers.link_to(@transfer_point.short_description, url_for([current_view, @transfer_point]))) 
    else
      render :action => "transfer_points/new"
    end
  end

  def update
    @transfer_point = TransferPoint.find(params[:id])
    if @transfer_point.update_attributes(params[:transfer_point])
      #expire_action :action => :show
      redirect_to(params[:fromPath] || [current_view, :transfer_points], :notice => 'ÜP upgedatet: '+self.class.helpers.link_to(@transfer_point.short_description, url_for([current_view, @transfer_point])))
    else
      render :action => "transfer_points/edit"
    end
  end

  def destroy
    @transfer_point = TransferPoint.find(params[:id])
    @transfer_point.destroy
    #expire_action :action => :show
    redirect_to(params[:fromPath] || service_transfer_points_url, :notice => 'ÜP wurde gelöscht.')
  end
end