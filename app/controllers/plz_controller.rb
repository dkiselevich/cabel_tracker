class PlzController < ApplicationController
  def index
  	q = params[:q] || ""
  	@plzs = Job.all.collect{|j| if j.kunde_plz.starts_with?(q) then j.kunde_plz end }.uniq.compact.sort
  		p @plzs
    render :layout => false
  end
end
