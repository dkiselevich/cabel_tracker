class BugtrackersController < InheritedResources::Base
	def new
		@bugtracker = Bugtracker.new
		@bugtracker.user = current_user.vorname + " " + current_user.nachname
		@bugtracker.email = current_user.email
		render layout: false
	end

	def create
		@bugtracker = Bugtracker.new(params[:bugtracker])
		@bugtracker.bug_solved = false

		if @bugtracker.save
			BugtrackerMailer.new_bug(@bugtracker).deliver
			render :layout => false
		else
			format.html { render action: "new" }
		end
	end

end
