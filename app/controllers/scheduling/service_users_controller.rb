class Scheduling::ServiceUsersController < InheritedResources::Base
  actions :index, :show 
  before_filter :get_job_count_by_status

  def index
    #@service_users = User.service.assigned_to_same_company(current_user).paginate :per_page => Settings.DEFAULT_ITEMS_PER_TABLE, :page => params[:page]
    @search = User.search(params[:q])
    if params[:q] && params[:q][:s] && params[:q][:s].include?("client_names")
      @service_users = User.joins("LEFT JOIN assignments on assignments.user_id = users.id LEFT JOIN clients on assignments.client_id = clients.id ").assigned_to_same_company(current_user).order(params[:q][:s].sub("client_names","clients.name")).paginate :per_page => Settings.DEFAULT_ITEMS_PER_TABLE, :page => params[:page]
    else
      @service_users = @search.result.assigned_to_same_company(current_user).paginate :per_page => Settings.DEFAULT_ITEMS_PER_TABLE, :page => params[:page]
    end
  end

  def show
    @service_user = User.service.assigned_to_same_company(current_user).find(params[:id]) 
    @appointments = @service_user.appointments.joins(:job).where("jobs.status = 1").paginate :per_page => Settings.DEFAULT_ITEMS_PER_TABLE, :page => params[:page]
    @service_appointments = ServiceAppointment.belonging_to_user_client(current_user).where(:user_id =>@service_user.id).paginate :per_page => Settings.DEFAULT_ITEMS_PER_TABLE, :page => params[:page_service]
    @days_of_absence = @service_user.days_of_absence.order("starting_point DESC").paginate :per_page => Settings.DEFAULT_ITEMS_PER_TABLE, :page => params[:page]
  end

  def create_pdf
    @service_user = User.service.assigned_to_same_company(current_user).find(params[:id])
    @appointments = @service_user.appointments.joins(:job).where("jobs.status = 1").paginate :per_page => Settings.DEFAULT_ITEMS_PER_TABLE, :page => params[:page]
    @service_appointments = ServiceAppointment.belonging_to_user_client(current_user).where(:user_id =>@service_user.id).paginate :per_page => Settings.DEFAULT_ITEMS_PER_TABLE, :page => params[:page_service]
    render "appointments_pdf", :layout => false
  end

  def mail_appointments_list
    @service_user = User.service.assigned_to_same_company(current_user).find(params[:id])
    @appointments = @service_user.appointments.joins(:job).where("jobs.status = 1").where("appointments.day = ?", Date.tomorrow).paginate :per_page => Settings.DEFAULT_ITEMS_PER_TABLE, :page => params[:page]
    @days_of_absence = @service_user.days_of_absence.order("starting_point DESC").paginate :per_page => Settings.DEFAULT_ITEMS_PER_TABLE, :page => params[:page]
		AppointmentsMailer.delay.upcoming_appointments(@service_user, current_user, @appointments)
    flash[:notice] = 'Email gesendet'
  end

  def mail_tomorrow_appointments_forms
    @service_user = User.service.assigned_to_same_company(current_user).find(params[:id])
    @appointments = @service_user.appointments.joins(:job).where("jobs.status = 1").where("appointments.day = ?", Date.tomorrow).paginate :per_page => Settings.DEFAULT_ITEMS_PER_TABLE, :page => params[:page]

    @days_of_absence = @service_user.days_of_absence.order("starting_point DESC").paginate :per_page => Settings.DEFAULT_ITEMS_PER_TABLE, :page => params[:page]
		
		#AppointmentsMailer.delay.tomorrow_appointments_forms(@service_user, current_user, @appointments)
		@appointments.each do |appointment|
			AppointmentsMailer.delay.single_appointment(@service_user, current_user, appointment)
		end

    flash[:notice] = 'Email gesendet'
    render :action => "show"
  end

  def appointments
    @service_user = User.service.assigned_to_same_company(current_user).find(params[:id])
  end

  def appointments_pop_up
    @service_user = User.service.assigned_to_same_company(current_user).find(params[:id])
    render :layout => "pop_up"
  end

  def appointments_data
    @service_user = User.service.assigned_to_same_company(current_user).find(params[:id])
    render :layout => false
  end
end
