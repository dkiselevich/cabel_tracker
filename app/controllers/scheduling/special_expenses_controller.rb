# encoding: utf-8
class Scheduling::SpecialExpensesController < SpecialExpensesController
  before_filter :get_job_count_by_status

  def index
    @appointment = Appointment.find(params[:appointment_id])
    @disabled = (@appointment.job.status == 3)
    @special_expenses = @appointment.special_expenses.paginate :per_page => Settings.DEFAULT_ITEMS_PER_TABLE, :page => params[:page]
    render "index", :layout => "pop_up"
  end

  def new
    @special_expense = SpecialExpense.new
    @special_expense.appointment = Appointment.find(params[:appointment_id])
    render "new", :layout => "pop_up"
  end

  def show
    @special_expense = SpecialExpense.find(params[:id])
    render "show", :layout => "pop_up"
  end

  def edit
    @special_expense = SpecialExpense.find(params[:id])
    render "edit", :layout => "pop_up"
  end
  
  def create
    @special_expense = SpecialExpense.new(params[:special_expense])
    @special_expense.appointment = Appointment.find(params[:special_expense][:appointment_id])
    if @special_expense.save
      redirect_to([current_view, @special_expense.appointment, :special_expenses], :notice => 'Mehraufwand erstellt: '+self.class.helpers.link_to(@special_expense.job.ot_auftragsnummer, url_for([current_view, @special_expense]))) 
    else
   		render "new", :layout => "pop_up"
    end
  end

  def update
    @special_expense = SpecialExpense.find(params[:id])
    @special_expense.appointment = Appointment.find(params[:special_expense][:appointment_id])
    if @special_expense.update_attributes(params[:special_expense])
      redirect_to(params[:fromPath] || [current_view, @special_expense.appointment, :special_expenses], :notice => 'Mehraufwand upgedatet: '+self.class.helpers.link_to(@special_expense.job.ot_auftragsnummer, url_for([current_view, @special_expense]))) 
    else
      render "edit", :layout => "pop_up"
    end
  end

  def destroy
    @special_expense = SpecialExpense.find(params[:id])
    @appointment = @special_expense.appointment
    @special_expense.destroy
    redirect_to(params[:fromPath] || [current_view, @appointment, :special_expenses], :notice => 'Mehraufwand wurde gelöscht.')
  end
end