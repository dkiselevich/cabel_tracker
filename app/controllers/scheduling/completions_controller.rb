class Scheduling::CompletionsController < CompletionsController
  	before_filter :get_job_count_by_status
end