class Scheduling::BaseController < ApplicationController
  before_filter :get_job_count_by_status

	def index
		cookies[:last_view] = "scheduling"
		session[:mobile_param] = nil
	
		@search_new = Job.search(params[:q])
    #	@new_jobs = @search_new.result.paginate :per_page => Settings.DEFAULT_ITEMS_PER_TABLE, :page => params[:page]


    @new_jobs = @search_new.result.belonging_to_scheduling_user_group(current_user).where(:status => 0).where(:client_id => current_user.clients.collect{|c|c.id}).paginate :per_page => 5, :page => params[:page_new]



		@search_new_service = ServiceJob.search(params[:q])
    @new_service_jobs = @search_new_service.result.belonging_to_scheduling_user_group(current_user).where(:status => 0).where(:client_id => current_user.clients.collect{|c|c.id}).paginate :per_page => 5, :page => params[:page_new]
   
   	#	@search_cancellations = Cancellation.search(params[:search_cancellations]) 
   	#	@cancellations = @search_cancellations.paginate :per_page => 5, :page => params[:page_cancellations]
   
    @search_cancelled_jobs= Job.search(params[:search_cancelled_jobs])

    @cancelled_jobs = @search_cancelled_jobs.result.belonging_to_scheduling_user_group(current_user).where(:client_id => current_user.clients.collect{|c|c.id}).where(:status => 2).paginate :per_page => 5, :page => params[:page_waiting]
	end

	def all
		@search_new = Job.search(params[:q])
		@new_jobs = @search_new.result.where(:status => 0).where(:client_id => current_user.clients.collect{|c|c.id}).paginate :per_page => 5, :page => params[:page_new]

		@search_new_service = ServiceJob.search(params[:q])
    @new_service_jobs = @search_new_service.result.where(:status => 0).where(:client_id => current_user.clients.collect{|c|c.id}).paginate :per_page => 5, :page => params[:page_new]
   
    @search_cancelled_jobs= Job.search(params[:search_cancelled_jobs])
    @cancelled_jobs = @search_cancelled_jobs.result.where(:client_id => current_user.clients.collect{|c|c.id}).where(:status => 2).paginate :per_page => 5, :page => params[:page_waiting]

	end

	def quotes_per_controller
		@is_pop_up = true if params[:popup]
		params[:period] ||= Settings.DEFAULT_PERIOD_FOR_CHARTS
		@user_selection = false 
		controllers ||= User.where(:is_controller => true).where(:id => current_user)	
		@quotes_per_controller = StatisticsHelper.get_quotes_per_controller(:period => params[:period], :controllers => controllers, :clients => current_user.clients)
		render :template => 'general/quotes_per_controller', :layout => "chart" 
	end 

	def volume_per_controller
		@is_pop_up = true if params[:popup]
		params[:period] ||= Settings.DEFAULT_PERIOD_FOR_CHARTS
		@user_selection = false 
		controllers ||= User.where(:is_controller => true).where(:id => current_user)
		@volume_per_controller = StatisticsHelper.get_volume_per_controller(:period => params[:period], :controllers => controllers, :clients => current_user.clients)
		render :template => 'general/volume_per_controller', :layout => "chart" 
	end 

	def import_zde_jobs_form
		render "admin/base/import_zde_jobs_form"
	end

	def import_zde_jobs
		client = Client.find(params[:client_id])
		OtHelper.delay.get_zde_jobs(client, current_user)
		render "admin/base/show_import_started"
	end

	def import_zde_service_jobs_form
		render "admin/base/import_zde_service_jobs_form"
	end

	def import_zde_service_jobs
		client = Client.find(params[:client_id])
		OtHelper.delay.get_zde_service_jobs(client, current_user)
		render "admin/base/show_import_started"
	end


	def import_jobs_from_text #deprecated
		render "admin/base/import_jobs_from_text"
	end
	
	def import_service_jobs_from_text
		render "admin/base/import_service_jobs_from_text"
	end

	def create_jobs_from_text #deprecated
		puts params[:jobs_text]
		client = Client.find(params[:client_id])
		#default_scheduling_group = SchedulingUserGroup.find(params[:scheduling_group_id])
		#@new_jobs, @blocked_jobs = OtHelper.get_jobs_from_text(params[:jobs_text], client)
		OtHelper.delay.get_jobs_from_text(params[:jobs_text], client, current_user)
		render "admin/base/show_import_started"
	end

	def create_service_jobs_from_text
		client = Client.find(params[:client_id])
		#default_scheduling_group = SchedulingUserGroup.find(params[:scheduling_group_id])
		#@new_jobs, @blocked_jobs = OtHelper.get_jobs_from_text(params[:jobs_text], client)
		OtHelper.delay.get_service_jobs_from_text(params[:jobs_text], client, current_user)
		render "admin/base/show_import_started"		
	end

	def import_job
	end

	def create_job
		puts params[:job_id]
		client = Client.find(params[:client_id])	
		if (params[:job_kind] == "0") then #installation
			matching_jobs = Job.where(:ot_id => params[:job_id])
      if !matching_jobs.blank? then
        flash[:warning] = "Auftrag wurde bereits importiert: #{self.class.helpers.link_to(matching_jobs.first.short_description, url_for([current_view, matching_jobs.first]))}"
        @new_job = nil
      else
        @new_job = OtHelper.add_job_from_ot(params[:job_id], client, false)
      end
			render "show_new_job"
		else #service
			matching_jobs = ServiceJob.where(:ot_id => params[:job_id])
      if !matching_jobs.blank? then
        flash[:warning] = "Auftrag wurde bereits importiert: #{self.class.helpers.link_to(matching_jobs.first.short_description, url_for([current_view, matching_jobs.first]))}"
        @new_job = nil
      else
        @new_job = OtHelper.add_service_job_from_ot(params[:job_id], client, false)
      end
			render "show_new_job"
		end
	end

	def route_planer
    
	end
	
	def test_ot
	  client = Client.find(3) # 3kabel
	  job = Job.find(304)
	  modem_aktiviert = OtHelper.get_modemaktivierung(job, client)
	  
	  render :inline => "Modem aktiviert: "+modem_aktiviert.to_s
	end
	
end