class Scheduling::AppointmentsController < AppointmentsController
  	before_filter :get_job_count_by_status

  	def create_pdf
	    @appointment = Appointment.belonging_to_user_client(current_user).find(params[:id])

	    @completion = Completion.new
	    @completion.appointment = @appointment
	    @completion.job_type = @appointment.job.job_type
	    @completion.network_type = @appointment.job.network_type
	    @completion.vorverstaerker_id = 0
	    @completion.technician = @appointment.user

	    @empty_form = true

    	file_name = "#{@completion.job.kunde_plz} #{@completion.job.kunde_ort} #{@completion.job.kunde_strasse} #{@completion.job.kunde_hausnummer}"
	    html = render_to_string "completions/pdf_v4", :layout => false, :format => "pdf"
	    pdf = PDFKit.new(html).to_pdf
	    send_data(pdf, :filename => "#{file_name}.pdf", :type => 'application/pdf', :disposition => 'inline')
  	end

  	def mail_pdf
	    @appointment = Appointment.belonging_to_user_client(current_user).find(params[:id])
			AppointmentsMailer.delay.single_appointment(@appointment.user, current_user, @appointment)
  		flash[:notice] = "Email versendet"
  		render :action => "show"
  	end

  	def save_calendar_position
  	  if params[:appointment_id].to_i != -1
  	    appointment = Appointment.find(params[:appointment_id])
        appointment.user_id = params[:technician_id]
        appointment.day = params[:date]
        t = Time.new(1993, 02, 24, params[:time], 00)
        appointment.start_time = t
        appointment.start_time += appointment.start_time.utc_offset
        appointment.end_time = t
        appointment.end_time += appointment.end_time.utc_offset + params[:hours].to_i.hours
        appointment.save
        Admin::LogsController.create(current_user.vorname+" "+current_user.nachname+" moved appointment with ID "+appointment.id.to_s+" to another timeframe", appointment.id, "appointment")
  	  elsif params[:service_appointment_id].to_i != -1
  	    service_appointment = ServiceAppointment.find(params[:service_appointment_id])
        service_appointment.user_id = params[:technician_id]
        service_appointment.day = params[:date]
        t = Time.new(1993, 02, 24, params[:time], 00)
        service_appointment.start_time = t
        service_appointment.start_time += service_appointment.start_time.utc_offset
        service_appointment.end_time = t
        service_appointment.end_time += service_appointment.end_time.utc_offset + params[:hours].to_i.hours
        service_appointment.save
        Admin::LogsController.create(current_user.vorname+" "+current_user.nachname+" moved service appointment with ID "+service_appointment.id.to_s+" to another timeframe", service_appointment.id, "service_appointment")
  	  end

      ajaxResponse = "Successfully saved."

      begin
        if(Time.now.strftime("%H:%M") > '14:00' && Time.now.strftime("%H:%M") <= '21:59') # 16:00 - 23:59 deutscher Zeit
          Delayed::Job.enqueue(
            TaskHelper.send_pdf_with_appointments_for_tomorrow_to_all_technicians(Client.last)
          )
          Admin::LogsController.create("System sent pdf with appointments for tomorrow to all technicians for "+Client.last.name+" (e-mail dispatch when moving tasks after 16:00)")
          Delayed::Job.enqueue(
            TaskHelper.send_emails_with_blank_completion_pdf_for_each_appointment_tomorrow_to_all_technicians(Client.last)
          )
          Admin::LogsController.create("System sent emails with blank completion pdf for each appointment tomorrow to all technicians for "+Client.last.name+" (e-mail dispatch when moving tasks after 16:00)")
        end
      rescue Exception => e
        ajaxResponse+= " Warning: one or more delayed jobs were not executed."
      end

  	  render :inline => ajaxResponse
  	end

  	def get_job_id
      if params[:ot][0..1] == "OT"
        job = Job.find_by_ot_auftragsnummer(params[:ot])

        result = Hash.new
        result[:job_id] = job.id
        render :json => result
      elsif params[:ot][0] == "S"
        service_job = ServiceJob.find_by_ot_auftragsnummer(params[:ot])

        result = Hash.new
        result[:job_id] = service_job.id
        render :json => result
      else
        render :json => nil
      end
  	end

 	  # method unused
    def save_new_calendar_task
      if params[:ot][0..1] == "OT"
        job = Job.find_by_ot_auftragsnummer(params[:ot])
        job.status = 1 # = DI
        job.save
        appointment = Appointment.new
        appointment.job_id = job.id
        appointment.user_id = params[:technician_id]
        appointment.scheduling_user_id = current_user.id
        appointment.day = params[:date]
        t = Time.new(1993, 02, 24, params[:time], 00)
        appointment.start_time = t
        appointment.start_time += appointment.start_time.utc_offset
        appointment.end_time = t + 1.hour
        appointment.end_time += appointment.end_time.utc_offset
        appointment.save

        result = Hash.new
        result[:task] = params[:ot]+" | "+job.kunde_vorname+" "+job.kunde_nachname+"<br>"+job.kunde_plz+", "+job.kunde_ort+", "+job.kunde_strasse
        result[:appointment_id] = appointment.id
        result[:service_appointment_id] = 0
        render :json => result
      elsif params[:ot][0] == "S"
        service_job = ServiceJob.find_by_ot_auftragsnummer(params[:ot])
        service_job.status = 1 # = DI
        service_appointment = ServiceAppointment.new
        service_appointment.service_job_id = service_job.id
        service_appointment.user_id = params[:technician_id]
        service_appointment.scheduling_user_id = current_user.id
        service_appointment.day = params[:date]
        t = Time.new(1993, 02, 24, params[:time], 00)
        service_appointment.start_time = t
        service_appointment.start_time +=  service_appointment.start_time.utc_offset
        service_appointment.end_time = t + 1.hour
        service_appointment.end_time +=  service_appointment.end_time.utc_offset
        service_appointment.save

        result = Hash.new
        result[:task] = params[:ot]+" | "+service_job.kunde_vorname+" "+service_job.kunde_nachname+"<br>"+service_job.kunde_plz+", "+service_job.kunde_ort+", "+service_job.kunde_strasse
        result[:appointment_id] = 0
        result[:service_appointment_id] = service_appointment.id
        render :json => result
      else
        render :json => nil
      end

      if Time.now.strftime("%H:%M") > '14:00'
        Delayed::Job.enqueue(
          TaskHelper.send_pdf_with_appointments_for_tomorrow_to_all_technicians(Client.last)
        )
        Delayed::Job.enqueue(
          TaskHelper.send_emails_with_blank_completion_pdf_for_each_appointment_tomorrow_to_all_technicians(Client.last)
        )
      end
    end

end
