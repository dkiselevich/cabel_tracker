class Scheduling::ServiceAppointmentsController < ServiceAppointmentsController
    before_filter :get_job_count_by_status

    def create_pdf
        @service_appointment = ServiceAppointment.belonging_to_user_client(current_user).find(params[:id])

        @service_completion = ServiceCompletion.new
        @service_completion.service_appointment = @service_appointment
        @service_completion.technician = @service_appointment.user

        @empty_form = true

        file_name = "#{@service_completion.job.kunde_plz} #{@service_completion.job.kunde_ort} #{@service_completion.job.kunde_strasse} #{@service_completion.job.kunde_hausnummer}"
        html = render_to_string "service_completions/pdf", :layout => false, :format => "pdf"
        pdf = PDFKit.new(html).to_pdf
        send_data(pdf, :filename => "#{file_name}.pdf", :type => 'application/pdf', :disposition => 'inline')
	end

	def mail_pdf
        @service_appointment = ServiceAppointment.belonging_to_user_client(current_user).find(params[:id])
		AppointmentsMailer.delay.single_service_appointment(@service_appointment.user, current_user, @service_appointment)
		flash[:notice] = "Email versandt"
		render :action => "show"
	end
 end


