# encoding: utf-8
class Scheduling::CommentsController < CommentsController
  	before_filter :get_job_count_by_status
end
