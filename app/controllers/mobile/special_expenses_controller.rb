# encoding: utf-8
class Mobile::SpecialExpensesController < SpecialExpensesController

  def index
    @appointment = Appointment.find(params[:appointment_id])
    @special_expenses = @appointment.special_expenses.paginate :per_page => 100, :page => params[:page]
    render "index", :layout => "mobile"
  end

  def new
    @appointment = Appointment.find(params[:appointment_id])
    @special_expense = SpecialExpense.new
    @special_expense.appointment = Appointment.find(params[:appointment_id])
    render "new", :layout => "mobile"
  end

  def show
    @special_expense = SpecialExpense.find(params[:id])
    render "show", :layout => "mobile"
  end

  def edit
    @special_expense = SpecialExpense.find(params[:id])
    render "edit", :layout => "mobile"
  end
  
  def create
    @special_expense = SpecialExpense.new(params[:special_expense])
    @special_expense.appointment = Appointment.find(params[:special_expense][:appointment_id])
    if @special_expense.save
      redirect_to(["mobile", @special_expense.appointment, :special_expenses], :notice => 'Mehraufwand erstellt: '+self.class.helpers.link_to(@special_expense.job.ot_auftragsnummer, url_for(["mobile", @special_expense.appointment, @special_expense]))) 
    else
   		render "new", :layout => "mobile"
    end
  end

  def update
    @special_expense = SpecialExpense.find(params[:id])
    @special_expense.appointment = Appointment.find(params[:special_expense][:appointment_id])
    if @special_expense.update_attributes(params[:special_expense])
      redirect_to(params[:fromPath] || ["mobile", @special_expense.appointment, :special_expenses], :notice => 'Mehraufwand upgedatet: '+self.class.helpers.link_to(@special_expense.job.ot_auftragsnummer, url_for(["mobile", @special_expense.appointment, @special_expense]))) 
    else
      render "edit", :layout => "mobile"
    end
  end

  def destroy
    @special_expense = SpecialExpense.find(params[:id])
    @appointment = @special_expense.appointment
    @special_expense.destroy
    redirect_to(params[:fromPath] || ["mobile", @appointment, :special_expenses], :notice => 'Mehraufwand wurde gelöscht.')
  end
end