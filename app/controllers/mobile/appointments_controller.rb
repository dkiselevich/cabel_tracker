class Mobile::AppointmentsController < AppointmentsController
	def index
		@day = Date.today
		@day = Date.parse(params[:day]) if params[:day]
		@appointments = Appointment.joins(:job).where(:user_id => current_user.id).where("jobs.status" => 1).where(:day => @day).order(:day)
 		render :layout => "mobile"
	end

	def next_open_appointment
    	@day = Date.today
		@appointment = Appointment.joins(:job).where(:user_id => current_user.id).where("jobs.status" => 1).where(:day => @day).limit(1)[0]
 		render "show", :layout => "mobile"
	end
	def show
   		@appointment = Appointment.find(params[:id])
   		render :layout => "mobile"
	end
	
	def map
   		@appointment = Appointment.find(params[:id])
   		render :layout => "mobile"
	end

	def calendar
		@service_user = current_user
   		render :layout => "mobile"
	end

	def appointments_data
	@service_user = current_user
	render :layout => false
	end
end
