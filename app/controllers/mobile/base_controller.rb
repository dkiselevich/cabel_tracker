class Mobile::BaseController < ApplicationController
	def index   		
		if !current_user.is_technician?
			render "no_technician", :layout => "mobile"
		else
			render "index", :layout => "mobile"
		end
	end
end
