class CitiesController < ApplicationController
  def index
  	q = params[:q] || ""
  	plz = params[:plz] || ""
  	@cities = Job.all.collect do |j| 
	  	if ((j.kunde_ort.downcase.starts_with?(q.downcase)) && (j.kunde_plz == plz)) then
			j.kunde_ort
		end
	end.uniq.compact.sort

	puts @cities

    render :layout => false
  end
end
