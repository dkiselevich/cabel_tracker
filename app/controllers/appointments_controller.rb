# encoding: utf-8
class AppointmentsController < InheritedResources::Base
  caches_action :index, :show, :edit
  cache_sweeper :appointment_sweeper, :only => [:create, :update, :destroy]

  def index
    @search = Appointment.search(params[:q])
    if params[:q] && params[:q][:s] && params[:q][:s].include?("full_name")
      @appointments = Appointment.joins("LEFT OUTER JOIN users ON appointments.user_id = users.id").belonging_to_user_client(current_user).order(params[:q][:s].sub("full_name","users.vorname")).order(params[:q][:s].sub("full_name","users.nachname")).paginate :per_page => Settings.DEFAULT_ITEMS_PER_TABLE, :page => params[:page]
    elsif params[:q] && params[:q][:s] && params[:q][:s].include?("short_description")
      @appointments = Appointment.joins("LEFT OUTER JOIN jobs ON appointments.job_id = jobs.id").belonging_to_user_client(current_user).order(params[:q][:s].sub("short_description","jobs.ot_auftragsnummer")).order(params[:q][:s].sub("short_description","jobs.ot_auftragsnummer")).paginate :per_page => Settings.DEFAULT_ITEMS_PER_TABLE, :page => params[:page]
    else
      @appointments = @search.result.belonging_to_user_client(current_user).paginate :per_page => Settings.DEFAULT_ITEMS_PER_TABLE, :page => params[:page]
    end
    @service_appointments = ServiceAppointment.belonging_to_user_client(current_user).paginate :per_page => Settings.DEFAULT_ITEMS_PER_TABLE, :page => params[:page]
    render "appointments/index"
  end

  def open
    @appointments = Appointment.where("day > ?",Date.yesterday).belonging_to_user_client(current_user).paginate :per_page => Settings.DEFAULT_ITEMS_PER_TABLE, :page => params[:page]
    @service_appointments = ServiceAppointment.where("day > ?",Date.yesterday).belonging_to_user_client(current_user).paginate :per_page => Settings.DEFAULT_ITEMS_PER_TABLE, :page => params[:page]
    @header = "Anstehende Installationstermine"
    @header_service = "Anstehende Entstörungstermine"
    render "appointments/index"
  end

  def get_users_from_param(users_string)
     users_string = "ALL" if users_string.blank? || users_string == "?users=" #handle bug
     if users_string.starts_with?("ALL") then
      service_users = User.service.assigned_to_same_company(current_user)
    elsif users_string.starts_with?("CLIENT:") then
      client_id = users_string.gsub("CLIENT:","").to_i
      service_users = []
      desired_client = Client.where(:id => client_id).first
      if (desired_client) && current_user.clients.include?(desired_client) then
        desired_client.users.each{|u| service_users << u if (u.is_technician && u.active)}
      end
    elsif users_string.starts_with?("USER:") then
      user_id = users_string.gsub("USER:","").to_i
      service_users = User.where(:id => user_id).service.assigned_to_same_company(current_user)
    elsif users_string.starts_with?("SERVICE_USER_GROUP:") then
      service_user_group_id = users_string.gsub("SERVICE_USER_GROUP:","").to_i
      service_users = ServiceUserGroup.where(:id => service_user_group_id).first.users
    end
    return service_users
  end

  def calendar
    @days_to_show = 1
    @pop_up_path = "/#{current_view}/appointments/pop_up"

    @service_users = get_users_from_param(params[:users])
    get_grouped_options()
  end

  def calendar_data
    @users = get_users_from_param(params[:users])
    if !params[:date].nil?
      date = Date.parse(params[:date])
    else
      date = Date.today
    end
    @appointments_week = Appointment.where("day >= ?", date.beginning_of_week.strftime('%Y-%m-%d')).where("day < ?", (date.beginning_of_week+7.days).strftime('%Y-%m-%d'))
    @service_appointments_week = ServiceAppointment.where("day >= ?", date.beginning_of_week.strftime('%Y-%m-%d')).where("day < ?", (date.beginning_of_week+7.days).strftime('%Y-%m-%d'))

    respond_to do |format|
      format.js{
        render :partial => "appointments/calendar_data"
      }
    end
  end

  def get_grouped_options
    @clients = current_user.clients
    @grouped_options = {}
    @grouped_options[" Alle meine Techniker"] = [[" Alle Techniker","ALL"]]

    @clients.each do |client|
      options = [[" Alle (#{client.name})","CLIENT:#{client.id}"]]
      client.users.each do |user|
        options << [user.full_name, "USER:#{user.id}"] if (user.is_technician && user.active)
        options.sort! { |a, b| a[0] <=> b[0]}
      end
      @grouped_options[client.name] = options
    end

    @service_user_groups = ServiceUserGroup.all()
    options = Array.new
    @service_user_groups.each do |service_user_group|
      options << ["#{service_user_group.name}","SERVICE_USER_GROUP:#{service_user_group.id}"]
    end
    @grouped_options[" Alle meine Technikergruppen"] = options
    #TODO Technikergruppen


    if !params[:date].nil?
      date = Date.parse(params[:date])
    else
      date = Date.today
    end
    @appointments_week = Appointment.where("day >= ?", date.beginning_of_week.strftime('%Y-%m-%d')).where("day < ?", (date.beginning_of_week+7.days).strftime('%Y-%m-%d'))
    @service_appointments_week = ServiceAppointment.where("day >= ?", date.beginning_of_week.strftime('%Y-%m-%d')).where("day < ?", (date.beginning_of_week+7.days).strftime('%Y-%m-%d'))


    if params[:format] == "csv"
      calendar_csv
    else
      render :layout => "calendar_only"
    end

  end

  # CSV Export (techniker, disponent, ot nummer, installationstermin (di+ag tag))
  def calendar_csv
    table = Ebru::Table.new()
    table.header = ('Techniker').lines.to_a + ('Disponent').lines.to_a + ('OT-Nummer').lines.to_a + ('Installationstermin').lines.to_a
    if !params[:week].nil?
      date = Date.commercial(Date.current.year, params[:week].to_i)
    else
      date = Date.commercial(Date.current.year, Time.now.strftime("%W").to_i+1)
    end

    # Appointments
    all_appointments = Appointment.where("day >= ?", date.beginning_of_week.strftime('%Y-%m-%d')).where("day < ?", (date.beginning_of_week+7.days).strftime('%Y-%m-%d')).order('day DESC').order('start_time DESC')
    jobs = []
    appointments = []
    all_appointments.each do |a|
      appointments.push( a ) unless jobs.include?(a.job)
      jobs.push( a.job ) unless jobs.include?(a.job)
    end
    appointments.each do |a|
      techniker = a.user.nachname + " " + a.user.vorname
      disponent = a.scheduling_user.nachname + " " + a.scheduling_user.vorname
      table.data += [techniker.lines.to_a + disponent.lines.to_a + a.job.ot_auftragsnummer.lines.to_a + a.day.strftime('%d.%m.%Y').lines.to_a]
    end

    # ServiceAppointments
    all_service_appointments = ServiceAppointment.where("day >= ?", date.beginning_of_week.strftime('%Y-%m-%d')).where("day < ?", (date.beginning_of_week+7.days).strftime('%Y-%m-%d')).order('day DESC').order('start_time DESC')
    service_jobs = []
    service_appointments = []
    all_service_appointments.each do |sa|
      service_appointments.push( sa ) unless service_jobs.include?(sa.service_job)
      service_jobs.push( sa.service_job ) unless service_jobs.include?(sa.service_job)
    end
    service_appointments.each do |sa|
      techniker = sa.user.nachname + " " + sa.user.vorname
      disponent = sa.scheduling_user.nachname + " " + sa.scheduling_user.vorname
      table.data += [techniker.lines.to_a + disponent.lines.to_a + sa.service_job.ot_auftragsnummer.lines.to_a + sa.day.strftime('%d.%m.%Y').lines.to_a]
    end

    respond_to do |format|
      format.csv{ send_data(Ebru::Report.create_csv_from_data(table.data, table.header))}
    end

    # file = "my_file.csv"
    # CSV.open( file, 'w' ) do |csv|
      # csv << table.header
    # end
    # send_file(file)
  end

  def pop_up
    @days_to_show = 1
    @service_users = get_users_from_param(params[:users])
    get_grouped_options()
    render :layout => "pop_up"
  end

  def data
    @start_date = Date.parse(params[:startDate])
    @end_date = Date.parse(params[:endDate])
    @service_users = get_users_from_param(params[:users])
    render :layout => false
  end

  def new
    @appointment = Appointment.new
    @job = Job.find(params[:job_id]) unless params[:job_id].blank?
    @appointment.job = @job
    @appointment.day = Date.tomorrow
    @appointment.day = @job.verbindlicher_installationstermin if @job.verbindlicher_installationstermin

    @appointment.start_time = Time.now.beginning_of_day+8.hours
    @appointment.end_time = Time.now.beginning_of_day+9.hours

    if params[:date]
      @appointment.day = Date.parse(params[:date])
    end
    if params[:start]
      @appointment.start_time = Time.new(1993, 02, 24, params[:start], 00)
      @appointment.end_time = Time.new(1993, 02, 24, params[:start], 00) + 1.hour
    end
    if params[:technician]
      @appointment.user_id = params[:technician]
    end


    @appointment.bemerkungen = @job.ot_kommentare

    @appointment_request = AppointmentRequest.find(params[:requestId]) if params[:requestId]
    if @appointment_request then
      @appointment.user = @appointment_request.technician
      @appointment.day =Date.new(@appointment_request.termin_datum.year, @appointment_request.termin_datum.month, @appointment_request.termin_datum.day) #@appointment_request.termin_datum# + @appointment_request.zeitfenster_start
      @appointment.start_time = DateTime.new(@appointment_request.termin_datum.year, @appointment_request.termin_datum.month, @appointment_request.termin_datum.day, @appointment_request.zeitfenster_start.hour, @appointment_request.zeitfenster_start.min, 0, DateTime.now.offset) #@appointment_request.termin_datum# + @appointment_request.zeitfenster_start
      @appointment.end_time = DateTime.new(@appointment_request.termin_datum.year, @appointment_request.termin_datum.month, @appointment_request.termin_datum.day, @appointment_request.zeitfenster_ende.hour, @appointment_request.zeitfenster_ende.min, 0, DateTime.now.offset) #@appointment_request.termin_datum# + @appointment_request.zeitfenster_start
    end

    params[:update_ot] = true

    if params[:popup]=="1"
      render "appointments/new", :layout => 'layouts/calendar_only'
    else
      render "appointments/new"
    end
  end

  def create
    @appointment = Appointment.new(params[:appointment])
    @appointment.scheduling_user = current_user
    @job = Job.find(params[:appointment][:job_id]) unless params[:appointment][:job_id].blank?
    if (@appointment.user && @appointment.user.is_absent?(@appointment))
   	 	flash[:error] = "Termin kann nicht auf dieses Datum gespeichert werden, da Techniker nicht anwesend (Urlaub oder Krankheit)"
   	 	render "appointments/new"
    	return
    end

    appointments_today, service_appointments_today = get_appointments_today
    if (appointments_today.count + service_appointments_today.count) != 0
      flash[:error] = "Termin kann nicht auf dieses Datum gespeichert werden, da der Termin bereits vergeben ist."
      render "appointments/new"
      return
    end

    if @appointment.save
      Admin::LogsController.create(current_user.vorname+" "+current_user.nachname+" created appointment with ID "+@appointment.id.to_s, @appointment.id, "appointment")
      flash[:notice] = "Termin erstellt"
      @job.status = 1 #disponiert
      @job.save
      p @job.status

      if @appointment.job.client.automatic_email_to_technician_on_appointment_change_to_client then
        AppointmentsMailer.delay.single_appointment(@appointment.user, current_user, @appointment)
        flash[:warning] = "Email versendet"
      end

      note = "ct - Disponent: #{@appointment.scheduling_user.full_name}, Techniker: #{@appointment.user.full_name}"
      note = note + ", Hinweis für Techniker: #{@appointment.bemerkungen}" if @appointment.bemerkungen
      dt = DateTime.parse(@appointment.day.strftime("%d-%m-%Y "+@appointment.start_time.strftime("%H:%M:%S")))

      if (params[:update_ot] == "1") then
      	puts "UPDATING !!!!!!!!!!!!!!!!!!!!!!!!!!"
        result = OtHelper.set_state_to_di(@appointment.job.ot_id, nil, dt, note, @job.client)
      else
        result = false
      end
      if (result) then
        redirect_to([current_view, :appointments], :notice => 'Termin erstellt: '+ self.class.helpers.link_to(@appointment.short_description, url_for([current_view, @appointment])))
      else
        omnitracker_link = self.class.helpers.link_to("OMNITRACKER", Settings.OT_DETAILS_URL.gsub("##ID##",@appointment.job.ot_id), :style => "text-decoration:underline", :target => "_blank")
        error_notice = "Termin erstellt ABER Omnitracker konnte nicht upgedatet werden. Bitte setzen Sie den Termin in #{omnitracker_link} von Hand."
        redirect_to([current_view, @appointment], :error => error_notice)
      end

    else
      render "appointments/new"
    end
  end

  def edit
    @appointment = Appointment.belonging_to_user_client(current_user).find(params[:id])
    @job = @appointment.job
    #fresh_when(:etag => @appointment, :last_modified => @appointment.updated_at.utc, :public => false) unless (Rails.env == "development")

    if params[:popup]=="1"
      render "appointments/edit", :layout => 'layouts/calendar_only'
    else
      render "appointments/edit"
    end
  end

  def has_special_expenses
    @appointment = Appointment.belonging_to_user_client(current_user).find(params[:id])
    render :inline => "<%= !@appointment.special_expenses.blank? %>"
  end

  def show
    @appointment = Appointment.belonging_to_user_client(current_user).find(params[:id])
    @job = @appointment.job
    #fresh_when(:etag => @appointment, :last_modified => @appointment.updated_at.utc, :public => false) unless (Rails.env == "development")

    if params[:popup]=="1"
      render "appointments/show", :layout => 'layouts/calendar_only'
    else
      render "appointments/show"
    end
  end

  def special_expenses_pop_up
    @appointment = Appointment.belonging_to_user_client(current_user).find(params[:id])
    render "appointments/special_expenses_pop_up", :layout => "pop_up"
  end

  def update
    @appointment = Appointment.belonging_to_user_client(current_user).find(params[:id])
    if @appointment.user.is_absent?(@appointment)
   	 	flash[:error] = "Termin kann nicht auf dieses Datum gespeichert werden, da Techniker nicht anwesend (Urlaub oder Krankheit)"
   	 	@job = @appointment.job
    	render "appointments/edit"
    	return
    end

    appointments_today, service_appointments_today = get_appointments_today
    if (appointments_today.count + service_appointments_today.count) != 0
      puts "drin"
      flash[:error] = "Termin kann nicht auf dieses Datum gespeichert werden, da der Termin bereits vergeben ist."
      @job = @appointment.job
      render "appointments/edit"
      return
    end

    if @appointment.update_attributes(params[:appointment])
      Admin::LogsController.create(current_user.vorname+" "+current_user.nachname+" updated appointment with ID "+@appointment.id.to_s, @appointment.id, "appointment")
      @appointment.job.save

      if @appointment.job.client.automatic_email_to_technician_on_appointment_change_to_client then
        AppointmentsMailer.delay.single_appointment(@appointment.user, current_user, @appointment)
        flash[:warning] = "Email versendet"
      end

      note = "ct - Disponent: #{@appointment.scheduling_user.full_name}, Techniker: #{@appointment.user.full_name}"
      note = note + ", Hinweis für Techniker: #{@appointment.bemerkungen}" if @appointment.bemerkungen
      dt = DateTime.parse(@appointment.day.strftime("%d-%m-%Y "+@appointment.start_time.strftime("%H:%M:%S")))
      if (params[:update_ot] == "1") then
        puts "UPDATING !!!!!!!!!!!!!!!!!!!!!!!!!!"
        result = OtHelper.set_state_to_di(@appointment.job.ot_id, nil, dt, note, @appointment.job.client)
      else
        result = false
      end
      expire_action :action => :show
      if (result) then
        redirect_to([current_view, :appointments], :notice => 'Termin erstellt: '+ self.class.helpers.link_to(@appointment.short_description, url_for([current_view, @appointment])))
      else
        omnitracker_link = self.class.helpers.link_to("OMNITRACKER", Settings.OT_DETAILS_URL.gsub("##ID##",@appointment.job.ot_id), :style => "text-decoration:underline", :target => "_blank")
        error_notice = "Termin erstellt ABER Omnitracker konnte nicht upgedatet werden. Bitte setzen Sie den Termin in #{omnitracker_link} von Hand."
        redirect_to([current_view, @appointment], :notice => error_notice)
      end

      if(Time.now.strftime("%H:%M") > '14:00' && Time.now.strftime("%H:%M") <= '21:59') # 16:00 - 23:59 deutscher Zeit
        Delayed::Job.enqueue(
          Admin::TaskHelper.send_pdf_with_appointments_for_tomorrow_to_all_technicians(Client.last)
        )
        LogsController.create("System sent pdf with appointments for tomorrow to all technicians for "+Client.last.name)
        Delayed::Job.enqueue(
          TaskHelper.send_emails_with_blank_completion_pdf_for_each_appointment_tomorrow_to_all_technicians(Client.last)
        )
        Admin::LogsController.create("System sent emails with blank completion pdf for each appointment tomorrow to all technicians for "+Client.last.name)
      end

    else
    render "appointments/edit"
    end
  end

  def update_from_js
    @appointment = Appointment.belonging_to_user_client(current_user).find(params[:id])

    start = Time.at(params[:start].to_i/1000)
    start_time = Time.at(params[:start].to_i/1000)
    start_time += start_time.utc_offset
    @appointment.start_time=start_time

    end_time = Time.at(params[:end].to_i/1000)
    end_time += end_time.utc_offset
    @appointment.end_time = end_time

    @appointment.day = Date.new(start.year, start.month, start.day)
    @appointment.user_id = params[:userId].to_s
    @appointment.save
    render :inline => "true"
  end

  def latest_update
    latest_updated = Appointment.find(:first, :order => "updated_at DESC")
    render :inline => latest_updated.updated_at.to_s
  end

  def destroy
    @appointment = Appointment.belonging_to_user_client(current_user).find(params[:id])
    @job = @appointment.job
    if (@appointment.processed_already?) then
      flash[:warning] = "Termin kann nicht gelöscht werden, da er bereits vom Techniker bearbeitet wurde."
      render "appointments/edit"
    else
      Admin::LogsController.create(current_user.vorname+" "+current_user.nachname+" deleted appointment with ID "+@appointment.id.to_s, @appointment.id, "appointment")
      @appointment.destroy
      expire_action :action => :show
      redirect_to(@job ? [current_view, @job] : [current_view, :appointments], :notice => 'Termin gelöscht.')
    end
  end

  # prepares check of appointments for technician for today
  # method by James, mod by Tobse
  def get_appointments_today
    user_id = params[:appointment][:user_id]
    start_time = params[:appointment]["start_time(4i)"] + ":" + params[:appointment]["start_time(5i)"] + ":00"
    end_time = params[:appointment]["end_time(4i)"] + ":" + params[:appointment]["end_time(5i)"] + ":00"
    day = Date.parse(params[:appointment]["day(1i)"] + "-" + params[:appointment]["day(2i)"] + "-" + params[:appointment]["day(3i)"])
    whereclause = "(start_time <= ? AND end_time >= ?) OR (start_time <= ? AND end_time >= ?) OR (start_time >= ? AND end_time <= ?)"
    appointments_today = Appointment.where(:day => day)
                                    .where(:user_id => user_id)
                                    .where(whereclause, start_time, start_time, end_time, end_time, start_time, end_time)
    service_appointments_today = ServiceAppointment.where(:day => day)
                                    .where(:user_id => user_id)
                                    .where(whereclause, start_time, start_time, end_time, end_time, start_time, end_time)
    return appointments_today, service_appointments_today
  end

end
