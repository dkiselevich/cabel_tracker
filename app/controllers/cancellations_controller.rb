# encoding: utf-8
class CancellationsController < InheritedResources::Base
  #caches_action :show, :edit
  cache_sweeper :cancellation_sweeper

  def index
    @cancellations = Cancellation.belonging_to_user_client(current_user).paginate :per_page => Settings.DEFAULT_ITEMS_PER_TABLE, :page => params[:page]
    render "cancellations/index"
  end

  def new
    @cancellation = Cancellation.new
    @cancellation.appointment = Appointment.belonging_to_user_client(current_user).find(params[:appointment_id])
    @cancellation.netztopologie_id = @cancellation.appointment.job.network_type.id
    Settings.MAX_ASSETS_UPLOAD.times { @cancellation.photos.build }
    render "cancellations/new"
  end

  def show
    @cancellation = Cancellation.belonging_to_user_client(current_user).find(params[:id])
    render "cancellations/show"
  end

  def edit
    @cancellation = Cancellation.belonging_to_user_client(current_user).find(params[:id])
    Settings.MAX_ASSETS_UPLOAD.times { @cancellation.photos.build }
    p @cancellation.photos
    render "cancellations/edit"
  end
  
  def create
    @cancellation = Cancellation.new(params[:cancellation])
    @cancellation.user = current_user
    @cancellation.appointment = Appointment.belonging_to_user_client(current_user).find(params[:cancellation][:appointment_id])
    @cancellation.appointment.job.status = 5 #storniert
    @cancellation.appointment.job.save
    if @cancellation.save
#      note = "ct - auf wk gesetzt durch Techniker: #{@cancellation.user.full_name} - Wartegrund: #{wartegrund}"
#      result = OtHelper.set_state_to_om(@cancellation.job.ot_id, nil, note)
#      if (result) then
        redirect_to(params[:fromPath] || [current_view, :cancellations], :notice => 'Klärungsfall erstellt: '+self.class.helpers.link_to(@cancellation.job.ot_auftragsnummer, url_for([current_view, @cancellation]))) 
#      else
#        omnitracker_link = self.class.helpers.link_to("OMNITRACKER", Settings.OT_DETAILS_URL.gsub("##ID##",@cancellation.job.ot_id), :style => "text-decoration:underline", :target => "_blank") 
#        error_notice = "Klärungsfall erstellt ABER Omnitracker konnte nicht upgedatet werden. Bitte setzen Sie den Auftrag in #{omnitracker_link} von Hand auf 'wk'."
#        redirect_to([current_view, @cancellation], :notice => error_notice)
#      end
    else
    Settings.MAX_ASSETS_UPLOAD.times { @cancellation.photos.build }
    render "cancellations/new"
    end
  end

  def update
    @cancellation = Cancellation.belonging_to_user_client(current_user).find(params[:id])
    p @cancellation.photos
    @cancellation.appointment = Appointment.find(params[:cancellation][:appointment_id])
    if @cancellation.update_attributes(params[:cancellation])
      p @cancellation.photos
      redirect_to(params[:fromPath] || [current_view, :cancellations], :notice => 'Klärungsfall upgedatet: '+self.class.helpers.link_to(@cancellation.job.ot_auftragsnummer, url_for([current_view, @cancellation]))) 
    else
    Settings.MAX_ASSETS_UPLOAD.times { @cancellation.photos.build }
    render "cancellations/edit"
    end
  end

  def destroy
    @cancellation = Cancellation.belonging_to_user_client(current_user).find(params[:id])
    @cancellation.destroy
    redirect_to(params[:fromPath] || [current_view, :cancellations], :notice => 'Klärungsfall wurde gelöscht.')
  end
end