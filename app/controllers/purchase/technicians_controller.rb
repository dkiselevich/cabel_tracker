class Purchase::TechniciansController < InheritedResources::Base
  actions :index, :show
  def index
    @technicians = User.paginate :per_page => Settings.DEFAULT_ITEMS_PER_TABLE, :page => params[:page]
  end

  def show
    @technician = User.find(params[:id])
    @equipments = @technician.equipments.paginate :per_page => Settings.DEFAULT_ITEMS_PER_TABLE, :page => params[:page]
  end
end
