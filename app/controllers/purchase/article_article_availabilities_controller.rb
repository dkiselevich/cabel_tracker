# encoding: utf-8
class Purchase::ArticleArticleAvailabilitiesController < InheritedResources::Base

  def index
    @article_availabilities = ArticleAvailability.where(:article_id => params[:article_id]).order("CREATED_AT DESC").paginate :per_page => Settings.DEFAULT_ITEMS_PER_TABLE, :page => params[:page]
    render "purchase/article_article_availabilities/index", :layout => false
  end

  def new
    @article_availability = ArticleAvailability.new
    @article_availability.article_id = params[:article_id]
    
    render :layout => "pop_up"
  end

  def show
    @article_availability = ArticleAvailability.find(params[:id])
    render :layout => "pop_up"
  end

  def edit
    @article_availability = ArticleAvailability.find(params[:id])
    render :layout => "pop_up"
  end

  def create
    @article_availability = ArticleAvailability.new(params[:article_availability])
    @article_availability.article = Article.find(params[:article_id])

    if @article_availability.save
      render :layout => false 
    else
      render :action => "new", :layout => "pop_up" 
    end
  end

  def update
    @article_availability = ArticleAvailability.find(params[:id])
    if @article_availability.update_attributes(params[:article_availability])
      render :action => "create", :layout => false 
    else
      render :action => "edit", :layout => "pop_up" 
    end
  end

  def destroy
    @article_availability = ArticleAvailability.find(params[:id])
    @article_availability.destroy
    render :layout => "pop_up"
  end
end
