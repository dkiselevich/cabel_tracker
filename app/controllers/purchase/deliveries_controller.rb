# encoding: utf-8
class Purchase::DeliveriesController < InheritedResources::Base

  def index
    @deliveries = Delivery.belonging_to_user_client(current_user).paginate :per_page => Settings.DEFAULT_ITEMS_PER_TABLE, :page => params[:page]
  end

  def show
    @delivery = Delivery.find(params[:id])
    @delivery_items = @delivery.delivery_items.paginate :per_page => Settings.DEFAULT_ITEMS_PER_TABLE, :page => params[:page]
  end

  def edit
    @delivery = Delivery.find(params[:id])
    @delivery_items = @delivery.delivery_items.paginate :per_page => Settings.DEFAULT_ITEMS_PER_TABLE, :page => params[:page]
  end

  def create
    @delivery = Delivery.new(params[:delivery])
    @delivery.client_id = current_user.clients.first
    if @delivery.save then
      redirect_to([current_view, :deliveries], :notice => 'Lieferung erstellt: '+self.class.helpers.link_to(@delivery, url_for([current_view, @delivery])))
    else
      render :action => "new"
    end
  end

  def update
    @delivery = Delivery.find(params[:id])
    @delivery.client_id = current_user.clients.first
    if @delivery.update_attributes(params[:delivery])
      redirect_to([current_view, :deliveries], :notice => 'Lieferung upgedatet: '+self.class.helpers.link_to(@delivery, url_for([current_view, @delivery])))
    else
      render :action => "edit"
    end
  end

  def process_delivery
    @delivery = Delivery.find(params[:id])
    @delivery.process_delivery
    redirect_to([current_view, :deliveries], :notice => 'Lieferung upgedatet: '+self.class.helpers.link_to(@delivery, url_for([current_view, @delivery])))
  end

  def destroy
    @delivery = Delivery.find(params[:id])
    @delivery.destroy
    redirect_to([current_view, :deliveries], :notice => 'Lieferung wurde gelöscht.')
  end
end
