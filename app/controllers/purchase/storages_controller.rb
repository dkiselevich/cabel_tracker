class Purchase::StoragesController < InheritedResources::Base
  
  def index
    cookies[:last_view] = "purchase"
    session[:mobile_param] = nil
  
      @articles_in_storage = Article.belonging_to_user_client(current_user).where("current_quantity_in_storage > 0").paginate :per_page => Settings.DEFAULT_ITEMS_PER_TABLE, :page => params[:page]
      @articles_to_order = Article.belonging_to_user_client(current_user).select{|a| a.residue == 0}.paginate :per_page => Settings.DEFAULT_ITEMS_PER_TABLE, :page => params[:page]

  end
  
end
