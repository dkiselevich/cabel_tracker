# encoding: utf-8
class Purchase::DeliveryItemsController < InheritedResources::Base

  def index
    @delivery_items = DeliveryItem.where(:delivery_id => params[:delivery_id]).paginate :per_page => Settings.DEFAULT_ITEMS_PER_TABLE, :page => params[:page]
    render "purchase/delivery_items/index", :layout => false
  end

  def new
    @delivery_item = DeliveryItem.new
    @delivery_item.delivery_id = params[:delivery_id]
    render :layout => "pop_up"
  end

  def show
    @delivery_item = DeliveryItem.find(params[:id])
    render :layout => "pop_up"
  end

  def edit
    @delivery_item = DeliveryItem.find(params[:id])
    render :layout => "pop_up"
  end

  def create
    @delivery_item = DeliveryItem.new(params[:delivery_item])
    @delivery_item.delivery = Delivery.find(params[:delivery_id])

    if @delivery_item.save
      render :layout => false 
    else
      render :action => "new", :layout => "pop_up" 
    end
  end

  def update
    @delivery_item = DeliveryItem.find(params[:id])
    if @delivery_item.update_attributes(params[:delivery_item])
      render :action => "create", :layout => false 
    else
      render :action => "edit", :layout => "pop_up" 
    end
  end

  def destroy
    @delivery_item = DeliveryItem.find(params[:id])
    @delivery_item.destroy
    render :action => "create", :layout => false 
  end
end
