# encoding: utf-8
class Purchase::SuppliersController < InheritedResources::Base

  def index
    @suppliers = Supplier.belonging_to_user_client(current_user).paginate :per_page => Settings.DEFAULT_ITEMS_PER_TABLE, :page => params[:page]
  end

  def show
    @supplier = Supplier.find(params[:id])
    @article_availabilities = @supplier.article_availabilities.order("CREATED_AT DESC").paginate :per_page => Settings.DEFAULT_ITEMS_PER_TABLE, :page => params[:page]
  end
  
  def get_article_availabilities
    @supplier = Supplier.find(params[:id])
    @article_availabilities = @supplier.article_availabilities.paginate :per_page => Settings.DEFAULT_ITEMS_PER_TABLE, :page => params[:page] 
    render :layout => false
  end
  
  def create
    @supplier = Supplier.new(params[:supplier])
    @supplier.client_id = current_user.clients.first
    if @supplier.save then
      redirect_to([current_view, :suppliers], :notice => 'Lieferant erstellt: '+self.class.helpers.link_to(@supplier.name, url_for([current_view, @supplier])))
    else
      render :action => "new"
    end
  end
  
  def update
    @supplier = Supplier.find(params[:id])
    @supplier.client_id = current_user.clients.first
    if @supplier.update_attributes(params[:supplier])
      redirect_to([current_view, :suppliers], :notice => 'Lieferant upgedatet: '+self.class.helpers.link_to(@supplier.name, url_for([current_view, @supplier])))
    else
      render :action => "edit"
    end
  end
end
