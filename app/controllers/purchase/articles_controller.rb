# encoding: utf-8
class Purchase::ArticlesController < InheritedResources::Base

  def index
    @articles = Article.belonging_to_user_client(current_user).paginate :per_page => Settings.DEFAULT_ITEMS_PER_TABLE, :page => params[:page]
  end

  def show
    @article = Article.find(params[:id])
    @article_availabilities = @article.article_availabilities.order("CREATED_AT DESC").paginate :per_page => Settings.DEFAULT_ITEMS_PER_TABLE, :page => params[:page]
  end
  
  def get_article_availabilities
    @article = Article.find(params[:id])
    @article_availabilities = @article.article_availabilities.paginate :per_page => Settings.DEFAULT_ITEMS_PER_TABLE, :page => params[:page] 
    render :layout => false
  end

  def create
    @article = Article.new(params[:article])
    @article.client_id = current_user.clients.first
    if @article.save then
      redirect_to([current_view, :articles], :notice => 'Artikel erstellt: '+self.class.helpers.link_to(@article.name, url_for([current_view, @article])))
    else
      render :action => "new"
    end
  end

  def update
    @article = Article.find(params[:id])
    @article.client_id = current_user.clients.first
    if @article.update_attributes(params[:article])
      redirect_to([current_view, :articles], :notice => 'Artikel upgedatet: '+self.class.helpers.link_to(@article.name, url_for([current_view, @article])))
    else
      render :action => "edit"
    end
  end

  def destroy
    @article = Article.find(params[:id])
    @article.destroy
    redirect_to([current_view, :articles], :notice => 'Artikel wurde gelöscht.')
  end
end
