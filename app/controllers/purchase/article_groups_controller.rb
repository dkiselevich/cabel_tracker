# encoding: utf-8
class Purchase::ArticleGroupsController < InheritedResources::Base

  def index
    @article_groups = ArticleGroup.belonging_to_user_client(current_user).paginate :per_page => Settings.DEFAULT_ITEMS_PER_TABLE, :page => params[:page]
  end
  
  def show
    @article_group = ArticleGroup.find(params[:id])
  end
  
  def edit
     @article_group = ArticleGroup.find(params[:id])
  end

  def create
    @article_group = ArticleGroup.new(params[:article_group])
    @article_group.client_id = current_user.clients.first
    if @article_group.save then
      redirect_to([current_view, :article_groups], :notice => 'Warengruppe erstellt: '+self.class.helpers.link_to(@article_group.name, url_for([current_view, @article_group])))
    else
      render :action => "new"
    end
  end

  def update
    @article_group = ArticleGroup.find(params[:id])
    @article_group.client_id = current_user.clients.first
    if @article_group.update_attributes(params[:article_group])
      redirect_to([current_view, :article_groups], :notice => 'Warengruppe upgedatet: '+self.class.helpers.link_to(@article_group.name, url_for([current_view, @article_group])))
    else
      render :action => "edit"
    end
  end

  def destroy
    @article_group = ArticleGroup.find(params[:id])
    @article_group.destroy
    redirect_to([current_view, :article_groups], :notice => 'Warengruppe wurde gelöscht.')
  end
end
