# encoding: utf-8
class Purchase::ArticleAvailabilitiesController < InheritedResources::Base
  
  def index
    @article_availabilities = ArticleAvailability.belonging_to_user_client(current_user).paginate :per_page => Settings.DEFAULT_ITEMS_PER_TABLE, :page => params[:page]
  end


  def new
    @article_availability = ArticleAvailability.new
  end

  def show
    @article_availability = ArticleAvailability.find(params[:id])
  end

  def edit
    @article_availability = ArticleAvailability.find(params[:id])
  end

  def create
    @article_availability = ArticleAvailability.new(params[:article_availability])
    @article_availability.client = current_user.clients.first
    
    if @article_availability.save
      redirect_to :action => "index"
    else
      render :action => "new"
    end
  end

  def update
    @article_availability = ArticleAvailability.find(params[:id])
    
    if @article_availability.update_attributes(params[:article_availability])
      redirect_to :action => "index"
    else
      render :action => "edit"
    end
  end

end