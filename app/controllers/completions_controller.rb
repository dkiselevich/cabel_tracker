class CompletionsController < InheritedResources::Base
  #caches_action :show, :edit
  cache_sweeper :completion_sweeper

  def index
    @search = Completion.search(params[:q])
    @completions = @search.result.belonging_to_user_client(current_user).paginate :per_page => Settings.DEFAULT_ITEMS_PER_TABLE, :page => params[:page]
    render "completions/index"
  end

  def new
    @completion = Completion.new
    @completion.appointment = Appointment.find(params[:appointment_id])
    @completion.job_type = @completion.appointment.job.job_type
    @completion.network_type = @completion.appointment.job.network_type
    @completion.wohneinheiten = @completion.appointment.job.wohneinheiten
#    Settings.MAX_ASSETS_UPLOAD.times { @completion.completion_photos.build }
    render "completions/new"
  end

  def show
    @completion = Completion.belonging_to_user_client(current_user).find(params[:id])
    render "completions/show"
  end

  def pdf
    @completion = Completion.belonging_to_user_client(current_user).find(params[:id])

    file_name = "#{@completion.job.kunde_plz} #{@completion.job.kunde_ort} #{@completion.job.kunde_strasse} #{@completion.job.kunde_hausnummer}"
    html = render_to_string "completions/pdf_v4", :layout => false, :format => "pdf"
    pdf = PDFKit.new(html).to_pdf
    send_data(pdf, :filename => "#{file_name}.pdf", :type => 'application/pdf', :disposition => 'inline')
  end

  def edit
    @completion = Completion.belonging_to_user_client(current_user).find(params[:id])
    #Settings.MAX_ASSETS_UPLOAD.times { @completion.completion_photos.build }
    #fresh_when(:etag => @completion, :last_modified => @completion.updated_at.utc, :public => false) unless (Rails.env == "development")
  end
  
  def create
    @completion = Completion.new(params[:completion])
 #   Settings.MAX_ASSETS_UPLOAD.times { @completion.completion_photos.build }
    @completion.technician = current_user
    @completion.appointment = Appointment.belonging_to_user_client(current_user).find(params[:completion][:appointment_id])
    if @completion.save
      #@completion.delay.create_signature(@completion.signature, @completion.job.ot_auftragsnummer)   
      @completion.appointment.job.status = 3 #abgeschlossen
      @completion.appointment.job.save
      redirect_to(params[:fromPath] || [current_view, :completions], :notice => 'Auftragsabschluss erstellt: '+self.class.helpers.link_to(@completion.job.ot_auftragsnummer, url_for([current_view, @completion]))) 
    else
      render "completions/new"
    end
  end

  def update
    @completion = Completion.belonging_to_user_client(current_user).find(params[:id])
    #Settings.MAX_ASSETS_UPLOAD.times { @completion.completion_photos.build }
    @completion.appointment = Appointment.belonging_to_user_client(current_user).find(params[:completion][:appointment_id])
    
    if @completion.update_attributes(params[:completion])
    #  @completion.messwerte_photo_file_name = "#{params[:completion][:messwerte_photo_file_name]}"
    #  @completion.save
      
      @completion.delay.create_signature(@completion.signature, @completion.job.ot_auftragsnummer)   
      @completion.appointment.job.status = 3 #technisch abgeschlossen
      @completion.appointment.job.save
      redirect_to([current_view, @completion], :notice => 'Auftragsabschluss upgedatet: '+self.class.helpers.link_to(@completion.job.ot_auftragsnummer, url_for([current_view, @completion]))) 
    else
      render "completions/edit"
    end
  end

  def destroy
    @completion = Completion.belonging_to_user_client(current_user).find(params[:id])
    @completion.destroy
    redirect_to(params[:fromPath] || [current_view, :completions], :notice => 'Auftragsabschluss wurde geloescht.')
  end

  def get_zip
    headers['Cache-Control'] = 'no-cache'  
    c = Completion.belonging_to_user_client(current_user).find(params[:id])
    text = render_to_string(:template => "completions/pdf_v4", :locals => {:completion => c}, :layout => false)
    filename = c.create_zip(text)
    send_data(File.open(Rails.root.to_s+"/tmp/"+filename, "rb+").read, :type => 'application/zip', :disposition => 'attachment', :filename => filename+".zip")
    File.delete Rails.root.to_s+"/tmp/"+filename
  end
end


