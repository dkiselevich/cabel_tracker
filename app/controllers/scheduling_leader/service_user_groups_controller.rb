# encoding: utf-8
class SchedulingLeader::ServiceUserGroupsController < ServiceUserGroupsController
  	before_filter :get_job_count_by_status
end
