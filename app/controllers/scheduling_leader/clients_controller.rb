# encoding: utf-8
class SchedulingLeader::ClientsController < InheritedResources::Base
  before_filter :get_job_count_by_status

  def index
    @clients = Client.is_assigned_to_user(current_user).paginate :per_page => Settings.DEFAULT_ITEMS_PER_TABLE, :page => params[:page]
  end

  def show
    @client = Client.is_assigned_to_user(current_user).where(:id => params[:id]).first
  end

  def update
    @client = Client.find(params[:id])
    if @client.update_attributes(params[:client])
      redirect_to(scheduling_leader_clients_path, :notice => 'Mandant upgedatet: '+self.class.helpers.link_to(@client.name, scheduling_leader_client_path(@client)))
    else
      render :action => "edit"
    end
  end

end
