class SchedulingLeader::JobsController < JobsController
  actions :index, :new, :create, :show, :edit, :update
  before_filter :get_job_count_by_status

  def review
    @job = Job.find(params[:id])
    render :layout => "pop_up"
  end

  def index
    @search = Job.search(params[:q])
    @jobs = @search.result.where(:client_id => current_user.clients.collect{|c|c.id}).paginate :per_page => Settings.DEFAULT_ITEMS_PER_TABLE, :page => params[:page]
    render :layout => "application"
  end

  def closed
    @closed_jobs = Job.where(:status => 3).where(:client_id => current_user.clients.collect{|c|c.id}).paginate :per_page => Settings.DEFAULT_ITEMS_PER_TABLE, :page => params[:page]
    @cancelled_jobs = Job.where(:status => 5).where(:client_id => current_user.clients.collect{|c|c.id}).paginate :per_page => Settings.DEFAULT_ITEMS_PER_TABLE, :page => params[:page]
    render :layout => "application"
  end

  def close
    @job = Job.find(params[:id])
    @job.status = 4
    @job.closed = true
    @job.save 
    redirect_to :action => "closed"
  end
    
  def show
    @job = Job.find(params[:id])
    @other_jobs_in_object = get_other_jobs_in_object(@job)
#    #fresh_when(:etag => @job, :last_modified => @job.updated_at.utc, :public => false) unless (Rails.env == "development")
  end

  def edit
    @job = Job.find(params[:id])
    @other_jobs_in_object = get_other_jobs_in_object(@job)
 #   #fresh_when(:etag => @job, :last_modified => @job.updated_at.utc, :public => false) unless (Rails.env == "development")
  end

  def create
    @job = Job.new(params[:job])
    if @job.save
      redirect_to(scheduling_job_path(@job), :notice => 'Auftrag erstellt: '+self.class.helpers.link_to(@job.short_description, scheduling_job_path(@job)))
    else
      render :action => "new"
    end
  end

  def update
    @job = Job.find(params[:id])
    if @job.update_attributes(params[:job])
      redirect_to(scheduling_jobs_path(@job), :notice => 'Auftrag upgedated: '+ self.class.helpers.link_to(@job.short_description, scheduling_job_path(@job)))        
    else
      render :action => "edit"
    end
  end

  private 
    def get_other_jobs_in_object(job)
      Job.where(:kunde_plz => job.kunde_plz).where(:kunde_ort => job.kunde_ort).where(:kunde_strasse => job.kunde_strasse).where(:kunde_hausnummer => job.kunde_hausnummer).where("id != "+job.id.to_s)
    end
end
