class SchedulingLeader::BaseController < ApplicationController
  	before_filter :get_job_count_by_status

	def index
		cookies[:last_view] = "scheduling_leader"
		session[:mobile_param] = nil
    	@search_open_wk_jobs= Job.search(params[:search_open_wk_jobs])
    	@open_wk_jobs = @search_open_wk_jobs.result.joins(:appointment_requests).belonging_to_user_client(current_user).where("appointment_requests.created_at < ?",4.days.ago).where(:status => 2).paginate :per_page => 5, :page => params[:page_waiting]
		@search_cancelled_jobs= Job.search(params[:search_cancelled_jobs])
    	@cancelled_jobs = @search_cancelled_jobs.result.joins(:cancellations).belonging_to_user_client(current_user).where("cancellations.cause_id = ?", 1).paginate :per_page => 5, :page => params[:page_waiting]
	end

	def quotes_per_controller
		params[:period] ||= Settings.DEFAULT_PERIOD_FOR_CHARTS
		@frame_width = params[:frameWidth] ? params[:frameWidth].to_i : 450
		if (params[:controllers]) then
			if params[:controllers] == "ALL" then
				controllers ||= User.assigned_to_same_company(current_user).scheduling.where(:active => true)
			else
				controller_id = params[:controllers]
				controllers ||= User.assigned_to_same_company(current_user).scheduling.where(:active => true).where(:id => controller_id)
			end
		end
		@is_pop_up = true if params[:popup]
		controllers ||= User.assigned_to_same_company(current_user).scheduling.where(:active => true)
		@user_selection = false if (params[:userSelection] == "false")

		@quotes_per_controller = StatisticsHelper.get_quotes_per_controller(:period => params[:period], :controllers => controllers, :clients => current_user.clients)
		if @is_pop_up then
			@chart_width = "100%" 
		else
			@chart_width = (60*@quotes_per_controller.length)
			border = 15
			@chart_width = @frame_width - border if (@chart_width < (@frame_width - border))
			@chart_width = @chart_width.to_s+"px"
		end
		render :template => 'general/quotes_per_controller', :layout => "chart" 
	end 

	def volume_per_controller
		params[:period] ||= Settings.DEFAULT_PERIOD_FOR_CHARTS
		@frame_width = params[:frameWidth] ? params[:frameWidth].to_i : 450
		if (params[:controllers]) then
			if params[:controllers] == "ALL" then
				controllers ||= User.assigned_to_same_company(current_user).scheduling.where(:active => true)
			else
				controller_id = params[:controllers]
				controllers ||= User.assigned_to_same_company(current_user).scheduling.where(:active => true).where(:id => controller_id)
			end
		end
		@is_pop_up = true if params[:popup]
		controllers ||= User.assigned_to_same_company(current_user).scheduling.where(:active => true)
		@user_selection = false if (params[:userSelection] == "false")

		@volume_per_controller = StatisticsHelper.get_volume_per_controller(:period => params[:period], :controllers => controllers, :clients => current_user.clients)
		if params[:popup] then
			@chart_width = "100%" 
		else
			@chart_width = (60*@volume_per_controller.length)
			border = 15
			@chart_width = @frame_width - border if (@chart_width < (@frame_width - border))
			@chart_width = @chart_width.to_s+"px"
		end
		render :template => 'general/volume_per_controller', :layout => "chart" 
	end 

end