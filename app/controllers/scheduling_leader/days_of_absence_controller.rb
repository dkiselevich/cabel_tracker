# encoding: utf-8
class SchedulingLeader::DaysOfAbsenceController < ApplicationController
  
  def index
    @days_of_absence_list = DaysOfAbsence.where(:user_id => params[:user_id]).order("CREATED_AT DESC").paginate :per_page => Settings.DEFAULT_ITEMS_PER_TABLE, :page => params[:page]
    render "scheduling_leader/days_of_absence_list/index", :layout => false
  end

  def new
    @days_of_absence = DaysOfAbsence.new
    @days_of_absence.starting_point = Date.today
    @days_of_absence.end_point = Date.today
    @days_of_absence.user = User.find(params[:service_user_id])
    
    render :layout => "pop_up"
  end
  
  def create
    @days_of_absence = DaysOfAbsence.new(params[:days_of_absence])
    @days_of_absence.user = User.find(params[:user_id])

    if @days_of_absence.user.has_appointment?(@days_of_absence) 
   	 	flash[:error] = "Im angegebenen Zeitraum existieren bereits Termine für den Techniker. Sie sollten diese umdisponieren."
    end

    if @days_of_absence.save
      	render :layout => "pop_up"
	  else
      render :action => "new", :layout => "pop_up" 
    end
  end
  
  def edit
    @days_of_absence = DaysOfAbsence.find(params[:id])
    render :layout => "pop_up"
  end
  
  def show
    @days_of_absence = DaysOfAbsence.find(params[:id])
    render :layout => "pop_up"
  end
  
  
  def update
    @days_of_absence = DaysOfAbsence.find(params[:id])
    @days_of_absence.user = User.find(params[:user_id])

    if @days_of_absence.user.has_appointment?(@days_of_absence) 
   	 	flash[:error] = "Im angegebenen Zeitraum existieren bereits Termine für den Techniker. Sie sollten diese umdisponieren."
    end

    if @days_of_absence.update_attributes(params[:days_of_absence])
    	render :layout => "pop_up"
    else
      render :action => "edit", :layout => "pop_up" 
    end
  end
  
  def destroy
    @days_of_absence = DaysOfAbsence.find(params[:id])
    @days_of_absence.destroy
    render :layout => "pop_up"
  end
end
