class SchedulingLeader::SchedulingUsersController < InheritedResources::Base
  actions :index, :show, :new, :edit, :update, :create 
  before_filter :get_job_count_by_status

  def index
    #@scheduling_users = User.scheduling.assigned_to_same_company(current_user).paginate :per_page => Settings.DEFAULT_ITEMS_PER_TABLE, :page => params[:page]
    @scheduling_users = User.find_by_sql(['SELECT u.* 
                                        FROM users u LEFT OUTER JOIN assignments a ON a.user_id=u.id
                                        WHERE a.client_id IN (SELECT client_id FROM assignments WHERE user_id = ?)
                                        AND is_controller = true 
                                        AND active = true', current_user.id]) 
                         .paginate :per_page => Settings.DEFAULT_ITEMS_PER_TABLE, :page => params[:page]    
  end

  def show
    @scheduling_user = User.scheduling.assigned_to_same_company(current_user).find(params[:id])
  end

  def edit
    @scheduling_user = User.scheduling.assigned_to_same_company(current_user).find(params[:id])
  end

	def new
    @scheduling_user = User.new
    @scheduling_user.is_controller = true
  end

  def update
    @scheduling_user = User.find(params[:id])
    @scheduling_user.attributes = {'client_ids' => []}.merge(params[:user] || {})
    if @scheduling_user.update_attributes(params[:user])
      redirect_to(scheduling_leader_scheduling_users_path, :notice => 'Disponent upgedated: '+self.class.helpers.link_to(@scheduling_user.full_name, scheduling_leader_scheduling_user_path(@scheduling_user)))
    else
      render :action => "edit"
    end
  end

  def create
    @scheduling_user = User.new(params[:user])
    @scheduling_user.is_controller = true
		@scheduling_user.attributes = {'client_ids' => []}.merge(params[:user] || {})
    
    if @scheduling_user.save
      redirect_to(scheduling_leader_scheduling_users_path, :notice => 'Disponent erstellt: '+self.class.helpers.link_to(@scheduling_user.full_name, scheduling_leader_scheduling_user_path(@scheduling_user)))
    else
      render :action => "new"
    end
  end

end
