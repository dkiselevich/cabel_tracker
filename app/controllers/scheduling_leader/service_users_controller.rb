# encoding: utf-8
class SchedulingLeader::ServiceUsersController < InheritedResources::Base
  actions :index, :show 
  before_filter :get_job_count_by_status

  def index
    #@service_users = User.service.assigned_to_same_company(current_user).paginate :per_page => Settings.DEFAULT_ITEMS_PER_TABLE, :page => params[:page]
    @service_users = User.find_by_sql(['SELECT u.* 
                                        FROM users u LEFT OUTER JOIN assignments a ON a.user_id=u.id
                                        WHERE a.client_id IN (SELECT client_id FROM assignments WHERE user_id = ?)
                                        AND is_technician = true 
                                        AND active = true', current_user.id]) 
                         .paginate :per_page => Settings.DEFAULT_ITEMS_PER_TABLE, :page => params[:page]
    @test_users = User.service.assigned_to_same_company(current_user)
  end

  def show
    @service_user = User.service.assigned_to_same_company(current_user).find(params[:id])
    @appointments = @service_user.appointments.joins(:job).where("jobs.status = 1").paginate :per_page => Settings.DEFAULT_ITEMS_PER_TABLE, :page => params[:page]
    @days_of_absence = @service_user.days_of_absence.order("starting_point DESC").paginate :per_page => Settings.DEFAULT_ITEMS_PER_TABLE, :page => params[:page]
  end

  def edit
    @service_user = User.service.assigned_to_same_company(current_user).find(params[:id])
    @appointments = @service_user.appointments.joins(:job).where("jobs.status = 1").paginate :per_page => Settings.DEFAULT_ITEMS_PER_TABLE, :page => params[:page]
    @days_of_absence = @service_user.days_of_absence.order("starting_point DESC").paginate :per_page => Settings.DEFAULT_ITEMS_PER_TABLE, :page => params[:page]
  end

	def new
    @service_user = User.new
    @service_user.is_technician = true
  end

  def update
    @service_user = User.find(params[:id])
    @service_user.attributes = {'client_ids' => []}.merge(params[:user] || {})
   
    if @service_user.update_attributes(params[:user])
    	fix_working_hours(@service_user, params)
	  	redirect_to(scheduling_leader_service_users_path, :notice => 'Disponent upgedated: '+self.class.helpers.link_to(@service_user.full_name, scheduling_leader_service_user_path(@service_user)))
    else
      render :action => "edit"
    end
  end

  def fix_working_hours(service_user, params) 
	  service_user.monday_work_start = nil if params[:user]['monday_work_start(4i)'].blank?
	  service_user.monday_work_end = nil if params[:user]['monday_work_end(4i)'].blank?
	  service_user.monday_lunch_break_start = nil if params[:user]['monday_lunch_break_start(4i)'].blank?
	  service_user.monday_lunch_break_end = nil if params[:user]['monday_lunch_break_end(4i)'].blank?

	  service_user.tuesday_work_start = nil if params[:user]['tuesday_work_start(4i)'].blank?
	  service_user.tuesday_work_end = nil if params[:user]['tuesday_work_end(4i)'].blank?
	  service_user.tuesday_lunch_break_start = nil if params[:user]['tuesday_lunch_break_start(4i)'].blank?
	  service_user.tuesday_lunch_break_end = nil if params[:user]['tuesday_lunch_break_end(4i)'].blank?

	  service_user.wednesday_work_start = nil if params[:user]['wednesday_work_start(4i)'].blank?
	  service_user.wednesday_work_end = nil if params[:user]['wednesday_work_end(4i)'].blank?
	  service_user.wednesday_lunch_break_start = nil if params[:user]['wednesday_lunch_break_start(4i)'].blank?
	  service_user.wednesday_lunch_break_end = nil if params[:user]['wednesday_lunch_break_end(4i)'].blank?

	  service_user.thursday_work_start = nil if params[:user]['thursday_work_start(4i)'].blank?
	  service_user.thursday_work_end = nil if params[:user]['thursday_work_end(4i)'].blank?
	  service_user.thursday_lunch_break_start = nil if params[:user]['thursday_lunch_break_start(4i)'].blank?
	  service_user.thursday_lunch_break_end = nil if params[:user]['thursday_lunch_break_end(4i)'].blank?

	  service_user.friday_work_start = nil if params[:user]['friday_work_start(4i)'].blank?
	  service_user.friday_work_end = nil if params[:user]['friday_work_end(4i)'].blank?
	  service_user.friday_lunch_break_start = nil if params[:user]['friday_lunch_break_start(4i)'].blank?
	  service_user.friday_lunch_break_end = nil if params[:user]['friday_lunch_break_end(4i)'].blank?

	  service_user.saturday_work_start = nil if params[:user]['saturday_work_start(4i)'].blank?
	  service_user.saturday_work_end = nil if params[:user]['saturday_work_end(4i)'].blank?
	  service_user.saturday_lunch_break_start = nil if params[:user]['saturday_lunch_break_start(4i)'].blank?
	  service_user.saturday_lunch_break_end = nil if params[:user]['saturday_lunch_break_end(4i)'].blank?

	  service_user.sunday_work_start = nil if params[:user]['sunday_work_start(4i)'].blank?
	  service_user.sunday_work_end = nil if params[:user]['sunday_work_end(4i)'].blank?
	  service_user.sunday_lunch_break_start = nil if params[:user]['sunday_lunch_break_start(4i)'].blank?
	  service_user.sunday_lunch_break_end = nil if params[:user]['sunday_lunch_break_end(4i)'].blank?
	  service_user.save
	end

  def create
    @service_user = User.new(params[:user])
    @service_user.is_technician = true
		@service_user.attributes = {'client_ids' => []}.merge(params[:user] || {})
    
    if @service_user.save
      redirect_to(scheduling_leader_service_users_path, :notice => 'Disponent erstellt: '+self.class.helpers.link_to(@service_user.full_name, scheduling_leader_service_user_path(@service_user)))
    else
      render :action => "new"
    end
  end

  def show_send_emails
  end

  def send_emails
  	client = Client.find(params[:client_id])
  	TaskHelper.delay.send_emails_with_blank_completion_pdf_for_each_appointment_tomorrow_to_all_technicians(client)
  	redirect_to(scheduling_leader_service_users_path, :notice => "Emails für "+client.name+" werden jetzt gesendet.")
  end

  def create_pdf
    @service_user = User.service.assigned_to_same_company(current_user).find(params[:id])
    @appointments = @service_user.appointments.joins(:job).where("jobs.status = 2").paginate :per_page => Settings.DEFAULT_ITEMS_PER_TABLE, :page => params[:page]
    
    html = render_to_string "appointments_pdf", :layout => false, :format => "pdf"
    pdf = PDFKit.new(html).to_pdf
    send_data(pdf, :filename => "Termine-#{@service_user.full_name}.pdf", :type => 'application/pdf', :disposition => 'inline')
  end

  def appointments
    @service_user = User.service.assigned_to_same_company(current_user).find(params[:id])
  end

  def appointments_pop_up
    @service_user = User.service.assigned_to_same_company(current_user).find(params[:id])
    render :layout => "pop_up"
  end

  def appointments_data
    @service_user = User.service.assigned_to_same_company(current_user).find(params[:id])
    render :layout => false
  end

  def get_days_of_absence
    @service_user = User.find(params[:id])
    @days_of_absence = @service_user.days_of_absence.order("starting_point DESC").paginate :per_page => Settings.DEFAULT_ITEMS_PER_TABLE, :page => params[:page] 
    render :layout => false
  end

end
