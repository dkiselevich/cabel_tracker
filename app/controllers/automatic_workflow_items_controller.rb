# encoding: utf-8
class AutomaticWorkflowItemsController < InheritedResources::Base
 before_filter :get_job_count_by_status
  #caches_action :show, :edit

  def index
    @automatic_workflow_items = AutomaticWorkflowItem.belonging_to_user_client(current_user).paginate :per_page => Settings.DEFAULT_ITEMS_PER_TABLE, :page => params[:page]
    render "automatic_workflow_items/index"
  end

  def new
    @automatic_workflow_item = AutomaticWorkflowItem.new
    render "automatic_workflow_items/new"
  end

  def show
    @automatic_workflow_item = AutomaticWorkflowItem.belonging_to_user_client(current_user).find(params[:id])
    render "automatic_workflow_items/show"
  end

  def edit
    @automatic_workflow_item = AutomaticWorkflowItem.belonging_to_user_client(current_user).find(params[:id])
    render "automatic_workflow_items/edit"
  end
  
  def create
    @automatic_workflow_item = AutomaticWorkflowItem.new(params[:automatic_workflow_item])   
    if @automatic_workflow_item.save
      redirect_to([current_view, :automatic_workflow_items], :notice => 'Ablauf erstellt: ' + self.class.helpers.link_to(@automatic_workflow_item.start_time, url_for([current_view,@automatic_workflow_item])))
    else
      render :action => "new"
    end
  end

  def update
    @automatic_workflow_item = AutomaticWorkflowItem.belonging_to_user_client(current_user).find(params[:id])
    if @automatic_workflow_item.update_attributes(params[:automatic_workflow_item])
      #expire_action :action => :show
      redirect_to([current_view, :automatic_workflow_items], :notice => 'Ablauf upgedatet: ' + self.class.helpers.link_to(@automatic_workflow_item.start_time, url_for([current_view,@automatic_workflow_item])))
    else
      render :action => "edit"
    end   
  end

  def destroy
    @automatic_workflow_item = AutomaticWorkflowItem.belonging_to_user_client(current_user).find(params[:id])
    @automatic_workflow_item.destroy
    #expire_action :action => :show
    redirect_to([current_view, :automatic_workflow_items], :notice => 'Ablauf gelöscht.')
  end
end