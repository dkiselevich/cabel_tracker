# encoding: utf-8
class Management::ReportsController < ApplicationController
  
  def index
  end

  def overview_performance_appointments
    if params[:client_id] 
      client = Client.find(params[:client_id]) 
    else
      client = Client.find(3)
    end
    jobs = Job.where(:client_id => client).where("created_at >= ?", Date.today.beginning_of_month).where("created_at <= ?", Date.today.end_of_month)
    @report = create_overview_performance_appointments_report(client, jobs)  
    render :layout => false 
  end

  def create_overview_performance_appointments_report(client, jobs)
  	report = Ebru::Report.new
        
    container1 = Ebru::Container.new
    report.add(container1)
    
    performance_data = get_performance_data(client, jobs)
    performance_data_table = performance_data
    kw = []
    jobs = []
    appointments = []
    max_value = [0]
    performance_data.each do |r|
      kw << r[0]
      jobs << r[1]
      appointments << r[2]

      axis_value_jobs = r[1]
      axis_value_appointments = r[2]
      max_value.push(axis_value_jobs, axis_value_appointments)
    end
    sum_jobs = 0
    sum_appointments = 0
    avg_success = 0

    performance_data_table.each do |row|
      sum_jobs += row[1]
      sum_appointments += row[2]
      avg_success += row[3].to_f / row[3].length
    end
    cf_performance = Ebru::ColumnChart.new({:title => "Aufträge/Termine",
      :data =>[jobs, appointments],
      :legend => ["Aufträge","Termine"],
      :bar_width => 20,
      :axis_x => [kw],
      :axis_y => [nil,(0..max_value.max).to_a],
      :chart_width => 310,
      :pdf_height => 300,
    })
    container1.add(cf_performance)
  	return report
  end
  
  def get_performance_data(client, jobs)
    data = []
    jobs_by_date = 0
    appointments = 0

    (Date.today.beginning_of_month..Date.today.end_of_month).each do |date|
      kw = date.cweek
      appointments = Appointment.where(:day => date).length
      jobs.each do |job|
        if (job.created_at.to_date == date) then
        jobs_by_date += 1
        end
      end
      data << [kw, jobs_by_date, appointments]
    end
    return summarize_by_calendar_weeks_performance(data)
  end
  
  def summarize_by_calendar_weeks_performance(data)
    data2 = []
    data.each do |r|
      kw = r[0]
      jobs = r[1]
      appointments = r[2]

      if (data2[kw] == nil)
      total_jobs = 0
      total_appointments = 0
      else
      total_jobs = data2[kw][1]+ jobs
      total_appointments = data2[kw][1]+ appointments
      end
      performance = (total_appointments.to_f/total_jobs.to_f) *100
      if (performance.nan? == true) then
      performance = 0
      end
      data2[kw] = ["KW #{kw}",total_jobs,total_appointments,"#{performance.round(1)}%"]
    end
    return data2.compact
  end
  
  def overview_performance_success
    if params[:client_id] 
      client = Client.find(params[:client_id]) 
    else
      client = Client.find(3)
    end
    jobs = Job.where(:client_id => client).where("created_at >= ?", Date.today.beginning_of_month).where("created_at <= ?", Date.today.end_of_month)
    completions = Completion.joins(:appointment => :job).where("completions.created_at >= ?",  Date.today.beginning_of_month).where("completions.created_at <= ?", Date.today.end_of_month).where("jobs.client_id = ?", client.id) 
    @report = create_overview_performance_success_report(client, jobs, completions)
    render :layout => false 
  end

  def create_overview_performance_success_report(client, jobs, completions)
    report = Ebru::Report.new
    report.client = client

    container1 = Ebru::Container.new()
    report.add(container1)

    performance_success_rate_data = get_performance_success_rate_data(client, jobs, completions)
    performance_success_rate_data_table = performance_success_rate_data
    kw = []
    jobs = []
    completions = []
    max_value = [0]
    performance_success_rate_data.each do |r|
      kw << r[0]
      jobs << r[1]
      completions << r[2]

      axis_value_jobs = r[1]
      axis_value_completions = r[2]
      max_value.push(axis_value_jobs, axis_value_completions)
    end
    sum_jobs = 0
    sum_completions = 0
    avg_success = 0
    performance_success_rate_data_table.each do |row|
      sum_jobs += row[1]
      sum_completions += row[2]
      avg_success += row[3].to_f / row[3].length
    end

    cf_performance = Ebru::ColumnChart.new({:title => "Aufträge/Erfolgreiche Abschlüsse",
      :data => [jobs, completions],
      :legend => ["Aufträge","Erfolgreiche Abschlüsse"],
      :bar_width => 20,
      :axis_x => [kw],
      :axis_y => [nil,(0..max_value.max).to_a],
      :chart_width => 310,
      :pdf_height => 300,
    })
    container1.add(cf_performance)

    return report
  end

  def get_performance_success_rate_data(client, jobs, completions)
    data = []
    jobs_by_date = 0
    completions_by_date = 0

    (Date.today.beginning_of_month..Date.today.end_of_month).each do |date|
      kw = date.cweek

      jobs.each do |job|
        if (job.created_at.to_date == date) then
        jobs_by_date += 1
        end
      end
      completions.each do |completion|
        if (completion.created_at.to_date == date) then
        completions_by_date += 1
        end
      end
      data << [kw, jobs_by_date, completions_by_date]
    end
    return summarize_by_calendar_weeks_performance_success_rate(data)
  end

  def summarize_by_calendar_weeks_performance_success_rate(data)
    data2 = []
    data.each do |r|
      kw = r[0]
      jobs = r[1]
      completions = r[2]

      if (data2[kw] == nil)
      total_jobs = 0
      total_completions = 0
      else
      total_jobs = data2[kw][1]+ jobs
      total_completions = data2[kw][1]+ completions
      end
      performance_success_rate = (total_completions.to_f/total_jobs.to_f) *100
      if (performance_success_rate.nan? == true) then
      performance_success_rate = 0
      end
      data2[kw] = ["KW #{kw}",total_jobs,total_completions,"#{performance_success_rate.round(1)}%"]
    end
    return data2.compact
  end
  
  def overview_dispositions
    if params[:client_id] 
      client = Client.find(params[:client_id]) 
    else
      client = Client.find(3)
    end
    @report = create_overview_dispositions_report(client)  
   render :layout => false
  end

  def create_overview_dispositions_report(client)  
    report = Ebru::Report.new
    report.client = client

    container1 = Ebru::Container.new()
    report.add(container1)

    number_of_dispositions_data = get_number_of_dispositions_data(client)
    number_of_dispositions_data_table = number_of_dispositions_data
    kw = []
    di = []

    max_value = [0]
    number_of_dispositions_data.each do |r|
      kw << r[0]
      di << r[1]

      axis_value_di = r[1]
      max_value.push(axis_value_di)
    end
    sum_di = 0
    number_of_dispositions_data_table.each do |row|
      sum_di += row[1]
    end

    cf_dispositions = Ebru::ColumnChart.new({:title => "Dispositionen",
      :data => [di],
      :legend => ["Dispositionen"],
      :bar_width => 30,
      :axis_x => [kw],
      :axis_y => [nil,(0..max_value.max).to_a],
      :chart_width => 310,
      :pdf_height => 300,
    })

    container1.add(cf_dispositions)
    
    return report
  end

  def get_number_of_dispositions_data(client)
    data = []
    dispositions = 0
    (Date.today.beginning_of_month..Date.today.end_of_month).each do |date|
      kw = date.cweek

      dispositions = Appointment.where(:day => date).length

      data << [kw, dispositions]
    end
    return summarize_by_calendar_weeks_di(data)
  end

  def summarize_by_calendar_weeks_di(data)
    data2 = []
    data.each do |r|
      kw = r[0]
      di = r[1]

      if (data2[kw] == nil)
      total_di = 0
      else
      total_di = data2[kw][1]+di
      end
      data2[kw] = ["KW #{kw}",total_di]
    end
    return data2.compact
  end
end
