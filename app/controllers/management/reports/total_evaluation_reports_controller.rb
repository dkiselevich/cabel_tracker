# encoding: utf-8
class Management::Reports::TotalEvaluationReportsController < ApplicationController
  def index

  end

  def total_evaluation
    begin
      params[:start].to_date
      params[:end].to_date
    rescue
      flash[:error] = "Bitte nur gültigen Datum angeben"
      render "index"
    return
    end

    if (params[:end].to_date < params[:start].to_date)
      flash[:error] = "Start muss vor Ende liegen."
      render "index"
    return
    end

    client = Client.find_by_id(params[:client])
    report = create_total_evaluation_report(client, params[:period], params[:start], params[:end]) 
  end

  def get_total_evolution_table_data(users, jobs, completions, start_date, end_date)
    data = []
    istwert_k = 0
    akt_modem_wert_k = 0
    terminwert_k = 0
    ave_akt_modem = 0
    durchschnitt_erfuellung = 0
    durchschnitt_produktivitaet = 0
    i = 0
    @ist = []
    @produktivitaet = []
    @termine = []
    @akt_modem = []
    a = []
    max_ist_value = 0
    max_termin_value = 0
    max_akt_modem_value = 0
    max_produktivitaets_value = 0
    veraenderung = 0

    (start_date.to_date..end_date.to_date).each do |date|

      if (date.wday == 0)
      @ist.push(0)
      @produktivitaet.push(0)
      @termine.push(0)
      @akt_modem.push(0)
      next
      end

      #aus crp
      planwert = 0
      users.each do |user|
        planwert += user.actual_working_hours_as_string(date,date)
      end

      istwert = 0
      status_di = 0
      status_wk = 0
      disponiert_plus_wakant = 0

      v = 0
      j = 0
      jobs.each do |job|
        if(job.created_at.to_date == date) then
          istwert += 1
          if(job.status == 1) then
          status_di += 1
          end
          if(job.status == 2) then
          status_wk += 1
          end
        disponiert_plus_wakant = status_di + status_wk
        end
      end

      a = a.push(disponiert_plus_wakant)

      a.push(disponiert_plus_wakant).each_with_index do |e, index|
        veraenderung = e[index]
      end

      istwert_k += istwert

      max_ist_value = istwert if (istwert > max_ist_value)
      @ist.push(istwert)

      akt_modem_wert = 0
      completions.each do |completion|
        if (completion.created_at.to_date == date) then
        akt_modem_wert += 1
        end
      end
      akt_modem_wert_k += akt_modem_wert

      max_akt_modem_value = akt_modem_wert if (akt_modem_wert > max_akt_modem_value)
      @akt_modem.push(akt_modem_wert)

      terminwert = Appointment.where(:day => date).length
      terminwert_k += terminwert

      erfuellungswert = 0
      if (akt_modem_wert > 0) then
      erfuellungswert = (akt_modem_wert_k.to_f / istwert_k.to_f) * 100
      erfuellungswert = erfuellungswert.round(1)
      end
      i += 1

      ave_akt_modem = (akt_modem_wert_k.to_f) / i

      techniker = users.find_all{|u| u.is_present_on_day(date.to_date)}.length
      produktivitaetswert = 0
      produktivitaetswert += akt_modem_wert.to_f / techniker

      data << [#user.full_name,
        date.strftime("%d.%m.%Y"),
        #plan = user.get_total_number_of_technician(start_param.to_date, end_param.to_date),
        plan = planwert.round,
        ist  = istwert,
        termine  = terminwert,
        akt_modem = akt_modem_wert,
        soll_k = istwert_k,
        termine_k = terminwert_k,
        akt_modem_k = akt_modem_wert_k,
        erfuellung = erfuellungswert.to_s + "%",
        durchschnitt_akt_modem = ave_akt_modem.round(1),
        produktivitaet = produktivitaetswert.round(1).to_s + " Stk./TK",
        di_plus_wk = disponiert_plus_wakant,
      ]
    end
    # Spalte Veränderung
    di_wk_alt = -999
    data.each_with_index do |zeile, idx|
      if (di_wk_alt > -999)
      data[idx-1][12] =  zeile[11] - di_wk_alt
      end
      di_wk_alt = zeile[11]
    end
    return data
  end

  def create_total_evaluation_report(client, period, start_param, end_param)

    report = Ebru::Report.new
    report.client = client    
    report.logo_path =  client.logo_img.url
    
    container1 = Ebru::Container.new
    container1.display_order = "vertical"
    container1.header_title = "Gesamtauswertung"
    report.add(container1)

    @users = User.where(:is_technician => true).where(:active => true).assigned_to_company(report.client)
    container1.add(Ebru::StringField.new("Zeitraum:", start_param.to_date.strftime("%d.%m.%Y") +" - "+ end_param.to_date.strftime("%d.%m.%Y")))
    container1.add(Ebru::StringField.new("Erstellt von:", current_user.full_name))

    data = []
    istwert_k = 0
    akt_modem_wert_k = 0
    terminwert_k = 0
    ave_akt_modem = 0
    durchschnitt_erfuellung = 0
    durchschnitt_produktivitaet = 0
    i = 0
    @ist = []
    @produktivitaet = []
    @termine = []
    @akt_modem = []
    a = []
    max_ist_value = 0
    max_termin_value = 0
    max_akt_modem_value = 0
    max_produktivitaets_value = 0
    veraenderung = 0

    (start_param.to_date..end_param.to_date).each do |date|

      if (date.wday == 0)
      @ist.push(0)
      @produktivitaet.push(0)
      @termine.push(0)
      @akt_modem.push(0)
      next
      end

      #aus crp
      planwert = 0
      @users.each do |user|
        planwert += user.actual_working_hours_as_string(date,date)
      end

      istwert = 0
      status_di = 0
      status_wk = 0
      disponiert_plus_wakant = 0

      @jobs = Job.where(:client_id => report.client).where("created_at >= ?", start_param.to_date).where("created_at <= ?", end_param.to_date)
      @jobs.each do |job|
        if(job.created_at.to_date == date) then
          istwert += 1
          if(job.status == 1) then
          status_di += 1
          end
          if(job.status == 2) then
          status_wk += 1
          end
        disponiert_plus_wakant = status_di + status_wk
        end
      end
      istwert_k += istwert

      max_ist_value = istwert if (istwert > max_ist_value)
      @ist.push(istwert)

      akt_modem_wert = 0
      @completions = Completion.joins(:appointment => :job).where("completions.created_at >= ?", start_param.to_date).where("completions.created_at <= ?", end_param.to_date).where("jobs.client_id = ?", client.id)
      @completions.each do |completion|
        if (completion.created_at.to_date == date) then
        akt_modem_wert += 1
        end
      end
      akt_modem_wert_k += akt_modem_wert

      max_akt_modem_value = akt_modem_wert if (akt_modem_wert > max_akt_modem_value)
      @akt_modem.push(akt_modem_wert)

      terminwert = Appointment.where(:day => date).length
      terminwert_k += terminwert

      max_termin_value = terminwert if (terminwert > max_termin_value)
      @termine.push(terminwert)

      erfuellungswert = 0
      if (akt_modem_wert > 0) then
      erfuellungswert = (akt_modem_wert_k.to_f / istwert_k.to_f) * 100
      erfuellungswert = erfuellungswert.round(1)
      end
      i += 1

      durchschnitt_erfuellung += erfuellungswert
      @durchschnitt_erfuellung = durchschnitt_erfuellung / i

      ave_akt_modem = (akt_modem_wert_k.to_f) / i

      techniker = @users.find_all{|u| u.is_present_on_day(date.to_date)}.length
      produktivitaetswert = 0
      produktivitaetswert += akt_modem_wert.to_f / techniker

      max_produktivitaets_value = produktivitaetswert if (produktivitaetswert > max_produktivitaets_value)
      @produktivitaet.push(produktivitaetswert)

      durchschnitt_produktivitaet += produktivitaetswert
      @durchschnitt_produktivitaet = durchschnitt_produktivitaet / i

      @offene_installationen = ((istwert_k * 0.8 ) - (akt_modem_wert_k))
      @offene_installationen_prozent = (akt_modem_wert_k.to_f / istwert_k.to_f) * 100
    end

     cf = Ebru::ColumnChart.new({:title => 'Aktive Modem pro Termin',
                                  :data => [@ist,@termine,@akt_modem],
                                  :legend => ["Ist","Termine","akt. Modem"],
                                  :bar_width => 6,
                                  :axis_x => [(1..31).to_a],
                                  :axis_y => [nil,(0..max_ist_value).to_a],
                                  :chart_width => 930,
                                  :pdf_height => 500, 
                                })
    container1.add(cf)
    
    table = Ebru::Table.new({ 
      :data => get_total_evolution_table_data(@users, @jobs, @completions, start_param, end_param), 
      :header => ["Datum","Plan","Ist","Termine","akt.Modem","Soll (k)","Termine (k)","akt. Modem (k)","Erfüllung","Ø akt. Modem","Produktivität","DI + WK","Veränderung"], 
      :emphasized_columns => [0],
      }) 
    report.add(table)
    
    table_attachment = Ebru::Container.new
    table_attachment.display_order = "vertical"
    table_attachment.add(Ebru::StringField.new("Offene Installationen bis 80%: ","Zwischenergebnis: #{@offene_installationen.to_f.round(1)} Prozentual: #{@offene_installationen_prozent.round(1)}%"))
    table_attachment.add(Ebru::StringField.new("Ø Erfüllungsrate:","#{@durchschnitt_erfuellung.round(2)}%"))
    table_attachment.add(Ebru::StringField.new("Ø Produktivität:","#{@durchschnitt_produktivitaet.to_f.round(2)}%"))
    table_attachment.add(Ebru::StringField.new("noch zu leisten",""))
    table_attachment.add(Ebru::StringField.new("derzeitiges Mitte",""))
    table_attachment.add(Ebru::StringField.new("Terminerfolg",""))
    table_attachment.add(Ebru::StringField.new("Abzubauender Umlauf",""))
    table_attachment.add(Ebru::StringField.new("LZ vermutlich am Monatsende",""))
    table_attachment.add(Ebru::StringField.new("Plan/ aktueller Wert",""))

    report.add(table_attachment)
    
    report.pdf_path = "#{request.protocol}#{request.host_with_port}#{request.fullpath}".gsub("?",".pdf?")
    report.csv_path = "#{request.protocol}#{request.host_with_port}#{request.fullpath}".gsub("?",".csv?")
    return format_report(report)
  end
end
