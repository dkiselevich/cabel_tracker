# encoding: utf-8
class Management::Reports::QualityReportsController < ApplicationController

 def index
     @quality_reports = QualityReport.order('created_at DESC').paginate :per_page => Settings.DEFAULT_ITEMS_PER_TABLE, :page => params[:page]
    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @quality_reports }
    end
  end


  def show
    @quality_report = QualityReport.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml =>  @quality_report }
    end
  end

  def show_as_pdf
    @quality_report = QualityReport.find(params[:id])
    
    if (!@quality_report.pdf_created) then
      @quality_report.pdf_created = Date.current 
      @quality_report.save
    end
    
    report = create_ebru_report(@quality_report)
  end
  
   def show_as_html
    @quality_reports = QualityReport.all
    @quality_report = QualityReport.find(params[:id])
    
    if (!@quality_report.pdf_created) then
      @quality_report.pdf_created = Date.current 
      @quality_report.save
    end
    
    report = create_ebru_report(@quality_report)    
    render :inline => report.render_html(), :layout => "reports"
  end

  def create_ebru_report(quality_report) 

    report = Ebru::Report.new
    report.client = Client.first
    report.logo_path = "http://glasfaserkompetenzzentrum.de/images/seesys_logo.jpg"

    container1 = Ebru::Container.new
    container1.display_order = "vertical"
    container1.header_title = "Qualitätsreport"    
    container1.add(Ebru::StringField.new("Erstellt von", quality_report.user.full_name))
    container1.add(Ebru::StringField.new("Report erstellt am", quality_report.created_at.strftime("%d.%m.%Y")))
    container1.add(Ebru::StringField.new("PDF erstellt am", quality_report.pdf_created.strftime("%d.%m.%Y")))
    report.add(container1)
    
    container2 = Ebru::Container.new
    container2.display_order = "vertical"
    container2.add(Ebru::TextField.new("Geplante Maßnahmen für die kommenden zwei Monate", quality_report.planned_measures_next_two_month))
    container2.add(Ebru::TextField.new("Erkannte Qualitätsprobleme", quality_report.detected_quality_problems))
    container2.add(Ebru::TextField.new("Erkannte und Erforderliche Maßnahmen", quality_report.identified_necessary_measures))
    container2.add(Ebru::TextField.new("Erarbeitete Einführungsplanungen", quality_report.launch_plannings))
    container2.add(Ebru::TextField.new("Umgesetzte Maßnahmen", quality_report.implemented_measures))
    container2.add(Ebru::TextField.new("Nachhaltigkeitskontrolle", quality_report.sustainability_control))
    report.add(container2)

    return format_report(report)
  end


  def new
     @quality_report = QualityReport.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml =>  @quality_report }
    end
  end


  def edit
     @quality_report = QualityReport.find(params[:id])
    
  end


  def create
     @quality_report = QualityReport.new(params[:quality_report])
     @quality_report.user = current_user

    respond_to do |format|
      if  @quality_report.save
        format.html { redirect_to management_reports_quality_report_path(@quality_report), :notice => 'Quality Report was successfully created.' }
        format.xml  { render :xml =>  @quality_report, :status => :created, :location =>  @quality_report }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml =>  @quality_report.errors, :status => :unprocessable_entity }
      end
    end
  end


  def update
     @quality_report = QualityReport.find(params[:id])

    respond_to do |format|
      if @quality_report.update_attributes(params[:quality_report])
        format.html { redirect_to management_reports_quality_report_path(@quality_report), notice: 'Quality Report was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @quality_report.errors, status: :unprocessable_entity }
      end
    end
  end


  def destroy
     @quality_report = QualityReport.find(params[:id])
     @quality_report.destroy

    expire_action :action => :show
    respond_to do |format|
      format.html { redirect_to(management_reports_quality_reports_url) }
      format.xml  { head :ok }
    end
  end
end
