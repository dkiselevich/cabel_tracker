# encoding: utf-8
class Management::Reports::InstallationReportsController < ApplicationController
  before_filter :validate_date_params
  def validate_date_params()
    begin
      params[:start].to_date
      params[:end].to_date
    rescue
      flash[:error] = "Bitte nur gültigen Datum angeben"
      # redirect_to "/management/reports/total_evaluation_reports/index"
      render :template => "/management/reports/total_evaluation_reports/index"
    return
    end

    if (params[:end].to_date < params[:start].to_date)
      flash[:error] = "Start muss vor Ende liegen."
      redirect_to "/management/reports/total_evaluation_reports"
    # render :template => "/management/reports/total_evaluation_reports/index"
    return
    end
  end

  # Installationszahlen - Beauftragungen
  def number_of_jobs
    client = Client.find_by_id(params[:client])
    jobs = Job.where(:client_id => client).where("created_at >= ?", params[:start].to_date).where("created_at <= ?", params[:end].to_date)
    report = create_number_of_jobs_report(client, jobs, params[:start], params[:end])
  end

  def create_number_of_jobs_report(client, jobs, start_param, end_param)
    report = Ebru::Report.new
    report.client = client

    container1 = Ebru::Container.new()
    container1.display_order = "vertical"
    container1.header_title = "Installationszahlen"
    container1.add(Ebru::StringField.new("Zeitraum:", start_param.to_date.strftime("%d.%m.%Y") +" - "+ end_param.to_date.strftime("%d.%m.%Y")))
    container1.add(Ebru::StringField.new("Erstellt von:", current_user.full_name))
    report.add(container1)

    number_of_jobs_data = get_number_of_jobs_data(client, jobs, start_param, end_param)
    number_of_jobs_data_for_table = number_of_jobs_data
    kw = []
    ea = []
    fa = []
    max_value = [0]
    number_of_jobs_data .each do |r|
      kw << r[0]
      ea << r[1]
      fa << r[2]
      axis_ea = r[1]
      axis_fa = r[2]
      max_value.push(axis_ea,axis_fa)
    end

    sum_ea = 0
    sum_fa = 0
    number_of_jobs_data_for_table.each do |row|
      sum_ea += row[1]
      sum_fa += row[2]
    end
    
    cf_jobs = Ebru::ColumnChart.new({:title => "Beauftragungen",
      :data => [ea,fa],
      :legend => ["Erstanschlüsse","Folgeanschlüsse"],
      :bar_width => 15,
      :axis_x => [kw],
      :axis_y => [nil,(0..max_value.max).to_a],
      :chart_width => 930,
      :pdf_height => 500,
    })
    container1.add(cf_jobs)

    number_of_jobs_table = Ebru::Table.new({ 
      :data => number_of_jobs_data_for_table, 
      :header =>  ["Wochen","Erstanschlüsse","Folgeanschlüsse"],
      :emphasized_columns => [0],
      :footer => ["Gesamt",sum_ea, sum_fa]
      }) 
    report.add(number_of_jobs_table)

    report.pdf_path = "#{request.protocol}#{request.host_with_port}#{request.fullpath}".gsub("?",".pdf?")
    report.csv_path = "#{request.protocol}#{request.host_with_port}#{request.fullpath}".gsub("?",".csv?")
    return format_report(report)
  end

  def get_number_of_jobs_data(client, jobs, start_param, end_param)
    data = []
    jobs_ea = 0
    jobs_fa = 0

    (start_param.to_date..end_param.to_date).each do |date|
      kw = date.cweek

      jobs.each do |job|
        if(job.created_at.to_date == date && job.auftragsart_id == 1) then
        jobs_ea += 1
        end
        if(job.created_at.to_date == date && job.auftragsart_id == 2) then
        jobs_fa += 1
        end
      end
      data << [kw, jobs_ea, jobs_fa]
    end

    return summarize_by_calendar_weeks(data)
  end

  def summarize_by_calendar_weeks(data)
    data2 = []
    data.each do |r|
      kw = r[0]
      ea = r[1]
      fa = r[2]

      if (data2[kw] == nil)
      total_ea = 0
      total_fa = 0
      else
      total_ea = data2[kw][1]+ea
      total_fa = data2[kw][2]+fa
      end
      data2[kw] = ["KW #{kw}",total_ea,total_fa]
    end

    return data2.compact
  end

  # Installationszahlen - Disposition
  def number_of_dispositions
    client = Client.find_by_id(params[:client])
    report = create_number_of_dispositions_report(client,params[:start], params[:end])
  end

  def create_number_of_dispositions_report(client, start_param, end_param)
    report = Ebru::Report.new
    report.client = client

    container1 = Ebru::Container.new()
    container1.display_order = "vertical"
    container1.header_title = "Installationszahlen"
    container1.add(Ebru::StringField.new("Zeitraum:", start_param.to_date.strftime("%d.%m.%Y") +" - "+ end_param.to_date.strftime("%d.%m.%Y")))
    container1.add(Ebru::StringField.new("Erstellt von:", current_user.full_name))
    report.add(container1)

    number_of_dispositions_data = get_number_of_dispositions_data(client,start_param, end_param)
    number_of_dispositions_data_table = number_of_dispositions_data
    kw = []
    di = []

    max_value = [0]
    number_of_dispositions_data.each do |r|
      kw << r[0]
      di << r[1]

      axis_value_di = r[1]
      max_value.push(axis_value_di)
    end
    sum_di = 0
    number_of_dispositions_data_table.each do |row|
      sum_di += row[1]
    end

    cf_dispositions = Ebru::ColumnChart.new({:title => "Dispositionen",
      :data => [di],
      :legend => ["Dispositionen"],
      :bar_width => 30,
      :axis_x => [kw],
      :axis_y => [nil,(0..max_value.max).to_a],
      :chart_width => 930,
      :pdf_height => 500,
    })

    container1.add(cf_dispositions)

    number_of_dispositions_table = Ebru::Table.new({ 
      :data => number_of_dispositions_data, 
      :header =>  ["Wochen","Dispositionen"],
      :emphasized_columns => [0],
      :footer =>["Gesamt",sum_di]
      }) 
    report.add(number_of_dispositions_table)

    report.pdf_path = "#{request.protocol}#{request.host_with_port}#{request.fullpath}".gsub("?",".pdf?")
    report.csv_path = "#{request.protocol}#{request.host_with_port}#{request.fullpath}".gsub("?",".csv?")
    return format_report(report)
  end

  def get_number_of_dispositions_data(client,start_param, end_param)
    data = []
    dispositions = 0
    (start_param.to_date..end_param.to_date).each do |date|
      kw = date.cweek

      dispositions = Appointment.where(:day => date).length

      data << [kw, dispositions]
    end
    return summarize_by_calendar_weeks_di(data)
  end

  def summarize_by_calendar_weeks_di(data)
    data2 = []
    data.each do |r|
      kw = r[0]
      di = r[1]

      if (data2[kw] == nil)
      total_di = 0
      else
      total_di = data2[kw][1]+di
      end
      data2[kw] = ["KW #{kw}",total_di]
    end
    return data2.compact
  end

  def successful_installations
    client = Client.find_by_id(params[:client])
    completions = Completion.joins(:appointment => :job).where("completions.created_at >= ?", params[:start].to_date).where("completions.created_at <= ?", params[:end].to_date).where("jobs.client_id = ?", client.id)
    report = create_successful_installations_report(client,completions,params[:start], params[:end])
  end

  def create_successful_installations_report(client, completions, start_param, end_param)
    report = Ebru::Report.new
    report.client = client

    container1 = Ebru::Container.new()
    container1.display_order = "vertical"
    container1.header_title = "Installationszahlen"
    container1.add(Ebru::StringField.new("Zeitraum:", start_param.to_date.strftime("%d.%m.%Y") +" - "+ end_param.to_date.strftime("%d.%m.%Y")))
    container1.add(Ebru::StringField.new("Erstellt von:", current_user.full_name))
    report.add(container1)

    successful_installations_data = get_successful_installations_data(client, completions, start_param, end_param)
    successful_installations_data_table = successful_installations_data
    kw = []
    completions = []
    max_value = [0]
    successful_installations_data.each do |r|
      kw << r[0]
      completions << r[1]

      axis_value_completions = r[1]
      max_value.push(axis_value_completions)
    end
    sum_completions = 0
    successful_installations_data_table.each do |row|
      sum_completions += row[1]
    end
    
    cf_success_installation = Ebru::ColumnChart.new({:title => "Erfolgreiche Abschlüsse",
      :data => [completions],
      :legend => ["Abschlüsse"],
      :bar_width => 30,
      :axis_x => [kw],
      :axis_y => [nil,(0..max_value.max).to_a],
      :chart_width => 930,
      :pdf_height => 500,
    })
    container1.add(cf_success_installation)

    successful_installations_table = Ebru::Table.new({ 
      :data => successful_installations_data, 
      :header => ["Wochen","Erfolgreiche Abschlüsse"],
      :emphasized_columns => [0],
      :footer => ["Gesamt",sum_completions]
      }) 
    report.add(successful_installations_table)

    report.pdf_path = "#{request.protocol}#{request.host_with_port}#{request.fullpath}".gsub("?",".pdf?")
    report.csv_path = "#{request.protocol}#{request.host_with_port}#{request.fullpath}".gsub("?",".csv?")
    return format_report(report)
  end

  def get_successful_installations_data(client, completions, start_param, end_param)
    data = []
    akt_modem_wert = 0
    (start_param.to_date..end_param.to_date).each do |date|
      kw = date.cweek
      akt_modem= 0
      completions.each do |completion|
        if (completion.created_at.to_date == date) then
        akt_modem += 1
        end
      end
      data << [kw, akt_modem]
    end
    return summarize_by_calendar_weeks_successfull_installations(data)
  end

  def summarize_by_calendar_weeks_successfull_installations(data)
    data2 = []
    data.each do |r|
      kw = r[0]
      completions = r[1]

      if (data2[kw] == nil)
      total_completions = 0
      else
      total_completions = data2[kw][1]+completions
      end
      data2[kw] = ["KW #{kw}",total_completions]
    end
    return data2.compact
  end

  #Performance Aufträge/Termine
  def performance
    client = Client.find_by_id(params[:client])
    jobs = Job.where(:client_id => client).where("created_at >= ?", params[:start].to_date).where("created_at <= ?", params[:end].to_date)
    report = create_performance_report(client,jobs,params[:start], params[:end])
  end

  def create_performance_report(client, jobs, start_param, end_param)
    report = Ebru::Report.new
    report.client = client

    container1 = Ebru::Container.new()
    container1.display_order = "vertical"
    container1.header_title = "Installationszahlen"
    container1.add(Ebru::StringField.new("Zeitraum:", start_param.to_date.strftime("%d.%m.%Y") +" - "+ end_param.to_date.strftime("%d.%m.%Y")))
    container1.add(Ebru::StringField.new("Erstellt von:", current_user.full_name))
    report.add(container1)

    performance_data = get_performance_data(client, jobs, start_param, end_param)
    performance_data_table = performance_data
    kw = []
    jobs = []
    appointments = []
    max_value = [0]
    performance_data.each do |r|
      kw << r[0]
      jobs << r[1]
      appointments << r[2]

      axis_value_jobs = r[1]
      axis_value_appointments = r[2]
      max_value.push(axis_value_jobs, axis_value_appointments)
    end
    sum_jobs = 0
    sum_appointments = 0
    avg_success = 0

    performance_data_table.each do |row|
      sum_jobs += row[1]
      sum_appointments += row[2]
      avg_success += row[3].to_f / row[3].length
    end
    cf_performance = Ebru::ColumnChart.new({:title => "Aufträge/Termine",
      :data =>[jobs, appointments],
      :legend => ["Aufträge","Termine"],
      :bar_width => 20,
      :axis_x => [kw],
      :axis_y => [nil,(0..max_value.max).to_a],
      :chart_width => 930,
      :pdf_height => 500,
    })
    container1.add(cf_performance)

    performance_table = Ebru::Table.new({ 
      :data => performance_data, 
      :header => ["Wochen","Auträge","Termine","Erfolgsquote"],
      :emphasized_columns => [0],
      :footer => ["Gesamt",sum_jobs,sum_appointments,"#{avg_success.round(1)}%"]
      }) 
    report.add(performance_table)

    report.pdf_path = "#{request.protocol}#{request.host_with_port}#{request.fullpath}".gsub("?",".pdf?")
    report.csv_path = "#{request.protocol}#{request.host_with_port}#{request.fullpath}".gsub("?",".csv?")
    return format_report(report)
  end

  def get_performance_data(client, jobs, start_param, end_param)
    data = []
    jobs_by_date = 0
    appointments = 0

    (start_param.to_date..end_param.to_date).each do |date|
      kw = date.cweek
      appointments = Appointment.where(:day => date).length
      jobs.each do |job|
        if (job.created_at.to_date == date) then
        jobs_by_date += 1
        end
      end
      data << [kw, jobs_by_date, appointments]
    end
    return summarize_by_calendar_weeks_performance(data)
  end

  def summarize_by_calendar_weeks_performance(data)
    data2 = []
    data.each do |r|
      kw = r[0]
      jobs = r[1]
      appointments = r[2]

      if (data2[kw] == nil)
      total_jobs = 0
      total_appointments = 0
      else
      total_jobs = data2[kw][1]+ jobs
      total_appointments = data2[kw][1]+ appointments
      end
      performance = (total_appointments.to_f/total_jobs.to_f) *100
      if (performance.nan? == true) then
      performance = 0
      end
      data2[kw] = ["KW #{kw}",total_jobs,total_appointments,"#{performance.round(1)}%"]
    end
    return data2.compact
  end

  #Performance_ Auftragserfolgsquote Aufträge/Completions
  def performance_success_rate
    client = Client.find_by_id(params[:client])
    jobs = Job.where(:client_id => client).where("created_at >= ?", params[:start].to_date).where("created_at <= ?", params[:end].to_date)
    completions = Completion.joins(:appointment => :job).where("completions.created_at >= ?", params[:start].to_date).where("completions.created_at <= ?", params[:end].to_date).where("jobs.client_id = ?", client.id)
    report = create_performance_success_rate_report(client, jobs, completions, params[:start], params[:end])
  end

  def create_performance_success_rate_report(client, jobs, completions, start_param, end_param)
    report = Ebru::Report.new
    report.client = client

    container1 = Ebru::Container.new()
    container1.display_order = "vertical"
    container1.header_title = "Installationszahlen"
    container1.add(Ebru::StringField.new("Zeitraum:", start_param.to_date.strftime("%d.%m.%Y") +" - "+ end_param.to_date.strftime("%d.%m.%Y")))
    container1.add(Ebru::StringField.new("Erstellt von:", current_user.full_name))
    report.add(container1)

    performance_success_rate_data = get_performance_success_rate_data(client, jobs, completions, start_param, end_param)
    performance_success_rate_data_table = performance_success_rate_data
    kw = []
    jobs = []
    completions = []
    max_value = [0]
    performance_success_rate_data.each do |r|
      kw << r[0]
      jobs << r[1]
      completions << r[2]

      axis_value_jobs = r[1]
      axis_value_completions = r[2]
      max_value.push(axis_value_jobs, axis_value_completions)
    end
    sum_jobs = 0
    sum_completions = 0
    avg_success = 0
    performance_success_rate_data_table.each do |row|
      sum_jobs += row[1]
      sum_completions += row[2]
      avg_success += row[3].to_f / row[3].length
    end

    cf_performance = Ebru::ColumnChart.new({:title => "Aufträge/Erfolgreiche Abschlüsse",
      :data => [jobs, completions],
      :legend => ["Aufträge","Erfolgreiche Abschlüsse"],
      :bar_width => 20,
      :axis_x => [kw],
      :axis_y => [nil,(0..max_value.max).to_a],
      :chart_width => 930,
      :pdf_height => 500,
    })
    container1.add(cf_performance)

    performance_success_rate_table = Ebru::Table.new({ 
      :data => performance_success_rate_data, 
      :header => ["Wochen","Auträge","Erfolgreiche Abschlüsse","Erfolgsquote"],
      :emphasized_columns => [0],
      :footer => ["Gesamt",sum_jobs,sum_completions,"#{avg_success.round(1)}%"]
      }) 
    report.add(performance_success_rate_table)

    report.pdf_path = "#{request.protocol}#{request.host_with_port}#{request.fullpath}".gsub("?",".pdf?")
    report.csv_path = "#{request.protocol}#{request.host_with_port}#{request.fullpath}".gsub("?",".csv?")
    return format_report(report)
  end

  def get_performance_success_rate_data(client, jobs, completions, start_param, end_param)
    data = []
    jobs_by_date = 0
    completions_by_date = 0

    (start_param.to_date..end_param.to_date).each do |date|
      kw = date.cweek

      jobs.each do |job|
        if (job.created_at.to_date == date) then
        jobs_by_date += 1
        end
      end
      completions.each do |completion|
        if (completion.created_at.to_date == date) then
        completions_by_date += 1
        end
      end
      data << [kw, jobs_by_date, completions_by_date]
    end
    return summarize_by_calendar_weeks_performance_success_rate(data)
  end

  def summarize_by_calendar_weeks_performance_success_rate(data)
    data2 = []
    data.each do |r|
      kw = r[0]
      jobs = r[1]
      completions = r[2]

      if (data2[kw] == nil)
      total_jobs = 0
      total_completions = 0
      else
      total_jobs = data2[kw][1]+ jobs
      total_completions = data2[kw][1]+ completions
      end
      performance_success_rate = (total_completions.to_f/total_jobs.to_f) *100
      if (performance_success_rate.nan? == true) then
      performance_success_rate = 0
      end
      data2[kw] = ["KW #{kw}",total_jobs,total_completions,"#{performance_success_rate.round(1)}%"]
    end
    return data2.compact
  end
end