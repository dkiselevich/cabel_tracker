# encoding: utf-8
class Management::Reports::TestReportsController < ApplicationController

  def test
    report = nil
    case (params[:tid].to_i)
    when (1)
        report = get_test_report1()
    when (2)
        report = get_test_report2()
    when (3)
        report = get_test_report3()
    when (4)
        #render with template
        @report = get_test_report1()
        render :template => "/management/reports/test_reports/test4", :layout => "reports"
        return
    when (5)
        report = get_test_report5()
    when (6)
        report = get_test_report6()
    when (7)
        report = get_test_report7()
    end
    format_report(report)
  end
  

  def get_test_report1
    report = Ebru::Report.new("Testreport Nr 1")
    report.add(Ebru::StringField.new("Hello","World"))
    report.add(Ebru::Component.new)
    report.add(Ebru::ImageField.new("Testkatze","http://placekitten.com/g/260/200",260,200))

    table = Ebru::Table.new()
    table.header = ("a".."j").to_a
    table.data = [(1..10).to_a, (11..20).to_a, [6,3,8,5,1,4,3,4,7,8]]
    table.footer = ("a".."j").to_a
    table.emphasized_columns = [1,2,3]
    report.add(table)

    return report
  end

  def get_test_report2
    report = Ebru::Report.new("Testreport Nummer 2")
    #report.add(Ebru::StringField.new({:label => "Hello", :value => "World"}))
    report.add(Ebru::StringField.new("Hello","World"))
    report.add(Ebru::Component.new)
    report.add(Ebru::ImageField.new("Testkatze is watching you","http://placekitten.com/g/940/200",940,200))

    #table api 1
    table = Ebru::Table.new()
    table.header = (1..50).to_a
    table.data = [(1..50).to_a, (101..150).to_a]
    table.footer = (201..250).to_a
    table.max_width = 500
    report.add(table)

    #table api 2
    table = Ebru::Table.new({ 
      :data => [(1..50).to_a, (101..150).to_a], 
      :header => (1..50).to_a, 
      :footer => (201..250).to_a,
      :emphasized_columns => [2,5,8,10],
      :max_width => 500,
      :styles => { 
        :table_header_background_color => "blue",
        :table_header_color => "red"
        }
      })
    report.add(table)

    return report
  end

  def get_test_report3
    report = Ebru::Report.new
    report.add(Ebru::ImageField.new("Testkatze is watching you","http://placekitten.com/g/940/200",940,200))

    #tabellen im container
    container = Ebru::Container.new
    container.header_title = "Testtabellen"
    container.children_styles << ["margin-right","10px"]
    report.add(container)

    table = Ebru::Table.new({ 
      :data => [(1..50).to_a, (101..150).to_a, (101..150).to_a, (101..150).to_a, (101..150).to_a, (101..150).to_a, (101..150).to_a, (101..150).to_a, (101..150).to_a, (101..150).to_a], 
      :header => (1..50).to_a, 
      :footer => (201..250).to_a,
      :emphasized_columns => [2,5,8,10],
      :max_width => 460,
      :styles => { 
        :table_header_background_color => "blue",
        :table_header_color => "red"
        }
      })
    table.styles << ["width", "460px"]
    container.add(table)

    table = Ebru::Table.new({ 
      :data => [(1..50).to_a, (101..150).to_a, (101..150).to_a, (101..150).to_a, (101..150).to_a, (101..150).to_a, (101..150).to_a, (101..150).to_a, (101..150).to_a, (101..150).to_a], 
      :header => (1..50).to_a, 
      :footer => (201..250).to_a,
      :emphasized_columns => [2,5,8,10],
      :max_width => 460,
      :styles => { 
        :table_header_background_color => "blue",
        :table_header_color => "red"
        }
      })
    table.styles << ["width", "460px"]
    container.add(table)

    
    #bilder im container
    container = Ebru::Container.new
    container.header_title = "Testbilder"
    report.add(container)

    container.add(Ebru::ImageField.new("","http://placekitten.com/g/305/200",305,200))
    container.add(Ebru::ImageField.new("","http://placekitten.com/g/315/200",315,200))
    container.add(Ebru::ImageField.new("","http://placekitten.com/g/300/200",300,200))

    #charts im container
    container = Ebru::Container.new
    container.header_title = "Testcharts"
    report.add(container)
    
        
    cf = Ebru::ChartField.new({
       :type => 'column',
       :title => 'Balken vertikal', 
       :data => [(1..5).to_a, (11..15).to_a], 
       :legend => ["legend1","legend2"], 
       :bar_width => 15, 
       :axis_x => (1..5).to_a, 
       :axis_y => (1..15), 
       :chart_width => 300, 
       :pdf_height => 200})
    container.add(cf)
    
    cf = Ebru::PieChart.new({
      :type => 'pie',
      :title => 'Kuchen', 
      :data => [10,20,100,1,1], 
      :legend => ["Tobse","Frank","James","ad","dadss"], 
      :axis_x => (1..5).to_a, 
      :axis_y => (1..15), 
      :chart_width => 300, 
      :pdf_height => 200})
    container.add(cf)    
    
    cf = Ebru::ChartField.new({
      :type => 'line',
      :title => 'Linien', 
      :data => [(1..5).to_a, (11..15).to_a], 
      :legend => ["legend1","legend2"], 
      :bar_width => 15, 
      :axis_x => (1..5).to_a, 
      :axis_y => (1..15), 
      :chart_width => 300, 
      :pdf_height => 200})
    container.add(cf)
 
    cf = Ebru::ChartField.new({
      :type => 'column',
      :title => 'Balken vertikal', 
      :data => [(1..5).to_a, (11..15).to_a], 
      :legend => ["legend1","legend2"], 
      :bar_width => 15, 
      :axis_x => (1..5).to_a, 
      :axis_y => (1..15), 
      :chart_width => 460, 
      :pdf_height => 200})
    container.add(cf)    

    cf = Ebru::ChartField.new({
      :type => 'bar',
      :title => 'Balken horizontal', 
      :data => [(1..5).to_a, (11..15).to_a], 
      :legend => ["legend1","legend2"], 
      :bar_width => 15, 
      :axis_x => (1..5).to_a, 
      :axis_y => (1..15), 
      :chart_width => 460, 
      :pdf_height => 200})
    container.add(cf)    

    

    #linkbündig
    container1 = Ebru::Container.new
    container1.header_title = "Containertest - horizontal - linksbündig"
    report.add(container1)
    container1.children_styles << ["margin","5px"]
    container1.children_styles << ["padding","5px"]
    container1.children_styles << ["border","1px solid #aaa"]
    container1.children_styles << ["margin-right","15px"]
    container1.children_styles << ["height","200px"]
    container1.children_styles << ["background-color","#eee"]

    container1.add(Ebru::StringField.new("dasdsa","asdasd"))

    tf = Ebru::TextField.new("Lorem","Lorem ipsum exercitation consequat in elit eiusmod dolore magna tempor fugiat ut id est aliqua dolor. ")
    tf.styles << ["width","200px"]
    container1.add(tf)

    container1.add(Ebru::ImageField.new("","http://placekitten.com/g/300/200",300,200))


    #block
    container1 = Ebru::Container.new
    container1.header_title = "Containertest - horizontal - block"
    report.add(container1)
    container1.children_styles << ["border","1px solid #aaa"]
    container1.children_styles << ["margin-right","0px"]
    container1.children_styles << ["padding","10px"]
    container1.children_styles << ["height","200px"]
    container1.children_styles << ["width","30%"]
    container1.children_styles << ["background-color","#eee"]

    tf = Ebru::TextField.new("Lorem","Lorem ipsum exercitation consequat in elit eiusmod dolore magna tempor fugiat ut id est aliqua dolor. ")
    container1.add(tf)
    tf = Ebru::TextField.new("Lorem","Lorem ipsum exercitation consequat in elit eiusmod dolore magna tempor fugiat ut id est aliqua dolor. ")
    container1.add(tf)
    tf = Ebru::TextField.new("Lorem","Lorem ipsum exercitation consequat in elit eiusmod dolore magna tempor fugiat ut id est aliqua dolor. ")
    container1.add(tf)
    

    #container in container
    container1 = Ebru::Container.new
    container1.header_title = "Hauptcontainer - mit Untercontainer blockmässig"
    container1.display_order = "horizontal"
    container1.children_styles << ["width","400px"]
    container1.children_styles << ["border","1px solid #aaa"]
    container1.children_styles << ["background-color","#aff"]
    report.add(container1)

    container2 = Ebru::Container.new
    container2.header_title = "Untercontainer 1"
    container1.add(container2)

    container3 = Ebru::Container.new
    container3.header_title = "Untercontainer 2"
    container1.add(container3)


    #container in container
    container1 = Ebru::Container.new
    container1.header_title = "Hauptcontainer - Untercontainer mit Einzebreiten"
    container1.display_order = "horizontal"
    container1.children_styles << ["border","1px solid #aaa"]
    container1.children_styles << ["background-color","#faf"]
    report.add(container1)

    container2 = Ebru::Container.new
    container2.header_title = "Untercontainer 1"
    container2.styles << ["width","300px"]
    container1.add(container2)

    container3 = Ebru::Container.new
    container3.header_title = "Untercontainer 2"
    container3.styles << ["width","600px"]
    container1.add(container3)



    #container in container
    container1 = Ebru::Container.new
    container1.header_title = "Mehrfachverschachtelung"
    container1.display_order = "horizontal"
    container1.children_styles << ["border","1px solid #aaa"]
    container1.children_styles << ["background-color","#faf"]
    report.add(container1)

    container2 = Ebru::Container.new
    container2.header_title = "UC 1"
    container2.styles << ["width","300px"]
    container1.add(container2)

    container3 = Ebru::Container.new
    container3.header_title = "UC 2"
    container3.styles << ["width","600px"]
    container3.children_styles << ["background-color","#ffa"]
    container1.add(container3)

    container4 = Ebru::Container.new
    container4.header_title = "C"
    container4.styles << ["width","180px"]
    container3.add(container4)

    container4 = Ebru::Container.new
    container4.header_title = "C"
    container4.styles << ["width","180px"]
    container3.add(container4)

    container4 = Ebru::Container.new
    container4.header_title = "C"
    container4.styles << ["width","180px"]
    container3.children_styles << ["padding","5px"]
    container3.add(container4)

    container4.add(Ebru::ImageField.new("","http://placekitten.com/g/150/100",150,100))
    container4.add(Ebru::ImageField.new("","http://placekitten.com/g/150/100",150,100))
    container4.add(Ebru::ImageField.new("","http://placekitten.com/g/150/100",150,100))

    return report
  end

  def get_test_report5
    report = Ebru::Report.new("Testreport Nr 1")
    

    container1 = Ebru::Container.new
    container1.header_title = "Hauptcontainer"
    container1.display_order = "horizontal"
    container1.children_styles << ["border","1px solid #aaa"]
    container1.children_styles << ["background-color","#faf"]
    report.add(container1)

    container1.add(Ebru::StringField.new("Hello","World"))
    container1.add(Ebru::Component.new)
    container1.add(Ebru::ImageField.new("Testkatze","http://placekitten.com/g/260/200",260,200))

    table = Ebru::Table.new()
    table.header = ("a".."j").to_a
    table.data = [(1..10).to_a, (11..20).to_a, [6,3,8,5,1,4,3,4,7,8]]
    table.footer = ("a".."j").to_a
    table.emphasized_columns = [1,2,3]
    container1.add(table)

    cf = Ebru::ChartField.new('column','Balken vertikal', [(1..5).to_a, (11..15).to_a], ["legend1","legend2"], 15, (1..5).to_a, (1..15), 300, 200)
    container1.add(cf)
    cf = Ebru::PieChartField.new('pie','Kuchen', (1..5).to_a, (1..5).to_a, 15, (1..5).to_a, (1..15), 300, 200)
    container1.add(cf)
    cf = Ebru::ChartField.new('line','Linien', [(1..5).to_a, (11..15).to_a], ["legend1","legend2"], 15, (1..5).to_a, (1..15), 300, 200)
    container1.add(cf)
    
    cf = Ebru::ChartField.new('column','Balken vertikal', [(1..5).to_a, (11..15).to_a], ["legend1","legend2"], 15, (1..5).to_a, (1..15), 460, 200)
    container1.add(cf)
    cf = Ebru::ChartField.new('bar','Balken horizontal', [(1..5).to_a, (11..15).to_a], ["legend1","legend2"], 15, (1..5).to_a, (1..15), 460, 200)
    container1.add(cf)
    
    container2 = Ebru::Container.new
    container2.header_title = "Untercontainer 1"
    container2.styles << ["width","300px"]
    container1.add(container2)

    container3 = Ebru::Container.new
    container3.header_title = "Untercontainer 2"
    container3.styles << ["width","600px"]
    container1.add(container3)

    return report
  end

  def get_test_report6
    report = Ebru::Report.new("Testreport Nr 6")
    report.add(Ebru::ImageField.new("Testkatze is watching you","http://placekitten.com/g/940/200",940,200))

    #tabellen im container
    container = Ebru::Container.new
    container.header_title = "Testtabellen"
    container.children_styles << ["margin-right","10px"]
    report.add(container)

    table = Ebru::Table.new({ 
      :data => [(1..10).to_a, (101..110).to_a, (101..110).to_a, (101..110).to_a, (101..110).to_a, (101..110).to_a, (101..110).to_a, (101..110).to_a, (101..110).to_a, (101..110).to_a], 
      :header => (1..10).to_a, 
      :footer => (201..210).to_a,
      :emphasized_columns => [2,5,8,10],
      :max_width => 460,
      :styles => { 
        :table_header_background_color => "blue",
        :table_header_color => "red"
        }
      })
    table.styles << ["width", "460px"]
    container.add(table)

    table = Ebru::Table.new({ 
      :data => [(1..10).to_a, (101..110).to_a, (101..110).to_a, (101..110).to_a, (101..110).to_a, (101..110).to_a, (101..110).to_a, (101..110).to_a, (101..110).to_a, (101..110).to_a], 
      :header => (1..10).to_a, 
      :footer => (201..210).to_a,
      :emphasized_columns => [2,5,8,10],
      :max_width => 460,
      :styles => { 
        :table_header_background_color => "blue",
        :table_header_color => "red"
        }
      })
    table.styles << ["width", "460px"]
    container.add(table)

    
    #bilder im container
    container = Ebru::Container.new
    container.header_title = "Testbilder"
    report.add(container)

    container.add(Ebru::ImageField.new("","http://placekitten.com/g/305/200",305,200))
    container.add(Ebru::ImageField.new("","http://placekitten.com/g/315/200",315,200))
    container.add(Ebru::ImageField.new("","http://placekitten.com/g/300/200",300,200))

    #linkbündig
    container1 = Ebru::Container.new
    container1.header_title = "Containertest - horizontal - linksbündig"
    report.add(container1)
    container1.children_styles << ["margin","5px"]
    container1.children_styles << ["padding","5px"]
    container1.children_styles << ["border","1px solid #aaa"]
    container1.children_styles << ["margin-right","15px"]
    container1.children_styles << ["height","200px"]
    container1.children_styles << ["background-color","#eee"]

    container1.add(Ebru::StringField.new("dasdsa","asdasd"))

    tf = Ebru::TextField.new("Lorem","Lorem ipsum exercitation consequat in elit eiusmod dolore magna tempor fugiat ut id est aliqua dolor. ")
    tf.styles << ["width","200px"]
    container1.add(tf)

    container1.add(Ebru::ImageField.new("","http://placekitten.com/g/300/200",300,200))


    #block
    container1 = Ebru::Container.new
    #container1.page_break = true
    container1.header_title = "Containertest - horizontal - block"
    report.add(container1)
    container1.children_styles << ["border","1px solid #aaa"]
    container1.children_styles << ["margin-right","0px"]
    container1.children_styles << ["padding","10px"]
    container1.children_styles << ["height","200px"]
    container1.children_styles << ["width","30%"]
    container1.children_styles << ["background-color","#eee"]

    tf = Ebru::TextField.new("Lorem","Lorem ipsum exercitation consequat in elit eiusmod dolore magna tempor fugiat ut id est aliqua dolor. ")
    container1.add(tf)
    tf = Ebru::TextField.new("Lorem","Lorem ipsum exercitation consequat in elit eiusmod dolore magna tempor fugiat ut id est aliqua dolor. ")
    container1.add(tf)
    tf = Ebru::TextField.new("Lorem","Lorem ipsum exercitation consequat in elit eiusmod dolore magna tempor fugiat ut id est aliqua dolor. ")
    container1.add(tf)
    

    #container in container
    container1 = Ebru::Container.new
    container1.header_title = "Hauptcontainer - mit Untercontainer blockmässig"
    container1.display_order = "horizontal"
    container1.children_styles << ["width","400px"]
    container1.children_styles << ["border","1px solid #aaa"]
    container1.children_styles << ["background-color","#aff"]
    report.add(container1)

    container2 = Ebru::Container.new
    container2.header_title = "Untercontainer 1"
    container1.add(container2)

    container3 = Ebru::Container.new
    container3.header_title = "Untercontainer 2"
    container1.add(container3)


    #container in container
    container1 = Ebru::Container.new
    container1.header_title = "Hauptcontainer - Untercontainer mit Einzebreiten"
    container1.display_order = "horizontal"
    container1.children_styles << ["border","1px solid #aaa"]
    container1.children_styles << ["background-color","#faf"]
    report.add(container1)

    container2 = Ebru::Container.new
    container2.header_title = "Untercontainer 1"
    container2.styles << ["width","300px"]
    container1.add(container2)

    container3 = Ebru::Container.new
    container3.header_title = "Untercontainer 2"
    container3.styles << ["width","600px"]
    container1.add(container3)



    #container in container
    container1 = Ebru::Container.new
    container1.header_title = "Mehrfachverschachtelung"
    container1.display_order = "horizontal"
    container1.children_styles << ["border","1px solid #aaa"]
    container1.children_styles << ["background-color","#faf"]
    report.add(container1)

    container2 = Ebru::Container.new
    container2.header_title = "UC 1"
    container2.styles << ["width","300px"]
    container1.add(container2)

    container3 = Ebru::Container.new
    container3.header_title = "UC 2"
    container3.styles << ["width","600px"]
    container3.children_styles << ["background-color","#ffa"]
    container1.add(container3)

    container4 = Ebru::Container.new
    container4.header_title = "C"
    container4.styles << ["width","180px"]
    container3.add(container4)

    container4 = Ebru::Container.new
    container4.header_title = "C"
    container4.styles << ["width","180px"]
    container3.add(container4)

    container4 = Ebru::Container.new
    container4.header_title = "C"
    container4.styles << ["width","180px"]
    container3.children_styles << ["padding","5px"]
    container3.add(container4)

    container4.add(Ebru::ImageField.new("","http://placekitten.com/g/150/100",150,100))
    container4.add(Ebru::ImageField.new("","http://placekitten.com/g/150/100",150,100))
    container4.add(Ebru::ImageField.new("","http://placekitten.com/g/150/100",150,100))




    table = Ebru::Table.new({ 
      :data => [(1..10).to_a, 
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a
                ], 
      :header => (1..10).to_a, 
      :footer => (201..210).to_a,
      :emphasized_columns => [2,5,8,10],
      :styles => { 
        :table_header_background_color => "blue",
        :table_header_color => "red"
        }
      })
    table.styles << ["width", "860px"]
    table.page_break = true
    report.add(table)


    table = Ebru::Table.new({ 
      :data => [(1..10).to_a, 
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a
                ], 
      :header => (1..10).to_a, 
      :footer => (201..210).to_a,
      :emphasized_columns => [2,5,8,10],
      :styles => { 
        :table_header_background_color => "blue",
        :table_header_color => "red"
        }
      })
    table.styles << ["width", "860px"]
    table.page_break = true
    report.add(table)

    return report
  end

  def get_test_report7
    report = Ebru::Report.new("Testreport Nr 7")
    #report.add(Ebru::ImageField.new("Testkatze is watching you","http://placekitten.com/g/940/1300",940,1300))
    

    table = Ebru::Table.new({ 
      :data => [(1..10).to_a, 
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a
                ], 
      :header => (1..10).to_a, 
      :footer => (201..210).to_a,
      :emphasized_columns => [2,5,8,10],
      :styles => { 
        :table_header_background_color => "blue",
        :table_header_color => "red"
        }
      })
    table.title = "Tabelle 1"
    table.styles << ["width", "860px"]
    report.add(table)    

    image = Ebru::ImageField.new("Testkatze is watching you","http://placekitten.com/g/940/100",940,100)
    image.width = 940
    image.height = 405
    report.add(image)
    
    sf_number = 0
    sf = Ebru::StringField.new(sf_number += 1,"Lorem ipsum commodo velit dolor magna. ")
    report.add(sf)
    sf = Ebru::StringField.new(sf_number += 1,"Lorem ipsum cupidatat nostrud id aliqua irure. ")
    report.add(sf)
    sf = Ebru::StringField.new(sf_number += 1,"Lorem ipsum nostrud in fugiat sunt dolor. ")
    report.add(sf)
    sf = Ebru::StringField.new(sf_number += 1,"Lorem ipsum et reprehenderit cupidatat in. ")
    report.add(sf)
    sf = Ebru::StringField.new(sf_number += 1,"Lorem ipsum dolore aliqua sed cupidatat. ")
    report.add(sf)
    sf = Ebru::StringField.new(sf_number += 1,"Lorem ipsum ea culpa Ut in. ")
    report.add(sf)
    sf = Ebru::StringField.new(sf_number += 1,"Lorem ipsum do eu sit in minim est magna. ")
    report.add(sf)
    sf = Ebru::StringField.new(sf_number += 1,"Lorem ipsum cupidatat mollit magna dolore consectetur dolore. ")
    report.add(sf)
    sf = Ebru::StringField.new(sf_number += 1,"Lorem ipsum et commodo sed consequat et nulla sit. ")
    report.add(sf)
    
    table = Ebru::Table.new({ 
      :data => [(1..10).to_a, 
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a
                ], 
      :header => (1..10).to_a, 
      :footer => (201..210).to_a,
      :emphasized_columns => [2,5,8,10],
      :styles => { 
        :table_header_background_color => "blue",
        :table_header_color => "red"
        }
      })
    table.title = "Tabelle 2"
    table.styles << ["width", "860px"]
    report.add(table)    

    table = Ebru::Table.new({ 
      :data => [(1..10).to_a, 
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a
                ], 
      :header => (1..10).to_a, 
      :footer => (201..210).to_a,
      :emphasized_columns => [2,5,8,10],
      :styles => { 
        :table_header_background_color => "blue",
        :table_header_color => "red"
        }
      })
    table.title = "Tabelle 3"
    table.styles << ["width", "860px"]
    report.add(table)    

    table = Ebru::Table.new({ 
      :data => [(1..10).to_a, 
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a
                ], 
      :header => (1..10).to_a, 
      :footer => (201..210).to_a,
      :emphasized_columns => [2,5,8,10],
      :styles => { 
        :table_header_background_color => "blue",
        :table_header_color => "red"
        }
      })
    table.title = "Tabelle 4"
    table.styles << ["width", "860px"]
    report.add(table)    

    table = Ebru::Table.new({ 
      :data => [(1..10).to_a, 
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a
                ], 
      :header => (1..10).to_a, 
      :footer => (201..210).to_a,
      :emphasized_columns => [2,5,8,10],
      :styles => { 
        :table_header_background_color => "blue",
        :table_header_color => "red"
        }
      })
    table.title = "Tabelle 5"
    table.styles << ["width", "860px"]
    report.add(table)    

    table = Ebru::Table.new({ 
      :data => [(1..10).to_a, 
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a,
                (101..110).to_a
                ], 
      :header => (1..10).to_a, 
      :footer => (201..210).to_a,
      :emphasized_columns => [2,5,8,10],
      :styles => { 
        :table_header_background_color => "blue",
        :table_header_color => "red"
        }
      })
    table.title = "Tabelle 6"
    table.styles << ["width", "860px"]
    report.add(table)

    report.auto_page_break = true
    report.inter_table_break = true

    return report
  end
end