# encoding: utf-8
class Management::Reports::CrpReportsController < ApplicationController
  
  def capacity_requirements_planning
    client = Client.find_by_id(params[:client])
    report = create_capacity_requirements_planning_report(client, params[:period], params[:start], params[:end])
  end

  def get_crp_table_data(users, start_date, end_date, period)

    data = []
    start_dates = []
    end_dates = []

    (1..12).each do |n|
      start_dates[n] = "01-#{n}-#{start_date.year}".to_date
      end_dates[n] = start_dates[n].end_of_month
    end

    total_actual_working_hours  = 0
    total_possible_appointments = 0

    users.each do |user|
      if (period == 'week')
        data << [ user.nachname,
          user.working_hours_monday,
          user.working_hours_tuesday,
          user.working_hours_wednesday,
          user.working_hours_thursday,
          user.working_hours_friday,
          user.working_hours_saturday,
          user.working_hours_week,
          user.get_total_days_of_absence(start_date, end_date).length,
          user.actual_working_hours_as_string(start_date, end_date),
          user.possible_appointments]

      end
      if (period == 'month')
        data << [ user.nachname,
          user.working_hours_week,
          user.working_hours_month,
          user.get_total_days_of_absence(start_date, end_date).length,
          user.actual_working_hours_as_string(start_date, end_date),
          user.possible_appointments]
      end
      if (period == 'quarter')
        if(start_date.strftime("%m-%d") == "01-01")
          get_total_days_of_absence =  [user.get_total_days_of_absence(start_dates[1], end_dates[1]).length,
            user.get_total_days_of_absence(start_dates[2], end_dates[2]).length,
            user.get_total_days_of_absence(start_dates[3], end_dates[3]).length
          ]
        end

        if(start_date.strftime("%m-%d") == "04-01")
          get_total_days_of_absence =  [user.get_total_days_of_absence(start_dates[4], end_dates[4]).length,
            user.get_total_days_of_absence(start_dates[5], end_dates[5]).length ,
            user.get_total_days_of_absence(start_dates[6], end_dates[6]).length
          ]
        end
        if(start_date.strftime("%m-%d") == "07-01")
          get_total_days_of_absence =  [user.get_total_days_of_absence(start_dates[7], end_dates[7]).length,
            user.get_total_days_of_absence(start_dates[8], end_dates[8]).length,
            user.get_total_days_of_absence(start_dates[9], end_dates[9]).length
          ]
        end
        if(start_date.strftime("%m-%d") == "10-01")
          get_total_days_of_absence =  [user.get_total_days_of_absence(start_dates[10], end_dates[10]).length,
            user.get_total_days_of_absence(start_dates[11], end_dates[11]).length,
            user.get_total_days_of_absence(start_dates[12], end_dates[12]).length
          ]
        end

        data << [ user.nachname,
          user.working_hours_week,
          user.working_hours_month,
          get_total_days_of_absence[0],
          get_total_days_of_absence[1],
          get_total_days_of_absence[2],
          user.get_total_days_of_absence(start_date, end_date).length,
          user.actual_working_hours_as_string(start_date, end_date),
          user.possible_appointments]

      end
      if (period == 'year')

        get_total_days_of_absence =  [user.get_total_days_of_absence(start_dates[1], end_dates[1]).length,
          user.get_total_days_of_absence(start_dates[2], end_dates[2]).length,
          user.get_total_days_of_absence(start_dates[3], end_dates[3]).length,
          user.get_total_days_of_absence(start_dates[4], end_dates[4]).length,
          user.get_total_days_of_absence(start_dates[5], end_dates[5]).length,
          user.get_total_days_of_absence(start_dates[6], end_dates[6]).length,
          user.get_total_days_of_absence(start_dates[7], end_dates[7]).length,
          user.get_total_days_of_absence(start_dates[8], end_dates[8]).length,
          user.get_total_days_of_absence(start_dates[9], end_dates[9]).length,
          user.get_total_days_of_absence(start_dates[10], end_dates[10]).length,
          user.get_total_days_of_absence(start_dates[11], end_dates[11]).length,
          user.get_total_days_of_absence(start_dates[12], end_dates[12]).length
        ]
        username = Ebru::LinkField.new(user.nachname, management_service_user_path(user.id))
        data << [ username,
          user.working_hours_week,
          user.working_hours_month,
          get_total_days_of_absence[0],
          get_total_days_of_absence[1],
          get_total_days_of_absence[2],
          get_total_days_of_absence[3],
          get_total_days_of_absence[4],
          get_total_days_of_absence[5],
          get_total_days_of_absence[6],
          get_total_days_of_absence[7],
          get_total_days_of_absence[8],
          get_total_days_of_absence[9],
          get_total_days_of_absence[10],
          get_total_days_of_absence[11],
          user.get_total_days_of_absence(start_date, end_date).length,
          user.actual_working_hours_as_string(start_date, end_date),
          max_value = user.possible_appointments]
      end
      total_actual_working_hours  += user.actual_working_hours_as_string(start_date, end_date)
      total_possible_appointments += user.possible_appointments
    end
    footer = []
    header = []
    if (period == 'week') then
      header = ["Name", "Mo", "Di", "Mi", "Do", "Fr", "Sa", "Std./Woche", "Fehltage", "tatsächliche Arbeitszeit/Std.", "mögliche Termine"]
      footer = ["GESAMT","","","","","","","","",total_actual_working_hours,total_possible_appointments]
    end
    if (period == 'month') then
      header = ["Name","Std./Woche", "Std./Monat", "Fehltage", "tatsächliche Arbeitszeit/Std.", "mögliche Termine"]
      footer = ["GESAMT","","","",total_actual_working_hours,total_possible_appointments]
    end
    if (period == 'quarter') then
       if (start_date.strftime("%m-%d") == "01-01")
        month_name = ["Jan","Feb","März"]
      end
      if (start_date.strftime("%m-%d") == "04-01")
        month_name = ["April","Mai","Juni"]
      end
      if (start_date.strftime("%m-%d") == "07-01")
        month_name = ["Juli","Aug","Sept"]
      end
      if (start_date.strftime("%m-%d") == "10-01")
        month_name = ["Okt","Nov","Dez"]
      end      
      header = ["Name", "Std./Woche", "Std./Monat", month_name[0], month_name[1], month_name[2], "Fehltage", "tatsächliche Arbeitszeit/Std.", "mögliche Termine"]
      footer = ["GESAMT","","","","","","",total_actual_working_hours,total_possible_appointments]
    end
    if (period == 'year') then
      header = ["Name", "Std./Woche", "Std./Monat","Jan","Feb","März","April","Mai","Juni","Juli","Aug","Sept","Okt","Nov","Dez", "Fehltage", "tatsächliche Arbeitszeit/Std.", "mögliche Termine"]
      footer = ["GESAMT","","","","","","","","","","","","","","","",total_actual_working_hours,total_possible_appointments]
    end
    return data, header, footer
  end

  def create_capacity_requirements_planning_report(client, period, start_param, end_param)
    @possible_appointments = []
    @max_value = []
    if start_param == nil
      start_date = Date.today.beginning_of_month
    else
    start_date = start_param.to_date
    end

    if end_param == nil
      end_date = Date.today.end_of_month
    else
    end_date = end_param.to_date
    end

    report_title = "Wochenbericht" if (period == "week")
    report_title = "Monatsbericht" if (period == "month")
    report_title = "Quartalsbericht" if (period == "quarter")
    report_title = "Jahresbericht" if (period == "year")

    start_dates = []
    end_dates = []

    (1..12).each do |n|
      start_dates[n] = "01-#{n}-#{start_date.year}".to_date
      end_dates[n] = start_dates[n].end_of_month
    end

    report = Ebru::Report.new
    report.client = client
    container1 = Ebru::Container.new
    container1.display_order = "vertical"
    container1.header_title = "Kapazitätsbedarfsplanung: "+report_title
    report.add(container1)

    @users = User.where(:is_technician => true).where(:active => true).assigned_to_company(report.client)
    container1.add(Ebru::StringField.new("Zeitraum:", start_date.strftime("%d.%m.%Y") +" - "+ end_date.strftime("%d.%m.%Y")))
    container1.add(Ebru::StringField.new("Anzahl aktiver Techniker:", @users.count))

    table_year = Ebru::Table.new
    table_year.emphasized_columns = [0]
    table_year.data, table_year.header, table_year.footer = get_crp_table_data(@users, start_date, end_date, params[:period])
    report.add(table_year)
    
    report.pdf_path = "#{request.protocol}#{request.host_with_port}#{request.fullpath}".gsub("?",".pdf?")
    report.csv_path = "#{request.protocol}#{request.host_with_port}#{request.fullpath}".gsub("?",".csv?")
    return format_report(report)
  end
end