class Management::ServiceUsersController < InheritedResources::Base
  actions :index, :show 

  def index
    @search = User.search(params[:q])
    @service_users = @search.result.service.assigned_to_same_company(current_user).paginate :per_page => Settings.DEFAULT_ITEMS_PER_TABLE, :page => params[:page]
  end

  def show
    @service_user = User.service.assigned_to_same_company(current_user).where(:active => true).find(params[:id])
#    get_quotes_for_user(@service_user)
  end

  def appointments
    @service_user = User.service.assigned_to_same_company(current_user).where(:active => true).find(params[:id])
  end

  def appointments_pop_up
    @service_user = User.service.assigned_to_same_company(current_user).where(:active => true).find(params[:id])
    render :layout => "pop_up"
  end

  def appointments_data
    @service_user = User.service.assigned_to_same_company(current_user).where(:active => true).find(params[:id])
    render :layout => false
  end
end
