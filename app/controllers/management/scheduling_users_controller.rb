class Management::SchedulingUsersController < InheritedResources::Base
  actions :index, :show 

  def index
    @search = User.search(params[:q])
    @scheduling_users = @search.result.scheduling.assigned_to_same_company(current_user).where(:active => true).paginate :per_page => Settings.DEFAULT_ITEMS_PER_TABLE, :page => params[:page]
  end

  def show
    @scheduling_user = User.scheduling.assigned_to_same_company(current_user).where(:active => true).find(params[:id])
    @appointments = @scheduling_user.appointments.where(:status => 0).paginate :per_page => Settings.DEFAULT_ITEMS_PER_TABLE, :page => params[:page]
#    get_quotes_for_user(@scheduling_user)
  end

end
