class Management::BaseController < ApplicationController
	def index
		cookies[:last_view] = "management"
		session[:mobile_param] = nil
	end

	def jobs_per_period
		params[:period] ||= Settings.DEFAULT_PERIOD_FOR_CHARTS
		@jobs_per_period = StatisticsHelper.get_jobs_per_period(:period => params[:period], :clients => current_user.clients)

    @frame_width = params[:frameWidth] ? params[:frameWidth].to_i : 450
		@is_pop_up = true if params[:popup]
		if params[:popup] then
			@chart_width = "100%" 
		else
			@chart_width = (60*@jobs_per_period.size)
			border = 15
			@chart_width = @frame_width - border if (@chart_width < (@frame_width - border))
			@chart_width = @chart_width.to_s+"px"
		end
		render :template => 'general/jobs_per_period', :layout => "chart" 
	end 

	def appointments_per_period
		params[:period] ||= Settings.DEFAULT_PERIOD_FOR_CHARTS
		@frame_width = params[:frameWidth] ? params[:frameWidth].to_i : 450
		@is_pop_up = true if params[:popup]
		@jobs_per_period = StatisticsHelper.get_appointments_per_period(:period => params[:period], :technician => current_user.id, :clients => current_user.clients)
		render :template => 'general/jobs_per_period', :layout => "chart" 
	end 

	def quotes_per_job_type
		params[:period] ||= Settings.DEFAULT_PERIOD_FOR_CHARTS
		@frame_width = params[:frameWidth] ? params[:frameWidth].to_i : 450
		@is_pop_up = true if params[:popup]
		@quotes_per_job_type = StatisticsHelper.get_quotes_per_job_type(:period => params[:period], :clients => current_user.clients)
		@chart_width = "100%" 
		render :template => 'general/quotes_per_job_type', :layout => "chart" 
	end 

	def quotes_per_technician
		params[:period] ||= Settings.DEFAULT_PERIOD_FOR_CHARTS
		@frame_width = params[:frameWidth] ? params[:frameWidth].to_i : 450
		if (params[:technicians]) then
			if params[:technicians] == "ALL" then
				technicians ||= User.assigned_to_same_company(current_user).service.where(:active => true)
			else
				technician_id = params[:technicians]
				technicians ||= User.assigned_to_same_company(current_user).service.where(:active => true).where(:id => technician_id)
			end
		end
		technicians ||= User.assigned_to_same_company(current_user).service.where(:active => true)
		@is_pop_up = true if params[:popup]
		@user_selection = false if (params[:userSelection] == "false")
		@quotes_per_technician = StatisticsHelper.get_quotes_per_technician(:period => params[:period], :technicians => technicians, :clients => current_user.clients)
		if params[:popup] then
			@chart_width = "100%" 
		else
			@chart_width = (60*@quotes_per_technician.size)
			border = 15
			@chart_width = @frame_width - border if (@chart_width < (@frame_width - border))
			@chart_width = @chart_width.to_s+"px"
		end
		render :template => 'general/quotes_per_technician', :layout => "chart" 
	end 

	def quotes_per_controller
		params[:period] ||= Settings.DEFAULT_PERIOD_FOR_CHARTS
		@frame_width = params[:frameWidth] ? params[:frameWidth].to_i : 450
		if (params[:controllers]) then
			if params[:controllers] == "ALL" then
				controllers ||= User.assigned_to_same_company(current_user).scheduling.where(:active => true)
			else
				controller_id = params[:controllers]
				controllers ||= User.assigned_to_same_company(current_user).scheduling.where(:active => true).where(:id => controller_id)
			end
		end
		@is_pop_up = true if params[:popup]
		controllers ||= User.assigned_to_same_company(current_user).scheduling.where(:active => true)
		@user_selection = false if (params[:userSelection] == "false")
    
		@quotes_per_controller = StatisticsHelper.get_quotes_per_controller(:period => params[:period], :controllers => controllers, :clients => current_user.clients)
		#debugger
    if @is_pop_up then
			@chart_width = "100%" 
		else
			@chart_width = (60*@quotes_per_controller.size)
			border = 15
			@chart_width = @frame_width - border if (@chart_width < (@frame_width - border))
			@chart_width = @chart_width.to_s+"px"
		end
		render :template => 'general/quotes_per_controller', :layout => "chart" 
	end 

	def volume_per_controller
		params[:period] ||= Settings.DEFAULT_PERIOD_FOR_CHARTS
		@frame_width = params[:frameWidth] ? params[:frameWidth].to_i : 450
		if (params[:controllers]) then
			if params[:controllers] == "ALL" then
				controllers ||= User.assigned_to_same_company(current_user).scheduling.where(:active => true)
			else
				controller_id = params[:controllers]
				controllers ||= User.assigned_to_same_company(current_user).scheduling.where(:active => true).where(:id => controller_id)
			end
		end
		@is_pop_up = true if params[:popup]
		controllers ||= User.assigned_to_same_company(current_user).scheduling.where(:active => true)
		@user_selection = false if (params[:userSelection] == "false")

		@volume_per_controller = StatisticsHelper.get_volume_per_controller(:period => params[:period], :controllers => controllers, :clients => current_user.clients)
		if params[:popup] then
			@chart_width = "100%" 
		else
			@chart_width = (60*@volume_per_controller.size)
			border = 15
			@chart_width = @frame_width - border if (@chart_width < (@frame_width - border))
			@chart_width = @chart_width.to_s+"px"
		end
		render :template => 'general/volume_per_controller', :layout => "chart" 
	end 
end