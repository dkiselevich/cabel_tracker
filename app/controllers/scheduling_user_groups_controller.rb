# encoding: utf-8
class SchedulingUserGroupsController < InheritedResources::Base
  before_filter :get_job_count_by_status
  #caches_action :show, :edit

  def index
    @scheduling_user_groups = SchedulingUserGroup.belonging_to_user_client(current_user).paginate :per_page => Settings.DEFAULT_ITEMS_PER_TABLE, :page => params[:page]
    render "scheduling_user_groups/index"
  end

  def new
    @scheduling_user_group = SchedulingUserGroup.new
    render "scheduling_user_groups/new"
  end

  def show
    @scheduling_user_group = SchedulingUserGroup.belonging_to_user_client(current_user).find(params[:id])
    render "scheduling_user_groups/show"
  end

  def edit
    @scheduling_user_group = SchedulingUserGroup.belonging_to_user_client(current_user).find(params[:id])
    render "scheduling_user_groups/edit"
  end
  
  def create
    @scheduling_user_group = SchedulingUserGroup.new(params[:scheduling_user_group])
    postal_codes = params[:postal_codes_csv][0].split(",")
    postal_codes.each do |c|
      pc = PostalCode.where(:code => c)
      if (pc.blank?) then
        pc = PostalCode.new
        pc.code = c.strip
        pc.save
      end
      @scheduling_user_group.postal_codes << pc
    end
    if @scheduling_user_group.save
      redirect_to([current_view, :scheduling_user_groups], :notice => 'Disponentengruppe erstellt: ' + self.class.helpers.link_to(@scheduling_user_group.name, url_for([current_view,@scheduling_user_group])))
    else
      render :action => "new"
    end
  end

  def update
    params[:scheduling_user_group][:user_ids] ||= []
    @scheduling_user_group = SchedulingUserGroup.belonging_to_user_client(current_user).find(params[:id])
    @scheduling_user_group.postal_codes.clear()
    postal_codes = params[:postal_codes_csv][0].split(",")
    postal_codes.each do |c|
      pc = PostalCode.where(:code => c)
      if (pc.blank?) then
        pc = PostalCode.new
        pc.code = c.strip
        pc.save
      else
        pc = pc.first
      end
      @scheduling_user_group.postal_codes << pc
    end
    if @scheduling_user_group.update_attributes(params[:scheduling_user_group])
      #expire_action :action => :show
      redirect_to([current_view, :scheduling_user_groups], :notice => 'Disponentengruppe upgedatet: ' + self.class.helpers.link_to(@scheduling_user_group.name, url_for([current_view,@scheduling_user_group])))
    else
      render :action => "edit"
    end   
  end

  def destroy
    @scheduling_user_group = SchedulingUserGroup.belonging_to_user_client(current_user).find(params[:id])
    @scheduling_user_group.destroy
    #expire_action :action => :show
    redirect_to([current_view, :scheduling_user_groups], :notice => 'Disponentengruppe gelöscht.')
  end
end
