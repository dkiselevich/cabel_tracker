# encoding: utf-8
class ServiceAppointmentsController < InheritedResources::Base
  #caches_action :show, :edit
  #cache_sweeper :service_appointment_sweeper

  def index
    @service_appointments = ServiceAppointment.belonging_to_user_client(current_user).paginate :per_page => Settings.DEFAULT_ITEMS_PER_TABLE, :page => params[:page]
    render "service_appointments/index"
  end

  def open
    @service_appointments = ServiceAppointment.where("day > ?",Date.yesterday).belonging_to_user_client(current_user).paginate :per_page => Settings.DEFAULT_ITEMS_PER_TABLE, :page => params[:page]
    @header = "Offene Termine"
    render "service_appointments/index"
  end

  def get_users_from_param(users_string)
     users_string = "ALL" if users_string.blank?
     if users_string.starts_with?("ALL") then
      service_users = User.service.assigned_to_same_company(current_user)
    elsif users_string.starts_with?("CLIENT:") then
      client_id = users_string.gsub("CLIENT:","").to_i
      service_users = []

      desired_client = Client.where(:id => client_id).first
      if (desired_client) && current_user.clients.include?(desired_client) then
        desired_client.users.each{|u| service_users << u if u.is_technician}
      end
    elsif users_string.starts_with?("USER:") then
      user_id = users_string.gsub("USER:","").to_i
      service_users = User.where(:id => user_id).service.assigned_to_same_company(current_user)
    end
    return service_users
  end

  def calendar
    @days_to_show = 1
    @pop_up_path = "pop_up"
   
    @service_users = get_users_from_param(params[:users])
    @clients = current_user.clients 
    @grouped_options = {}
    @grouped_options["Alle meine Techniker"] = [["Alle Techniker","ALL"]]

    @clients.each do |client|
      options = [["Alle (#{client.name})","CLIENT:#{client.id}"]]
      client.users.each do |user|
        options << [user.full_name, "USER:#{user.id}"] if user.is_technician
      end
      @grouped_options[client.name] = options
    end

    #TODO Technikergruppen
  end

  def pop_up
    @days_to_show = 1
    render :layout => "pop_up"
    @service_users = get_users_from_param(params[:users])
  end

  def data
    @service_users = get_users_from_param(params[:users])
    render :layout => false
  end

  def new
    @service_appointment = ServiceAppointment.new
    @service_job = ServiceJob.find(params[:service_job_id]) unless params[:service_job_id].blank?
    @service_appointment.service_job = @service_job 
    @service_appointment.day = Date.today
    @service_appointment.bemerkungen = @service_job.beschreibung
    #@service_appointment.day = @service_job.verbindlicher_installationstermin if @service_job.verbindlicher_installationstermin
    
    @service_appointment.start_time = Time.now.beginning_of_day+8.hours # Start at 08:00
    @service_appointment.end_time = Time.now.beginning_of_day+9.hours
    @service_appointment.high_priority = false
    
    if params[:date]
      @service_appointment.day = Date.parse(params[:date]) 
    end 
    if params[:start]
      @service_appointment.start_time = Time.new(1993, 02, 24, params[:start], 00)
      @service_appointment.end_time = Time.new(1993, 02, 24, params[:start], 00) + 1.hour
    end
    if params[:technician]
      @service_appointment.user_id = params[:technician]
    end    

    params[:update_ot] = true
    render "service_appointments/new"
  end

  def create
    @service_appointment = ServiceAppointment.new(params[:service_appointment])
    @service_appointment.scheduling_user = current_user
    @service_job = ServiceJob.find(params[:service_appointment][:service_job_id]) unless params[:service_appointment][:service_job_id].blank?

    user_id = params[:service_appointment][:user_id]
    start_time = params[:service_appointment]["start_time(4i)"]+":00:00"
    end_time = params[:service_appointment]["end_time(4i)"]+":00:00"
    day = Date.parse(params[:service_appointment]["day(1i)"]+"-"+params[:service_appointment]["day(2i)"]+"-"+params[:service_appointment]["day(3i)"])
    appointments_today = Appointment.where(:day => day)
                                      .where(:user_id => user_id)
                                      .where("(start_time <= ? AND end_time > ?) OR (start_time < ? AND end_time >= ?) OR (start_time > ? AND end_time < ?)", start_time, start_time, end_time, end_time, start_time, end_time)
    service_appointments_today = ServiceAppointment.where(:day => day)
                                      .where(:user_id => user_id)
                                      .where("(start_time <= ? AND end_time > ?) OR (start_time < ? AND end_time >= ?) OR (start_time > ? AND end_time < ?)", start_time, start_time, end_time, end_time, start_time, end_time)                                      
    if (appointments_today.count + service_appointments_today.count) != 0
      flash[:error] = "Termin kann nicht auf dieses Datum gespeichert werden, da der Termin bereits vergeben ist."  
      render "service_appointments/new"
      return      
    end

    if @service_appointment.save
      Admin::LogsController.create(current_user.vorname+" "+current_user.nachname+" created service appointment with ID "+@service_appointment.id.to_s, @service_appointment.id, "service_appointment") 
      @service_job.status = 1 #disponiert
      @service_job.save
      p @service_job.zustand

      note = "ct - Disponent: #{@service_appointment.scheduling_user.full_name}, Techniker: #{@service_appointment.user.full_name}"
      note = note + ", Hinweis für Techniker: #{@service_appointment.bemerkungen}" if @service_appointment.bemerkungen
      dt = DateTime.parse(@service_appointment.day.strftime("%d-%m-%Y "+@service_appointment.start_time.strftime("%H:%M:%S")))
      
      if (params[:update_ot] == "1") then
        result = OtHelper.set_state_to_di(@service_appointment.service_job.ot_id, nil, dt, note, @service_job.client)
      else
        result = false
      end
      if (result) then
        redirect_to([current_view, :service_appointments], :notice => 'Termin erstellt: '+ self.class.helpers.link_to(@service_appointment.short_description, url_for([current_view, @service_appointment])))
      else
        omnitracker_link = self.class.helpers.link_to("OMNITRACKER", Settings.OT_DETAILS_URL.gsub("##ID##",@service_appointment.service_job.ot_id), :style => "text-decoration:underline", :target => "_blank") 
        error_notice = "Termin erstellt ABER Omnitracker konnte nicht upgedatet werden. Bitte setzen Sie den Termin in #{omnitracker_link} von Hand."
        redirect_to([current_view, @service_appointment], :notice => error_notice)
      end
    else
      render "service_appointments/new"
    end
  end

  def edit
    @service_appointment = ServiceAppointment.belonging_to_user_client(current_user).find(params[:id])    
    @service_job = @service_appointment.service_job
    #fresh_when(:etag => @service_appointment, :last_modified => @service_appointment.updated_at.utc, :public => false) unless (Rails.env == "development")
    
    if params[:popup]=="1"
      render "service_appointments/edit", :layout => 'layouts/calendar_only'
    else
      render "service_appointments/edit" 
    end      
  end

  def has_special_expenses
    @service_appointment = ServiceAppointment.belonging_to_user_client(current_user).find(params[:id])    
    render :inline => "<%= !@service_appointment.special_expenses.blank? %>"
  end

  def show
    @service_appointment = ServiceAppointment.belonging_to_user_client(current_user).find(params[:id])    
    #fresh_when(:etag => @service_appointment, :last_modified => @service_appointment.updated_at.utc, :public => false) unless (Rails.env == "development")
  end

  def special_expenses_pop_up
    @service_appointment = ServiceAppointment.belonging_to_user_client(current_user).find(params[:id])    
    render "service_appointments/special_expenses_pop_up", :layout => "pop_up"
  end

  def update
    @service_appointment = ServiceAppointment.belonging_to_user_client(current_user).find(params[:id])

    user_id = params[:appointment][:user_id]
    start_time = params[:appointment]["start_time(4i)"]+":00:00"
    end_time = params[:appointment]["end_time(4i)"]+":00:00"
    day = Date.parse(params[:appointment]["day(1i)"]+"-"+params[:appointment]["day(2i)"]+"-"+params[:appointment]["day(3i)"])
    appointments_today = Appointment.where(:day => day)
                                      .where(:user_id => user_id)
                                      .where("(start_time <= ? AND end_time > ?) OR (start_time < ? AND end_time >= ?) OR (start_time > ? AND end_time < ?)", start_time, start_time, end_time, end_time, start_time, end_time)
    service_appointments_today = ServiceAppointment.where(:day => day)
                                      .where(:user_id => user_id)
                                      .where("(start_time <= ? AND end_time > ?) OR (start_time < ? AND end_time >= ?) OR (start_time > ? AND end_time < ?)", start_time, start_time, end_time, end_time, start_time, end_time)                                      
                                      .where("id != ?", @service_appointment.id)
    if (appointments_today.count + service_appointments_today.count) != 0
      flash[:error] = "Termin kann nicht auf dieses Datum gespeichert werden, da der Termin bereits vergeben ist."  
      #@job = @appointment.job
      render "service_appointments/edit"
      return      
    end 

    if @service_appointment.update_attributes(params[:service_appointment])
      Admin::LogsController.create(current_user.vorname+" "+current_user.nachname+" updated service appointment with ID "+@service_appointment.id.to_s, @service_appointment.id, "service_appointment") 
      @service_appointment.service_job.zustand = 1 #disponiert
      @service_appointment.service_job.save

      note = "ct - Disponent: #{@service_appointment.scheduling_user.full_name}, Techniker: #{@service_appointment.user.full_name}"
      note = note + ", Hinweis für Techniker: #{@service_appointment.bemerkungen}" if @service_appointment.bemerkungen
      dt = DateTime.parse(@service_appointment.day.strftime("%d-%m-%Y "+@service_appointment.start_time.strftime("%H:%M:%S")))
           
      if (params[:update_ot] == "1") then
        puts "UPDATING !!!!!!!!!!!!!!!!!!!!!!!!!!"
        result = OtHelper.set_state_to_di(@service_appointment.service_job.ot_id, nil, dt, note)
      else
        result = false
      end      
      #expire_action :action => :show
      if (result) then
        redirect_to([current_view, @service_appointment], :notice => 'Termin erstellt: '+ self.class.helpers.link_to(@service_appointment.short_description, url_for([current_view, @service_appointment])))
      else
        omnitracker_link = self.class.helpers.link_to("OMNITRACKER", Settings.OT_DETAILS_URL.gsub("##ID##",@service_appointment.service_job.ot_id), :style => "text-decoration:underline", :target => "_blank") 
        error_notice = "Termin erstellt ABER Omnitracker konnte nicht upgedatet werden. Bitte setzen Sie den Termin in #{omnitracker_link} von Hand."
        redirect_to([current_view, @service_appointment], :notice => error_notice)
      end
      
      if(Time.now.strftime("%H:%M") > '14:00' && Time.now.strftime("%H:%M") <= '21:59') # 16:00 - 23:59 deutscher Zeit
        Delayed::Job.enqueue(
          TaskHelper.send_pdf_with_appointments_for_tomorrow_to_all_technicians(Client.last)
        )
        Admin::LogsController.create("System sent pdf with appointments for tomorrow to all technicians for "+Client.last.name) 
        Delayed::Job.enqueue(
          TaskHelper.send_emails_with_blank_completion_pdf_for_each_appointment_tomorrow_to_all_technicians(Client.last)  
        )  
        Admin::LogsController.create("System sent emails with blank completion pdf for each appointment tomorrow to all technicians for "+Client.last.name)  
      end      
      
    else
    render "service_appointments/edit"
    end
  end

  def update_from_js
    @service_appointment = ServiceAppointment.belonging_to_user_client(current_user).find(params[:id])
    start = Time.at(params[:start].to_i/1000)
    @service_appointment.start_time = Time.at(params[:start].to_i/1000) + 2.hours
    @service_appointment.end_time = Time.at(params[:end].to_i/1000) + 2.hours
    @service_appointment.day = Date.new(start.year, start.month, start.day)
    @service_appointment.user_id = params[:userId].to_s
    @service_appointment.save
    render :inline => "true"
  end

  def destroy
    @service_appointment = ServiceAppointment.belonging_to_user_client(current_user).find(params[:id])
    @service_job = @service_appointment.service_job
    if (@service_appointment.processed_already?) then
      flash[:warning] = "Termin kann nicht gelöscht werden, da er bereits vom Techniker bearbeitet wurde."  
      render "service_appointments/edit"
    else
      Admin::LogsController.create(current_user.vorname+" "+current_user.nachname+" deleted service appointment with ID "+@service_appointment.id.to_s, @service_appointment.id, "service_appointment") 
      @service_appointment.destroy
      expire_action :action => :show
      redirect_to(@service_job ? [current_view, @service_job] : [current_view, :service_appointments], :notice => 'Termin gelöscht.')
    end
  end


  def preview
    @service_appointment = ServiceAppointment.find(params[:id])
    render :layout => "pop_up"
  end

end
