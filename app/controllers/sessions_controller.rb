class SessionsController < Devise::SessionsController

  # GET /resource/sign_in
  def new
    resource = build_resource
    clean_up_passwords(resource)
    if (mobile_device?)
    	render "new_mobile", :layout => false
    end
    render :layout => false
  end

end