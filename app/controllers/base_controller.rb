class BaseController < ApplicationController
	def index
		#use last view from cookie - user might have multiple views
		if !cookies[:last_view].blank? then
			redirect_to "/" + cookies[:last_view] 
			return
		end
		
		#no cookie? -> try different views
		if (current_user && user_signed_in?) then
			view = "service" if current_user.is_technician
			view = "accounting" if current_user.is_accountant
			view = "management" if current_user.is_boss
			view = "admin" if current_user.is_admin
			view = "scheduling" if current_user.is_controller
			puts view
			redirect_to "/"+view if !view.blank?
			return
		end

		#not logged in? -> go sign in
		redirect_to "/d/sign_in"
	end
	
	def no_right
		@desired_view = params[:view]
	end

	def issue_form
		render "general/wufoo_issue_form", :layout => false
	end
 
end