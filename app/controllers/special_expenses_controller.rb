# encoding: utf-8
class SpecialExpensesController < InheritedResources::Base
  #caches_action :show, :edit

  def index
    @special_expenses = SpecialExpense.paginate :per_page => Settings.DEFAULT_ITEMS_PER_TABLE, :page => params[:page]
    render "special_expenses/index"
  end

  def new
    @special_expense = SpecialExpense.new
    @special_expense.appointment = Appointment.find(params[:appointment_id])
    render "special_expenses/new"
  end

  def show
    @special_expense = SpecialExpense.find(params[:id])
    render "special_expenses/show"
  end

  def edit
    @special_expense = SpecialExpense.find(params[:id])
    render "special_expenses/edit"
  end
  
  def create
    @special_expense = SpecialExpense.new(params[:special_expense])
    @special_expense.appointment = Appointment.find(params[:special_expense][:appointment_id])
    if @special_expense.save
      redirect_to(params[:fromPath] || [current_view, :special_expenses], :notice => 'Mehraufwand erstellt: '+self.class.helpers.link_to(@special_expense.job.ot_auftragsnummer, url_for([current_view, @special_expense]))) 
    else
   		render "special_expenses/new"
    end
  end

  def update
    @special_expense = SpecialExpense.find(params[:id])
    @special_expense.appointment = Appointment.find(params[:special_expense][:appointment_id])
    if @special_expense.update_attributes(params[:special_expense])
      expire_action :action => :show
      redirect_to(params[:fromPath] || [current_view, :special_expenses], :notice => 'Mehraufwand upgedatet: '+self.class.helpers.link_to(@special_expense.job.ot_auftragsnummer, url_for([current_view, @special_expense]))) 
    else
    render "special_expenses/edit"
    end
  end

  def destroy
    @special_expense = SpecialExpense.find(params[:id])
    @special_expense.destroy
    expire_action :action => :show
    redirect_to(params[:fromPath] || [current_view, :special_expenses], :notice => 'Mehraufwand wurde gelöscht.')
  end
end