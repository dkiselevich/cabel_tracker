class Admin::BaseController < ApplicationController
	
	def index
		cookies[:last_view] = "admin"
	end

	def import_jobs
		@new_jobs = OtHelper.get_jobs(Settings.MAX_NUMBER_OF_JOBS_TO_GET)
		render "show_new_jobs"
	end

	def import_job
	end

	def create_job
		puts params[:job_id]
		client = Client.find(params[:client_id])
		@new_job = OtHelper.add_job_from_ot(params[:job_id], client, true)
		render "show_new_job"
	end

	def import_jobs_from_text
	end

	def create_jobs_from_text
		puts params[:jobs_text]
		input = params[:jobs_text].gsub("This XML file does not appear to have any style information associated with it. The document tree is shown below.","")
		@new_jobs, @blocked_jobs = OtHelper.get_jobs_from_text(input)
		render "show_new_jobs"
	end
end