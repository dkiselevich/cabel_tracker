# encoding: utf-8
class Admin::BuildingTypesController < InheritedResources::Base

  def index
    @building_types = BuildingType.paginate :per_page => Settings.DEFAULT_ITEMS_PER_TABLE, :page => params[:page]
  end

  def create
    @building_type = BuildingType.new(params[:building_type])
    if @building_type.save then
      redirect_to(admin_building_types_path, :notice => 'Objektart erstellt: '+self.class.helpers.link_to(@building_type.name, admin_building_type_path(@building_type)))
    else
      render :action => "new"
    end
  end

  def update
    @building_type = BuildingType.find(params[:id])
    if @building_type.update_attributes(params[:building_type])
      redirect_to(admin_building_types_path, :notice => 'Objektart upgedatet: '+self.class.helpers.link_to(@building_type.name, admin_building_type_path(@building_type)))
    else
      render :action => "edit"
    end
  end

  def destroy
    @building_type = BuildingType.find(params[:id])
    @building_type.destroy
    redirect_to(admin_building_types_url, :notice => 'Objektart wurde gelöscht.')
  end
end
