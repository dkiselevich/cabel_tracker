class Admin::BugtrackersController < InheritedResources::Base
	before_filter :authenticate_user!

	def index
		@bugtrackers = Bugtracker.all(:order => "bug_solved ASC, created_at DESC")
	end

	def reload_index
		@bugtrackers = Bugtracker.all(:order => "bug_solved ASC, created_at DESC")
		render "index", :layout => false
	end

	def create
		@bugtracker = Bugtracker.new(params[:bugtracker])
		@bugtracker.bug_solved = false
		    
		respond_to do |format|
			unless @bugtracker.save
				format.html { render action: "new" }
			else  
				format.html { redirect_to root_path, notice: 'Bug wurde erfolgreich gemeldet.' }
			end
		end
	end

	def edit
		@bugtracker = Bugtracker.find(params[:id])
	end

	def update
		@bugtracker = Bugtracker.find(params[:id])
		if @bugtracker.update_attributes(params[:bugtracker])
			BugtrackerMailer.bug_solved(@bugtracker).deliver if @bugtracker.bug_solved
			redirect_to(admin_bugtrackers_path)
		else
			render :action => "edit"
		end
 	end
end
