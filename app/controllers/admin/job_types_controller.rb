# encoding: utf-8
class Admin::JobTypesController < InheritedResources::Base

  def index
    @job_types = JobType.paginate :per_page => Settings.DEFAULT_ITEMS_PER_TABLE, :page => params[:page]
  end

  def create
    @job_type = JobType.new(params[:job_type])
    if @job_type.save then
      redirect_to(admin_job_type_path(@job_type), :notice => 'Auftragsart erstellt: '+self.class.helpers.link_to(@job_type.name, admin_job_type_path(@job_type)))
    else
      render :action => "new"
    end
  end

  def update
    @job_type = JobType.find(params[:id])
    if @job_type.update_attributes(params[:job_type])
      redirect_to(admin_job_types_path, :notice => 'Auftragsart upgedatet: '+self.class.helpers.link_to(@job_type.name, admin_job_type_path(@job_type)))      else
      render :action => "edit" 
    end
  end

  def destroy
    @job_type = JobType.find(params[:id])
    @job_type.destroy
    redirect_to(admin_job_types_url, :notice => 'Auftragsart wurd gelöscht.') 
  end
end
