# encoding: utf-8
class Admin::NetworkTypesController < InheritedResources::Base
  
  def index
    @network_types = NetworkType.paginate :per_page => Settings.DEFAULT_ITEMS_PER_TABLE, :page => params[:page]
  end

  def create
    @network_type = NetworkType.new(params[:network_type])
    if @network_type.save then
      redirect_to(admin_network_types_path, :notice => 'Netztopologie erstellt: ' + self.class.helpers.link_to(@network_type.name, admin_network_type_path(@network_type)))
    else
      render :action => "new"
    end
  end

  def update
    @network_type = NetworkType.find(params[:id])
    if @network_type.update_attributes(params[:network_type])
      redirect_to(admin_network_types_path, :notice => 'Netztopologie upgedatet: ' + self.class.helpers.link_to(@network_type.name, admin_network_type_path(@network_type)))
    else
      render :action => "edit"
    end
  end

  def destroy
    @network_type = NetworkType.find(params[:id])
    @network_type.destroy
    redirect_to(admin_network_types_url, :notice => 'Netztopologie wurde gelöscht.')
  end
end
