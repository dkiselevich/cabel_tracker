# encoding: utf-8
class Admin::WaitingCausesController < InheritedResources::Base

  def index
    @waiting_causes = WaitingCause.paginate :per_page => Settings.DEFAULT_ITEMS_PER_TABLE, :page => params[:page]
  end

  def create
    @waiting_cause = WaitingCause.new(params[:waiting_cause])
    if @waiting_cause.save then
      redirect_to(admin_waiting_causes_path, :notice => 'Wartegrund erstellt: ' + self.class.helpers.link_to(@waiting_cause.name, admin_waiting_cause_path(@waiting_cause)))
    else
      render :action => "new"
    end
  end

  def update
    @waiting_cause = WaitingCause.find(params[:id])
    if @waiting_cause.update_attributes(params[:waiting_cause])
      redirect_to(admin_waiting_causes_path, :notice => 'Wartegrund upgedatet: ' + self.class.helpers.link_to(@waiting_cause.name, admin_waiting_cause_path(@waiting_cause)))
    else
      render :action => "edit"
    end
  end

  def destroy
    @waiting_cause = WaitingCause.find(params[:id])
    @waiting_cause.destroy
    redirect_to(admin_waiting_causes_url, :notice => 'Wartegrund wurde gelöscht.')
  end
end
