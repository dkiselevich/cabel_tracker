# encoding: utf-8
class Admin::FiFuseTypesController < InheritedResources::Base
  
  def index
    @fi_fuse_types = FiFuseType.paginate :per_page => Settings.DEFAULT_ITEMS_PER_TABLE, :page => params[:page]
  end

  def create
    @fi_fuse_type = FiFuseType.new(params[:fi_fuse_type])
    if @fi_fuse_type.save then
      redirect_to(admin_fi_fuse_types_path, :notice => 'FI Sicherung erstellt: ' + self.class.helpers.link_to(@fi_fuse_type.name, admin_fi_fuse_type_path(@fi_fuse_type)))
    else
      render :action => "new"
    end
  end

  def update
    @fi_fuse_type = FiFuseType.find(params[:id])
    if @fi_fuse_type.update_attributes(params[:fi_fuse_type])
      redirect_to(admin_fi_fuse_types_path, :notice => 'FI Sicherung upgedatet: ' + self.class.helpers.link_to(@fi_fuse_type.name, admin_fi_fuse_type_path(@fi_fuse_type)))
    else
      render :action => "edit"
    end
  end

  def destroy
    @fi_fuse_type = FiFuseType.find(params[:id])
    @fi_fuse_type.destroy
    redirect_to(admin_fi_fuse_types_url, :notice => 'FI Sicherung wurde gelöscht.')
  end
end
