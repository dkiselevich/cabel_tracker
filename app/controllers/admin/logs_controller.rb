# encoding: utf-8
class Admin::LogsController < InheritedResources::Base
  
  def index
    #@logs = Log.paginate :per_page => 50, :page => params[:page]
    #@logs = Log.search(params[:search]).order(sort_column + " " + sort_direction).paginate(:per_page => 50, :page => params[:page])
    @logs = Log.search(params[:search], params[:fieldtype]).paginate(:per_page => 50, :page => params[:page])
  end
  
  def self.create(msg = nil, table_id = nil, table = nil)
    log = Log.new
    log.text = msg
    log.table_id = table_id
    log.tbl = table
    log.save
  end
    
end
