# encoding: utf-8
class Admin::ClientsController < InheritedResources::Base

  def index
    @clients = Client.paginate :per_page => Settings.DEFAULT_ITEMS_PER_TABLE, :page => params[:page]
  end

  def create
    @client = Client.new(params[:client])
    if @client.save then
      redirect_to(admin_client_path(@client), :notice => 'Mandant erstellt: '+self.class.helpers.link_to(@client.name, admin_client_path(@client)))
    else
      render :action => "new"
    end
  end

  def update
    @client = Client.find(params[:id])
    if @client.update_attributes(params[:client])
      redirect_to(admin_clients_path, :notice => 'Mandant upgedatet: '+self.class.helpers.link_to(@client.name, admin_client_path(@client)))
    else
      render :action => "edit"
    end
  end

  def destroy
    @client = Client.find(params[:id])
    @client.destroy
    redirect_to(admin_clients_url, :notice => 'Mandant wurd gelöscht.')
  end
end
