class Admin::UsersController < InheritedResources::Base

  def index
    
    if !params[:q].blank? && params[:q][:s].blank? && !params[:q].values.first.blank?
      @users = []
      @search_term = params[:q].values.first
      qs = params[:q].values.first.strip.split(" ")
      qs.each_with_index do |q, index|
        @search = User.search({params[:q].keys.first => q})
        result = params[:distinct].to_i.zero? ? @search.result : @search.result(distinct: true)
        result += User.scoped_by_active(convert_active(q)) if params[:q] && !convert_active(q).nil?
        if index > 0
          @users &= result
        else
          @users += result
        end
      end
      @users.uniq!
    else
      @search = User.search(params[:q])
      @users = params[:distinct].to_i.zero? ? @search.result : @search.result(distinct: true)
    end
    @users = @users.paginate(:per_page => Settings.DEFAULT_ITEMS_PER_TABLE, :page => params[:page])
    respond_with @users
  end
  
  def advanced_search
    @search = User.search(params[:q])
    @search.build_grouping unless @search.groupings.any?
    @users  = params[:distinct].to_i.zero? ? @search.result : @search.result(distinct: true)
    @users = @users.paginate(:per_page => Settings.DEFAULT_ITEMS_PER_TABLE, :page => params[:page])
    respond_with @users
  end

  def create
    @user = User.new(params[:user])

    if @user.save
      Admin::LogsController.create(current_user.vorname+" "+current_user.nachname+" created user "+@user.vorname+" "+@user.nachname, @user.id, "user") 
      @user.delay.create_signature(@user.signature, "technician_"+@user.id.to_s)   
      redirect_to(admin_users_path, :notice => 'Benutzer erstellt: '+self.class.helpers.link_to(@user.full_name, admin_user_path(@user)))
    else
      render :action => "new"
    end
  end
  
  def show
    @user = User.find_by_id(params[:id])
    render :edit
  end

  def update
    @user = User.find(params[:id])
    @user.attributes = {'client_ids' => []}.merge(params[:user] || {})
    if @user.update_attributes(params[:user])
      Admin::LogsController.create(current_user.vorname+" "+current_user.nachname+" updated user "+@user.vorname+" "+@user.nachname, @user.id, "user") 
      @user.delay.create_signature(@user.signature, "technician_"+@user.id.to_s)
      redirect_to(admin_users_path, :notice => 'Benutzer upgedated: '+self.class.helpers.link_to(@user.full_name, admin_user_path(@user)))
    else
      render :action => "edit"
    end
  end

  def new_shortcut
    @user = User.new
  end

  def create_shortcut
    @user = User.new(params[:user])
    unless @user.email.blank?
      @user.vorname = @user.email.split("@")[0].split(".")[0]
      @user.nachname = @user.email.split("@")[0].split(".")[1]
      @user.password = @user.vorname.downcase
    end
    if @user.save
      redirect_to(admin_users_path, :notice => 'Benutzer erstellt: '+self.class.helpers.link_to(@user.full_name, admin_user_path(@user)))
    else
      render :action => "new"
    end
  end

  def destroy
    @user = User.find(params[:id])
    Admin::LogsController.create(current_user.vorname+" "+current_user.nachname+" deleted user "+@user.vorname+" "+@user.nachname)
    @user.destroy     
    redirect_to(admin_users_url)
  end

  private
  def convert_active(value)
    if value.index("ja")
      return true
    elsif value.index("nein")
      return false
    end
    nil
  end
end
