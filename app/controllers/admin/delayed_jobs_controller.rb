class Admin::DelayedJobsController < ApplicationController
	
	def index
		cookies[:last_view] = "admin"
		@delayed_jobs = Delayed::Job.paginate :per_page => Settings.DEFAULT_ITEMS_PER_TABLE, :page => params[:page]
	end
end