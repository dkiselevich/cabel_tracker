# encoding: utf-8
class Admin::ModemTypesController < InheritedResources::Base
  def index
    @modem_types = ModemType.paginate :per_page => Settings.DEFAULT_ITEMS_PER_TABLE, :page => params[:page]
  end

  def create
    @modem_type = ModemType.new(params[:modem_type])
    if @modem_type.save then
      redirect_to(admin_modem_types_path, :notice => 'Modem erstellt: ' + self.class.helpers.link_to(@modem_type.name, admin_modem_type_path(@modem_type)))
    else
      render :action => "new"
    end
  end

  def update
    @modem_type = ModemType.find(params[:id])
    if @modem_type.update_attributes(params[:modem_type])
      redirect_to(admin_modem_types_path, :notice => 'Modem upgedatet: ' + self.class.helpers.link_to(@modem_type.name, admin_modem_type_path(@modem_type)))
    else
      render :action => "edit"
    end
  end

  def destroy
    @modem_type = ModemType.find(params[:id])
    @modem_type.destroy
    redirect_to(admin_modem_types_url, :notice => 'Modem wurde gelöscht.')
  end
end
