# encoding: utf-8
class JobsController < InheritedResources::Base
  caches_action :index# , :cache_path => Proc.new { |controller| controller.params.to_s + current_user.client_names }
  caches_action :show, :edit

  cache_sweeper :job_sweeper, :only => [:create, :update, :destroy]
  def index
    @search = Job.search(params[:q])
    @jobs = @search.result.belonging_to_user_client(current_user).paginate :per_page => Settings.DEFAULT_ITEMS_PER_TABLE, :page => params[:page]
    #@jobs = Job.ot_auftragsnummer_like(params[:ot_auftragsnummer]).workorder_like(params[:workorder]).mandant_id_like(params[:mandant_id]).kunde_plz_like(params[:kunde_plz]).kunde_ort_like(params[:kunde_ort]).kunde_strasse_like(params[:kunde_strasse]).kunde_hausnummer_like(params[:kunde_hausnummer]).kunde_nachname_like(params[:kunde_nachname]).kunde_vorname_like(params[:kunde_vorname]).kunde_plz_like(params[:kunde_kundennummer]).eigentuemer_nachname_like(params[:eigentuemer_nachname]).eigentuemer_vorname_like(params[:eigentuemer_vorname]).order(sort_column + " " + sort_direction).paginate :per_page => Settings.DEFAULT_ITEMS_PER_TABLE, :page => params[:page]
    #@jobs = Job.where("ot_auftragsnummer LIKE '%#{params[:ot_auftragsnummer]}%'").where("kunde_plz LIKE '%#{params[:kunde_plz]}%'").where("kunde_ort LIKE '%#{params[:kunde_ort]}%'").where("kunde_strasse LIKE '%#{params[:kunde_strasse]}%'").where("kunde_hausnummer LIKE '%#{params[:kunde_hausnummer]}%'").where("kunde_nachname LIKE '%#{params[:kunde_nachname]}%'").where("kunde_vorname LIKE '%#{params[:kunde_vorname]}%'").where("kunde_kundennummer LIKE '%#{params[:kunde_kundennummer]}%'").where("eigentuemer_nachname LIKE '%#{params[:eigentuemer_nachname]}%'").where("eigentuemer_vorname LIKE '%#{params[:eigentuemer_vorname]}%'").order(sort_column + " " + sort_direction).paginate :per_page => Settings.DEFAULT_ITEMS_PER_TABLE, :page => params[:page]
    #@jobs = Job.order(sort_column + " " + sort_direction).paginate :per_page => Settings.DEFAULT_ITEMS_PER_TABLE, :page => params[:page]
    render :layout => "application"
  end

  def show
    @job = Job.find(params[:id])
    @other_jobs_in_object = get_other_jobs_in_object(@job)
    #expire_action(:controller => 'admin/jobs', :action => 'show', :id => params[:id])
    #fresh_when(:etag => @job, :last_modified => @job.updated_at.utc, :public => false) unless (Rails.env == "development")
  end

  def edit
    @job = Job.find(params[:id])
    @other_jobs_in_object = get_other_jobs_in_object(@job)
    #fresh_when(:etag => @job, :last_modified => @job.updated_at.utc, :public => false) unless (Rails.env == "development")
  end

  def create
    @job = Job.new(params[:job])
    if @job.save
      redirect_to([current_view, :jobs], :notice => 'Auftrag erstellt: '+ self.class.helpers.link_to(@job.short_description, url_for([current_view, @job])))
    else
      render :action => "new"
    end
  end

  def update
    @job = Job.find(params[:id])
    if @job.update_attributes(params[:job])
      redirect_to([current_view, @job], :notice => 'Auftrag upgedated: '+ self.class.helpers.link_to(@job.short_description, url_for([current_view, @job])))
    else
      render :action => "edit"
    end
  end

  def destroy
    @job = Job.find(params[:id])
    if @job.destroy
      notice = 'Auftrag gelöscht.'
    else
      notice = "Unsuccessfully destroyed."
    end
    redirect_to([current_view, :jobs], :notice => notice)
  end

  def new
    @job = Job.new
    @job.client = current_user.clients.first
  end

  private

  def get_other_jobs_in_object(job)
    Job.where(:kunde_plz => job.kunde_plz).where(:kunde_ort => job.kunde_ort).where(:kunde_strasse => job.kunde_strasse).where(:kunde_hausnummer => job.kunde_hausnummer).where("id != "+job.id.to_s)
  end


end
