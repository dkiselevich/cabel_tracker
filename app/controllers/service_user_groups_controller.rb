# encoding: utf-8
class ServiceUserGroupsController < InheritedResources::Base
  #caches_action :show, :edit

  def index
    @search = ServiceUserGroup.search(params[:q])
    if params[:q] && params[:q][:s] && params[:q][:s].include?("user")
      @service_user_groups = ServiceUserGroup.joins('LEFT JOIN "service_user_groups_users"  ON "service_user_groups"."id" = "service_user_groups_users"."service_user_group_id"  LEFT JOIN "users" ON "service_user_groups_users.user_id" = "users.id"').belonging_to_user_client(User.first).order(params[:q][:s].sub("user","users.vorname")).order(params[:q][:s].sub("user","users.nachname")).uniq!
      @service_user_groups = @service_user_groups.paginate :per_page => Settings.DEFAULT_ITEMS_PER_TABLE, :page => params[:page]
    else
      @service_user_groups = @search.result.belonging_to_user_client(current_user).paginate :per_page => Settings.DEFAULT_ITEMS_PER_TABLE, :page => params[:page]
    end
    
    render "service_user_groups/index"
  end

  def new
    @service_user_group = ServiceUserGroup.new
    render "service_user_groups/new"
  end

  def show
    @service_user_group = ServiceUserGroup.belonging_to_user_client(current_user).find(params[:id])
    render "service_user_groups/show"
  end

  def edit
    @service_user_group = ServiceUserGroup.belonging_to_user_client(current_user).find(params[:id])
    render "service_user_groups/edit"
  end
  
  def appointments
    @service_user_group = ServiceUserGroup.belonging_to_user_client(current_user).find(params[:id])
    render "service_user_groups/appointments"
  end

  def appointments_pop_up
    @service_user_group = ServiceUserGroup.belonging_to_user_client(current_user).find(params[:id])
    render "service_user_groups/appointments_pop_up", :layout => "pop_up"
  end

  def appointments_data
    @service_user_group = ServiceUserGroup.belonging_to_user_client(current_user).find(params[:id])
    render "service_user_groups/appointments_data", :layout => false
  end
 
  def create
    @service_user_group = ServiceUserGroup.new(params[:service_user_group])
    postal_codes = params[:postal_codes_csv][0].split(",")
    postal_codes.each do |c|
      pc = PostalCode.where(:code => c)
      if (pc.blank?) then
        pc = PostalCode.new
        pc.code = c.strip
        pc.save
      end
      @service_user_group.postal_codes << pc
    end
    if @service_user_group.save
      redirect_to([current_view, :service_user_groups], :notice => 'Technikergruppe erstellt: ' + self.class.helpers.link_to(@service_user_group.name, admin_service_user_group_path(@service_user_group)))
    else
      render :action => "new"
    end
  end

  def update
    params[:service_user_group][:user_ids] ||= []
    @service_user_group = ServiceUserGroup.belonging_to_user_client(current_user).find(params[:id])
    @service_user_group.postal_codes.clear()
    postal_codes = params[:postal_codes_csv][0].split(",")
    postal_codes.each do |c|
      pc = PostalCode.where(:code => c)
      if (pc.blank?) then
        pc = PostalCode.new
        pc.code = c.strip
        pc.save
      end
      @service_user_group.postal_codes << pc
    end

    if @service_user_group.update_attributes(params[:service_user_group])
      #expire_action :action => :show

		  #check users to make sure they match the selected client
		  selected_users = @service_user_group.users.select{|u| u.clients.include?(@service_user_group.client) }
		  puts "+++++++++++++++++++++++++"
		  puts selected_users
		  @service_user_group.users = selected_users
		  
      redirect_to([current_view, :service_user_groups], :notice => 'Technikergruppe upgedatet: ' + self.class.helpers.link_to(@service_user_group.name, admin_service_user_group_path(@service_user_group)))
    else
      render :action => "service_user_groups/edit"
    end   
  end

  def destroy
    @service_user_group = ServiceUserGroup.find(params[:id])
    @service_user_group.destroy
    #expire_action :action => :show
    redirect_to(scheduling_service_user_groups_url, :notice => 'Technikergruppe gelöscht.')
  end
end
