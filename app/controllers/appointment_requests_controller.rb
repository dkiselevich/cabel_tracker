# encoding: utf-8
class AppointmentRequestsController < InheritedResources::Base
  #caches_action :show, :edit
  cache_sweeper :appointment_request_sweeper
  
  def index
    @appointment_requests = AppointmentRequest.paginate :per_page => Settings.DEFAULT_ITEMS_PER_TABLE, :page => params[:page]
    render "appointment_requests/index"
  end

  def show
    @appointment_request = AppointmentRequest.belonging_to_user_client(current_user).find(params[:id])
    render "appointment_requests/show"
  end
  
  def edit
    @appointment_request = AppointmentRequest.belonging_to_user_client(current_user).find(params[:id])
    render "appointment_requests/edit"
  end

  def new
    @appointment_request = AppointmentRequest.new
    @appointment_request.appointment = Appointment.find(params[:appointment_id])
    render "appointment_requests/new"
  end
  
  def create
    @appointment_request = AppointmentRequest.new(params[:appointment_request])
    @appointment_request.technician = current_user
    @appointment_request.appointment = Appointment.find(params[:appointment_request][:appointment_id])
    if @appointment_request.save
      @appointment_request.appointment.job.status = 2 #wartend
      @appointment_request.appointment.job.save

      waiting_cause_code = "W05" #todo: je nach dem
      note = "ct - auf wk gesetzt durch Techniker: #{@appointment_request.technician.full_name} - Wartegrund: #{waiting_cause_code}"
      result = OtHelper.set_state_to_wk(@appointment_request.job.ot_id, nil, waiting_cause_code, note, @appointment_request.appointment.job.client)
      if (result) then
        redirect_to(params[:fromPath] || [current_view, :appointment_requests], :notice => 'Terminanfrage erstellt: '+self.class.helpers.link_to(@appointment_request.job.ot_auftragsnummer, url_for([current_view, @appointment_request]))) 
      else
        omnitracker_link = self.class.helpers.link_to("OMNITRACKER", Settings.OT_DETAILS_URL.gsub("##ID##",@appointment_request.job.ot_id), :style => "text-decoration:underline", :target => "_blank") 
        error_notice = "Terminanfrage erstellt ABER Omnitracker konnte nicht upgedatet werden. Bitte setzen Sie den Auftrag in #{omnitracker_link} von Hand auf 'wk'."
        redirect_to([current_view, @appointment_request], :notice => error_notice)
      end
    else
      render :action => "new"
    end
  end

  def update
    @appointment_request = AppointmentRequest.belonging_to_user_client(current_user).find(params[:id])
    @appointment_request.appointment = Appointment.find(params[:appointment_request][:appointment_id])
    if @appointment_request.update_attributes(params[:appointment_request])
      redirect_to(params[:fromPath] || [current_view, :appointment_requests], :notice => 'Terminanfrage upgedatet: '+self.class.helpers.link_to(@appointment_request.job.ot_auftragsnummer, url_for([current_view, @appointment_request])))
    else
      render :action => "appointment_requests/edit"
    end
  end

  def destroy
    @appointment_request = AppointmentRequest.belonging_to_user_client(current_user).find(params[:id])
    @appointment_request.destroy
    redirect_to(params[:fromPath] || service_appointment_requests_url, :notice => 'Terminanfrage wurde gelöscht.')
  end
end