class Test::BaseController < ApplicationController
	def index   		
    cookies[:last_view] = "test"
    render :layout => "pop_up"
	end

	def pdf
	    headers['Cache-Control'] = 'no-cache'  
    	html = "<p> Hello world </p>"  
  		pdfkit_instance = PDFKit.new(html)
      
  		send_data(pdfkit_instance.to_pdf)  
      pdfkit_instance.to_file  "#{RAILS_ROOT}/tmp/test.pdf"

  		#@completion = Completion.last()
  		#render :layout => false

	end 


end
