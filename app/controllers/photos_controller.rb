# encoding: utf-8
class PhotosController < InheritedResources::Base

  def index
    @photos = Photo.paginate :per_page => Settings.DEFAULT_ITEMS_PER_TABLE, :page => params[:page]
    render "photos/index"
  end

  def new
    @photo = Photo.new
    render "photos/new"
  end

  def show
    @photo = Photo.find(params[:id])
    render "photos/show"
  end

  def edit
    @photo = Photo.find(params[:id])
    render "photos/edit"
  end
  
  def create
    @photo = Photo.new(params[:photo])
    if @photo.save
      redirect_to(params[:fromPath] || [current_view, :photos], :notice => 'Foto erstellt: ')#+self.class.helpers.link_to(@photo.job.ot_auftragsnummer, url_for([current_view, @photo]))) 
    else
    render "photos/new"
    end
  end

  def update
    @photo = Photo.find(params[:id])
    if @photo.update_attributes(params[:photo])
      redirect_to(params[:fromPath] || [current_view, :photos], :notice => 'Foto upgedatet: ')#+self.class.helpers.link_to(@photo.job.ot_auftragsnummer, url_for([current_view, @photo]))) 
    else
    render "photos/edit"
    end
  end

  def destroy
    @photo = Photo.find(params[:id])
    @photo.destroy
    redirect_to(params[:fromPath] || [current_view, :photos], :notice => 'Foto wurde gelöscht.')
  end
end