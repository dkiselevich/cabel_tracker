# encoding: utf-8
class Service::BaseController < ApplicationController
	
	def index
		cookies[:last_view] = "service"
		session[:mobile_param] = nil
		@help_path = "service.html"

		@search_installation_today = Appointment.joins(:job).search(params[:q])
		@today_installation_appointments = @search_installation_today.result.where(:user_id => current_user.id).where("jobs.status = 1").where(:day => Date.today)#.where(:day => Date.today).joins(:jobs).where("jobs.status = 1")#.where(:day => Date.today).paginate :per_page => 5, :page => params[:page_today]

		@search_service_today = ServiceAppointment.joins(:service_job).search(params[:q])
		@today_service_appointments = @search_service_today.result.where(:user_id => current_user.id).where("service_jobs.status = 1").where(:day => Date.today)#.joins(:service_job)#.where(:day => Date.today).paginate :per_page => 5, :page => params[:page_today]

		today_appointments = []
		@today_installation_appointments.each do |a|
			today_appointments << a
		end
		@today_service_appointments.each do |a|
			today_appointments << a
		end
		@today_appointments = today_appointments.sort!{ |a,b| a.start_time <=> b.start_time }.paginate  :per_page => 5, :page => params[:page]
  		if (current_user.signature.blank?) then
  			flash[:error] = "Unterschrift benötigt: Klicken Sie #{self.class.helpers.link_to 'hier', edit_user_path(current_user), :style => 'text-decoration:underline'} um zu unterschreiben"
  		end
	end

	def quotes_per_technician
		@is_pop_up = true if params[:popup]
		params[:period] ||= Settings.DEFAULT_PERIOD_FOR_CHARTS
		technicians = User.where(:id => current_user.id)

		@quotes_per_technician = StatisticsHelper.get_quotes_per_technician(:period => params[:period], :technicians => technicians, :clients => current_user.clients)
		@user_selection = false 
		render :template => 'general/quotes_per_technician', :layout => "chart"
	end 

	def appointments_per_period
		@is_pop_up = true if params[:popup]
		params[:period] ||= Settings.DEFAULT_PERIOD_FOR_CHARTS
		@jobs_per_period = StatisticsHelper.get_appointments_per_period(:period => params[:period], :technician => current_user.id, :clients => current_user.clients)
		#render :partial => 'general/jobs_per_period', :layout => "chart" 
		render :template => 'general/jobs_per_period', :layout => "chart" 
	end 

	def appointments_data
	    @service_user = current_user
    	render "general/appointments_data_per_user", :layout => false
	end

	def create_pdf
    	@service_user = current_user
    	installation_appointments = Appointment.where(:day => Date.today).where(:user_id => current_user)
    	service_appointments = ServiceAppointment.where(:day => Date.today).where(:user_id => current_user)
    	@appointments = (service_appointments + installation_appointments).sort!{ |a,b| a.start_time <=> b.start_time }

	    html = render_to_string "general/appointments_pdf", :layout => false, :format => "pdf"
	    pdf = PDFKit.new(html).to_pdf
	    send_data(pdf, :filename => "Termine-#{@service_user.full_name}.pdf", :type => 'application/pdf', :disposition => 'inline')
  	end

  	def equipment
  		@equipments = current_user.equipments.paginate :per_page => Settings.DEFAULT_ITEMS_PER_TABLE, :page => params[:page]
  	end
end
