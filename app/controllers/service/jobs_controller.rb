class Service::JobsController < JobsController
  actions :index, :show

  def index
    @search = Job.joins{appointments}.where{(status != 1)}.where("appointments.user_id" => current_user.id).search(params[:search])
    @jobs = @search.result.paginate :per_page => Settings.DEFAULT_ITEMS_PER_TABLE, :page => params[:page]

    @service_search = ServiceJob.joins{service_appointment}.where{(status != 1)}.where("service_appointments.user_id" => current_user.id).search(params[:search])
    @service_jobs = @service_search.result.paginate :per_page => Settings.DEFAULT_ITEMS_PER_TABLE, :page => params[:page]
    render :layout => "application"
  end
end
