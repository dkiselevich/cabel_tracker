class Service::AppointmentsController < AppointmentsController
  actions :index, :show
  
  def index
    @service_user = current_user
    @appointments = Appointment.paginate :per_page => Settings.DEFAULT_ITEMS_PER_TABLE, :page => params[:page]
  end
  
  def appointments_data
    @service_user = User.find(params[:id])
    render "general/appointments_data_per_user", :layout => false
  end

  def preview
    @appointment = Appointment.find(params[:id])
    render :layout => "pop_up"
  end

  def pop_up
    @service_user = current_user
    @appointments = Appointment.paginate :per_page => Settings.DEFAULT_ITEMS_PER_TABLE, :page => params[:page]
    render :layout => "pop_up"
  end
end
