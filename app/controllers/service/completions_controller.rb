# encoding: utf-8
class Service::CompletionsController < CompletionsController
  actions :index, :new, :create, :edit, :show, :update

  def edit
    redirect_to(service_path, :notice => 'Als Service-Mitarbeiter können Sie den Abschluss nicht nachträglich bearbeiten. Wenden Sie sich an Ihren Disponent.') 
  end
end