class StreetsController < ApplicationController
  def index
  	q = params[:q] || ""
  	ort = params[:ort] || ""
  	@streets = Job.all.collect do |j| 
	  	if ((j.kunde_strasse.downcase.starts_with?(q.downcase)) && (j.kunde_ort == ort)) then
			j.kunde_strasse
		end
	end.uniq.compact.sort

    render :layout => false
  end
end
