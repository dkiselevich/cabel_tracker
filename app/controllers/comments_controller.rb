# encoding: utf-8
class CommentsController < InheritedResources::Base
  actions :index, :new, :create, :show, :edit, :update

  def index
    @comments = Comment.where(:job_id => params[:job_id])
    render "comments/index", :layout => false
  end

  def new
    @comment = Comment.new
    @comment.job_id = params[:job_id]
    render :layout => "pop_up"
  end

  def create
    @comment = Comment.new(params[:comment])
    @comment.user = current_user
    @comment.job = Job.find(params[:job_id])
    @comment.save
    render :layout => false
  end
end
