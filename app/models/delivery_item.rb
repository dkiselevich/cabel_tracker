class DeliveryItem < ActiveRecord::Base
	belongs_to	:delivery
	belongs_to 	:article
	
	validates :delivery_id, :presence => {:message => "^Geben Sie eine Bestellung an."}
	validates :article_id, :presence => {:message => "^Geben Sie einen Artikel an."}
	validates :quantity, :presence => {:message => "^Geben Sie eine Menge an."}

end
