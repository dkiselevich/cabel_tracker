class CompletionPhoto < ActiveRecord::Base
	belongs_to :completion
	has_attached_file :photo,
	  :styles =>{
	    :thumb  => "100x100",
	    :medium => "200x200",
	    :large => "600x400"
	  },
	  :storage => :s3,
	  :s3_credentials => "#{Rails.root.to_s}/config/s3.yml",
      :path => "/completions/other/:style/:id/:filename"

end
