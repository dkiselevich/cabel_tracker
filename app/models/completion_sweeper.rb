class CompletionSweeper < ActionController::Caching::Sweeper
  observe Completion

  # If our sweeper detects that a Completion was created call this
  def after_create(completion)
    expire_cache_for(completion)
  end
 
  # If our sweeper detects that a Completion was updated call this
  def after_update(completion)
    expire_cache_for(completion)
  end
 
  # If our sweeper detects that a Completion was deleted call this
  def after_destroy(completion)
    expire_cache_for(completion)
  end
 
private
  def expire_cache_for(completion)
    begin
      SweeperHelper.expire_completion_views(completion, self)
      SweeperHelper.expire_job_views(completion.job, self)
    rescue Exception => ex
      puts "ERROR while clearing caches: #{ex}"
    end
  end
end