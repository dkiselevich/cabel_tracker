#!/usr/bin/env ruby
#encoding: UTF-8

# Nick R (nick.rndl@gmail.com)

require 'rubygems'
require 'nokogiri'
require 'iconv' unless String.method_defined?(:encode)

module ScrapingUtils

  module NokoParser
    attr_accessor :encoding
  
    def build_doc body
      @doc = Nokogiri::HTML(body)
    end
    
    def encoding
      @encoding ||= ((@doc.meta_encoding rescue nil)  || "UTF-8")
    end

   # todo
    def extract_strings(query, d=nil)
      extract_string(query, d, "\x00").split("\x00")
    end
  
    def extract_string(query, d=nil, sep=nil)
      doc = d || @doc
      separator = sep || ''
      substrings = []
      found = doc.xpath(query)  
      if found.kind_of? Enumerable
        found.each do |el| 
          substrings << normalize_substring(el.content)
        end
      else
        substrings << found.content
      end
      normalize_string substrings.join(separator)
    end
    
    def to_utf8 str
      if String.method_defined?(:encode)
        str.encode!('UTF-8', encoding, :invalid => :replace)
      else
        Iconv.new('UTF-8', "#{encoding}//IGNORE").iconv(str)
      end
    end
    
    def normalize_string str
      to_utf8 str
      #str
    end
    
    def normalize_substring str
      normalize_string str
    end
  end
end
