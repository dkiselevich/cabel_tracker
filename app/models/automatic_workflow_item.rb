# encoding: utf-8
class AutomaticWorkflowItem < ActiveRecord::Base
	belongs_to :client

	validates_presence_of :workflow_action_id, :message => "^Geben Sie eine Aktion an."
	validates_presence_of :start_time, :message => "^Geben Sie eine Startzeit an an."

	def self.belonging_to_user_client(user)
		self.where(:client_id => user.clients.collect{|c|c.id})
	end

	def action
		return "Test Email" if workflow_action_id == 1
		return "Email mit Terminen an alle Techniker" if workflow_action_id == 2
		return "Emails pro Termin mit Auftragsabschlussformular an alle Techniker" if workflow_action_id == 3
		return "automatische Emails bei Terminänderungen - aktivieren" if workflow_action_id == 4
		return "automatische Emails bei Terminänderungen - deaktivieren" if workflow_action_id == 5
	end
end
