class AppointmentRequestSweeper < ActionController::Caching::Sweeper
  observe AppointmentRequest

  # If our sweeper detects that a AppointmentRequest was created call this
  def after_create(appointment_request)
    expire_cache_for(appointment_request)
  end
 
  # If our sweeper detects that a AppointmentRequest was updated call this
  def after_update(appointment_request)
    expire_cache_for(appointment_request)
  end
 
  # If our sweeper detects that a AppointmentRequest was deleted call this
  def after_destroy(appointment_request)
    expire_cache_for(appointment_request)
  end
 
  private
  def expire_cache_for(appointment_request)
    begin
      SweeperHelper.expire_appointment_request_views(appointment_request, self)
      SweeperHelper.expire_job_views(appointment_request.job, self)
    rescue Exception => ex
      puts "ERROR while clearing caches: #{ex}"
    end
  end
end