# encoding: utf-8
class Appointment < ActiveRecord::Base
	belongs_to :job
	belongs_to :user
	belongs_to :scheduling_user, :foreign_key => "scheduling_user_id", :class_name => "User"
	has_one :cancellation
	has_one :completion
	has_one :appointment_request
	has_many :special_expenses
  
	validates :day, :presence => {:message => "^Geben Sie ein Datum an."}
	validates :start_time, :presence => {:message => "^Geben Sie einen Startzeitpunkt an."}
	validates :end_time, :presence => {:message => "^Geben Sie einen Endzeitpunkt an."}
	validates :job_id, :presence => {:message => "^Geben Sie einen Auftrag an."}
  validates :user, :presence => {:message => "^Wählen Sie einen Techniker."}

  validate :start_is_before_ende



	def short_description
		"#{job.ot_auftragsnummer} | #{day.strftime('%d.%m.%Y')} #{start_time.strftime('%H:%M')}-#{end_time.strftime('%H:%M')} | #{user.full_name}"
	end

	def short_for_mail
#		tag monat, start-uhrzeit, plz, ort, strasse, nummer, name, ot-nummer
		"#{day.strftime('%d.%m.')}, #{start_time.strftime('%H:%M')}, #{job.adresse_short}, #{job.kunde_name}, #{job.ot_auftragsnummer}"
	end

	def processed_already?
		(self.completion.blank? && self.appointment_request.blank? && self.cancellation.blank?) ? false : true
	end

	def self.belonging_to_user_client(user)
		self.includes(:job).where("jobs.client_id in (#{user.clients.collect{|c| c.id}.join(',')})")
	end


  private
  def start_is_before_ende
    errors.add(:start_time, '^Terminanfang muss vor Terminende liegen!') if (start_time > end_time)
  end

end
