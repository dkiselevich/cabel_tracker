class ServiceComment < ActiveRecord::Base
	belongs_to :service_job
	belongs_to :user
	
	validates :body, :presence => {:message => "^Geben Sie einen Kommentar ein."}
end
