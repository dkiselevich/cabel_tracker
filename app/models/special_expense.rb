class SpecialExpense < ActiveRecord::Base
	belongs_to :appointment
	has_one :job, :through => :appointment

	validates :name, :presence => {:message => "^Geben Sie eine Bezeichnung an."}
	validates :costs, :presence => {:message => "^Geben Sie einen Betrag in Euro ODER einen Faktor an.", :unless => :factor}
	validates :costs, :numericality => { :message => "^Der Eurobetrag muss eine Zahl sein."}, :if => :costs
	validates :factor, :numericality => { :message => "^Der Faktor muss eine Zahl sein."}, :if => :factor

end
