class Equipment < ActiveRecord::Base
	belongs_to :article
	belongs_to :user
	
	def quantity_with_unit
		[quantity, article.unit].join(" ")
	end
end
