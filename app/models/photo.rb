class Photo < ActiveRecord::Base
	belongs_to :cancellation
	has_attached_file :photo,
	  :styles =>{
	    :thumb  => "100x100",
	    :medium => "200x200",
	    :large => "600x400"
	  },
	  :storage => :s3,
	  :s3_credentials => "#{Rails.root.to_s}/config/s3.yml",
      :path => "/cancellations/:style/:id/:filename"

    #attr_accessible :photo, :photo_file_name, :photo_content_type, :photo_file_size, :photo_updated_at
end
