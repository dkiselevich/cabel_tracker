class DaysOfAbsence < ActiveRecord::Base
	belongs_to :user
  # attr_accessible :title, :body

	validates :cause_id, :presence => {:message => "^Geben Sie einen Grund an."}
	validates :starting_point, :presence => {:message => "^Geben Sie einen Startzeitpunkt an."}
	validates :end_point, :presence => {:message => "^Geben Sie einen Endzeitpunkt an."}

  def cause 
  	return "Urlaub" if cause_id == 1
  	return "Krankheit" if cause_id == 2
  	return "unbekannt"
  end
end