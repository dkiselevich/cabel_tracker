class Client < ActiveRecord::Base
	validates :name, :presence => {:message => "^Geben Sie eine Bezeichnung an."}
	has_many :assignments
	has_many :users, :through => :assignments
	has_many :scheduling_user_groups

  scope :active, where(:active => true)

  class << self
    def is_assigned_to_user(user)
      self.where(:id => user.clients.collect{|c|c.id})
    end

    def options_for_select
      active.collect {|c| [ c.name, c.id ] }
    end
  end
  
  has_attached_file :logo_img, styles: {
    thumb: '150x110'
  },
  :path => "/clients/:id/logo/:filename"
  
  validates_attachment_content_type :logo_img, :content_type => ['image/jpeg', 'image/png']
  
  def show_logo_img
    if (self.logo_img.url(:thumb) != '/logo_imgs/thumb/missing.png')
    return self.logo_img.url(:thumb)
    end
  end   
end
