#!/usr/bin/env ruby
#encoding: UTF-8

#
# Author: Nick R <nick.rndl@gmail.com>
#

require 'rubygems'
require 'mechanize'
require 'logger'
require 'pp'
#require 'date'
#require 'digest'
require File.expand_path(File.join(File.dirname(__FILE__), 'scraping_utils.rb'))


class OtScraper
  attr_reader :agent
  def initialize
    @agent = Mechanize.new
    # Windows specific
    @agent.agent.http.verify_mode = OpenSSL::SSL::VERIFY_NONE  
    @agent.follow_meta_refresh = true
    #@agent.user_agent_alias = 'Windows Mozilla'
    #@agent.user_agent = 'Mozilla/5.0 (X11; Linux i686; rv:19.0) Gecko/20100101 Firefox/19.0'
    @agent.user_agent = 'Mozilla/5.0 (X11; Linux i686; rv:20.0) Gecko/20121230 Firefox/20.0'
    @agent.log = nil # Logger.new(STDOUT)
  end
  
  LOGIN_POST_PARAMS = {
    "LangCode"          => "Deutsch",
    "__EVENTARGUMENT"   => "",
    "__EVENTTARGET"     => "thebutton",
    "__EVENTVALIDATION" => nil,        # must
    "__VIEWSTATE"       => nil,        # must
    "cbolistLangCode"   => "de",
    "theloginbox"       => nil,        # must
    "thepasswordbox"    => nil,        # must
  }
  def log msg = nil
    puts "#{Time.now}: #{caller[0].gsub(/^.+?`([^']+)'.*$/,'\1')}: #{msg}"
    STDOUT.flush
  end
  
  LOGIN_URL = "https://otweb.kabelbw.com/otwg/Login.aspx?guestlogin=2&autologin=3"  
  def fetch_login_page
    log "try"
    @login_page = LoginPage.new(@agent.get(LOGIN_URL))
    if @login_page.login_page?
      log "ok"
    else
      raise OtException.new(@login_page), "can't fetch login_page"
    end
    @login_page
  end

  def login client={:ot_login=>"3Kabel GmbH", :ot_password=>"6k66"}
    log client
    fetch_login_page
    sleep 1
    #p @login_page.eventvalidation
    #p @login_page.viewstate
    
    params = LOGIN_POST_PARAMS.merge({
      "__EVENTVALIDATION"=>@login_page.eventvalidation,
      "__VIEWSTATE"=>@login_page.viewstate,
      "theloginbox" => client[:ot_login],
      "thepasswordbox" => client[:ot_password] 
    })
    pp params
    #abort
    @frameset_page = FramesetPage.new(@agent.post(LOGIN_URL, params))
    if @frameset_page.frameset_page?
      log "ok"
      
    else
      raise OtException.new(@frameset_page), "can't login (can't fetch frameset_page)"      
    end
    sleep 1
    @frameset_page
  end
  
  SHORTCUTBAR_URL = "https://otweb.kabelbw.com/otwg/OTWGShortCutBar.aspx?ID=0"
  def fetch_shortcutbar_frame
    log "try"
    c = caller[0].gsub(/^.+?`([^']+)'.*$/,'\1')
    raise "must be called from OtScraper::shortcuts" unless c=='shortcuts_params'
    @shortcutbar_frame = ShortcutbarPage.new(@agent.get SHORTCUTBAR_URL)
    if  @shortcutbar_frame.shortcutbar_page?
      log "ok"
    else
      raise OtException.new(@shortcutbar_frame), "can't fetch shortcutbar_frame"
    end
    sleep 1
    @shortcutbar_frame
  end
  
  def shortcuts_params
    log "try"
    #@shortcuts_params ||= (
    @shortcuts_params = (
      fetch_shortcutbar_frame
      @shortcutbar_frame.change_to_folder_shortcuts_params
    )
    log "ok"
    @shortcuts_params
  end

  FRAME112_URL = "https://otweb.kabelbw.com/otwg/OTWGObjectListFrame1_1_2.aspx?viewID=0&ShortCutBarID=0"
  def fetch_frame112
    log "try"
    @frame112_page = Frame112Page.new(@agent.get FRAME112_URL)
    if @frame112_page.frame112_page?
      log "ok"
    else
      raise OtException.new(@frame112_page), "can't fetch frame112_page"
    end
    sleep 1
    @frame112_page
  end
  
  FRAME122_URL = "https://otweb.kabelbw.com/otwg/OTWGObjectListFrame1_2_2.aspx?ShortCutBarID=0"
  def fetch_frame122
    log "try"
    @frame122_page = Frame122Page.new(@agent.get FRAME122_URL)
    if @frame122_page.frame122_page?
      log "ok"
    else
      raise OtException.new(@frame122_page), "can't fetch frame112_page"
    end
    sleep 1
    @frame122_page
  end
  
  def frame122_zde_request(status_text, status_code)

		#TODO now make this shit configurable..
  	#nur verifon zde
  	#status_text = "3Kabel Verifon zdE"
		#status_code = "97506"

		#nur Wend zde
  	#status_text = "3Kabel Wend zdE"
		#status_code = "97503"

		#alle zdE
  	#status_text = "Zustand zdE"
		#status_code = "21259"

		#alle di (for test)
  	#status_text = "Zustand di"
		#status_code = "13798"

		#alle ab (for test)
  	#status_text = "Zustand ab"
		#status_code = "13797"

		#alle ta (for test)
  	#status_text = "Zustand ta"
		#status_code = "20143"

    log "try"
    #fetch_frame122
    params = {
      "GRID_Properties"         => "",
      "SF_LST_F"                => status_text,
      "SF_MEN_F"                => "Filter:",
      "__EVENTARGUMENT"         => "40",
      "__EVENTTARGET"           => "SF_LST_F",
      "__EVENTVALIDATION"       => @frame122_page.eventvalidation,
      "__LASTFOCUS"             => "",
      "__VIEWSTATE"             => @frame122_page.viewstate,
      "cboReports"              => " ",
      "cboTaskExport"           => " ",
      "cboTaskReport"           => " ",
      "cboViewAction"           => "Ansicht:",
      "cboViewName"             => "(benutzerdefiniert)",
      "cbolistSF_LST_F"         => status_code, 
      "cbolistSF_MEN_F"         => "0",
      "cbolistcboReports"       => "0",
      "cbolistcboTaskExport"    => "0",
      "cbolistcboTaskReport"    => "0",
      "cbolistcboViewAction"    => "0",
      "cbolistcboViewName"      => "0",
      "otwgc_CheckedItems"      => "",
      "otwgc_ExpandedPaths"     => "",
      "otwgc_FirstSelected"     => "",
      "otwgc_HeaderPath"        => "",
      "otwgc_LastClickedColumn" => "0",
      "otwgc_LastClickedHeader" => "0",
      "otwgc_SelectedItems"     => "",
      "otwgc_SelectedPath"      => "",
      "otwgc_SelectedRow"       => "-1",
      "otwgc_Start"             => "0",
      "otwgc_fieldId"           => ""
    }
    pp params
    @agent.post(FRAME122_URL, params)
    sleep 2
  end
  
  #@OBJECTLIST_URL = "https://otweb.kabelbw.com/otwg/GetObjectList.aspx?folder=831&path=&start=0&count=45&fieldpath=&request=0&guid=&sortcol=-1"
  def fetch_objectlist_page
    log "try"
    #@objectlist_page = ObjectlistPage.new(@agent.get OBJECTLIST_URL)
    @objectlist_page = ObjectlistPage.new(@agent.get @objectlist_url)
    if @objectlist_page.objectlist_page?
      log "ok"
    else
      raise OtException.new(@objectlist_page), "can't fetch objectlist_page"
    end
    sleep 2
    @objectlist_page
  end
  
  
  def logout
    log "try"
    logout_page = Frame122Page.new(@agent.get FRAME122_URL)
    unless logout_page.logout_btn.empty?
      params = {
        "__EVENTTARGET"           => "otBtnLogout",
        "__EVENTVALIDATION"       => logout_page.eventvalidation,
        "__VIEWSTATE"             => logout_page.viewstate,
        "__EVENTARGUMENT"         => ""
      }
      @agent.post(FRAME122_URL, params)
    end
    begin 
      fetch_frame122
    rescue OtException => e
      str = %q{<script type="text/javascript" language="javascript">alert('Your session has timed out. Please login again!');</script><script type="text/javascript" language="javascript">parent.parent.location='OTWGGuestLogin.aspx?id=2';</script>}
      if e.ot_page.body.strip == str
        log "successfully logged out"
      else
        log "not sure if have logged out"
      end
    end
  end
  
  
  
  
  def get_job_list(client, status_text, status_code)
    log "try"
    login(client)
    @agent.get "https://otweb.kabelbw.com/otwg/OTWGMain.aspx"
    sleep 1
    shortcuts_params
    sleep 1
    @agent.get "https://otweb.kabelbw.com/otwg/OTWGObjectList.aspx?ShortCutBarID=0"
    sleep 1 
    fetch_frame112
    @agent.get "https://otweb.kabelbw.com/otwg/Empty.htm"
    sleep 1
    fetch_frame122
    @agent.get "https://otweb.kabelbw.com/otwg/OTWGMain.js"
    sleep 1
    @agent.get "https://otweb.kabelbw.com/otwg/InplaceSelection.js"
    sleep 1
    @agent.get "https://otweb.kabelbw.com/otwg/OTWGDropDownList.js"
    sleep 1
    @agent.get "https://otweb.kabelbw.com/otwg/WebGridControl.js"
    sleep 1
    puts
    @objectlist_url = "https://otweb.kabelbw.com/otwg/GetObjectList.aspx?folder=684&path=&start=0&count=45&fieldpath=&request=0&guid=&sortcol=-1"
    fetch_objectlist_page
    puts
    p @objectlist_page.total
    p @objectlist_page.zustands
    pp @objectlist_page.rows.map{|r| [r["id"], r["Zustand"], r["AuftragsNr."]] }
    puts
    STDOUT.flush
    sleep 5
    
    @objectlist_url = "https://otweb.kabelbw.com/otwg/GetObjectList.aspx?folder=831&path=&start=0&count=45&fieldpath=&request=0&guid=&sortcol=-1"    
    @agent.post(SHORTCUTBAR_URL, shortcuts_params["01. L4 Provide"])
    sleep 1
    @agent.get "https://otweb.kabelbw.com/otwg/OTWGObjectList.aspx?fld=831&ShortCutBarID=0"
    sleep 1
    fetch_frame112
    sleep 1
    @agent.get "https://otweb.kabelbw.com/otwg/Empty.htm"
    sleep 1
    fetch_frame122
    fetch_objectlist_page
    p @objectlist_page.total
    p @objectlist_page.zustands
    pp @objectlist_page.rows.map{|r| [r["id"], r["Zustand"], r["AuftragsNr."], r["Tatsächl. Abschlusszeit"]] }
    puts
    STDOUT.flush
    sleep 5

 		result = []
    frame122_zde_request(status_text, status_code)
    sleep 1
    fetch_objectlist_page
    sleep 1
    puts "rows total: #{@objectlist_page.total}"
    puts "rows on this page: #{@objectlist_page.rows.size}"
    
    p @objectlist_page.zustands
    pp @objectlist_page.rows.map{|r| [r["id"], r["Zustand"], r["AuftragsNr."]] }
    @objectlist_page.rows.each{|r| result << r }

    start = 45
    while (start < @objectlist_page.total) 
      @objectlist_url = "https://otweb.kabelbw.com/otwg/GetObjectList.aspx?folder=831&path=&start=#{start}&count=45&fieldpath=&request=0&guid=&sortcol=-1"
      start += 45
      fetch_objectlist_page
      sleep 1
      puts "rows total: #{@objectlist_page.total}"
      puts "rows on this page: #{@objectlist_page.rows.size}"
      p @objectlist_page.zustands
      pp @objectlist_page.rows.map{|r| [r["id"], r["Zustand"], r["AuftragsNr."]] }
			@objectlist_page.rows.each{|r| result << r }
    end
    return result #@objectlist_page.rows #.collect{|r| [r["id"], r["Zustand"], r["AuftragsNr."]] }
  end
  
  private

  class OtException < RuntimeError
    attr_reader :ot_page
    def initialize(page)
      @ot_page = page
    end
  end
  
  class OtPage < DelegateClass(Mechanize::Page)    
    include ScrapingUtils::NokoParser
    def initialize page
      @page = page
      if @page.respond_to? :root
        @doc = @page.root 
      else
        @doc = Nokogiri::HTML(@page.body)
      end
      create_asp_methods
      super @page
    end
    
    private
    def create_asp_methods
      ["__EVENTARGUMENT", "__EVENTTARGET", "__EVENTVALIDATION", "__VIEWSTATE"].each do |asp_input_id|
        method_name = asp_input_id.downcase.gsub(/^_+/,'')
        #puts "method_name: #{method_name}"
        #puts "asp_input_id: #{asp_input_id}"
        method_str =<<-HERE
          def #{method_name}
            @#{method_name} ||= (
              v = extract_string("//input[@id='#{asp_input_id}']/@value")
              #puts "#{method_name}:"
              #puts v
              v
            )
          end
        HERE
        self.class.class_eval(method_str)   
      end
    end
    
    def href2url href
      href = href.to_s.strip
      href.empty? ? nil : "https://otweb.kabelbw.com/otwg/#{href}"
    end
    
    def normalize_string str
      to_utf8(str).gsub(/[[:space:]]+/,' ').strip
    end
  end
  
  class LoginPage < OtPage
    def login_page?
      qs = [
        "//title[normalize-space(.)='OMNITRACKER Web Gateway Login']",
        "//input[@id='theloginbox']/@id",
        "//input[@id='thepasswordbox']/@id"
      ]
      qs.all?{|q| !@doc.xpath(q).empty? }
    end
  end
  
  class FramesetPage < OtPage
    def shortcutbar_frame_url
      @shortcutbar_frame_url ||= (
        q = "//frameset[@id]/frame[@id='OTWGShortCutBar']/@src"
        href2url extract_string(q)
      )
    end
    def objectlistframeset_frame_url
      @objectlist_frame_url ||= (
        q = "//frameset[@id]/frame[@id='Main']/@src"
        href2url extract_string(q)
      )
    end
    def frameset_page?
      @frameset_page ||= [shortcutbar_frame_url,objectlistframeset_frame_url].all?{|url| !url.nil? }
    end
  end
  
  class ShortcutbarPage < OtPage
    def change_to_folder_shortcuts_params
      @change_to_folder_shortcuts_params ||= (
        q = "//input[@type='submit' and @class='shortcutlink' and @ot_action_type='change_to_folder']"
        arr = @doc.xpath(q).map do |input|
          n = extract_string("./@name", input)  # e.g. "ctl00$ctl19"
          v = extract_string("./@value", input) # e.g. "SP NE4 Abrechnungsauftrag"
          [v, {n => v, "__EVENTVALIDATION" => eventvalidation, "__VIEWSTATE" => viewstate}]
        end
        Hash[arr]
      )
    end    
    def shortcutbar_page?
      [change_to_folder_shortcuts_params,
      eventvalidation,viewstate].all?{|x| !x.empty? }
    end
  end
  
  class Frame112Page < OtPage
    def title
      q = "//title[normalize-space(.)='OTWGObjectListFrame1_1_2']/."
      extract_strings q
    end
    def requests
      q = "//table[@id='TR_REQUESTS_TABLE']/tr/td/a[contains(@href,'__doPostBack')]/p/."
      extract_strings q
    end
    
    def frame112_page?
      frame112_page ||= [title, requests].all?{|x| !x.empty? }
    end
  end
  
  class Frame122Page < OtPage
    def title
      q = "//title[normalize-space(.)='OTWGObjectListFrame1_2_2']/."
      extract_strings q
    end
  
    def logout_btn
      q = "//div[@id='otBtnLogout']/."
      extract_strings(q)
    end
    
    def sf_lst_f
      q = "//div[@id='SF_LST_F']/@id"
      extract_strings q
    end
    
    def frame122_page?
      frame122_page ||= [title, logout_btn, sf_lst_f].all?{|x| !x.empty? }
    end
  end
  
  class ObjectlistPage < OtPage
    def initialize page
      super page
      @page = page
      @doc = Nokogiri::XML(@page.body)
    end
    
    def objectlist_page?
      #headers.size == 32
      headers.size > 2
    end
    
    def total
      q = "//rows/@total"
      extract_string(q).to_i
    end
    
    def headers
      @headers ||= (
        q = "//headers/header[position()>1]/."
        hs = %w(id icon) + extract_strings(q)
      )
    end
    
    def rows
      @rows ||= (
        q = "//rows/row"
        @doc.xpath(q).map do |row|
          row_arr = row.xpath("./col").to_a.each_with_index.map do |c,i|
            i+=1
            if i==1
              c_data = extract_string("./@icon",c)
            else
              c_data = extract_string("./.",c)
            end
            [headers[i],c_data]
          end
          row_arr.unshift( [headers[0], extract_string("./@id",row)] )
          Hash[row_arr]
        end
      )
    end
    
    def zustands
      rows.map{|row| row["Zustand"] }
    end
  end
end


=begin
ots = OtScraper.new
begin
  ots.get_job_list
ensure
  ots.logout
end
=end
