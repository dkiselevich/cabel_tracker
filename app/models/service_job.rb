# encoding: utf-8
class ServiceJob < ActiveRecord::Base
	audited
	has_one :service_appointment, :dependent => :destroy
	has_one :service_completion, :dependent => :destroy, :through => :service_appointment
	has_many :service_comments, :dependent => :destroy
	belongs_to :client
	belongs_to :scheduling_user_group

	attr_protected :id
	
	validates :client_id, :presence => {:message => "^Geben Sie einen Mandant an."}
	

	def kunde_string 
		"#{kunde_name} | #{adresse_short}"
	end

	def job_type
		jt = JobType.new
		jt.shortname = "ST"
		jt.name = "Enstörung"
		return jt
	end

  	def short
  		return "#{ot_auftragsnummer}"# | #{kunde_plz} #{kunde_ort} | #{kunde_strasse}#{kunde_hausnummer} | #{kunde_nachname}"
  	end

	def short_description
		return "#{ot_auftragsnummer} | #{adresse_short}"# | #{kunde_plz} #{kunde_ort} | #{kunde_strasse}#{kunde_hausnummer} | #{kunde_nachname}, #{kunde_vorname} | #{kunde_telefon}"
	end

	def adresse
		"#{kunde_plz} | #{kunde_ort}"# | #{kunde_strasse}#{kunde_hausnummer} | #{kunde_nachname}, #{kunde_vorname} | #{kunde_telefon}"
	end

	def adresse_short
		"#{kunde_plz} #{kunde_ort}, #{kunde_strasse}#{kunde_hausnummer}"
	end

	def kunde_name
		"#{kunde_vorname} #{kunde_nachname}"
	end

	def kunde_strasse_full
		"#{kunde_strasse} #{kunde_hausnummer}"
	end

	def kunde_telefon
		kunde_telefon_privat || kunde_telefon_mobil || kunde_telefon_geschaeftlich
	end

	def status_name
		return "zu disponieren" if (status == 0)
		return "disponiert" if (status == 1)
		return "wartend" if (status == 2)
		return "abgeschlossen" if (status == 3)
		return "technisch abgeschlossen" if (status == 4)
		return "storniert" if (status == 5)
	end

	def status_short
		return "ZDE" if (status == 0)
		return "DI" if (status == 1)
		return "WK" if (status == 2)
		return "AG" if (status == 3)
		return "TA" if (status == 4)
		return "OM" if (status == 5)
	end

	def abgeschlossen?
		((status == 3) || (status == 4))
	end

	def self.belonging_to_user_client(user)
		self.where(:client_id => user.clients.collect{|c|c.id})
	end

	def self.belonging_to_scheduling_user_group(user)
		if user.scheduling_user_group then
			self.where(:scheduling_user_group_id => user.scheduling_user_group.id)
		else
			self.where(:scheduling_user_group_id => -1)
		end
	end
end