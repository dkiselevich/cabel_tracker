# encoding: utf-8
class HasEigentuemerTelefonValidator < ActiveModel::Validator
  def validate(record)
    if record.eigentuemer_telefon_1.blank? && record.eigentuemer_telefon_2.blank? then
      record.errors[:base] << "Geben Sie mindestens eine Telefonnummer für den Eigentümer an."
    end
  end
end