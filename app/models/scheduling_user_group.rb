class SchedulingUserGroup < ActiveRecord::Base
	has_many :users
	has_many :postal_codes
	belongs_to :client
	validates_presence_of :name, :message => "^Geben Sie eine Bezeichnung an."
	validates_presence_of :client, :message => "^Geben Sie einen Mandant an."
	validates_presence_of :sender_email, :message => "^Geben Sie eine Absender-Email an."

	def postal_codes_csv
		postal_codes.collect {|postal_code| postal_code.code }.sort().join(",") 
	end

	def self.available_scheduling_users_for(controller)
	 	available_scheduling_users = Array.new
	    Client.is_assigned_to_user(controller).each do |client|
	      client.users.scheduling.where(:active => true).each do |user| 
	        available_scheduling_users << user
	      end
	    end 
	    return available_scheduling_users.uniq
	end
	
	def self.belonging_to_user_client(user)
		self.where(:client_id => user.clients.collect{|c|c.id})
	end
end
