class Bugtracker < ActiveRecord::Base
  attr_accessible :bug, :bug_solved, :description, :email, :location, :user
end
