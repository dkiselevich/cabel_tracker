# encoding: utf-8
class DatabaseHelper

	def self.reset_hard
		delete_database_models
		delete_job_models
		delete_users

		create_database_models
		create_admin
	end

	def self.delete_users
		User.delete_all
	end

	def self.create_admin
		u = User.new
		u.is_admin = true
		u.is_technician = true
		u.is_controller = true
		u.is_accountant = true
		u.is_boss = true
		u.vorname = "Frank"
		u.nachname = "Duffner"
		u.email = "frank.duffner@gmail.com"
		u.password = "asdasd"
		Client.all.each{|c| u.clients << c}
		u.save
	end

	def self.create_demo_users
		u = User.new
		u.is_technician = true
		u.vorname = "Hans"
		u.nachname = "Techniker"
		u.email = "techniker@seesys.de"
		u.password = "techniker"
		u.save

		u = User.new
		u.is_controller = true
		u.vorname = "Horst"
		u.nachname = "Disponent"
		u.email = "disponent@seesys.de"
		u.password = "disponent"
		u.save
	end

	def self.create_test_users
		u = User.new
		u.is_admin = true
		u.is_technician = true
		u.is_controller = true
		u.is_accountant = true
		u.is_boss = true
		u.vorname = "Daniel"
		u.nachname = "Meier"
		u.email = "daniel.meier@firma.de"
		u.password = "daniel"
		u.save

		u = User.new
		u.is_admin = true
		u.is_technician = true
		u.is_controller = true
		u.is_accountant = true
		u.is_boss = true
		u.vorname = "Martin"
		u.nachname = "Schmidt"
		u.email = "martin.schmidt@firma.de"
		u.password = "martin"
		u.save

		u = User.new
		u.is_admin = true
		u.is_technician = true
		u.is_controller = true
		u.is_accountant = true
		u.is_boss = true
		u.vorname = "Torsten"
		u.nachname = "Baum"
		u.email = "torsten.baum@firma.de"
		u.password = "torsten"
		u.save
	end

	def self.reset
		delete_job_models
	end

	def self.create_demo_data
		create_jobs(200)
		create_appointments
		create_jobs(20)
	end

	def self.delete_job_models
		p "deleting old jobs, appointments, cancellations, completions and appointment requests, etc."
		Job.delete_all
		Appointment.delete_all
		Cancellation.delete_all
		Completion.delete_all
		AppointmentRequest.delete_all
		SpecialExpense.delete_all
		TransferPoint.delete_all
		ServiceJob.delete_all
		ServiceAppointment.delete_all
		ServiceCompletion.delete_all
	end

	def self.delete_database_models
		Client.delete_all
		JobType.delete_all
		BuildingType.delete_all
		NetworkType.delete_all
		FiFuseType.delete_all
		ModemType.delete_all
		WaitingCause.delete_all
		SpecialExpense.delete_all
		ServiceUserGroup.delete_all
	end

	def self.create_database_models
		c = Client.new
		c.name = "3kabel"
		c.ot_login = "3Kabel GmbH"
		c.ot_password = "6k66"
		c.save

		jt = JobType.new
		jt.name = "Erstanschluss"
		jt.shortname = "EA"
		jt.color = "blue"
		jt.save
		jt = JobType.new
		jt.name = "Folgeanschluss"
		jt.shortname = "FA"
		jt.color = "green"
		jt.save

		bt = BuildingType.new
		bt.name = "Einfamilienhaus"
		bt.save
		bt = BuildingType.new
		bt.name = "Mehrfamilienhaus"
		bt.save

		nt = NetworkType.new
		nt.name = "unbekannt"
		nt.save
		nt = NetworkType.new
		nt.name = "Stern"
		nt.save
		nt = NetworkType.new
		nt.name = "Etagenstern"
		nt.save
		nt = NetworkType.new
		nt.name = "SingleLine"
		nt.save
		nt = NetworkType.new
		nt.name = "Mischnetz"
		nt.save
		nt = NetworkType.new
		nt.name = "Baum"
		nt.save

		ft = FiFuseType.new
		ft.name = "90004"
		ft.save
		ft = FiFuseType.new
		ft.name = "RF456"
		ft.save

		mt = ModemType.new
		mt.name = "Docsis 2.0"
		mt.save
		mt = ModemType.new
		mt.name = "Docsis 3.0"
		mt.save
		mt = ModemType.new
		mt.name = "AVM"
		mt.save

		create_waiting_causes
	end

	def self.create_waiting_causes
		wc = WaitingCause.new
		wc.code = "W01"
		wc.name = "Termin in Abstimmung"
		wc.ot_id = "16122943"
		wc.save

		wc = WaitingCause.new
		wc.code = "W02"
		wc.name = "Störung NE3"
		wc.ot_id = "16122947"
		wc.save

		wc = WaitingCause.new
		wc.code = "W03"
		wc.name = "WV - Vollausbau"
		wc.ot_id = "16122953"
		wc.save
		
		wc = WaitingCause.new
		wc.code = "W05"
		wc.name = "Warten auf Installationsgenehmigung"
		wc.ot_id = "11215440"
		wc.save
		
		wc = WaitingCause.new
		wc.code = "W08"
		wc.name = "Klärung Projektleiter"
		wc.ot_id = "18608314"
		wc.save
		
		wc = WaitingCause.new
		wc.code = "W09"
		wc.name = "Kunde am Netz - Nacharbeiten notwendig"
		wc.ot_id = "22148367"
		wc.save
		
		wc = WaitingCause.new
		wc.code = "W90"
		wc.name = "installiert - nicht abgerechnet"
		wc.ot_id = "27471000"
		wc.save
	end

	def self.create_jobs(n)
		p "creating #{n} jobs"
		n.times{create_job}
	end

	def self.create_job
		j = Job.new
		j.client = Client.where(:active => true).sample
		j.job_type = JobType.where(:active => true).sample
		j.workorder = rand(12332534654)
		j.kunde_kundennummer = rand(12332534654)
		j.ot_auftragsnummer = rand(12332534654).to_s+"-OT"
		j.ot_id = rand(12332534654)
		j.kunde_plz = get_plz
		j.kunde_ort = get_ort
		j.kunde_strasse = get_strasse
		j.kunde_hausnummer = get_hausnummer
		j.kunde_nachname = get_nachname
		j.kunde_vorname = get_vorname
		j.kunde_telefon_privat = get_telefon
		j.kunde_telefon_mobil = get_mobiltelefon
		j.kunde_telefon_geschaeftlich = get_telefon
		j.kunde_email = get_email(j.kunde_vorname, j.kunde_nachname)
		j.telefon_neu = get_telefon	
		j.eigentuemer_vorname = get_vorname		
		j.eigentuemer_nachname = get_nachname		
		j.eigentuemer_plz = get_plz		
		j.eigentuemer_ort = get_ort		
		j.eigentuemer_strasse = get_strasse	
		j.eigentuemer_hausnummer = get_hausnummer	
		j.eigentuemer_telefon_1 = get_telefon		
		j.eigentuemer_telefon_2 = (rand(2)==1) ? get_telefon : get_mobiltelefon
		j.eigentuemer_email = get_email(j.eigentuemer_vorname, j.eigentuemer_nachname)		
		j.eigentuemer_ansprechpartner = get_vorname+" "+get_nachname 
		j.eigentuemer_telefon_ansprechpartner = get_telefon
	    j.rks_anlagennummer = rand(12332534654)
	    j.building_type = BuildingType.where(:active => true).sample
	    j.wohneinheiten = rand(10)
	    j.aktive_kunden = rand(j.wohneinheiten)
	    j.network_type = NetworkType.where(:active => true).sample
	    j.erstinstallation_erfolgt = (rand(2) == 1)
	    j.status = 0
	    j.modem_type = ModemType.where(:active => true).sample
	    j.beauftragte_leistungen = "CleverAll Classic (1RN)\nSicherheitspaket\nTelefon CK10/CK16 (1+2RN)\nBE CleverAll Classic\nN-WLAN-Router Aktion"
	    j.save

		j.errors.each{|e| p e}
	end


	def self.create_appointment_for_job(job, user, day, start, ende)
		a = Appointment.new
		a.job = job
		a.user = user
		scheduling_users = User.where(:is_controller => true)		
		a.scheduling_user = scheduling_users[rand(scheduling_users.size)]
		a.day = day
		a.start_time = start
		a.end_time = ende
		a.save
		a.job.status = 1
		a.job.save
	end

	def self.create_appointments
		p "creating appointments"
		d = 0
		jobs_per_day = 5
		i = 0
		users = User.where(:active => true).where(:is_technician => true)
		u = 0
		h = 0
		
		days_per_user = (Job.count / jobs_per_day) / users.length

		Job.all.each do |j|

			user = users[u]
			day = Date.today + d.days
			start = Time.now.in_time_zone.beginning_of_day + 8.hours + h.hours + [0,15,30,45].sample.minutes
			ende = start + (1 + rand(2)).hours + [0,15,30,45].sample.minutes

			create_appointment_for_job(j,user,day,start,ende)
			h += 2
			i += 1
			if (i == jobs_per_day) then
				i = 0
				d += 1
				h = 0
			end

			if (d == days_per_user) then
				d = 0
				h = 0
				i = 0
				u += 1
			end
		end
		return nil
	end


	def self.get_ort
		[
			"Singen",
			"Konstanz",
			"Berlin",
			"München",
			"Radolfzell",
			"Engen",
			"Ehingen",
			"Koeln",
			"Dresden",
			"Frankfurt",
			"Hannover"
		].sample
	end

	def self.get_strasse
		[
			"Rathausplatz",
			"Parkallee",
			"Alemannenstr.",
			"Rheingasse",
			"Seestrasse",
			"Katzgasse",
			"Inselgasse",
			"Berndtstr.",
			"Bismarckplatz",
			"Frankfurter Strasse"
		].sample
	end

	def self.get_nachname
		[
			"Duffner",
			"Mueller",
			"Meier",
			"Mayer",
			"Schmidt",
			"Oetgers",
			"Storch",
			"Hund",
			"Wendig"
		].sample
	end

	def self.get_vorname
		[
			"Frank",
			"Peter",
			"Anna",
			"Christoph",
			"Martin",
			"Martina",
			"Daniel",
			"Torsten",
			"Anita",
			"Alexander"
		].sample
	end
	
	def self.get_plz
		5.times.map{ rand(9) }.join()
	end
	
	def self.get_hausnummer
		(1..99).to_a.sample.to_s + ((rand(2) == 1)?("a".."f").to_a.sample : "")
	end

	def self.get_telefon
		"0"+4.times.map{ rand(9) }.join()+"/"+5.times.map{ rand(9) }.join()
	end

	def self.get_mobiltelefon
		"01"+2.times.map{ rand(9) }.join()+"/"+8.times.map{ rand(9) }.join()
	end

	def self.get_email(v, n)
		[v+n, v, n, v+"."+n].sample + ((rand(2) == 1)?(1..99).to_a.sample.to_s : "")+ "@" + ["gmail.com","web.de","gmx.de", "gmx.net", "hotmail.com"].sample
	end

end