 # encoding: utf-8
 class Cancellation < ActiveRecord::Base
	belongs_to :scheduling_user, :foreign_key => "scheduling_user_id", :class_name => "User"
	belongs_to :appointment
	has_one :job, :through => :appointment
	belongs_to :user, :foreign_key => "technician_id"
	has_many :photos, :dependent => :destroy
	accepts_nested_attributes_for :photos, :allow_destroy => true

	validates :appointment, :presence => {:message => "^Abschluss muss zu einem Termin gehören."}
	validates :user, :presence => {:message => "^Abschluss muss zu einem Techniker gehören."}
	
	validates :anzahl_der_etagen, :presence => {:message => "^Füllen Sie das Feld 'Anzahl der Etagen' aus."}
	validates :maximale_anzahl_HH_auf_etage, :presence => {:message => "^Füllen Sie das Feld 'maximale Anzahl der HH auf Etage aus' aus."}
	validates :netztopologie_id, :presence => {:message => "^Geben Sie eine 'Netztopologie' an."}
	validates :mitversorgung, :inclusion => {:in => [true, false], :message => "^Füllen Sie das Feld 'Mitversorgung' aus."}
	validates :cause_id, :presence => {:message => "^Geben Sie den Klärungsfallsgrund an."}

	validates :installation_verweigerer_id, :presence => {:message => "^Füllen Sie das Feld 'Inst. verweigert durch' aus.", :if => :keine_installation_moeglich?}

	validates :up_rohre_vorhanden_und_geeignet, :inclusion => {:in => [true, false], :message => "^Füllen Sie das Feld 'UP-Rohre Tel./TV vorhanden und geeignet' aus."}
	validates :up_rohre_tausch_hinderungsgrund_id, :presence => {:message => "^Füllen Sie das Feld 'vorhandene Kabel können nicht getauscht werden' aus."}
	validates :ap_verlegung_option_id, :presence => {:message => "^Füllen Sie das Feld 'AP Verlegung moeglich ueber' aus."}
	validates :stromeinspeisung_per_dach_moeglich, :inclusion => {:in => [true, false], :message => "^Füllen Sie das Feld 'Stromeinspeisung ueber Dach' aus."}
	validates :pot_erstellung_moeglich, :inclusion => {:in => [true, false], :message => "^Füllen Sie das Feld 'POT Erstellung möglich' aus."}

	validates :installation_verweigerer_id, :presence => {:message => "^Füllen Sie das Feld 'Inst. verweigert durch' aus."}, :if => :keine_installation_moeglich?
	validates :verweigerer_nachname, :presence => {:message => "^Füllen Sie das Feld 'Nachname' aus.", :if => :keine_installation_moeglich?}
	validates :verweigerer_vorname, :presence => {:message => "^Füllen Sie das Feld 'Vorname' aus.", :if => :keine_installation_moeglich?}
	validates :verweigerer_telefon, :presence => {:message => "^Füllen Sie das Feld 'Telefon' aus.", :if => :keine_installation_moeglich?}

	validates :eigentuemer_im_haus, :inclusion => {:in => [true, false], :message => "^Füllen Sie das Feld Eigentümer im Haus' aus."}, :if => :genehmigung_notwendig?
	validates :eigentuemer_nachname, :presence => {:message => "^Füllen Sie das Feld 'Eigentümer Nachname' aus.", :if => :eigentuemer_nicht_im_haus?}
	validates :eigentuemer_vorname, :presence => {:message => "^Füllen Sie das Feld 'Eigentümer Vorname' aus.", :if => :eigentuemer_nicht_im_haus?}
	validates :eigentuemer_telefon, :presence => {:message => "^Füllen Sie das Feld 'Telefon' aus.", :if => :eigentuemer_nicht_im_haus?}

	validates :verweigerungsgrund_id, :presence => {:message => "^Füllen Sie das Feld 'Installation verweigert wegen' oder das Feld 'zu hoher Aufwand für' aus.", :unless => :genehmigung_notwendig?}
	validates :verweigerungsgrund_sonstiges_beschreibung, :presence => {:message => "^Füllen Sie das Feld 'andere Gründe' aus.", :if => :verweigerungsgrund_is_other?}
	validates :zu_hoher_aufwand_beschreibung, :presence => {:message => "^Füllen Sie das Feld 'Beschreibung des zu hohen Aufwands' aus.", :if => :zu_hoher_aufwand_id}

	def short_description
		"erstellt am "+created_at.strftime(Settings.TIME_FORMAT)+" durch "+user.full_name
	end

	def self.belonging_to_user_client(user)
		self.includes(:job).where("jobs.client_id in (#{user.clients.collect{|c| c.id}.join(',')})")
	end
	
	private
		def eigentuemer_nicht_im_haus?
			eigentuemer_im_haus == false
		end
		def verweigerungsgrund_is_other?
			(verweigerungsgrund_id == 3)
		end

		def keine_installation_moeglich?
			cause_id == 0
		end

		def genehmigung_notwendig?
			cause_id == 1
		end


end
