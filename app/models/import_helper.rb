require 'nokogiri'
require 'net/ftp'
require 'fileutils'
class ImportHelper
  def self.add_xml_files_to_job(client_id)
    begin
      directory = "#{Rails.root}/data/"
      unless File.directory?("#{Rails.root}/data")
        FileUtils.mkdir "#{Rails.root}/data"
      end
      Net::FTP.open('88.84.155.141') do |ftp|
        if ftp.closed?
          puts "FTP server closed"
        else  
          ftp.login('laolaos', 'lollo')
          ftp.chdir('pub')
          files = ftp.nlst()
          files.each do |file|
            last_modify = ftp.mtime(file, false) #false to get UTC time
            if last_modify >= Time.now.ago(3600).utc && last_modify <= Time.now.utc
              ftp.getbinaryfile(file, directory + File.basename(file))
            end
          end
        end
      end 
      files = Dir.entries(directory).reject{|f| (f == "." || f == "..")}
      files.each do |xml_name|
        next unless File.exist?("#{directory}/#{xml_name}")
        file = File.new directory + xml_name
        self.add_job(xml_name, client_id)
        File.delete(file.path)
      end
      puts "Add jobs successfully for #{xml_name}"
    rescue Exception => e
      puts e.message
    end
  end
  
  def self.add_job(xml_name, client_id, postal_code = nil)
    begin
      xml_reader = Nokogiri.XML(File.open(Rails.root.to_s + '/data/' + File.basename(xml_name)))
      jobs = []
      job_attrs = {}
      xml_reader.xpath("//Order").each_with_index do |order, j|
        elements = Nokogiri::XML.parse(order.to_s).root.children
        elements.each_with_index do |node, i|
          next if node.name == 'text'
          begin
            job_attrs = { 
             "client_id"=> client_id,
             "auftragsart_id"=> -1,
             "workorder"=> elements[57].content,
             "ot_auftragsnummer"=> elements[27].content,
             "kunde_plz"=> elements[15].content,
             "kunde_ort"=> elements[17].content,
             "kunde_strasse"=> elements[19].content,
             "kunde_hausnummer"=> [elements[21].content,elements[23].content].join(" "),
             "kunde_nachname"=> elements[3].content,
             "kunde_vorname"=> elements[5].content,
             "kunde_telefon_privat"=> elements[7].content,
             "kunde_telefon_mobil"=> elements[11].content,
             "kunde_telefon_geschaeftlich"=> elements[9].content,
             "kunde_email"=> elements[13].content,
             "telefon_neu"=> "unbekannt",
             "eigentuemer_vorname"=> "unbekannt",
             "eigentuemer_nachname"=> "unbekannt",
             "eigentuemer_plz"=> "unbekannt",
             "eigentuemer_ort"=> "unbekannt",
             "eigentuemer_strasse"=>"unbekannt",
             "eigentuemer_telefon_1"=> "unbekannt",
             "eigentuemer_telefon_2"=> "unbekannt",
             "eigentuemer_email"=> "unbekannt",
             "eigentuemer_ansprechpartner"=> "unbekannt",
             "eigentuemer_telefon_ansprechpartner"=> "unbekannt",
             "rks_anlagennummer"=> elements[41].content,
             "objektart_id"=> -1,
             "wohneinheiten"=> elements[39].content,
             "aktive_kunden"=> elements[39].content,
             "netztopologie_id"=> -1,
             "erstinstallation_erfolgt"=> elements[13].content,
             "status" => 1,
             "kunde_kundennummer"=> elements[1].content,
             "eigentuemer_hausnummer"=> "unbekannt",
             "kunde_is_eigentuemer"=>false,
             "modemtyp_id"=> elements[71].content,
             "modemaktivierung"=> elements[75].content,
             "beauftragte_leistungen"=> "unbekannt",
             "verbindlicher_installationstermin"=> elements[61].content,
             "ot_id"=> "unbekannt",
             "verbindliche_tageszeit"=> elements[63].content,
             "uep_id"=> elements[49].content,
             "scheduling_user_group_id"=> postal_code,
             "clerver_pro"=> elements[67].content,
             "unbunled"=> elements[69].content
            }
          rescue Exception => e
            puts "******************XML invalid node******************"
            puts e.message
            next
          end
        end
        # Save to jobs table
        unless job_attrs.empty?
          job = Job.new(job_attrs)
          if job.save
            jobs << job
          else
            puts "****Invalid for some fields caused by node of <Order>[#{j}]***"
            puts job.errors.full_messages
          end
        end
      end
      puts "******************Results for jobs*******************"
      puts [jobs.count, "jobs have been just created successfully"].join(" ")
    rescue Exception => e
      puts "******************XML invalid******************"
      puts e.message
    end 
  end
end

