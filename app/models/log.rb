class Log < ActiveRecord::Base
  attr_accessible :text
  
  
  def self.search(search, type)
    if search
        case type
            when "text"
                where('text LIKE ?', "%#{search}%")
            when "user"
                where('tbl LIKE \'user\' AND text LIKE ?', "%#{search}%")
            when "appointment"
                where('tbl LIKE \'appointment\' AND text LIKE ?', "%#{search}%")
            when "service_appointment"
                where('tbl LIKE \'service_appointment\' AND text LIKE ?', "%#{search}%")   
            when "system"
                where('tbl IS NULL AND text LIKE ?', "%#{search}%")                                                
        end
    else
      scoped # find(:all)
    end
  end

end
