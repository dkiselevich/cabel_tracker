# encoding: utf-8
class ServiceCompletion < ActiveRecord::Base
	belongs_to :client
	belongs_to :service_appointment
	belongs_to :technician, :class_name => "User" , :foreign_key => "technician_id" 
	has_one :service_job, :through => :service_appointment

	validates :service_type_id, :presence => {:message => "^Geben Sie die Auftragsart an."}
	validates :invoice_option_id, :presence => {:message => "^Geben Sie die Kostenart an."}

	validates :transfer_point_reading, :presence => {:message => "^Geben Sie den Messwert für den ÜP an."}
	validates :amplifier_reading, :presence => {:message => "^Geben Sie den Messwert für den Verstärker an."}
	validates :mmd_reading, :presence => {:message => "^Geben Sie den Messwert für MMD an."}
  validates :signature, :presence => {:message => "^Der Kunde muss im Unterschriftsfeld unterschreiben."}

  def job
    service_job
  end
    
	def create_signature(signature, ot_auftragsnummer)
    PdfHelper.create_signature(signature, ot_auftragsnummer)
  end

	def short_description
		"erstellt am "+created_at.strftime(Settings.TIME_FORMAT)+" durch "+technician.full_name
	end

	def self.belonging_to_user_client(user)
		self.includes(:service_job).where("service_jobs.client_id in (#{user.clients.collect{|c| c.id}.join(',')})")
	end

	def call_out_cost_sum_before_taxes
		self.call_out_cost_per_unit*self.call_out_cost_quantity
	end	
	def call_out_cost_sum_after_taxes
		self.call_out_cost_sum_before_taxes*1.19
	end

	def service_time_cost_sum_before_taxes
		self.service_time_cost_per_unit*self.service_time_cost_quantity
	end	
	def service_time_cost_sum_after_taxes
		self.service_time_cost_sum_before_taxes*1.19
	end

	def additional_cost_sum_before_taxes
		self.additional_cost_per_unit*self.additional_cost_quantity
	end	
	def additional_cost_sum_after_taxes
		self.additional_cost_sum_before_taxes*1.19
	end

	def amplifier_sum_before_taxes
		self.amplifier_cost_per_unit
	end	
	def amplifier_sum_after_taxes
		self.amplifier_sum_before_taxes*1.19
	end

	def transfer_point_cost_sum_before_taxes
		self.transfer_point_cost_per_unit
	end	
	def transfer_point_cost_sum_after_taxes
		self.transfer_point_cost_sum_before_taxes*1.19
	end

	def mmd_cost_sum_before_taxes
		self.mmd_cost_per_unit
	end	
	def mmd_cost_sum_after_taxes
		self.mmd_cost_sum_before_taxes*1.19
	end

	def material_1_sum_before_taxes
		self.material_quantity_1_cost_per_unit*self.material_quantity_1
	end	
	def material_1_sum_after_taxes
		self.material_1_sum_before_taxes*1.19
	end
	def material_2_sum_before_taxes
		self.material_quantity_2_cost_per_unit*self.material_quantity_2
	end	
	def material_2_sum_after_taxes
		self.material_2_sum_before_taxes*1.19
	end
	def material_3_sum_before_taxes
		self.material_quantity_3_cost_per_unit*self.material_quantity_3
	end	
	def material_3_sum_after_taxes
		self.material_3_sum_before_taxes*1.19
	end
	def material_4_sum_before_taxes
		self.material_quantity_4_cost_per_unit*self.material_quantity_4
	end	
	def material_4_sum_after_taxes
		self.material_4_sum_before_taxes*1.19
	end
	def material_5_sum_before_taxes
		self.material_quantity_5_cost_per_unit*self.material_quantity_5
	end	
	def material_5_sum_after_taxes
		self.material_5_sum_before_taxes*1.19
	end

	def total_before_taxes
		call_out_cost_sum_before_taxes +
      service_time_cost_sum_before_taxes +
      additional_cost_sum_before_taxes +
      amplifier_sum_before_taxes +
      transfer_point_cost_sum_before_taxes +
      mmd_cost_sum_before_taxes +

      material_1_sum_before_taxes +
      material_2_sum_before_taxes +
      material_3_sum_before_taxes +
      material_4_sum_before_taxes +
      material_5_sum_before_taxes
	end
	def total_after_taxes
		total_before_taxes*1.19
	end


	def additional_attachment_1_url
		additional_attachment_1_file_name.blank? ? "/assets/blank.gif" : "#{Settings.COMPLETION_IMAGES_BASE_URL}#{job.id}/additional_attachment_1_file_name/#{additional_attachment_1_file_name}"
	end
	def additional_attachment_2_url
		additional_attachment_2_file_name.blank? ? "/assets/blank.gif" : "#{Settings.COMPLETION_IMAGES_BASE_URL}#{job.id}/additional_attachment_2_file_name/#{additional_attachment_2_file_name}"
	end
	def additional_attachment_3_url
		additional_attachment_3_file_name.blank? ? "/assets/blank.gif" : "#{Settings.COMPLETION_IMAGES_BASE_URL}#{job.id}/additional_attachment_3_file_name/#{additional_attachment_3_file_name}"
	end
	def additional_attachment_4_url
		additional_attachment_4_file_name.blank? ? "/assets/blank.gif" : "#{Settings.COMPLETION_IMAGES_BASE_URL}#{job.id}/additional_attachment_4_file_name/#{additional_attachment_4_file_name}"
	end
	def additional_attachment_5_url
		additional_attachment_5_file_name.blank? ? "/assets/blank.gif" : "#{Settings.COMPLETION_IMAGES_BASE_URL}#{job.id}/additional_attachment_5_file_name/#{additional_attachment_5_file_name}"
	end
	def additional_attachment_6_url
		additional_attachment_6_file_name.blank? ? "/assets/blank.gif" : "#{Settings.COMPLETION_IMAGES_BASE_URL}#{job.id}/additional_attachment_6_file_name/#{additional_attachment_6_file_name}"
	end
	def additional_attachment_7_url
		additional_attachment_7_file_name.blank? ? "/assets/blank.gif" : "#{Settings.COMPLETION_IMAGES_BASE_URL}#{job.id}/additional_attachment_7_file_name/#{additional_attachment_7_file_name}"
	end
	def additional_attachment_8_url
		additional_attachment_8_file_name.blank? ? "/assets/blank.gif" : "#{Settings.COMPLETION_IMAGES_BASE_URL}#{job.id}/additional_attachment_8_file_name/#{additional_attachment_8_file_name}"
	end
	def additional_attachment_9_url
		additional_attachment_9_file_name.blank? ? "/assets/blank.gif" : "#{Settings.COMPLETION_IMAGES_BASE_URL}#{job.id}/additional_attachment_9_file_name/#{additional_attachment_9_file_name}"
	end
	def additional_attachment_10_url
		additional_attachment_10_file_name.blank? ? "/assets/blank.gif" : "#{Settings.COMPLETION_IMAGES_BASE_URL}#{job.id}/additional_attachment_10_file_name/#{additional_attachment_10_file_name}"
	end
end
