# encoding: utf-8
require 'zip'

class Completion < ActiveRecord::Base

  belongs_to :scheduling_user, :foreign_key => "scheduling_user_id", :class_name => "User"
  belongs_to :appointment
  has_one :job, :through => :appointment
  belongs_to :technician, :class_name => "User" , :foreign_key => "technician_id"
# has_many :completion_photos, :dependent => :destroy #sonstige fotos
  belongs_to :job_type, :foreign_key => "auftragsart_id"
  belongs_to :network_type, :foreign_key => "netztopologie_id"



=begin

  has_attached_file :signature_photo,
    :styles =>{
      :thumb  => "100x100",
      :medium => "200x200",
      :large => "600x400"
    },
    :storage => :s3,
    :s3_credentials => "#{Rails.root.to_s}/config/amazon_s3.yml",
    :path => "/completions/:style/:id/:filename",
    :processors => [:sig]



  has_attached_file :messwerte_photo,
    :styles =>{
      :thumb  => "100x100",
      :medium => "200x200",
      :large => "600x400"
    },
    :storage => :s3,
    :s3_credentials => "#{Rails.root.to_s}/config/s3.yml",
      :path => "/completions/:style/:id/:filename"

  has_attached_file :bvt_photo,
    :styles =>{
      :thumb  => "100x100",
      :medium => "200x200",
      :large => "600x400"
    },
    :storage => :s3,
    :s3_credentials => "#{Rails.root.to_s}/config/s3.yml",
      :path => "/completions/:style/:id/:filename"

  has_attached_file :uep_photo,
  :styles =>{
  :thumb  => "100x100",
  :medium => "200x200",
  :large => "600x400"
  },
  :storage => :s3,
  :s3_credentials => "#{Rails.root.to_s}/config/s3.yml",
  :path => "/completions/:style/:id/:filename"

  has_attached_file :pot_schiene_photo,
  :styles =>{
  :thumb  => "100x100",
  :medium => "200x200",
  :large => "600x400"
  },
  :storage => :s3,
  :s3_credentials => "#{Rails.root.to_s}/config/s3.yml",
  :path => "/completions/:style/:id/:filename"

  has_attached_file :pot_gas_photo,
  :styles =>{
  :thumb  => "100x100",
  :medium => "200x200",
  :large => "600x400"
  },
  :storage => :s3,
  :s3_credentials => "#{Rails.root.to_s}/config/s3.yml",
  :path => "/completions/:style/:id/:filename"

  has_attached_file :pot_wasser_photo,
  :styles =>{
  :thumb  => "100x100",
  :medium => "200x200",
  :large => "600x400"
  },
  :storage => :s3,
  :s3_credentials => "#{Rails.root.to_s}/config/s3.yml",
  :path => "/completions/:style/:id/:filename"

  has_attached_file :pot_hak_photo,
  :styles =>{
  :thumb  => "100x100",
  :medium => "200x200",
  :large => "600x400"
  },
  :storage => :s3,
  :s3_credentials => "#{Rails.root.to_s}/config/s3.yml",
  :path => "/completions/:style/:id/:filename"

  has_attached_file :pot_heizung_photo,
  :styles =>{
  :thumb  => "100x100",
  :medium => "200x200",
  :large => "600x400"
  },
  :storage => :s3,
  :s3_credentials => "#{Rails.root.to_s}/config/s3.yml",
  :path => "/completions/:style/:id/:filename"

  has_attached_file :pot_oel_photo,
  :styles =>{
  :thumb  => "100x100",
  :medium => "200x200",
  :large => "600x400"
  },
  :storage => :s3,
  :s3_credentials => "#{Rails.root.to_s}/config/s3.yml",
  :path => "/completions/:style/:id/:filename"

  has_attached_file :mmd_unbundled_photo,
  :styles =>{
  :thumb  => "100x100",
  :medium => "200x200",
  :large => "600x400"
  },
  :storage => :s3,
  :s3_credentials => "#{Rails.root.to_s}/config/s3.yml",
  :path => "/completions/:style/:id/:filename"


    validates_attachment_presence :messwerte_photo, :message => "^Fügen Sie ein Foto 'Messwerte' hinzu."
  validates_attachment_size :messwerte_photo, :less_than => 5.megabytes
  validates_attachment_content_type :messwerte_photo, :content_type => ['image/jpeg', 'image/png']

    validates_attachment_presence :bvt_photo, :message => "^Fügen Sie ein Foto 'BVT' hinzu."
  validates_attachment_size :bvt_photo, :less_than => 5.megabytes
  validates_attachment_content_type :bvt_photo, :content_type => ['image/jpeg', 'image/png']

    validates_attachment_presence :uep_photo, :message => "^Fügen Sie ein Foto 'ÜP' hinzu."
  validates_attachment_size :uep_photo, :less_than => 5.megabytes
  validates_attachment_content_type :uep_photo, :content_type => ['image/jpeg', 'image/png']

    validates_attachment_presence :mmd_unbundled_photo, :message => "^Fügen Sie ein Foto 'MMD unbundelt' hinzu."
  validates_attachment_size :mmd_unbundled_photo, :less_than => 5.megabytes
  validates_attachment_content_type :mmd_unbundled_photo, :content_type => ['image/jpeg', 'image/png']

    validates_attachment_presence :pot_schiene_photo, :message => "^Fügen Sie ein Foto 'POT-Schiene' hinzu."
  validates_attachment_size :pot_schiene_photo, :less_than => 5.megabytes
  validates_attachment_content_type :pot_schiene_photo, :content_type => ['image/jpeg', 'image/png']

    validates_attachment_presence :pot_gas_photo, :message => "^Fügen Sie ein Foto 'POT-GAS' hinzu."
  validates_attachment_size :pot_gas_photo, :less_than => 5.megabytes
  validates_attachment_content_type :pot_gas_photo, :content_type => ['image/jpeg', 'image/png']

    validates_attachment_presence :pot_wasser_photo, :message => "^Fügen Sie ein Foto 'POT-Wasser' hinzu."
  validates_attachment_size :pot_wasser_photo, :less_than => 5.megabytes
  validates_attachment_content_type :pot_wasser_photo, :content_type => ['image/jpeg', 'image/png']

    validates_attachment_presence :pot_hak_photo, :message => "^Fügen Sie ein Foto 'POT-HAK' hinzu."
  validates_attachment_size :pot_hak_photo, :less_than => 5.megabytes
  validates_attachment_content_type :pot_hak_photo, :content_type => ['image/jpeg', 'image/png']

    validates_attachment_presence :pot_heizung_photo, :message => "^Fügen Sie ein Foto 'POT-Heizung' hinzu."
  validates_attachment_size :pot_heizung_photo, :less_than => 5.megabytes
  validates_attachment_content_type :pot_heizung_photo, :content_type => ['image/jpeg', 'image/png']

    validates_attachment_presence :pot_oel_photo, :message => "^Fügen Sie ein Foto 'POT-Öl' hinzu."
	validates_attachment_size :pot_oel_photo, :less_than => 5.megabytes
	validates_attachment_content_type :pot_oel_photo, :content_type => ['image/jpeg', 'image/png']
=end


	validates :appointment, :presence => {:message => "^Abschluss muss zu einem Termin gehören."}
	validates :technician, :presence => {:message => "^Abschluss muss zu einem Techniker gehören."}

	validates :clever_pro, :inclusion => {:in => [true, false], :message => "^Füllen Sie das Feld 'CleverPro' aus."}
	validates :unbundled, :inclusion => {:in => [true, false], :message => "^Füllen Sie das Feld 'Unbundled' aus."}
	validates :modemtyp_id, :presence => {:message => "^Geben Sie einen Modemtyp an."}
	validates :umzug, :inclusion => {:in => [true, false], :message => "^Füllen Sie das Feld 'Umzug' aus."}

	validates :installationsart_id, :presence => {:message => "^Geben Sie eine 'Installationsart' an."}
	validates :installationsart_sonstiges, :presence => {:message => "^Geben Sie eine 'Installationsart' an.", :if => :installationsart_is_other?}
	validates :fi_sicherung_id, :presence => {:message => "^Geben Sie eine 'FI Sicherung' an."}
	validates :potentialausgleich_id, :presence => {:message => "^Geben Sie einen 'Potentialausgleich' an."}
	validates :netztopologie_id, :presence => {:message => "^Geben Sie eine 'Netztopologie' an."}

	validates :vorverstaerker_id, :presence => {:message => "^Geben Sie einen 'Vorverstärker' an."}
	#validates :vorverstaerker_bezeichnung, :presence => {:message => "^Geben Sie eine 'Vorverstärkerbezeichnung' an.", :if => :vorverstaerker?}
	validates :geschlossener_bvt, :inclusion => {:in => [true, false], :message => "^Füllen Sie das Feld 'geschlossener BVT' aus." }
  #	validates_presence_of :linienverstaerker_id, :message => "^Geben Sie einen 'Linienverstärker' an."
  #	validates :mehraufwand, :inclusion => {:in => [true, false], :message => "^Füllen Sie das Feld 'Mehraufwand' aus."}

	validates :auftragsart_id, :presence => {:message => "^Geben Sie eine 'Auftragsart' an."}
  #	validates_presence_of :dosentausch, :message => "^Füllen Sie das Feld 'Dosentausch' aus."
	validates :kellerquerverkabelung, :inclusion => {:in => [true, false], :message => "^Füllen Sie das Feld 'Kellerquerverkabelung' aus."}
	validates :kellerquerverkabelung_laenge, :presence => {:message => "^Geben Sie die 'Länge der Kellerquerverkabelung' an.", :if => :kellerquerverkabelung?}

	validates :wohneinheiten, :presence => {:message => "^Geben Sie die 'Anzahl der WE im Objekt' an."}

	validates :D130, :presence => {:message => "^Füllen Sie das Feld 'D130' aus."}
	validates :K5, :presence => {:message => "^Füllen Sie das Feld 'K5' aus."}
	validates :K29, :presence => {:message => "^Füllen Sie das Feld 'K29' aus."}
	validates :D802, :presence => {:message => "^Füllen Sie das Feld 'D802' aus."}

	validates :HFC_MAC, :presence => {:message => "^Geben Sie die 'MAC-Adresse' an."}
	validates :adresspruefung, :inclusion => {:in => [true, false], :message => "^Füllen Sie das Feld 'Adresspruefung' aus."}

	validates :uebrige_modems_im_gebaeude_per_SBC_portal_geprueft, :inclusion => {:in => [true, false], :message => "^Füllen Sie das Feld 'Übrige Modems im Gebäude per SBC Portal geprueft' aus."}
	validates :tv_empfang_beim_kunden_erfolgreich_geprueft, :inclusion => {:in => [true, false], :message => "^Füllen Sie das Feld 'TV Empfang beim Kunden erfolgreich geprueft' aus."}
	validates :internetzugang_mit_montagelaptop_geprueft, :inclusion => {:in => [true, false], :message => "^Füllen Sie das Feld 'Internetzugang mit Montagelaptop geprueft' aus."}
	validates :telefon_abgehend_erfolgreich_getestet, :inclusion => {:in => [true, false], :message => "^Füllen Sie das Feld 'Telefon abgehend erfolgreich getestet' aus."}
	validates :telefon_ankommend_erfolgreich_getestet, :inclusion => {:in => [true, false], :message => "^Füllen Sie das Feld 'Telefon ankommend erfolgreich gestestet' aus."}


=begin
	# auskommentiert fürs Testen, James
	validates :messwerte_photo_file_name, :presence => {:message => "^Fügen Sie ein Foto 'Messwerte' hinzu."}

	validates :bvt_photo_file_name, :presence => {:message => "^Fügen Sie ein Foto 'BVT' hinzu.", :unless => :no_access_to_bvt}
	validates :uep_photo_file_name, :presence => {:message => "^Fügen Sie ein Foto 'ÜP' hinzu.", :unless => :no_access_to_uep}
	validates :pot_schiene_photo_file_name, :presence => {:message => "^Fügen Sie ein Foto 'POT-Schiene' hinzu.", :unless => :no_access_to_pot_schiene}

	validates :pot_gas_photo_file_name, :presence => {:message => "^Fügen Sie ein Foto 'POT-Gas' hinzu.", :unless => :no_access_to_pot_gas_required}
	validates :pot_wasser_photo_file_name, :presence => {:message => "^Fügen Sie ein Foto 'POT-Wasser' hinzu.", :unless => :no_access_to_pot_wasser_required}
	validates :pot_heizung_photo_file_name, :presence => {:message => "^Fügen Sie ein Foto 'POT-Heizung' hinzu.", :unless => :no_access_to_pot_heizung_required}
	validates :pot_hak_photo_file_name, :presence => {:message => "^Fügen Sie ein Foto 'POT-Hak' hinzu.", :unless => :no_access_to_pot_hak_required}
	validates :pot_oel_photo_file_name, :presence => {:message => "^Fügen Sie ein Foto 'POT-Öl' hinzu.", :unless => :no_access_to_pot_oel_required}
=end

  #validates_attachment_size :pot_oel_photo_file_size, :less_than => 5.megabytes
  #validates_attachment_content_type :pot_oel_photo_content_type, :content_type => ['image/jpeg', 'image/png']



=begin
  validates_presence_of :pot_gas_photo_file_name, :message => "^Fügen Sie ein Foto 'POT-Gas' hinzu."
  validates_presence_of :pot_wasser_photo_file_name, :message => "^Fügen Sie ein Foto 'POT-Wasser' hinzu."
  validates_presence_of :pot_hak_photo_file_name, :message => "^Fügen Sie ein Foto 'POT-HAK' hinzu."
  validates_presence_of :pot_heizung_photo_file_name, :message => "^Fügen Sie ein Foto 'POT-Heizung' hinzu."
  validates_presence_of :pot_oel_photo_file_name, :message => "^Fügen Sie ein Foto 'POT-Öl' hinzu."
  validates_presence_of :mmd_unbundled_photo_file_name, :message => "^Fügen Sie ein Foto 'MMD unbundled' hinzu."
=end

  #validates :signature, :presence => {:message => "^Der Kunde muss im Unterschriftsfeld unterschreiben.", :unless => :signature_photo_file_name}

  def no_access_to_pot_gas_required
    true if no_access_to_pot_gas || potentialausgleich_id == 2
  end

  def no_access_to_pot_wasser_required
    true if no_access_to_pot_wasser || potentialausgleich_id == 2
  end

  def no_access_to_pot_hak_required
    true if no_access_to_pot_hak || potentialausgleich_id == 2
  end

  def no_access_to_pot_heizung_required
    true if no_access_to_pot_heizung || potentialausgleich_id == 2
  end

  def no_access_to_pot_oel_required
    true if no_access_to_pot_oel || potentialausgleich_id == 2
  end

  def installationsart_is_other?
    (installationsart_id == 1)
  end

  def is_folgeauftrag?
    (job_type.shortname == "FA")
  end

  def kellerquerverkabelung?
    (kellerquerverkabelung == 1)
  end

  def vorverstaerker?
    (vorverstaerker_id == 1) || (vorverstaerker_id == 2)
  end

  def herstellung_potentialausgleich?
    (potentialausgleich_id == 1)
  end

  def herstellung_potentialausgleich_and_heizung?
    (potentialausgleich_id == 1) && (!pot_heizung_photo.blank?)
  end

  def short_description
    ["erstellt am", created_at.strftime(Settings.TIME_FORMAT), "durch", technician.full_name].join(" ")
  end

  def self.belonging_to_user_client(user)
    self.includes(:job).where("jobs.client_id in (#{user.clients.collect{|c| c.id}.join(',')})")
  end

  def create_zip(html)
    filename = "#{job.kunde_plz} #{job.kunde_ort} #{job.kunde_strasse} #{job.kunde_hausnummer}"
#   filename.gsub!(" ","_")
    filename.gsub!(".","")
    filename.gsub!("ß","ss")
    filename.gsub!("ü","ue")
    filename.gsub!("ö","oe")
    filename.gsub!("ä","ae")
    filename.gsub!("Ü","Ue")
    filename.gsub!("Ö","Oe")
    filename.gsub!("Ä","Ae")
    filename.gsub!("/","-")

    file_prefix = "#{job.kunde_plz} #{job.kunde_ort}, #{job.kunde_strasse} #{job.kunde_hausnummer}, #{job.ot_auftragsnummer}, #{job.kunde_nachname}, #{job.kunde_vorname}, "

    file_prefix.gsub!("ß","ss")
    file_prefix.gsub!("ü","ue")
    file_prefix.gsub!("ö","oe")
    file_prefix.gsub!("ä","ae")
    file_prefix.gsub!("Ü","Ue")
    file_prefix.gsub!("Ö","Oe")
    file_prefix.gsub!("Ä","Ae")
    file_prefix.gsub!("/","-")

    file_path = "#{filename}/"
    bundle_filename = "#{Rails.root.to_s}/tmp/"+filename
    pdf_name = "#{filename}.pdf"
    temp_pdf_name = "#{id}.pdf"

    pdfkit_instance = PDFKit.new(html)
    pdfkit_instance.to_file  "#{Rails.root.to_s}/tmp/#{temp_pdf_name}"

    file_name_mappings = Array.new
    file_name_mappings << ["BVT","https://s3.amazonaws.com/cabletracker/#{self.job.id}/bvt_photo_file_name/#{self.bvt_photo_file_name}"] if !self.bvt_photo_file_name.blank?
    file_name_mappings << ["UEP","https://s3.amazonaws.com/cabletracker/#{self.job.id}/uep_photo_file_name/#{self.uep_photo_file_name}"] if !self.uep_photo_file_name.blank?
    file_name_mappings << ["POT-SCHIENE","https://s3.amazonaws.com/cabletracker/#{self.job.id}/pot_schiene_photo_file_name/#{self.pot_schiene_photo_file_name}"] if !self.pot_schiene_photo_file_name.blank?
    file_name_mappings << ["POT-GAS","https://s3.amazonaws.com/cabletracker/#{self.job.id}/pot_gas_photo_file_name/#{self.pot_gas_photo_file_name}"] if !self.pot_gas_photo_file_name.blank?
    file_name_mappings << ["POT-WASSER","https://s3.amazonaws.com/cabletracker/#{self.job.id}/pot_wasser_photo_file_name/#{self.pot_wasser_photo_file_name}"] if !self.pot_wasser_photo_file_name.blank?
    file_name_mappings << ["POT-HAK","https://s3.amazonaws.com/cabletracker/#{self.job.id}/pot_hak_photo_file_name/#{self.pot_hak_photo_file_name}"] if !self.pot_hak_photo_file_name.blank?
    file_name_mappings << ["POT-Heizung","https://s3.amazonaws.com/cabletracker/#{self.job.id}/pot_heizung_photo_file_name/#{self.pot_heizung_photo_file_name}"] if !self.pot_heizung_photo_file_name.blank?
    file_name_mappings << ["POT-Oel","https://s3.amazonaws.com/cabletracker/#{self.job.id}/pot_oel_photo_file_name/#{self.pot_oel_photo_file_name}"] if !self.pot_oel_photo_file_name.blank?
    file_name_mappings << ["MMD unbundled","https://s3.amazonaws.com/cabletracker/#{self.job.id}/mmd_unbundled_photo_file_name/#{self.mmd_unbundled_photo_file_name}"] if !self.mmd_unbundled_photo_file_name.blank?
    file_name_mappings << ["Messwerte-Screenshot","https://s3.amazonaws.com/cabletracker/#{self.job.id}/messwerte_photo_file_name/#{self.messwerte_photo_file_name}"] if !self.messwerte_photo_file_name.blank?

    require 'RMagick'
      require 'open-uri'
      file_name_mappings.each do |f|
        if (f[1]) then
        open("#{Rails.root.to_s}/tmp/"+file_prefix+f[0]+".jpg", 'wb') do |file|
          begin
              file << open(f[1]).read

              name = "#{Rails.root.to_s}/tmp/"+file_prefix+f[0]+".jpg"
              img = Magick::Image::read(name).first
            small_version = img.change_geometry!('800x600') do |cols, rows, img|
              img.resize!(cols, rows)
            end
            small_version.strip!
            small_version.write(name) { self.quality = 80 }
          rescue
          end
        end
      end
    end

    #create images from pdf
    pdf = Magick::ImageList.new("#{Rails.root.to_s}/tmp/"+temp_pdf_name)


    img = pdf
    puts "   Format: #{img.format}"
    puts "   Geometry: #{img.columns}x#{img.rows}"
    puts "   Class: " + case img.class_type
                            when Magick::DirectClass
                                "DirectClass"
                            when Magick::PseudoClass
                                "PseudoClass"
                        end
    puts "   Depth: #{img.depth} bits-per-pixel"
    puts "   Colors: #{img.number_colors}"
    puts "   Filesize: #{img.filesize}"
    puts "   Resolution: #{img.x_resolution.to_i}x#{img.y_resolution.to_i} "+
        "pixels/#{img.units == Magick::PixelsPerInchResolution ?
        "inch" : "centimeter"}"
    if img.properties.length > 0
        puts "   Properties:"
        img.properties { |name,value|
            puts %Q|      #{name} = "#{value}"|
        }
    end

    pdf.each_with_index do |image, index |
      image.write("#{Rails.root.to_s}/tmp/pdf_image-#{index}.jpg")
    ##image.change_geometry!('<1000x1000') do |cols, rows, img|
    # img.resize!(cols, rows)
    # img.write("#{Rails.root.to_s}/tmp/pdf_image-#{index}.jpg")
    #end
    end
    #pdf.write("#{Rails.root.to_s}/tmp/pdf_image.jpg")
    # create the zip file
    puts "bundle_filename="+bundle_filename
    File.delete(bundle_filename) if File.file?(bundle_filename)
    Zip::ZipFile.open(bundle_filename, Zip::ZipFile::CREATE) do |zipfile|
        file_name_mappings.each do |f|
          if (f[1]) then
            zipfile.add(file_path+file_prefix+f[0]+".jpg", "#{Rails.root.to_s}/tmp/"+file_prefix+f[0]+".jpg") if f[1]
        end
      end

      zipfile.add(file_path+pdf_name, "#{Rails.root.to_s}/tmp/"+temp_pdf_name)

      pdf.length.times do |i|
        zipfile.add(file_path+filename+"-#{i}.jpg", "#{Rails.root.to_s}/tmp/pdf_image-#{i}.jpg")
      end
    end

    # set read permissions on the file
    File.chmod(0644, bundle_filename)

     return filename
  end

  def create_signature(signature, ot_auftragsnummer)
    PdfHelper.create_signature(signature, ot_auftragsnummer)
  end

  def additional_attachment_1_url
    additional_attachment_1_file_name.blank? ? "/assets/blank.gif" : "#{Settings.COMPLETION_IMAGES_BASE_URL}#{job.id}/additional_attachment_1_file_name/#{additional_attachment_1_file_name}"
  end

  def additional_attachment_2_url
    additional_attachment_2_file_name.blank? ? "/assets/blank.gif" : "#{Settings.COMPLETION_IMAGES_BASE_URL}#{job.id}/additional_attachment_2_file_name/#{additional_attachment_2_file_name}"
  end

  def additional_attachment_3_url
    additional_attachment_3_file_name.blank? ? "/assets/blank.gif" : "#{Settings.COMPLETION_IMAGES_BASE_URL}#{job.id}/additional_attachment_3_file_name/#{additional_attachment_3_file_name}"
  end

  def additional_attachment_4_url
    additional_attachment_4_file_name.blank? ? "/assets/blank.gif" : "#{Settings.COMPLETION_IMAGES_BASE_URL}#{job.id}/additional_attachment_4_file_name/#{additional_attachment_4_file_name}"
  end

  def additional_attachment_5_url
    additional_attachment_5_file_name.blank? ? "/assets/blank.gif" : "#{Settings.COMPLETION_IMAGES_BASE_URL}#{job.id}/additional_attachment_5_file_name/#{additional_attachment_5_file_name}"
  end

  def additional_attachment_6_url
    additional_attachment_6_file_name.blank? ? "/assets/blank.gif" : "#{Settings.COMPLETION_IMAGES_BASE_URL}#{job.id}/additional_attachment_6_file_name/#{additional_attachment_6_file_name}"
  end

  def additional_attachment_7_url
    additional_attachment_7_file_name.blank? ? "/assets/blank.gif" : "#{Settings.COMPLETION_IMAGES_BASE_URL}#{job.id}/additional_attachment_7_file_name/#{additional_attachment_7_file_name}"
  end

  def additional_attachment_8_url
    additional_attachment_8_file_name.blank? ? "/assets/blank.gif" : "#{Settings.COMPLETION_IMAGES_BASE_URL}#{job.id}/additional_attachment_8_file_name/#{additional_attachment_8_file_name}"
  end

  def additional_attachment_9_url
    additional_attachment_9_file_name.blank? ? "/assets/blank.gif" : "#{Settings.COMPLETION_IMAGES_BASE_URL}#{job.id}/additional_attachment_9_file_name/#{additional_attachment_9_file_name}"
  end

  def additional_attachment_10_url
    additional_attachment_10_file_name.blank? ? "/assets/blank.gif" : "#{Settings.COMPLETION_IMAGES_BASE_URL}#{job.id}/additional_attachment_10_file_name/#{additional_attachment_10_file_name}"
  end


end
