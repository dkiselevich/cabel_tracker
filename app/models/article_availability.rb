class ArticleAvailability < ActiveRecord::Base
	belongs_to :article
  belongs_to :supplier
  belongs_to :client

	validates :supplier, :presence => {:message => "^Geben Sie einen Lieferanten an."}
	validates :article, :presence => {:message => "^Geben Sie einen Artikel an."}
	validates :purchase_price, :presence => {:message => "^Geben Sie einen Preis an."}	
	validates :delivery_time_in_days, :presence => {:message => "^Geben Sie eine Lieferzeit an."}
	
	
  def self.belonging_to_user_client(user)
    self.where(:client_id => user.clients.first) #purchase manager is supposed to have only one client for now #user.clients.collect{|c|c.id})
  end
end
