# encoding: utf-8
class ServiceAppointment < ActiveRecord::Base
	belongs_to :service_job
	belongs_to :user
	belongs_to :scheduling_user, :foreign_key => "scheduling_user_id", :class_name => "User"
	has_one :service_completion
	validates :day, :presence => {:message => "^Geben Sie ein Datum an."}
	validates :start_time, :presence => {:message => "^Geben Sie einen Startzeitpunkt an."}
	validates :end_time, :presence => {:message => "^Geben Sie einen Endzeitpunkt an."}
	validates :service_job_id, :presence => {:message => "^Geben Sie einen Auftrag an."}
  validate :start_is_before_ende
  validates :user, :presence => {:message => "^Wählen Sie einen Techniker."}

	def job
		service_job
	end

	def short_description
		"#{service_job.ot_auftragsnummer} | #{day.strftime('%d.%m.%Y')} #{start_time.strftime('%H:%M')} | #{user.full_name}"
	end

	def short_for_mail
#		tag monat, start-uhrzeit, plz, ort, strasse, nummer, name, ot-nummer
		"#{day.strftime('%d.%m.')}, #{start_time.strftime('%H:%M')}, #{service_job.adresse_short}, #{service_job.kunde_name}, #{service_job.ot_auftragsnummer}"
	end

	def processed_already?
		(self.service_completion.blank?) ? false : true
	end

	def self.belonging_to_user_client(user)
		self.includes(:service_job).where("service_jobs.client_id in (#{user.clients.collect{|c| c.id}.join(',')})")
	end

  private
  def start_is_before_ende
    errors.add(:start_time, '^Terminanfang muss vor Terminende liegen!') if (start_time > end_time)
  end
end
