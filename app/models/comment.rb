# encoding: utf-8
class Comment < ActiveRecord::Base
	belongs_to :job
	belongs_to :user
	
	validates :body, :presence => {:message => "^Geben Sie einen Kommentar ein."}
end
