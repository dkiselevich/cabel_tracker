class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :token_authenticatable, :lockable, :timeoutable and :activatable
  devise :database_authenticatable, :registerable, 
         :recoverable, :rememberable, :trackable, :timeoutable,
         :token_authenticatable

  # Setup accessible (or protected) attributes for your model
  attr_accessible 	:email, :password, :password_confirmation, :vorname, :nachname, 
  					:plz, :ort, :strasse, :hausnummer, :telefon, :telefon_mobil, :signature, :signature_photo,
            :is_boss, :is_controller, :is_controller_leader, :is_technician, :is_accountant, :is_purchase_manager,
            :tabledibkom_zertifikat_nummer, 
            :monday_work_start, :monday_work_end, :monday_lunch_break_start, :monday_lunch_break_end,
            :tuesday_work_start, :tuesday_work_end, :tuesday_lunch_break_start, :tuesday_lunch_break_end,
            :wednesday_work_start, :wednesday_work_end, :wednesday_lunch_break_start, :wednesday_lunch_break_end,
            :thursday_work_start, :thursday_work_end, :thursday_lunch_break_start, :thursday_lunch_break_end,
            :friday_work_start, :friday_work_end, :friday_lunch_break_start, :friday_lunch_break_end,
            :saturday_work_start, :saturday_work_end, :saturday_lunch_break_start, :saturday_lunch_break_end,
            :sunday_work_start, :sunday_work_end, :sunday_lunch_break_start, :sunday_lunch_break_end,
            :is_admin, :active, :client_ids #TODO !!
  has_and_belongs_to_many :service_user_groups
  belongs_to :scheduling_user_group
  has_many :appointments  
  has_many :service_appointments  
  has_many :completions, :through => :appointments
  has_many :cancellations, :through => :appointments
  has_many :appointment_requests, :through => :appointments
  has_many :assignments
  has_many :equipments
  has_many :clients, :through => :assignments
  has_many :days_of_absence
  has_many :quality_reports
  
  scope :service, where(:is_technician => true, :active => true)
  scope :scheduling, where(:is_controller => true, :active => true)
  scope :management, where(:is_boss => true, :active => true)
  scope :accounting, where(:is_accountant => true, :active => true)
  scope :admin, where(:is_admin => true, :active => true)
=begin
  has_attached_file :signature_photo,
    :styles =>{
      :thumb  => "100x100",
      :medium => "200x200",
      :large => "600x400"
    },
    :storage => :s3,
    :s3_credentials => "#{Rails.root.to_s}/config/amazon_s3.yml",
    :path => "/users/:style/:id/:filename",
    :processors => [:sig]
=end

  validates :vorname, :presence => {:message => "^Geben Sie einen Vornamen an."}
  validates :nachname, :presence => {:message => "^Geben Sie einen Nachnamen an."}
  validates :email, :presence => {:message => "^Geben Sie eine Email an."}
  validates :email, :uniqueness => {:message => "^Die angegebene Email wird bereits verwendet Email muss einmalig sein.."}

  before_save do
    self.email.downcase! if self.email
  end

  def self.find_for_authentication(conditions)
    conditions[:email].downcase! 
    super(conditions) 
  end 
  
  def full_name
    [vorname, nachname].join(" ")
  end

  def client_names
    self.clients.collect{|c| c.name}.join(', ')
  end


  def full_name_and_companies
    #TODO: get bad data if client_names was empty
    "#{self.full_name} (#{self.client_names})"
  end

  def self.assigned_to_same_company(other_user)  
    where("clients.id in (#{other_user.clients.collect{|c| c.id}.join(',')})").includes(:clients)
  end

  def self.assigned_to_company(client)
    where("clients.id = #{client.id}").includes(:clients)
  end

  def self.array_assigned_to_company(client)
    service.assigned_to_company(client).collect {|p| [ p.full_name_and_companies, p.id ]}
  end

  def self.array_assigned_to_same_company(user)
    service.assigned_to_same_company(user).collect{|u| [u.full_name, u.id]}    
  end

  def self.scheduling_options_for_select
    scheduling.collect {|nt| [ nt.full_name, nt.id ] }
  end

  def self.service_options_for_select
    service.collect {|nt| [ nt.full_name, nt.id ] }
  end

  

  def create_signature(signature, ot_auftragsnummer)
    PdfHelper.create_signature(signature, ot_auftragsnummer) 
  end

  #https://github.com/plataformatec/devise/wiki/How-To:-Add-timeout_in-value-dynamically
  #http://stackoverflow.com/questions/4867298/setting-session-length-with-devise
  
  def timeout_in   
    if self.is_technician & !(self.is_admin || self.is_accountant || self.is_boss || self.is_controller || self.is_controller_leader)
      10.minutes
    else
      1.hour
    end
  end

  def is_absent?(appointment)
  	self.days_of_absence.each do |absence|
  		absence.starting_point.to_date.upto(absence.end_point.to_date).each do |day|
  			return true if day == appointment.day
  		end
  	end
  	return false
  end

  def has_appointment?(absence)
  	self.appointments.each do |appointment|
  		absence.starting_point.to_date.upto(absence.end_point.to_date).each do |day|
  			return true if day == appointment.day
  		end
  	end
  	return false
  end

  def working_hours_monday
  	lunch_time = 0
  	if (monday_lunch_break_end && monday_lunch_break_start)
  		lunch_time = monday_lunch_break_end - monday_lunch_break_start
  	end
  	if (monday_work_end && monday_work_start)
  		return ((monday_work_end - monday_work_start) - lunch_time)/1.hour
  	else
  		return 0
  	end
  end

  def working_hours_tuesday
  	lunch_time = 0
  	if (tuesday_lunch_break_end && tuesday_lunch_break_start)
  		lunch_time = tuesday_lunch_break_end - tuesday_lunch_break_start
  	end
  	if (tuesday_work_end && tuesday_work_start)
  		return ((tuesday_work_end - tuesday_work_start) - lunch_time)/1.hour
  	else
  		return 0
  	end
  end

  def working_hours_wednesday
  	lunch_time = 0
  	if (wednesday_lunch_break_end && wednesday_lunch_break_start)
  		lunch_time = wednesday_lunch_break_end - wednesday_lunch_break_start
  	end
  	if (wednesday_work_end && wednesday_work_start)
  		return ((wednesday_work_end - wednesday_work_start) - lunch_time)/1.hour
  	else
  		return 0
  	end
  end

  def working_hours_thursday
  	lunch_time = 0
  	if (thursday_lunch_break_end && thursday_lunch_break_start)
  		lunch_time = thursday_lunch_break_end - thursday_lunch_break_start
  	end
  	if (thursday_work_end && thursday_work_start)
  		return ((thursday_work_end - thursday_work_start) - lunch_time)/1.hour
  	else
  		return 0
  	end
  end

  def working_hours_friday
  	lunch_time = 0
  	if (friday_lunch_break_end && friday_lunch_break_start)
  		lunch_time = friday_lunch_break_end - friday_lunch_break_start
  	end
  	if (friday_work_end && friday_work_start)
  		return ((friday_work_end - friday_work_start) - lunch_time)/1.hour
  	else
  		return 0
  	end
  end

  def working_hours_saturday
  	lunch_time = 0
  	if (saturday_lunch_break_end && saturday_lunch_break_start)
  		lunch_time = saturday_lunch_break_end - saturday_lunch_break_start
  	end
  	if (saturday_work_end && saturday_work_start)
  		return ((saturday_work_end - saturday_work_start) - lunch_time)/1.hour
  	else
  		return 0
  	end
  end

  def working_hours_sunday
  	lunch_time = 0
  	if (sunday_lunch_break_end && sunday_lunch_break_start)
  		lunch_time = sunday_lunch_break_end - sunday_lunch_break_start
  	end
  	if (sunday_work_end && sunday_work_start)
  		return sunday = ((sunday_work_end - sunday_work_start) - lunch_time)/1.hour
  	else
  		return 0
  	end
  end 
  
  def working_hours_week
    if (working_hours_monday() && working_hours_tuesday() && working_hours_wednesday() && working_hours_thursday() && working_hours_friday() && working_hours_saturday())
      return working_hours_monday() + working_hours_tuesday() + working_hours_wednesday() + working_hours_thursday() + working_hours_friday() + working_hours_saturday()
    else
      return 0
    end
  end
  
  def working_hours_month
    if (working_hours_week())
      return working_hours_week() * 4 
    else
    return 0
    end
  end
  
  def actual_working_hours_as_string(start_date, end_date)
    @hours = 0
    (start_date..end_date).each do |day|
      #puts day.strftime("%Y-%m-%d")
      #puts day.strftime("%A")
      #puts day.wday
      if (self.is_present_on_day(day)) then
        @hours += working_hours_sunday if (day.wday == 0)
        @hours += working_hours_monday if (day.wday == 1)
        @hours += working_hours_tuesday if (day.wday == 2)
        @hours += working_hours_wednesday if (day.wday == 3)
        @hours += working_hours_thursday if (day.wday == 4)
        @hours += working_hours_friday if (day.wday == 5)
        @hours += working_hours_saturday if (day.wday == 6)
      end
    end

    return @hours
  end
 
  def get_total_number_of_technician(start_date, end_date)
    total_number_of_technician = 0
    (start_date..end_date).each do |day|
      if (self.is_present_on_day(day)) then
        total_number_of_technician += 1
      end
    end
    return total_number_of_technician
  end
  
  def get_day_of_absence_as_string(start_date, end_date)
    return get_total_days_of_absence(start_date, end_date).collect{|day| day.strftime("%Y-%m-%d")}.join(", ")
  end
  
  def get_total_days_of_absence(start_date, end_date)
    fehlzeiten = []
    (start_date..end_date).each do |day|
      if (!self.is_present_on_day(day)) then
        fehlzeiten << day
      end
    end
    return fehlzeiten
  end

  def is_present_on_day(day)
    self.days_of_absence.each do |fehlzeit|
        #puts "Fehlzeit:"
        #puts "von:"
        #puts fehlzeit.starting_point.strftime("%Y-%m-%d")
        #puts "bis:"
        #puts fehlzeit.end_point.strftime("%Y-%m-%d")
        if (fehlzeit.starting_point <= day) && (fehlzeit.end_point > day-1.day) then
          return false
        end
      end
      return true
  end
  
  def possible_appointments
   (@hours / 2).floor
  end
end

