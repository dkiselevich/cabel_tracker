class ArticleGroup < ActiveRecord::Base
  has_many :articles
  validates :name, :presence => {:message => "^Geben Sie einen Namen an."}
  validates :type_id, :presence => {:message => "^Geben Sie einen Typ an."}


	def self.belonging_to_user_client(user)
		self.where(:client_id => user.clients.first) #purchase manager is upposed to habe only one client for now #user.clients.collect{|c|c.id})
	end
	
	def type
		return "Beistellung" if type_id == 1
		return "Kauf" if type_id == 2
	end
end
