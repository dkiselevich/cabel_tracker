class Ebru::Compound < Ebru::Component
  attr_accessor :children

  def initialize()
    super()
    @children = Array.new
  end  

=begin
  def render_html
    return @children.collect{|child| child.render_html }
  end  
=end
  
  def render_csv
    return @children.collect{|child| child.render_csv +  ",\n" }.join()
  end

  def push(element)
    @children.push(element)
  end

  def add(element)
    push(element)
  end

  def pdf_height
    sum = 0
    @children.each { |a| sum += child.pdf_height }
    return sum
  end
end