class Ebru::Container < Ebru::Compound
  attr_accessor :header_title, :children, :display_order, :children_styles
  
  def initialize()
    super()
    @display_order = "horizontal"
    @header_title = ""
    @children = Array.new
    @children_styles = Array.new
    @children_styles << ["margin-right","5px"]
    @css_class = "container"
  end
 
  def render_body_html
    "<div class='container-header'>#{@header_title}</div> #{render_children_html}"
  end
 
  def render_children_html
    @children_styles << ["float","left"] if (@display_order == "horizontal")
    return @children.collect {|child| child.styles.concat( @children_styles); child.render_html }.join("") +"<br class='clear'/>"
  end
  
  def render_csv
    return "#{@header_title},\n" + super
  end

  def render_pdf
    render_body_pdf
  end

  def render_body_pdf
    "<div class='container-header'>#{@header_title}</div> #{render_children_pdf}"
  end
 
  def render_children_pdf
    @children_styles << ["float","left"] if (@display_order == "horizontal")
    return @children.collect {|child| child.styles.concat( @children_styles); child.render_pdf }.join("") +"<br class='clear'/>"
  end
  

end