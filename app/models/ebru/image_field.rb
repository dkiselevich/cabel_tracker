class Ebru::ImageField < Ebru::Component
  attr_accessor :width, :height

  def initialize(title, url, width = 0, height = 0)
    super()
    @css_class = "image-field"
    @url = url
    @width = width
    @height = height
  end
  
  def render_body_html
    return "<img src='#{@url}' style='width:#{@width}px; height:#{@height}px; '/>"
  end
  
  def render_csv
    ","
  end

  def pdf_height
    return @height
  end
end