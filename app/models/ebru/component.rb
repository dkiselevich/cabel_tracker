class Ebru::Component
  attr_accessor :styles, :page_break

  def initialize()
    @styles = []
    @css_class = ""
    @page_break = false
  end
  
  def render_styles
    return styles.collect { |e| "#{e[0]}: #{e[1]};" }.join()
  end
  
  def render_html
    @css_class += " page-break" if @page_break
    return "<div class='#{@css_class}' style='#{ render_styles }'>#{ render_body_html }</div>"
  end

  def render_body_html
    return "Component"
  end
  
  def render_pdf
    return render_html
  end
  
  def render_csv
    ",\n"
  end

  def pdf_height
    0
  end
  
  def can_split?
    false
  end
end