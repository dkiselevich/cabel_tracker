# encoding: utf-8
class Ebru::Report
  attr_accessor :body, :client, :logo_path, :pdf_path, :csv_path, :data, :name, :auto_page_break, :inter_table_break
  def initialize(name = "unnamed report")
    super()
    @name = name
    @body = Array.new
    @auto_page_break = false
    @inter_table_break = false
  end

  def push(element)
    @body.push(element)
  end

  def add(element)
    push(element)
  end

  def render_styles
    styles = "<style>" + IO.read(Rails.root.join('app', 'assets', 'stylesheets', 'pdf-styles.css')) + "</style>"
    return styles
  end

  def render_scripts
    jquery = "<script src='/assets/jquery-latest.js?body=1' type='text/javascript'></script>"
    table_sorter = "<script src='/assets/jquery.tablesorter.js?body=1' type='text/javascript''></script>
    <script src='/assets/highcharts.js?body=1' type='text/javascript''></script>
    <script src='/assets/jquery.quicksearch.js?body=1' type='text/javascript''></script>
    <script type='text/javascript' charset='UTF-8'>
    $(document).ready(function(){
      $('.tablesorter').tablesorter({ widgets: ['zebra'] });
     });
  </script>"
    return jquery+table_sorter
  end

  def render_header
    ""#return "<div class='main-header'><img class='application-logo' src='http://s3.amazonaws.com/cabletracker/assets/logo.png' /><img class='client-logo' src='#{@logo_path}' /></div>"
  end

  def render_footer
    return "<div class='footer'>Copyright &copy; #{client.name} #{Time.now.year} #{Settings.PRODUCT_NAME}</div>"
  end

  def render_body
    return @body.collect {|child| child.render_html }.join("")
  end

  def render_body_pdf
    return render_body_pdf_expiremental_auto_page_break if @auto_page_break
    return @body.collect do |child|
      child.render_pdf
    end.join("")
  end

  def render_body_pdf_expiremental_auto_page_break
    page_height = 1440
    current_height_on_current_page = 0
    return @body.collect do |child|
      child.inter_table_break = @inter_table_break if (child.class.name == "Ebru::Table")
      child.page_break = false
      if ((current_height_on_current_page + child.pdf_height) > page_height)
        if (child.can_split?)
        part_old_page, part_new_page = child.get_split_values(page_height, current_height_on_current_page)
        current_height_on_current_page = part_new_page.pdf_height
        part_old_page.render_pdf + part_new_page.render_pdf
        else
        child.page_break = true
        current_height_on_current_page = child.pdf_height
        child.render_pdf
        end
      else
      current_height_on_current_page += child.pdf_height
      child.render_pdf
      end
    end.join("")
  end

  def render_html
    html = render_styles+render_scripts+"<div><br/>"
    html.gsub!("<div class='container page-break", "<div class='page-break'>"+render_header+"</div><div class='container")
    html += render_body+"<br/>"
    html += "<div class='pdf_csv_btn'><button class='button'><a  class='pdf-link'  href='#{@pdf_path}'/>PDF</button>" if !@pdf_path.blank?
    html += "<button class='button'><a  class='pdf-link'  href='#{@csv_path}'/>CSV</button></div>" if !@csv_path.blank?
    html += "</div>"

    return html
  end

  def render_pdf
    html = render_styles+"<div style='width:1000px'>"+render_header+"<br/>"
    html.gsub!("<div class='container page-break", "<div class='page-break'>"+render_header+"</div><div class='container")
    html += render_body_pdf+"<br/></div>"

    return PDFKit.new(html).to_pdf
  end

  def self.create_csv_from_data(data, header)
    result = CSV.generate do |csv|
      csv << header
      data.each do |row|
        csv << row
      end
    end
    return result
  end

  def render_csv
    return @body.collect {|section| section.render_csv }.join("")
  end
end