class Ebru::PieChart < Ebru::ChartField 
  # def initialize(type, title, data, legend, bar_width, axis_x, axis_y, chart_width = nil, pdf_height = nil)
    # super(type, title, data, legend, bar_width, axis_x, axis_y, chart_width, pdf_height)
  # end
  
  # def initialize(hash = {})
    # super()
    # @type = "pie"
  # end

 def initialize(hash = {})
    super()
    @type = "pie"
    @title = hash[:title]
    @data = hash[:data]
    @legend = hash[:legend]
    @axis_x = hash[:axis_x]
    @axis_y = hash[:axis_y]
    @bar_width = hash[:bar_width]
    @chart_width = (hash[:chart_width] ? hash[:chart_width ] : 930)
    @pdf_height = hash[:pdf_height]
    @chart_id = SecureRandom.uuid

  end
  
  def render_html
    series = ""
    @data.each_with_index do |d, i|
      series += "['#{@legend[i]}',   #{@data[i]}],"
    end

    @css_class += "page-break" if @page_break
    return "<div id='ebru_highchart_graph_#{@chart_id}' style='#{ render_styles }'></div>
            <script type='text/javascript' charset='UTF-8'>
             $(function(){
                var chart = new Highcharts.Chart({
                 chart: {
                  renderTo: 'ebru_highchart_graph_#{@chart_id}',
                  type: '#{@type}',
                  width:  #{@chart_width},
                  height:  #{@pdf_height}
                 },
                 title:{
                   text: '#{@title}'
                 },
                 xAxis: {
                   categories: #{@axis_x.first}
                  },
                  yAxis: [{
                    title: {
                       text: ''
                    },
                  }],
                 series: [{
                        data: [
                          #{series}
                        ]
                    }]
                 });
               $('#ebru_highchart_graph_#{@chart_id}').resize();
             });
            </script>
            "
  end



  # def get_pdf_chart
    # options_hash =  {
      # :size => '1000x300',
      # :title => @title,
      # :thickness => '2',
      # :bar_width_and_spacing => {:spacing =>1, :group_spacing =>7, :width => @bar_width},
      # :axis_with_labels => ['x', 'y'],
      # :chart_bg => 'fff',
      # :grid_lines => 50,
      # :legend =>  @legend,
      # :grouped => true,
      # :line_colors => "2da725,2485db,db2586,f6ef04,435a60,21Bfaf,8221ba",
      # :data => @data,
      # :axis_labels => @axis_x,
      # :axis_range => @axis_y,
    # }
    # if (@type == 'line') then
      # url =  Gchart.line(options_hash)
    # end
    # if (@type == 'column' || @type == '') then
      # url =  Gchart.bar(options_hash)
    # end
    # return  url
  # end

end