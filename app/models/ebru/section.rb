class Ebru::Section < Ebru::Compound
  attr_accessor :header_title, :children
  
  def initialize()
    super()
    @header_title =""
    @children = Array.new
  end
  
  def render_html
    "<div class='section'><div class='section-header'>#{@header_title}</div> #{render_children}</div>"
  end
  
  def render_pdf
    "<div class='section'><div class='section-header'>#{@header_title}</div> #{render_children_pdf}</div>"
  end

  def render_csv
    "#{@header_title}\n \n  #{render_children_csv}"
  end
  
  def render_children_csv
    return @children.collect {|element| element.render_csv }.join("") 
  end

  def render_children
    return @children.collect {|element| element.render_html }.join("") 
  end

  def render_children_pdf
    return @children.collect {|element| element.render_pdf }.join("") 
  end

end