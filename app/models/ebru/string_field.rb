class Ebru::StringField < Ebru::Component
  
  def initialize(name = "", value = "")
    super()
    @name = name
    @value = value
    @css_class = "string-field"
  end

=begin
  def initialize(hash = {})
    super()
    hash = {:label => "", :value => ""}.merge!(hash)
    @name = hash[:label]
    @value = hash[:value]
    @css_class = "string-field"
  end
=end  

  def render_body_html
    return "<span class='label'>#{@name}</span> <span>#{@value}</span>"
  end
  
  def render_csv
    "#{@name}, #{@value} \n\n"
  end

  def pdf_height
    height = 21
=begin
    @styles.each do |s|
      height = (s[1].gsub("px")).to_s.to_i if (s[0] == "height")
    end
    return height
=end
  end
end