class Ebru::LinkField < Ebru::Component
  
  def initialize(name, url)
    super()
    @name = name
    @url = url
    @css_class = "link-field"
  end

  def to_s
  	render_body_html
  end
  
  def render_body_html
     return "<a href=#{@url}>#{@name}</a>"
  end
  
end