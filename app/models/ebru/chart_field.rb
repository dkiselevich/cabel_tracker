class Ebru::ChartField < Ebru::Component
  # def initialize(type, title, data, legend, bar_width, axis_x, axis_y,  chart_width = nil, pdf_height = nil)
  # super()
  # @title = title
  # @data = data
  # @type = type
  # @legend = legend
  # @axis_x = axis_x
  # @axis_y = axis_y
  # @bar_width =  bar_width
  # @chart_width = chart_width
  # @pdf_height = pdf_height
  # @chart_id = SecureRandom.uuid
  # end
  def initialize(hash = {})
    super()
    @type = hash[:type]
    @title = hash[:title]
    @data = hash[:data]
    @legend = hash[:legend]
    @axis_x = hash[:axis_x]
    @axis_y = hash[:axis_y]
    @bar_width = hash[:bar_width]
    @chart_width = (hash[:chart_width] ? hash[:chart_width ] : 930)
    @pdf_height = hash[:pdf_height]
    @chart_id = SecureRandom.uuid

  end

  def render_html
    series = ""
    @data.each_with_index do |row, i|
      series += "{
              data: #{@data[i]},
              name: '#{@legend[i]}',
              pointWidth: #{@bar_width}
         },"
    end

    @css_class += "page-break" if @page_break
    return "<div id='ebru_highchart_graph_#{@chart_id}' style='#{ render_styles }'></div>
            <script type='text/javascript' charset='UTF-8'>
             $(function(){
                var chart = new Highcharts.Chart({
                 chart: {
                  renderTo: 'ebru_highchart_graph_#{@chart_id}',
                  type: '#{@type}',
                  width:  #{@chart_width},
                  height:  #{@pdf_height}
                 },
                 title:{
                   text: '#{@title}'
                 },
                 xAxis: {
                   categories: #{@axis_x.first}
                  },
                  yAxis: [{
                    title: {
                       text: ''
                    },
                  }],
                 series: [
                    #{series}
                  ]
                 });
               $('#ebru_highchart_graph_#{@chart_id}').resize();
             });
            </script>
            "
  end

  def get_pdf_chart
    options_hash =  {
      :size => '1000x300',
      :title => @title,
      :thickness => '2',
      :bar_width_and_spacing => {:spacing =>1, :group_spacing =>7, :width => @bar_width},
      :axis_with_labels => ['x', 'y'],
      :chart_bg => 'fff',
      :grid_lines => 50,
      :legend =>  @legend,
      :grouped => true,
      :line_colors => "2da725,2485db,db2586,f6ef04,435a60,21Bfaf,8221ba",
      :data => @data,
      :axis_labels => @axis_x,
      :axis_range => @axis_y,
    }
    if (@type == 'line') then
      url =  Gchart.line(options_hash)
    end
    if (@type == 'pie') then
      url =  Gchart.pie(options_hash)
    end
    if (@type == "column" || @type == "bar") then
      url =  Gchart.bar(options_hash)
    end
    return url
  end

  def render_pdf
    custom_styles = ""
    custom_styles += "width: #{@chart_width}px" if !@chart_width.blank?
    return "<div class='text-field'><span><img style='#{custom_styles}' src='#{get_pdf_chart}'/></span></div>"
  end

end