class Ebru::TextField < Ebru::Component
  
  def initialize(name, value)
    super()
    @name = name
    @value = value
    @css_class = "text-field"
  end
  
  def render_body_html
    return "<span class='label'>#{@name.gsub(/(?:\n\r?|\r\n?)/, '<br>')}</span> <span>#{@value.gsub(/(?:\n\r?|\r\n?)/, '<br>') }</span>"
  end
  
  def render_csv
    "#{@name}, #{@value} \n\n"
  end
end