class Ebru::Table < Ebru::Component
  attr_accessor :title, :header, :data, :emphasized_columns, :footer, :max_width, :inter_table_break
  
  def initialize(hash = {})
    super()
    defaults = { 
      :data => [], 
      :header => [], 
      :footer => [],
      :emphasized_columns => [],
      :max_width => 940,
      :styles => { 
        :table_header_background_color => "black",
        :table_header_color => "white"
        }
      }
    hash = defaults.merge!(hash)

    @title = nil
    @header = hash[:header]
    @footer = hash[:footer]
    @data = hash[:data]
    @emphasized_columns = hash[:emphasized_columns]
    @max_width = hash[:max_width]
    @css_class = "table-field"
    @inter_table_break = false
  end
 
  def render_csv
    result = CSV.generate do |csv|
      csv << @header
      @data.each do |row|
        csv << row
      end
      csv << @footer
      csv << []
    end
    return result
  end  

  def render_html
    @css_class += " page-break" if @page_break
    table_id = "ebru_table_"+SecureRandom.uuid.to_s
    html = "<script type='text/javascript' charset='UTF-8'>
    $(document).ready(function(){
      $('##{table_id} tbody tr').quicksearch({
            labelText: 'Search: ',
            attached: '##{table_id}',
            position: 'before',
            delay: 100,
            loaderText: '',
            onAfter: function() {
                if ($('##{table_id} tbody tr:visible').length != 0) {
                    $('##{table_id}').trigger('update');
                    $('##{table_id}').trigger('appendCache');
                    $('##{table_id} tfoot tr').hide();
                }
                else {
                    $('##{table_id} tfoot tr').show();
                }
            }
        });
     });
  </script>"

    title_div = ""
    title_div = "<div class='title'>#{@title}</div>" if @title
    html += "<div class='#{@css_class}'>#{title_div}<table id='#{table_id}' class='tablesorter' style='#{ render_styles }'><thead>"
    html += "<tr>" + @header.collect { |h| "<th>#{h}</th>" }.join("") + "</tr></thead><tbody>"
    @data.each_with_index do |row, i|
      css_class = (i % 2 == 0) ? "odd" : "even"
      html += "<tr class='#{ css_class }'>"  
      row.each_with_index do |d,i|
        special_stuff = "class='emphasize'" if (@emphasized_columns.include?(i)) 
        html += "<td #{special_stuff} >#{d}</td>"
      end 
      html += "</tr>"
    end
    html += "</tbody>"
    html += "<tfoot><tr>" + @footer.collect { |h| "<th>#{h}</th>" }.join("") + "</tr></tfoot>"
    html += "</table></div>"
    return html 
  end

  def pdf_height
    height = data.length * 32
    height += 30 if @title
    height += 30 if @header
    height += 30 if @footer
  end

  def can_split?
    @inter_table_break
  end

  def get_split_values(page_height, current_height_on_current_page)
    available_space = page_height - current_height_on_current_page - 20 - 30
    cut_index = (available_space / 32) 

    tb_old_page = Ebru::Table.new({ 
      :data => [],
      :header => self.header, 
      :footer => self.footer,
      :emphasized_columns => self.emphasized_columns,
      })
    tb_old_page.title = self.title
    self.data.each_with_index do |row, index|
      tb_old_page.data << row unless (index > cut_index)
    end
    tb_old_page.styles.concat(self.styles)

    tb_new_page = Ebru::Table.new({
      :data => [],
      :header => self.header, 
      :footer => self.footer,
      :emphasized_columns => self.emphasized_columns,
      })
    tb_new_page.title = self.title + " (Fortsetzung)"
    tb_new_page.styles.concat(self.styles)

    self.data.each_with_index do |row, index|
      tb_new_page.data << row if (index > cut_index)
    end

    tb_new_page.page_break = true 
    current_height_on_current_page = tb_new_page.pdf_height
    return tb_old_page, tb_new_page
  end
   
end