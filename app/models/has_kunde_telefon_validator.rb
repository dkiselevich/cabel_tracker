# encoding: utf-8
class HasKundeTelefonValidator < ActiveModel::Validator
  def validate(record)
    if record.kunde_telefon_privat.blank? && record.kunde_telefon_geschaeftlich.blank?  && record.kunde_telefon_mobil.blank?  then
      record.errors[:base] << "Geben Sie mindestens eine Telefonnummer für den Kunden an."
    end
  end
end