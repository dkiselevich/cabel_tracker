class Delivery < ActiveRecord::Base
	belongs_to 	:technician, :class_name => "User"
	belongs_to 	:supplier
	has_many 	:delivery_items

	validates :type_id, :presence => {:message => "^Geben Sie einen Typ an."}
	validates :status_id, :presence => {:message => "^Geben Sie einen Status an."}

	def self.belonging_to_user_client(user)
		self.where("client_id in (#{user.clients.collect{|c| c.id}.join(',')})")
	end

	def status
		return "geordert" if status_id == 1
		return "empfangen" if status_id == 2
		return "storniert" if status_id == 3
	end

	def type
		return "extern" if type_id == 1
		return "intern" if type_id == 2
	end

	def process_delivery
		factor = (type_id == 1) ? 1 : -1
		delivery_items.each do |item|
			item.article.current_quantity_in_storage += (item.quantity * factor)
			item.article.save()

			article_is_already_in_equipment = false
			if (type_id == 2) then
				technician.equipments.each do |e|
					if (e.article == item.article) then
						article_is_already_in_equipment = true
						e.quantity += item.quantity
						e.save
					end
				end
			end

			if (!article_is_already_in_equipment) then
				e = Equipment.new
				e.article = item.article
				e.quantity = item.quantity
				e.user = technician
				e.save
			end
		end
		self.status_id = 2 #processed
		self.save()
	end
end
