class AppointmentSweeper < ActionController::Caching::Sweeper
  observe Appointment

  # If our sweeper detects that a Appointment was created call this
  def after_create(appointment)
    expire_cache_for(appointment)
  end
 
  # If our sweeper detects that a Appointment was updated call this
  def after_update(appointment)
    expire_cache_for(appointment)
  end
 
  # If our sweeper detects that a Appointment was deleted call this
  def after_destroy(appointment)
    expire_cache_for(appointment)
  end
 
  private
  def expire_cache_for(appointment)
    begin
      SweeperHelper.expire_appointment_views(appointment, self)
      SweeperHelper.expire_job_views(appointment.job, self)
    rescue Exception => ex
      puts "ERROR while clearing caches: #{ex}"
    end
  end
end