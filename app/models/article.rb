# encoding: utf-8
class Article < ActiveRecord::Base
	belongs_to :article_gruop
	has_many :supplier, :through => :article_availabilities
	belongs_to :client
	has_many :article_availabilities

	validates :name, :presence => {:message => "^Geben Sie einen Namen an."}
	validates :article_group_id, :presence => {:message => "^Geben Sie eine Warengruppe an."}
	#validates :supplier_id, :presence => {:message => "^Geben Sie einen Lieferant an."}
	validates :unit_id, :presence => {:message => "^Geben Sie eine Einheit an."}
	validates :order_quantity, :presence => {:message => "^Geben Sie einen Bestellmenge an."}
	validates :supply_to_technician_quantity, :presence => {:message => "^Geben Sie eine Abgabemenge an Techniker an."}
	validates :description, :presence => {:message => "^Geben Sie eine Beschreibung an."}
	validates :price1, :presence => {:message => "^Geben Sie einen VK Preis 1 (an UMKBW lt. LV) an."}
	validates :price2, :presence => {:message => "^Geben Sie einen VK Preis 2 (an Endkunde) an."}
	
	validates :status_id, :presence => {:message => "^Geben Sie einen Status an."}
	validates :current_quantity_in_storage, :presence => {:message => "^Geben Sie einen Lagerbestand an."}
	validates :minimum_quantity_in_storage, :presence => {:message => "^Geben Sie einen Mindestbestand an."}
	validates :minimum_quantity_pro_technician, :presence => {:message => "^Geben Sie eine Mindestmenge pro Techniker an."}

	def self.belonging_to_user_client(user)
		self.where(:client_id => user.clients.first) #purchase manager is upposed to habe only one client for now #user.clients.collect{|c|c.id})
	end

	def residue
		r = current_quantity_in_storage - minimum_quantity_in_storage
		(r < 0) ? 0 : r
	end

	def unit
		return "Stück" if unit_id == 1
		return "Meter" if unit_id == 2
		return "undefiniert"
	end
end
