class ServiceUserGroup < ActiveRecord::Base
	has_and_belongs_to_many :users
	has_and_belongs_to_many :postal_codes
	belongs_to :client
	validates :name, :presence => {:message => "^Geben Sie eine Bezeichnung an."}

	def postal_codes_csv
		postal_codes.collect {|postal_code| postal_code.code }.sort().join(", ") 
	end

	def self.available_service_users_for(controller)
	 	available_service_users = Array.new
	    Client.is_assigned_to_user(controller).each do |client|
	      client.users.service.where(:active => true).each do |user| 
	        available_service_users << user
	      end
	    end 
	    return available_service_users.uniq
	end

	def self.belonging_to_user_client(user)
		self.where(:client_id => user.clients.collect{|c|c.id})
	end
end
