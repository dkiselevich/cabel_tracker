class AppointmentRequest < ActiveRecord::Base
	belongs_to :scheduling_user, :foreign_key => "scheduling_user_id", :class_name => "User"
	belongs_to :appointment
	has_one :job, :through => :appointment
	belongs_to :technician, :class_name => "User" , :foreign_key => "technician_id"
	
	validates :hinderungsgrund, :presence => {:message => "^Geben Sie einen Hinderungsgrund an."}
	validates :scheduling_user_id, :presence => {:message => "^Geben Sie an mit welchem Disponenten ein neuer Termin abgesprochen wurde."}
	validate :start_is_before_ende

	def short_description
		"erstellt am "+created_at.strftime(Settings.TIME_FORMAT)+" durch "+technician.full_name
	end

	def self.belonging_to_user_client(user)
		self.includes(:job).where("jobs.client_id in (#{user.clients.collect{|c| c.id}.join(',')})")
	end

  private
  def start_is_before_ende
    errors.add(:zeitfenster_start, '^Terminanfang muss vor Terminende liegen!') if (zeitfenster_start && zeitfenster_ende && (zeitfenster_start > zeitfenster_ende))
  end
end
