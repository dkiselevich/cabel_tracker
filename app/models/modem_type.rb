class ModemType < ActiveRecord::Base

  scope :active, where(:active => true)

  def self.options_for_select
    active.collect {|c| [ c.name, c.id ] }
  end
  
end
