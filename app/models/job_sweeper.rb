class JobSweeper < ActionController::Caching::Sweeper
  observe Job

  # If our sweeper detects that a Job was created call this
  def after_create(job)
    expire_cache_for(job)
  end

  # If our sweeper detects that a Job was updated call this
  def after_update(job)
    expire_cache_for(job)
  end

  # If our sweeper detects that a Job was deleted call this
  def after_destroy(job)
    expire_cache_for(job)
  end


  private

  def expire_cache_for(job)
    begin
      SweeperHelper.expire_job_views(job, self)

      if (!job.appointments.blank?) then
        job.appointments.each do |appointment| 
          SweeperHelper.expire_appointment_views(appointment, self)
        end
      end  

      if (!job.completions.blank?) then
        job.completions.each do |completion| 
          SweeperHelper.expire_completion_views(completion, self)
        end
      end  

      if (!job.cancellations.blank?) then
        job.cancellations.each do |cancellation| 
          SweeperHelper.expire_cancellation_views(cancellation, self)
        end
      end  

      if (!job.appointment_requests.blank?) then
        job.appointment_requests.each do |appointment_request| 
          SweeperHelper.expire_appointment_request_views(appointment_request, self)  
        end
      end  

    rescue Exception => ex
      puts "ERROR while clearing caches: #{ex}"
    end
  end
end