class SweeperHelper < ActionController::Caching::Sweeper

	def self.expire_job_views(job, sweeper)
		puts "EXPIRING JOB: #{job.id}"
	    sweeper.expire_action(:controller => 'admin/jobs', :action => 'edit', :id => job.id)
	    sweeper.expire_action(:controller => 'scheduling/jobs', :action => 'edit', :id => job.id)
	    sweeper.expire_action(:controller => 'management/jobs', :action => 'edit', :id => job.id)
	    
	    sweeper.expire_action(:controller => 'admin/jobs', :action => 'show', :id => job.id)
	    sweeper.expire_action(:controller => 'scheduling/jobs', :action => 'show', :id => job.id)
	    sweeper.expire_action(:controller => 'scheduling_leader/jobs', :action => 'show', :id => job.id)
	    sweeper.expire_action(:controller => 'service/jobs', :action => 'show', :id => job.id)
	    sweeper.expire_action(:controller => 'management/jobs', :action => 'show', :id => job.id)

      sweeper.expire_action(:controller => 'admin/jobs', :action => 'index')
	    sweeper.expire_action(:controller => 'scheduling/jobs', :action => 'index')
	    sweeper.expire_action(:controller => 'scheduling_leader/jobs', :action => 'index')
	    sweeper.expire_action(:controller => 'service/jobs', :action => 'index')
	    sweeper.expire_action(:controller => 'management/jobs', :action => 'index')

	end

	def self.expire_appointment_views(appointment, sweeper)
		puts "EXPIRING APPOINTMENT: #{appointment.id}"
		sweeper.expire_action(:controller => 'admin/appointments', :action => 'edit', :id => appointment.id)
		sweeper.expire_action(:controller => 'scheduling/appointments', :action => 'edit', :id => appointment.id)
		sweeper.expire_action(:controller => 'management/appointments', :action => 'edit', :id => appointment.id)

		sweeper.expire_action(:controller => 'admin/appointments', :action => 'show', :id => appointment.id)
		sweeper.expire_action(:controller => 'scheduling/appointments', :action => 'show', :id => appointment.id)
		sweeper.expire_action(:controller => 'scheduling_leader/appointments', :action => 'show', :id => appointment.id)
		sweeper.expire_action(:controller => 'service/appointments', :action => 'show', :id => appointment.id)
		sweeper.expire_action(:controller => 'management/appointments', :action => 'show', :id => appointment.id)
	
    sweeper.expire_action(:controller => 'admin/appointments', :action => 'index')
		sweeper.expire_action(:controller => 'scheduling/appointments', :action => 'index')
		sweeper.expire_action(:controller => 'scheduling_leader/appointments', :action => 'index')
		sweeper.expire_action(:controller => 'service/appointments', :action => 'index')
		sweeper.expire_action(:controller => 'management/appointments', :action => 'index')
  end

	def self.expire_completion_views(completion, sweeper)
		puts "EXPIRING COMPLETION: #{completion.id}"
        sweeper.expire_action(:controller => 'admin/completions', :action => 'show', :id => completion.id)
        sweeper.expire_action(:controller => 'scheduling/completions', :action => 'show', :id => completion.id)
        sweeper.expire_action(:controller => 'scheduling_leader/completions', :action => 'show', :id => completion.id)
        sweeper.expire_action(:controller => 'service/completions', :action => 'show', :id => completion.id)
        #sweeper.expire_action(:controller => 'management/completions', :action => 'show', :id => completion.id)
	end

	def self.expire_cancellation_views(cancellation, sweeper)
        puts "EXPIRING CANCELLATION: #{cancellation.id}"
        sweeper.expire_action(:controller => 'admin/cancellations', :action => 'show', :id => cancellation.id)
        sweeper.expire_action(:controller => 'scheduling/cancellations', :action => 'show', :id => cancellation.id)
        sweeper.expire_action(:controller => 'scheduling_leader/cancellations', :action => 'show', :id => cancellation.id)
        sweeper.expire_action(:controller => 'service/cancellations', :action => 'show', :id => cancellation.id)
        #sweeper.expire_action(:controller => 'management/cancellations', :action => 'show', :id => cancellation.id)
	end

	def self.expire_appointment_request_views(appointment_request, sweeper)
	    puts "EXPIRING APPOINTMENT_REQUEST: #{appointment_request.id}"
        sweeper.expire_action(:controller => 'admin/appointment_requests', :action => 'show', :id => appointment_request.id)
        sweeper.expire_action(:controller => 'scheduling/appointment_requests', :action => 'show', :id => appointment_request.id)
        sweeper.expire_action(:controller => 'scheduling_leader/appointment_requests', :action => 'show', :id => appointment_request.id)
        sweeper.expire_action(:controller => 'service/appointment_requests', :action => 'show', :id => appointment_request.id)
        #sweeper.expire_action(:controller => 'management/appointment_requests', :action => 'show', :id => appointment_request.id)
	end
end