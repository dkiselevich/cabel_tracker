# encoding: utf-8
class OtHelper

  STATE_IDS = {
    "di" => 601,
    "wK" => 605,
    "om" => 602
  }

	def self.update_modem_type_for_job_from_ot(client, scheduling_user, job)
		puts "update_modem_type_for_job_from_ot for job with OT iD: #{job.ot_id}"
		id = job.ot_id
		begin
	    puts "attempting to retrieve job with OMNITRACKER-ID: #{id}"
	    job_info = OtHelper.get_job_info(id, client)
	    if job_info.blank? then
	    	puts "no job retrieved. probably blocked -> aborting"
	      return
	    end
	    new_id = OtHelper.get_or_create_modem_type(job_info["modemtyp"]).id
	    puts "old modem type id: #{job.modemtyp_id}"
	    puts "new modem type id: #{new_id}"
	    if (new_id != job.modemtyp_id)
	    	job.modemtyp_id = new_id
		    puts "attempting to save job"
	      result = job.save :validate => false
	      puts "Result: #{result}"
	      if !job.errors.empty?
	      	puts "Validierungsfehler:"
		      job.errors.each {|e| puts e}
		    end
	    end    
	  rescue Exception => e
	  	puts "ERROR SAVING"
	    puts e.message  
	    #puts e.backtrace.inspect
	  end
	end

	def self.get_zde_service_jobs(client, scheduling_user)

    a = Mechanize.new  
    # Windows specific
    a.agent.http.verify_mode = OpenSSL::SSL::VERIFY_NONE    
    login_ot(a, client)
    
    
		url = Settings.OT_LIST_URL.gsub("##FOLDER_ID##",Settings.OT_SERVICE_JOB_FOLDER_ID.to_s).gsub("##NUMBER_OF_JOBS_TO_GET##",Settings.MAX_NUMBER_OF_JOBS_TO_GET.to_s)
    #url = "https://otweb.kabelbw.com/otwg/GetObjectList.aspx?folder=684&path=&start=0&count=500&fieldpath=&request=0&guid=&sortcol=-1"
    puts "URL: "+url
    a.get(url)
    text = a.page.body.to_s
    text = text.gsub("This XML file does not appear to have any style information associated with it. The document tree is shown below.","")
    #text = text.gsub(/data>.*/, "data>").gsub(/<script>.*script>/,"")
    puts "--------------------------------------------------"
    #puts text
    
    doc = Nokogiri::XML(text)
    rows = doc.xpath('//row').map do |i|
    	number = -1
      {
        'id' => i["id"],
				'ak' => i.xpath('col')[number+=1].text,
        'auftragsnummer' => i.xpath('col')[number+=1].text,
        'netzbetreiber' => i.xpath('col')[number+=1].text,
        'objektpartner' => i.xpath('col')[number+=1].text,
				'verantwortlich' => i.xpath('col')[number+=1].text,
				'zustand' => i.xpath('col')[number+=1].text,
				'vereinbarter_kundentermin' => i.xpath('col')[number+=1].text,
				'ad_zeitfenster_fruehester_begin' => i.xpath('col')[number+=1].text,
				'ad_eitfenster_spaetester_begin' => i.xpath('col')[number+=1].text,
				'kundentermin_start' => i.xpath('col')[number+=1].text,
				'e_vsip_techniker' => i.xpath('col')[number+=1].text,
				'ort' => i.xpath('col')[number+=1].text,
				'plz' => i.xpath('col')[number+=1].text,
				'strasse' => i.xpath('col')[number+=1].text,
				'hausnummer' => i.xpath('col')[number+=1].text,
				'zusatz' => i.xpath('col')[number+=1].text,
				'teambereich' => i.xpath('col')[number+=1].text,
				'eingangsdatum_kg' => i.xpath('col')[number+=1].text,
				'produkte' => i.xpath('col')[number+=1].text,
				'wirktiefe' => i.xpath('col')[number+=1].text,
				'wiedervorlage' => i.xpath('col')[number+=1].text,
				'letzter_barbeiter' => i.xpath('col')[number+=1].text,
				'letzte_aenderung' => i.xpath('col')[number+=1].text
      }
    end

    puts rows.length.to_s + " service jobs found"
    blocked_jobs = Array.new
    existing_jobs = Array.new
    new_jobs = Array.new

    rows.each do |r|
    	puts "processing job:"
      puts r["id"] +": "+r["auftragsnummer"]
      puts "zustand: "+r["zustand"]
      if r["zustand"] == "di" then
     		ct_jobs = ServiceJob.where(:ot_auftragsnummer => r["auftragsnummer"])

        if ct_jobs.blank? then

          new_job = add_service_job_from_ot(r["id"], client)
          new_job.ot_auftragsnummer = r["auftragsnummer"]
          new_job.save
          if (new_job.blank?) then
            blocked_jobs << r
          else 
          	#add ct job to row and add all to new_jobs
          	r["job"] = new_job
            new_jobs << r
          end
        else
        	puts "job already exists in CT database"
        	#add ct job to row and add all to existing_jobs
          r["job"] = ct_jobs.first
        	existing_jobs << r
        end
      end

      puts "--------------------"
    end

    puts "sending email to #{scheduling_user.email}..."
    ImportMailer.report_service_job_imports(new_jobs, existing_jobs, blocked_jobs, scheduling_user).deliver
    puts "done."
    Admin::LogsController.create("System imported new service jobs") 
  end

  def self.get_zde_jobs(client, scheduling_user)
	
	  ots = OtScraper.new
	  rows = ots.get_job_list({:ot_login=> client.ot_login, :ot_password=> client.ot_password}, "Zustand zdE", "21259")
	  ots.logout

    puts rows.length.to_s + " jobs found"
    blocked_jobs = Array.new
    existing_jobs = Array.new
    new_jobs = Array.new

    rows.each do |r|
    	puts "processing job:"
      puts r["id"] +": "+r["AuftragsNr."] +": "+r["Zustand"]

     # if r["zustand"] == "zdE" then
     		ct_jobs = Job.where(:ot_auftragsnummer => r["AuftragsNr."])
        if ct_jobs.blank? then
          new_job = add_job_from_ot(r["id"], client)
          if (new_job.blank?) then
            blocked_jobs << r
          else 
          	#add ct job to row and add all to new_jobs
          	r["job"] = new_job
            new_jobs << r
          end
        else
        	puts "job already exists in CT database"
        	#add ct job to row and add all to existing_jobs
          r["job"] = ct_jobs.first
        	existing_jobs << r
        end
      #end
      puts "--------------------"
    end

    puts "sending email to #{scheduling_user.email}..."
    ImportMailer.report_imports(new_jobs, existing_jobs, blocked_jobs, scheduling_user).deliver
    puts "done."
    Admin::LogsController.create("System imported new jobs") 
  end



  def self.get_ta_jobs(client, scheduling_user)
		puts "attempting to get TA jobs from OT and update jobs in CT"
	  ots = OtScraper.new
	  rows = ots.get_job_list({:ot_login=> client.ot_login, :ot_password=> client.ot_password}, "Zustand ta", "20143")
	  ots.logout

    puts rows.length.to_s + " jobs found"
    #blocked_jobs = Array.new
    existing_jobs = Array.new
    #new_jobs = Array.new

    rows.each do |r|
    	puts "processing job:"
      puts r["id"] +": "+r["AuftragsNr."] +": "+r["Zustand"]
   		ct_jobs = Job.where(:ot_auftragsnummer => r["AuftragsNr."])
      if ct_jobs.blank? then
				#ignore 
      else
      	puts "Found job in CT!"
        job = ct_jobs.first
        puts "Setting state to TA"
      	job.status = 4
      	puts "saving"
      	job.save

        r["job"] = job
      	existing_jobs << r
      end
      puts "--------------------"
    end

    #puts "sending email to #{scheduling_user.email}..."
    #ImportMailer.report_imports(new_jobs, existing_jobs, blocked_jobs, scheduling_user).deliver
    puts "done."
  end



  def self.test_scraper #deprecated
  	client = Client.find(3)
		ots = OtScraper.new
		begin
		  rows = ots.get_job_list({:ot_login=>client.ot_login, :ot_password=>client.ot_password}, "Zustand zdE","21259")
		  puts "***************"
		  #puts job_mappings[0][0].to_s

			puts rows.length.to_s + " jobs found"
	    blocked_jobs = Array.new
	    existing_jobs = Array.new
	    new_jobs = Array.new

	    rows.each do |r|
	    	puts "processing job:"
  	    puts r["id"] +": "+r["AuftragsNr."] +": "+r["Zustand"]

     # if r["zustand"] == "zdE" then
     		ct_jobs = Job.where(:ot_auftragsnummer => r["auftragsnummer"])
        if ct_jobs.blank? then
          new_job = add_job_from_ot(r["id"], client)
          if (new_job.blank?) then
            blocked_jobs << r
          else 
          	#add ct job to row and add all to new_jobs
          	r["job"] = new_job
            new_jobs << r
          end
        else
        	puts "job already exists in CT database"
        	#add ct job to row and add all to existing_jobs
          r["job"] = ct_jobs.first
        	existing_jobs << r
        end
      #end

      puts "--------------------"
    end
		ensure
		  ots.logout
		end
  end
=begin
  def self.get_jobs(number_of_jobs_to_get)
    a = Mechanize.new
    # Windows specific
    a.agent.http.verify_mode = OpenSSL::SSL::VERIFY_NONE    
    login_ot(a)
    #a.get("https://otweb.kabelbw.com/otwg/OTWGObjectListFrame1_2_2.aspx?ShortCutBarID=0")
    #a.page.forms.first.cbolistSF_LST_F = "21259"
    a.get(Settings.OT_LIST_URL.gsub("##NUMBER_OF_JOBS_TO_GET##",number_of_jobs_to_get.to_s))
    return OtHelper.get_jobs_from_text(a.page.body)
  end
=end

  def xml_special_request(a)
  	httpHeaders = {
   		"Connection"      => "keep-alive",
   		"Accept"          =>	"text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
   		"User-Agent"      => "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_4) AppleWebKit/537.22 (KHTML, like Gecko) Chrome/25.0.1364.99 Safari/537.22",
 		  "Accept-Encoding"	=> "gzip,deflate,sdch",
 		  "pt-Language"	    => "de-DE,de;q=0.8,en-US;q=0.6,en;q=0.4",
 		  "Accept-Charset"  => "ISO-8859-1,utf-8;q=0.7,*;q=0.3",
 		  "Cookie"          => a.cookie_jar.jar.to_s,
 		  "Pragma"	        => "no-cache",
 		  "Cache-Control"	  => "no-cache"
  	}
  	a.setHeaders(httpHeaders)

  end

  def self.test
  	get_new_jobs(Client.find(3),User.first)
  end

  def self.get_new_jobs(client, scheduling_user)
  	
  	#extract xml
    a = Mechanize.new #{ |a| a.set_proxy('localhost', 8888) }
    # Windows specific	
    a.agent.http.verify_mode = OpenSSL::SSL::VERIFY_NONE
    a.user_agent = 'Mozilla/5.0 (Windows NT 5.2; WOW64) AppleWebKit/536.11 (KHTML, like Gecko) Chrome/20.0.1132.47 Safari/536.11'
   

    login_ot(a, client)

    puts "loading shortcut bar"
    #select correct list page
    shortcutbar_url = "https://otweb.kabelbw.com/otwg/OTWGShortCutBar.aspx?ID=0"
		a.get(shortcutbar_url)

=begin
		form = a.page.forms.first
		# get the button you want from the form
		button = form.button_with(:value => "01. L4 Provide")
		puts "BUTTON:"
		puts button
		# submit the form using that button
		a.submit(form, button)
=end

    puts "attempting to set correct list page"

 		a.page.forms.first['ctl00$ctl06.x'] = "21"
 		a.page.forms.first['ctl00$ctl06.y'] = "19"
		a.page.forms.first.submit

 		a.get("https://otweb.kabelbw.com/otwg/OTWGObjectListFrame1_2_2.aspx?ShortCutBarID=0")
 		result = a.page.content.include?("SP NE4 Wiedervorlage")
		puts "Result: "+(result ? "SUCCESS" : "FAILURE")

=begin
 		#cbolistSF_LST_F selectedIndex = 40
 		a.page.forms.first['cbolistSF_LST_F'] = "21259"
 		a.page.forms.first['SF_LST_F'] = "Zustand zdE"
 		
 		puts a.cookie_jar.jar

 		do_postback("SF_LST_F","40",a)

		url = Settings.OT_LIST_URL.gsub("##FOLDER_ID##",Settings.OT_JOB_FOLDER_ID.to_s).gsub("##NUMBER_OF_JOBS_TO_GET##","1")
 		puts "URL:"
 		puts url
    
    a.get(url) #,{},'cookie' => a.cookies[0]) #Settings.MAX_NUMBER_OF_JOBS_TO_GET.to_s))
    xml = a.page.body
    puts xml
=end
    logout_ot(a)

    #result = xml.include?("zdE") ? "SUCCESS" : "FAILURE"
    #puts "Result: "+result
    
    #extract jobs from xml
  	#return get_jobs_from_text(xml, client, scheduling_user)
  end


=begin
 		a.page.forms.first['cbolistcboViewAction'] = "0"
		a.page.forms.first['cboViewAction'] = "Ansicht:"
		a.page.forms.first['cbolistcboViewName'] = "0"
		a.page.forms.first['cboViewName'] = "(benutzerdefiniert)"
		a.page.forms.first['cbolistcboReports'] = "0"
		a.page.forms.first['cboReports'] = " "
		a.page.forms.first['cbolistcboTaskReport'] = "0"
		a.page.forms.first['cboTaskReport'] = " "
		a.page.forms.first['cbolistcboTaskExport'] = "0"
		a.page.forms.first['cboTaskExport'] = " "
		a.page.forms.first['cbolistSF_MEN_F'] = "0"
		a.page.forms.first['SF_MEN_F'] = "Filter:"

		a.page.forms.first['__LASTFOCUS'] = ""
		a.page.forms.first['otwgc_Start'] = "0"
		a.page.forms.first['otwgc_SelectedItems'] = ",37138376,"
		a.page.forms.first['otwgc_CheckedItems'] = ",37138376,"
		a.page.forms.first['otwgc_FirstSelected'] = ""
		a.page.forms.first['otwgc_LastClickedColumn'] = "0"
		a.page.forms.first['otwgc_LastClickedHeader'] = "0"
		a.page.forms.first['otwgc_HeaderPath'] = ""
		a.page.forms.first['otwgc_SelectedPath'] = "0"
		a.page.forms.first['otwgc_ExpandedPaths'] = ""
		a.page.forms.first['otwgc_fieldId'] = ""
		a.page.forms.first['otwgc_SelectedRow'] = "-1"
		a.page.forms.first['GRID_Properties'] = ""

		a.page.forms.first['__VIEWSTATE'] = "/wEPDwULLTE4ODU2MzExNTMPFgIeDVNob3J0Q3V0QmFySURmFgYCAQ8WAh4EVGV4dAVNPGxpbmsgaHJlZj0iU3lzdGVtX1N0eWxlU2hlZXQuY3NzPzguNS4zMDAiIHR5cGU9InRleHQvY3NzIiByZWw9InN0eWxlc2hlZXQiLz5kAgMPFgIfAQVQPGxpbmsgaHJlZj0iU3lzdGVtX1N0eWxlU2hlZXRNb3ouY3NzPzguNS4zMDAiIHR5cGU9InRleHQvY3NzIiByZWw9InN0eWxlc2hlZXQiLz5kAgUPZBYCAgEPZBYEAgEPZBYEAgEPZBYCZg9kFhQCAQ9kFgJmDw8WAh8BBRQzS2FiZWwgR21iSCBhYm1lbGRlbmRkAgMPZBYCZg8PFgYfAQUDTmV1HgdUb29sVGlwBQNOZXUeB0VuYWJsZWRoZGQCBQ9kFgJmDxAPFggeDEF1dG9Qb3N0QmFja2geDURhdGFUZXh0RmllbGQFBG5hbWUeDkRhdGFWYWx1ZUZpZWxkBQV2YWx1ZR4LXyFEYXRhQm91bmRnFgIeCG9uY2hhbmdlBZkBaWYodGhpcy5nZXRBdHRyaWJ1dGUoJ3NlbGVjdGVkSW5kZXgnKT09JzEnKXdpbmRvdy5vcGVuKCdPVFdHU2F2ZVZpZXdPckZpbHRlci5hc3B4P2ZsZD04MzEmdHlwZT1WaWV3JywnVmlldycsJ2hlaWdodD0zNjAsd2lkdGg9MzM1Jyk7dGhpcy5zZWxlY3RlZEluZGV4PTA7EBUCCEFuc2ljaHQ6CVNwZWljaGVybhUCATABMRQrAwJnZxYBZmQCBg9kFgJmDxAPFgYfBQUEbmFtZR8GBQJpZB8HZ2QQFQYTKGJlbnV0emVyZGVmaW5pZXJ0KQxEZWZhdWx0IFZpZXcMMTEwNTE5NzMucGRmDkluc3RhbGxhdGlvbmVuC01TVy1BbnNpY2h0GG5hY2ggSW5zdGFsbGF0aW9uc3Rlcm1pbhUGATAGMTM3MDA1BjEyMTE5MgYxMjEzNTUGMTE2Mzc0BjEyMzI5MBQrAwZnZ2dnZ2cWAWZkAgcPZBYCZg8QZBAVAg1MaXN0ZW5hbnNpY2h0C0JhdW1hbnNpY2h0FQIBMQE1FCsDAmdnFgECAWQCCQ9kFgJmDw8WAh4HVmlzaWJsZWhkZAIKD2QWAmYPDxYCHwloZGQCCw9kFgJmDw8WAh8JZ2RkAgwPZBYCZg8PFgQfAgUZU2hvcnRjdXRsZWlzdGUgYXVzYmxlbmRlbh8JZ2RkAg0PZBYCZg8PFgQfAgUVT3JkbmVyYmF1bSBhdXNibGVuZGVuHwlnZGQCAw9kFgJmD2QWBgIBD2QWAmYPDxYCHwEFCUF1ZmdhYmVuOmRkAgIPZBYCZg8QDxYGHwUFBG5hbWUfBgUJbG9uZ19uYW1lHwdnFgIfCAWFBGlmICh0aGlzLmdldEF0dHJpYnV0ZSgnc2VsVmFsJykuY2hhckF0KDApPT0nRScpIHtnZXRFbGVtZW50QnlJZCgnY2JvVGFza0V4cG9ydCcpLnN0eWxlLmRpc3BsYXk9J2Jsb2NrJztnZXRFbGVtZW50QnlJZCgnY2JvVGFza1JlcG9ydCcpLnN0eWxlLmRpc3BsYXk9J25vbmUnO31lbHNlIGlmKHRoaXMuZ2V0QXR0cmlidXRlKCdzZWxWYWwnKS5jaGFyQXQoMCk9PSdSJykge2dldEVsZW1lbnRCeUlkKCdjYm9UYXNrRXhwb3J0Jykuc3R5bGUuZGlzcGxheT0nbm9uZSc7Z2V0RWxlbWVudEJ5SWQoJ2Nib1Rhc2tSZXBvcnQnKS5zdHlsZS5kaXNwbGF5PSdibG9jayc7fWVsc2V7Z2V0RWxlbWVudEJ5SWQoJ2Nib1Rhc2tFeHBvcnQnKS5zdHlsZS5kaXNwbGF5PSdub25lJztnZXRFbGVtZW50QnlJZCgnY2JvVGFza1JlcG9ydCcpLnN0eWxlLmRpc3BsYXk9J25vbmUnO2lmICh0aGlzLmdldEF0dHJpYnV0ZSgnc2VsVmFsJykuY2hhckF0KDApPT0nQScpIHtfX2RvUG9zdEJhY2soJ09UV0dBY3Rpb25CdXR0b24nLCAnJyk7fX0QFQkBIBUtLS0gRHJ1Y2thdWZnYWJlbiAtLS0KKHN0YW5kYXJkKQ1BdWZ0cmFnc2RydWNrE1BsYW51bmdzYW5zY2hyZWliZW4VLS0tIEFrdGlvbnNsaXN0ZW4gLS0tGWluaXRpYWxfU2V0RW1haWx2b3JoYW5kZW4eTWFudWVsbGUgUsO8Y2tydWZiaXR0ZSAoRW1haWwpHE1hbnVlbGxlIFLDvGNrcnVmYml0dGUgKFNNUykVCQEwAAxSXyhzdGFuZGFyZCkPUl9BdWZ0cmFnc2RydWNrFVJfUGxhbnVuZ3NhbnNjaHJlaWJlbgEgG0FfaW5pdGlhbF9TZXRFbWFpbHZvcmhhbmRlbiBBX01hbnVlbGxlIFLDvGNrcnVmYml0dGUgKEVtYWlsKR5BX01hbnVlbGxlIFLDvGNrcnVmYml0dGUgKFNNUykUKwMJZ2dnZ2dnZ2dnFgFmZAIDD2QWBGYPEA8WBh8FBQRuYW1lHwYFAmlkHwdnZBAVBgEgE0hUTUwgKG1pdCBWb3JzY2hhdSkSUERGIChtaXQgVm9yc2NoYXUpA0RPQwNQREYDWExTFQYBMAhodG1sX2ludAdwZGZfaW50A2RvYwNwZGYJeGxzX3ByaW50FCsDBmdnZ2dnZxYBZmQCAQ8QDxYGHwUFBG5hbWUfBgUCaWQfB2dkEBUEASADQ1NWA01EQgNUWFQVBAEwA2NzdgNtZGIDdHh0FCsDBGdnZ2cWAWZkAgMPZBYCZg9kFgICAQ9kFgJmD2QWAmYPZBYCAgIPZBYCZg9kFgJmD2QWAgIBD2QWAmYPZBYCZg9kFgICAQ9kFgJmD2QWAmYPZBYCZg9kFgJmD2QWAmYPZBYCAgEPZBYCZg9kFgJmDxBkZBYBZmQYAQUeX19Db250cm9sc1JlcXVpcmVQb3N0QmFja0tleV9fFgIFBW90d2djBQVzZm1pbi9nRZzGNlgO/sJ+4xSfoo2pej1c"
		a.page.forms.first['__EVENTVALIDATION'] = "//wEWBALBpfnkAQLWraulBAKMx5ndAwLw5IbDD74XbT0tf6st4oBajfVU86gkR5wD"
=end

  def self.get_jobs_from_text(text, client, scheduling_user)
    text = text.gsub("This XML file does not appear to have any style information associated with it. The document tree is shown below.","")
    doc = Nokogiri::XML(text)
    rows = doc.xpath('//row').map do |i|
    	number = 0
      {

        'id' => i["id"],
        'icon' => i.xpath('col')[0]["icon"], 
        'anhaenge' => i.xpath('col')[number+=1].text, 

        'auftragsart' => i.xpath('col')[number+=1].text, 

        'ne4_objektpartner' => i.xpath('col')[number+=1].text, 
        'vertriebspartner' => i.xpath('col')[number+=1].text, 

        'auftragsart' => i.xpath('col')[number+=1].text, 
        'workorder' => i.xpath('col')[number+=1].text, 
        'cleverpro' => i.xpath('col')[number+=1].text, 
        'modemtyp' => i.xpath('col')[number+=1].text, 
        'modemaktivierung' => i.xpath('col')[number+=1].text, 
        'zustand' => i.xpath('col')[number+=1].text, 
        'verbindlicher_installationstermin' => i.xpath('col')[number+=1].text, 
        'tageszeit' => i.xpath('col')[number+=1].text, 
        'installationstermin' => i.xpath('col')[number+=1].text, 
        'bemerkungen' => i.xpath('col')[number+=1].text, 
        'wiedervorlagetermin' => i.xpath('col')[number+=1].text, 
        'wartegrund' => i.xpath('col')[number+=1].text, 
        'tatsaechliche_abschlusszeit' => i.xpath('col')[number+=1].text, 
        'nachname' => i.xpath('col')[number+=1].text, 
        'vorname' => i.xpath('col')[number+=1].text, 
        'plz' => i.xpath('col')[number+=1].text, 
        'ort' => i.xpath('col')[number+=1].text, 
        'strasse' => i.xpath('col')[number+=1].text, 
        'hausnummer' => i.xpath('col')[number+=1].text, 
        'zusatz' => i.xpath('col')[number+=1].text, 
        'uep_typ' => i.xpath('col')[number+=1].text, 
        'zuweisung_an_partner' => i.xpath('col')[number+=1].text, 
        'letzte_aenderung' => i.xpath('col')[number+=1].text, 
        'team' => i.xpath('col')[number+=1].text, 
        'hub' => i.xpath('col')[number+=1].text, 
        'auftragsnummer' => i.xpath('col')[number+=1].text
      }
    end

    puts rows.length.to_s + " jobs found"
    blocked_jobs = Array.new
    existing_jobs = Array.new
    new_jobs = Array.new

    rows.each do |r|
    	puts "processing job:"
      puts r["id"] +": "+r["auftragsnummer"] +": "+r["zustand"]

     # if r["zustand"] == "zdE" then
     		ct_jobs = Job.where(:ot_auftragsnummer => r["auftragsnummer"])
        if ct_jobs.blank? then
          new_job = add_job_from_ot(r["id"], client)
          if (new_job.blank?) then
            blocked_jobs << r
          else 
          	#add ct job to row and add all to new_jobs
          	r["job"] = new_job
            new_jobs << r
          end
        else
        	puts "job already exists in CT database"
        	#add ct job to row and add all to existing_jobs
          r["job"] = ct_jobs.first
        	existing_jobs << r
        end
      #end

      puts "--------------------"
    end

    #puts "sending email to #{scheduling_user.email}..."
    #ImportMailer.report_imports(new_jobs, existing_jobs, blocked_jobs, scheduling_user).deliver
    #puts "done."
  end
    	
  def self.get_service_jobs_from_text(text, client, scheduling_user)
    text = text.gsub("This XML file does not appear to have any style information associated with it. The document tree is shown below.","")
    text = text.gsub(/data>.*/, "data>").gsub(/<script>.*script>/,"")
    puts "--------------------------------------------------"
    #puts text

    
    doc = Nokogiri::XML(text)
    rows = doc.xpath('//row').map do |i|
    	number = -1
      {
        'id' => i["id"],
				'ak' => i.xpath('col')[number+=1].text,
				'auftragsnummer' => i.xpath('col')[number+=1].text,
				'verantwortlich' => i.xpath('col')[number+=1].text,
				'zustand' => i.xpath('col')[number+=1].text,
				'vereinbarter_kundentermin' => i.xpath('col')[number+=1].text,
				'ad_zeitfenster_fruehester_begin' => i.xpath('col')[number+=1].text,
				'ad_eitfenster_spaetester_begin' => i.xpath('col')[number+=1].text,
				'kundentermin_start' => i.xpath('col')[number+=1].text,
				'e_vsip_techniker' => i.xpath('col')[number+=1].text,
				'ort' => i.xpath('col')[number+=1].text,
				'plz' => i.xpath('col')[number+=1].text,
				'strasse' => i.xpath('col')[number+=1].text,
				'hausnummer' => i.xpath('col')[number+=1].text,
				'zusatz' => i.xpath('col')[number+=1].text,
				'teambereich' => i.xpath('col')[number+=1].text,
				'eingangsdatum_kg' => i.xpath('col')[number+=1].text,
				'produkte' => i.xpath('col')[number+=1].text,
				'wirktiefe' => i.xpath('col')[number+=1].text,
				'wiedervorlage' => i.xpath('col')[number+=1].text,
				'letzter_barbeiter' => i.xpath('col')[number+=1].text,
				'letzte_aenderung' => i.xpath('col')[number+=1].text
      }
    end

    puts rows.length.to_s + " service jobs found"
    blocked_jobs = Array.new
    existing_jobs = Array.new
    new_jobs = Array.new

    rows.each do |r|
    	puts "processing job:"
      puts r["id"] +": "+r["auftragsnummer"]

     # if r["zustand"] == "zdE" then
     		ct_jobs = ServiceJob.where(:ot_auftragsnummer => r["auftragsnummer"])
        if ct_jobs.blank? then
          new_job = add_service_job_from_ot(r["id"], client)
          if (new_job.blank?) then
            blocked_jobs << r
          else 
          	#add ct job to row and add all to new_jobs
          	r["job"] = new_job
            new_jobs << r
          end
        else
        	puts "job already exists in CT database"
        	#add ct job to row and add all to existing_jobs
          r["job"] = ct_jobs.first
        	existing_jobs << r
        end
      #end

      puts "--------------------"
    end

    puts "sending email to #{scheduling_user.email}..."
    ImportMailer.report_service_job_imports(new_jobs, existing_jobs, blocked_jobs, scheduling_user).deliver
    puts "done."

  end
    	
  def self.update_ot?
    #return !(Rails.env == "development")
    return Settings.UPDATE_OT 
  end

  def self.set_state_to_di(id, a = nil, date_time, note, client)
    puts "attempting to set state to 'di' on job: #{id}"
    if !update_ot? then
      puts "Settings.UPDATE_OT: false"
      return false
    end

    if a == nil then
      a = Mechanize.new
      # Windows specific
      a.agent.http.verify_mode = OpenSSL::SSL::VERIFY_NONE     
      login_ot(a, client)
    end

    puts "date_time: #{date_time}"
    puts "note: #{note}"

    url = Settings.OT_DETAILS_URL.gsub("##ID##",id.to_s)
    puts "URL: "+url
    a.get(url)
    if a.page.parser.xpath("//input[@id='cboinpfld4481']").blank? then
      puts "Auftrag ist gesperrt"
      return false
    else
      form = a.page.forms.first
      p "alter Zustand:"+form.fld4481.to_s
      p "alter Termin:"+form.fld4498.to_s
      form.fld4481 = "di"
      form.cbolistfld4481 = STATE_IDS["di"]
      form.fld4498 = date_time.strftime("%m/%d/%Y %H:%M:%S %p") #"28.11.2011 12:09:09"
      form.tsm_4482_new = note
      p "neuer Zustand:"+form.fld4481.to_s
      p "neuer Termin:"+form.fld4498.to_s
      do_postback('otBtnOK','',a)

      if a.page.parser.xpath("//input[@id='cboinpfld4481']").blank? then
        puts "speichern erfolgreich"
        return true
      else
        return false
      end
    end
  end

  def self.set_state_to_wk(id, a = nil, waiting_cause_code, note, client)
    begin
      puts "attempting to set state to 'wk' on job: #{id}"
      if !update_ot? then
        puts "Settings.UPDATE_OT: false"
        return false
      end

      if a == nil then
        a = Mechanize.new
        # Windows specific
        a.agent.http.verify_mode = OpenSSL::SSL::VERIFY_NONE        
        login_ot(a, client)
      end

      puts "waiting_cause_code: #{waiting_cause_code}"
      puts "note: #{note}"

      url = Settings.OT_DETAILS_URL.gsub("##ID##",id.to_s)
      puts "URL: "+url
      a.get(url)

      if a.page.parser.xpath("//input[@id='cboinpfld4481']").blank? then
        puts "Auftrag ist gesperrt"
        return false
      else
        form = a.page.forms.first
        p "alter Zustand:"+form.fld4481.to_s
        form.fld4481 = "wK"
        form.cbolistfld4481 = STATE_IDS["wK"]
        form.tsm_4482_new = note
        p "neuer Zustand:"+form.fld4481.to_s
        
        change_to_tab("tabbtn3",a)
        waiting_cause = WaitingCause.where(:code => waiting_cause_code).first
        form = a.page.forms.first

        p "alter Wartegrund:"+form.fld5403.to_s
        form.fld5403 = waiting_cause.full_name
        form.cbolistfld5403 = waiting_cause.ot_id
        p "neuer Wartegrund:"+form.fld5403.to_s
        
        do_postback('otBtnOK','',a)

        if a.page.parser.xpath("//input[@id='cboinpfld4481']").blank? then
          puts "speichern erfolgreich"
          return true
        else
          return false
        end
      end
    rescue Exception => e  
      puts e.message  
      #puts e.backtrace.inspect  
      return false
    end
  end

  def self.do_postback(target, argument, a)
    form = a.page.forms.first
    form['__EVENTTARGET'] = target
    form['__EVENTARGUMENT'] = argument
    form.submit
  end

  def self.login_ot(a, client = nil)
  	puts "attempting to login"
    a.get Settings.OT_LOGIN_URL
    form = a.page.forms.first
    form.theloginbox = client.ot_login
    form.thepasswordbox = client.ot_password
    do_postback('thebutton','',a)
  end


  def self.logout_ot(a)
    puts "attempting to logout"
    a.get Settings.OT_TEST_STATUS_URL
    form = a.page.forms.first
    #form.theloginbox = client.ot_login
    #form.thepasswordbox = client.ot_password
    do_postback('otBtnLogout','',a)

		puts "login status: " + is_logged_in?(a).to_s
  end

  def self.is_logged_in?(a)
  	a.get Settings.OT_TEST_STATUS_URL
    a.page.forms.first.blank? ? false : true
  end

  def self.get_field_by_id(id,a)
  	#puts "getting field with id: #{id}"
  	if a.page.parser.xpath("//div[@id='#{id}']").first then

      if (id == "rdo2153") then #beschreibung bei service jobs
        a.page.parser.xpath("//div[@id='#{id}']").first.to_s.gsub(/<div id="rdo2153" [^>]*>/,'').gsub("</div>","").gsub("<br>","\n")
      else
        a.page.parser.xpath("//div[@id='#{id}']").first.text
      end
  	else
      if a.page.parser.xpath("//input[@id='#{id}']").first then
        a.page.parser.xpath("//input[@id='#{id}']").first.value
      else
        ""
      end
    end
  end

  def self.get_trs_from_table(id,a)
        arr = []
        trs = a.page.parser.xpath("//table[@id='#{id}']/tr")
        trs.each do |tr|
          arr << tr.text.gsub("\t","").gsub("\r\n","")
        end
        return arr
  end

  def self.change_to_tab(tab, a)
    do_postback(tab,'',a)
  end

  def self.add_results_from_mappings(results, mappings, a)
      mappings.each_key do |key|
        results[key] = get_field_by_id(mappings[key],a)
      end
  end


  def self.get_job_info(id, client, a = nil)
    begin
    job_info = Hash.new

  	a = Mechanize.new if a == nil
    # Windows specific
    a.agent.http.verify_mode = OpenSSL::SSL::VERIFY_NONE
    
    login_ot(a, client)
  	
  	url = Settings.OT_DETAILS_URL.gsub("##ID##",id.to_s)
  	a.get(url)

  	#test if access not blocked
    if !(a.page.parser.xpath("//div[@id='rdo4457']").first)
    	puts "can't find field 'kunde' -> assume job is blocked" #no access
	    logout_ot(a)
    	return nil
    end

    hidden_slaves_node = a.page.parser.xpath("//input[@id='hiddenSlaves']").first

    if hidden_slaves_node.blank? then
      puts "SKIPPING JOB with id #{id} because locked!!" #no (write) access atm because somebody else has the job opened
      return nil
    end
		
		hidden_slaves = hidden_slaves_node.attributes["value"]
    hid_uid_node = a.page.parser.xpath("//input[@id='hidUID']").first
    if hid_uid_node.blank? then
      puts "SKIPPING JOB with id #{id} because locked!!"
      return nil
    end

  	hid_uid = hid_uid_node.attributes["value"]

    results = Hash.new
  	mappings1 = {
  	"auftragsart" => "rdo4504",
  	"workorder" => "rdo4538",
  	"kunde" => "rdo4457",
  	"kunde_telefon_privat" => "rdo4460",
  	"kunde_telefon_mobil" => "rdo4459",
    "verbindlicher_installationstermin" => "rdo5645",
    "verbindliche_tageszeit" => "rdo5646",
  	"ot_auftragsnummer" => "rdo4506",
  	"kunde_telefon_geschaeftlich" => "rdo4458",
  	"kunde_email" => "rdo4456",
  	"status" => "rdo4481",
  	"installationstermin" => "rdo4498",
  	"bemerkungen" => "rdo5064",
  	"rueckgabegrund" => "rdo5401",

  	#first tab
  	"clever_pro" => "rdo5653",
  	"modemtyp" => "rdo5774",
  	"unbundled" => "rdo5640",
  	"modem_aktivierung" => "rdo4568",
  	#"Kommentare" => "fld4482",
  	"auftragskommentare" => "rdo4566",
  	"beschreibung" => "rdo4503",
  	"aufnahmezeitpunkt" => "rdo4515",
  	"annahme_des_tickets" => "rdo4499"
  	}
    results.merge! get_data_from_tab("tabbtn0",mappings1,a)
  	#kommentare sind speziell
  	results["kommentare"] = get_trs_from_table("fld4482",a)

  	mappings2 = {
  	"grundstuecks_adresse" => "rdo5418",
    "uep" => "rdo4476"
  	}
    results.merge! get_data_from_tab("tabbtn1",mappings2,a)
   
	  results["uep_ot_id"] = a.page.parser.xpath("//div[@id='opn4476']").first.attributes["refid"].to_s
	  
  	mappings3 = {
  	"servicecode_01" => "rdo5348",
  	"servicecode_02" => "rdo5349",
  	"servicecode_03" => "rdo5350",
  	"servicecode_04" => "rdo5351",
  	"servicecode_05" => "rdo5352",
  	"servicecode_06" => "rdo5353",
  	"servicecode_07" => "rdo5354",
  	"servicecode_08" => "rdo5355",
  	"servicecode_09" => "rdo5356",
  	"servicecode_10" => "rdo5357",

  	"telefon_neu_01" => "rdo5368",
  	"telefon_neu_02" => "rdo5369",
  	"telefon_neu_03" => "rdo5370",
  	"telefon_neu_04" => "rdo5371",
  	"telefon_neu_05" => "rdo5372",
  	"telefon_neu_06" => "rdo5373",
  	}
    results.merge! get_data_from_tab("tabbtn2",mappings3,a)

    #get back to job list to prevent locking the job
    do_postback('otBtnMainView','',a)

    #get other fields for job from from additional xml(s?)
  	a.get Settings.OT_DATA_URL.gsub('##IDS##', hidden_slaves.to_s.gsub(';',':0;'))
    
  	doc = Nokogiri::XML(a.page.body)
  	#puts a.page.body
  	rows = doc.xpath('//field').map do |i|
  	{
  	  'master' => i['master'],
  	  'id' => i.xpath('value')[0]["id"],
  	  'type' => i.xpath('value')[0]["type"],
  	  'value' => i.xpath('value')[0].text
  	}
  	end
  #rdo5418 Grundstückdaten
  #rdo4476 UEP

    id_to_fieldnames = {
      "5418.4893"  => "eigentuemer_vorname",
      "5418.4888" => "eigentuemer_nachname",
      "5418.4332" => "ansprechpartner",
      "5418.4333" => "ansprechpartner_telefon",
      "5418.4882" => "eigentuemer_strasse_und_hausnummer",
      "5418.4883" => "eigentuemer_plz_und_ort",
      #"5418.4884" => "Adress_Zusatz", #???
      "5418.4884" => "eigentuemer_telefon_1",
      "5418.4890" => "eigentuemer_telefon_2",
      "5418.4887" => "eigentuemer_email",
      "5418.5297" => "rks_anlagennummer",
      "5418.3502" => "netztopologie",
      "5418.3279" => "objektart",
      "5418.4611" => "projektiert",
      "5418.2613" => "wohneinheiten",
      "5418.5487" => "erstinstallation_erfolgt",
      "5418.4349" => "aktive_kunden",
      "4476.2706" => "we_am_uep",
      "4476.3423" => "uep_typ",
      "4476.6033" => "mini_fibre_node",
      "4476.3578" => "anzahl_mitversorgung",
    }

    xml_hash = Hash.new
  	rows.each do |r|
      field_name = id_to_fieldnames[r["id"]].blank? ? r["id"] : id_to_fieldnames[r["id"]]
    	xml_hash[field_name] = r["value"]
  	end
    results.merge!(xml_hash)
    
    #anzeigen
    #results.each_key do |key|
    #  puts "#{key}: #{results[key]}"
    #end

    logout_ot(a)
	  return results
  rescue Exception => e  
  	puts "ERROR WHILE IMPORTING"
  	puts e.message  
  	#puts e.backtrace.inspect 
	  logout_ot(a)
  end


end

def self.get_service_job_info(id, client, a = nil)
    puts "getting service job with id #{id}"
    job_info = Hash.new

    a = Mechanize.new if a == nil
    # Windows specific
    a.agent.http.verify_mode = OpenSSL::SSL::VERIFY_NONE
    
    login_ot(a, client)
    
    url = Settings.OT_DETAILS_URL.gsub("##ID##",id.to_s)
    a.get(url)
=begin
    hidden_slaves_node = a.page.parser.xpath("//input[@id='hiddenSlaves']").first
    if hidden_slaves_node.blank? then
      puts "SKIPPING JOB with id #{id} because locked!!"
      return nil
    end
    hidden_slaves = hidden_slaves_node.attributes["value"]
    hid_uid_node = a.page.parser.xpath("//input[@id='hidUID']").first
    if hid_uid_node.blank? then
      puts "SKIPPING JOB with id #{id} because locked!!"
      return nil
    end
    hid_uid = hid_uid_node.attributes["value"]
=end
    results = Hash.new
    mappings1 = {
    "ot_auftragsnummer" => "rdo2782",
    "wirktiefe" => "rdo4295",
    "nachname_des_anrufers" => "rdo5770",
    "telefonnummer_des_anrufers" => "rdo3494",
    "auftragsklasse" => "rdo2317",
    "zustand" => "rdo2159",
    "vorname_des_anrufers" => "rdo5771",
    "email_des_anrufers" => "rdo3497",
    
    "kunde_string" => "rdo3481",
    "kunde_telefon_privat" => "rdo2313",
    "kunde_telefon_mobil" => "rdo2314",
    "kunde_telefon_geschaeftlich" => "rdo2315",
    "kunde_email" => "rdo2467",

    "kunde_plz" => "rdo3699",
    "kunde_strasse" => "rdo3700",
    "kunde_hausnummer_zusatz" => "rdo3696",
    "kunde_ort" => "rdo3697",
    "kunde_hausnummer" => "rdo3695",
    
    "uep_string" => "rdo3715",
    "uep_zustand" => "rdo3716",
    "uebk" => "rdo3713",
    "asb" => "rdo3704",
    "onkz" => "rdo3708",
    "we_angeschlossen" => "rdo3711",
    "we_max" => "rdo3712",
    "uebk" => "rdo3713",

    "uep_zustand_klartext" => "rdo3717",
    "grussz" => "rdo3706",
    "vrp" => "rdo3710",
    "c_linie" => "rdo3705",
    "versorgte_objekt" => "rdo3709",
    "inbetriebnahme" => "rdo3707",

    "ad_zeitfenster_fruehester_beginn" => "rdo5702",
    "e_vsip_kundentermin" => "rdo4929",
    "vereinbarter_kundentermin" => "rdo4931",
    "ad_zeitfenster_spaetester_beginn" => "rdo5703",
    "techniker" => "rdo4936",

    #first tab
    "textbaustein" => "rdo5293",
    "beschreibung" => "rdo2153",
    "rx" => "rdo3438",
    "tx" => "rdo3439",
    "bandbreite_downstream" => "rdo4991",
    "rufnummer_1" => "rdo4995",
    "modemtyp" => "rdo4881",
    "mahnlauf" => "rdo4993",

    "mac_adresse" => "rdo3470",
    "snr" => "rdo3440",
    "bandbreite_upstream" => "rdo4990",
    "rufnummer_2" => "rdo4996",
    "dienstmerkmalpaket" => "rdo4992",

    }
    results.merge! get_data_from_tab("tabbtn0",mappings1,a)

    #kommentare sind speziell
    results["kommentare"] = get_trs_from_table("fld2156",a)

    mappings2 = {
    "wartegrund" => "rdo5294",
    "wartegrund_beschreibung" => "rdo3734",
    "wiedervorlage" => "rdo4941",
    "netzebene" => "cbolistrdo3729",
    "stoerungswirkbreite" => "",
    "abschlussgrund" => "cboinprdo5290",
    "abschlussgrund_beschreibung" => "rdo3731",
    "stoerungsende_aus_kundensicht" => "rdo4902",
    "systemabschluss" => "rdo3764",
    "leistungsnachweis_vorhanden" => "rdo5639",
    }
    results.merge! get_data_from_tab("tabbtn1",mappings2,a)

=begin
    results["uep_ot_id"] = a.page.parser.xpath("//div[@id='opn4476']").first.attributes["refid"].to_s
=end

    #get back to job list to prevent locking the job
    do_postback('otBtnMainView','',a)

=begin
    #get other fields for job from from additional xml(s?)
    url2 = "https://otweb.kabelbw.com/otwg/GetObjectData.aspx?data=#{hidden_slaves.to_s.gsub(';',':0;')}:0;"
    a.get(url2)

    doc = Nokogiri::XML(a.page.body)
    #puts a.page.body
    rows = doc.xpath('//field').map do |i|
    {
      'master' => i['master'],
      'id' => i.xpath('value')[0]["id"],
      'type' => i.xpath('value')[0]["type"],
      'value' => i.xpath('value')[0].text
    }
    end
  #rdo5418 Grundstückdaten
  #rdo4476 UEP

    id_to_fieldnames = {
      "5418.4893"  => "eigentuemer_vorname",
      "5418.4888" => "eigentuemer_nachname",
      "5418.4332" => "ansprechpartner",
      "5418.4333" => "ansprechpartner_telefon",
      "5418.4882" => "eigentuemer_strasse_und_hausnummer",
      "5418.4883" => "eigentuemer_plz_und_ort",
      #"5418.4884" => "Adress_Zusatz", #???
      "5418.4884" => "eigentuemer_telefon_1",
      "5418.4890" => "eigentuemer_telefon_2",
      "5418.4887" => "eigentuemer_email",
      "5418.5297" => "rks_anlagennummer",
      "5418.3502" => "netztopologie",
      "5418.3279" => "objektart",
      "5418.4611" => "projektiert",
      "5418.2613" => "wohneinheiten",
      "5418.5487" => "erstinstallation_erfolgt",
      "5418.4349" => "aktive_kunden",
      "4476.2706" => "we_am_uep",
      "4476.3423" => "uep_typ",
      "4476.6033" => "mini_fibre_node",
      "4476.3578" => "anzahl_mitversorgung",
    }

    xml_hash = Hash.new
    rows.each do |r|
      field_name = id_to_fieldnames[r["id"]].blank? ? r["id"] : id_to_fieldnames[r["id"]]
      xml_hash[field_name] = r["value"]
    end
    results.merge!(xml_hash)
=end
    
    #anzeigen
    results.each_key do |key|
      puts "#{key}: #{results[key]} #{results[key].length}"
    end

    logout_ot(a)
    return results
  end



  def self.get_uep_info(id, a = nil, client)
    if a == nil then
      a = Mechanize.new
      # Windows specific
      a.agent.http.verify_mode = OpenSSL::SSL::VERIFY_NONE      
      login_ot(a,client)
    end

    puts "getting uep details for uep: #{id}"
    url = Settings.OT_DETAILS_URL.gsub("##ID##",id.to_s)
    a.get(url)

    results = Hash.new
    mappings1 = {
    "uip_string" => "rdo3585",
    "zustand" => "rdo3442",
    "inbetriebnahme" => "rdo2682",
    "dienstverfuegbarkeit" => "rdo3281",
    "typ" => "rdo3423",
    "we_am_uep" => "rdo2706",
    "aktive_kunden_am_uep" => "rdo5033"
    }
    results.merge! get_data_from_tab(nil,mappings1,a)
    results["ot_id"] = id

    #anzeigen
    #results.each_key do |key|
    #  puts "#{key}: #{results[key]}"
    #end      
    logout_ot(a)

    return results
  end

  def self.get_or_create_transfer_point(ot_id, mini_fibre_node = false, client = nil)
    puts "searching for uep with ot_id: #{ot_id}"
    query = TransferPoint.where(:ot_id => ot_id)
    if query.blank? then   
      puts "found none"
      return add_uep_from_ot(ot_id, mini_fibre_node, client)
    else
    	puts "found one"
      return query.first
    end
  end

  def self.add_uep_from_ot(ot_id, mini_fibre_node = false, client = nil)
    puts "creating new uep with ot_id: #{ot_id}"
    uep_info = OtHelper.get_uep_info(ot_id, nil, client)
    uep = TransferPoint.new
    uep.zustand = uep_info["zustand"]
    uep.dienstverfuegbarkeit = uep_info["dienstverfuegbarkeit"]
    uep.inbetriebnahme = uep_info["inbetriebnahme"]
    uep.typ = uep_info["typ"]
    uep.we_am_uep = uep_info["we_am_uep"]
    uep.aktive_kunden_am_uep = uep_info["aktive_kunden_am_uep"]
    uep.ot_id = uep_info["ot_id"]
    uep.uid = get_uep_uid(uep_info["uip_string"])
    uep.ort = get_uep_ort(uep_info["uip_string"])
    uep.plz = get_uep_plz(uep_info["uip_string"])
    uep.strasse = get_uep_strasse(uep_info["uip_string"])
    uep.hausnummer = get_uep_hausnummer(uep_info["uip_string"])
    uep.mini_fibre_node = mini_fibre_node # wird nur in auftragsformular angezeigt, drum übergabe
    uep.save
    return uep
  end

  #88255 Baienfurt Niederbiegen, Am Föhrenried 4 - ID: 0010641752
  def self.get_uep_uid(uep_string)
    puts "uep_string="+uep_string
    uep_array = uep_string.split(" - ")
    return uep_array.last.split(": ").last
  end
  
  def self.get_uep_strasse(uep_string)
    uep_array = uep_string.split(" - ")
    strasse_und_hausnummer = uep_array.first.split(", ").last
    strasse_und_hausnummer_array = strasse_und_hausnummer.split(" ")
    strasse_und_hausnummer_array = strasse_und_hausnummer_array - [strasse_und_hausnummer_array.last]
    return strasse_und_hausnummer_array.join(" ")
  end

  def self.get_uep_hausnummer(uep_string)
    uep_array = uep_string.split(" - ")
    strasse_und_hausnummer = uep_array.first.split(", ").last
    strasse_und_hausnummer_array = strasse_und_hausnummer.split(" ")
    return strasse_und_hausnummer_array.last
  end

  def self.get_uep_plz(uep_string)
    uep_array = uep_string.split(" - ")
    plz_und_ort = uep_array.first.split(", ").first
    plz_und_ort_array = plz_und_ort.split(" ")
    return plz_und_ort_array.first
  end
     
  def self.get_uep_ort(uep_string)
    uep_array = uep_string.split(" - ")
    plz_und_ort = uep_array.first.split(", ").first
    plz_und_ort_array = plz_und_ort.split(" ")
    plz_und_ort_array = plz_und_ort_array - [plz_und_ort_array.first]
    return plz_und_ort_array.join(" ")
  end 

  def self.get_data_from_tab(tabname, mappings, a)
    change_to_tab(tabname,a) unless tabname == nil
    results = Hash.new
    mappings.each_key do |key|
      results[key] = get_field_by_id(mappings[key],a)
    end
    return results
  end

  def self.get_or_create_buiding_type(s)
    query = BuildingType.where(:name => s)
    if query.blank? then
      bt = BuildingType.new
      bt.name = s
      bt.save
      return bt
    else
      return query.first
    end
  end

  def self.get_or_create_network_type(s)
    query = NetworkType.where(:name => s)
    if query.blank? then
      nt = NetworkType.new
      nt.name = s
      nt.save
      return nt
    else
      return query.first
    end
  end

  def self.get_or_create_modem_type(s)
    query = ModemType.where(:name => s)
    if query.blank? then
      mt = ModemType.new
      mt.name = s
      mt.save
      return mt
    else
      return query.first
    end
  end

  def self.add_job_from_ot(id, client, force_save = false)
  	begin
	    puts "attempting to retrieve job with OMNITRACKER-ID: #{id}"
	    job_info = OtHelper.get_job_info(id, client)
	    if job_info.blank? then
	    	puts "no job retrieved. probably blocked -> aborting"
	      return
	    end
      job = Job.new

	    #defaults
	    job.status = 0
	    job.wartegrund_id = 0
	    job.logs = ""

      #copy field values directly
	    job.workorder = job_info["workorder"]
	    job.ot_auftragsnummer = job_info["ot_auftragsnummer"]
	    job.kunde_telefon_privat = job_info["kunde_telefon_privat"]
	    job.kunde_telefon_mobil = job_info["kunde_telefon_mobil"]
	    job.kunde_telefon_geschaeftlich = job_info["kunde_telefon_geschaeftlich"]
	    job.kunde_email = job_info["kunde_email"]
	    job.telefon_neu = job_info["telefon_neu_01"]
	    job.eigentuemer_vorname = job_info["eigentuemer_vorname"]
	    job.eigentuemer_nachname = job_info["eigentuemer_nachname"]
	    job.eigentuemer_telefon_1 = job_info["eigentuemer_telefon_1"]
	    job.eigentuemer_telefon_2 = job_info["eigentuemer_telefon_2"]
	    job.eigentuemer_email = job_info["eigentuemer_email"]
	    job.eigentuemer_ansprechpartner = job_info["eigentuemer_ansprechpartner"]
	    job.eigentuemer_telefon_ansprechpartner = job_info["eigentuemer_telefon_ansprechpartner"]
	    job.eigentuemer_hausnummer = job_info["eigentuemer_hausnummer"]
	    job.rks_anlagennummer = job_info["rks_anlagennummer"]
	    job.wohneinheiten = job_info["wohneinheiten"]
	    job.aktive_kunden = job_info["aktive_kunden"]
	    job.erstinstallation_erfolgt = job_info["erstinstallation_erfolgt"]
	    job.modemaktivierung = job_info["modemaktivierung"]
	    job.beauftragte_leistungen = job_info["servicecode_01"]+"\n"+job_info["servicecode_02"]+"\n"+job_info["servicecode_03"]+"\n"+job_info["servicecode_04"]+"\n"+job_info["servicecode_05"]+"\n"+job_info["servicecode_06"]+"\n"+job_info["servicecode_07"]+"\n"+job_info["servicecode_08"]+"\n"+job_info["servicecode_09"]+"\n"+job_info["servicecode_10"]
	    job.verbindlicher_installationstermin = job_info["verbindlicher_installationstermin"]
	    job.verbindliche_tageszeit = job_info["verbindliche_tageszeit"]
	    puts "Kommentare:"
	    puts job_info["kommentare"]
      job.ot_kommentare = job_info["kommentare"]
   
      #extract eigentuemer data
	    job.eigentuemer_plz = OtHelper.extract_eigentuemer_plz(job_info["eigentuemer_plz_und_ort"])
	    job.eigentuemer_ort = OtHelper.extract_eigentuemer_ort(job_info["eigentuemer_plz_und_ort"])
	    job.eigentuemer_strasse = OtHelper.extract_eigentuemer_strasse(job_info["eigentuemer_strasse_und_hausnummer"])
	    job.eigentuemer_hausnummer = OtHelper.extract_eigentuemer_hausnummer(job_info["eigentuemer_strasse_und_hausnummer"])
	    
	    #extract customer data
	    kunde_string = job_info["kunde"]
	    if (!kunde_string.blank?)
	      job.kunde_nachname = extract_kunde_nachname(kunde_string)
	      job.kunde_vorname = extract_kunde_vorname(kunde_string)
	      job.kunde_plz = extract_kunde_plz(kunde_string)
	      job.kunde_ort = extract_kunde_ort(kunde_string)
	      job.kunde_strasse = extract_kunde_strasse(kunde_string)
	      job.kunde_hausnummer = extract_kunde_hausnummer(kunde_string)
	      job.kunde_kundennummer = extract_kunde_kundennummer(kunde_string)
	    end

      #logik
	    job.client_id = client.id
	    job.kunde_is_eigentuemer = (job.eigentuemer_vorname.blank? && job.eigentuemer_nachname.blank?)

			#jobtype_short = OtHelper.previous_jobs_in_same_building(job).blank? ? "EA" : "FA"
	    jobtype_short = "EA"

	    pcs = PostalCode.where(:code => job.kunde_plz)
	    if !pcs.blank? then
	      pc = pcs.first
	      if (pc) && (pc.scheduling_user_group) then
	        job.scheduling_user_group = pc.scheduling_user_group
	      end
	    end

	    if (job.aktive_kunden != nil) and (!job.aktive_kunden.blank?) then
	      if (job.aktive_kunden > 0) then
	        jobtype_short = "FA"
	      end
	    end

	    job.auftragsart_id = JobType.where(:shortname => jobtype_short).first.id

	    job.objektart_id = OtHelper.get_or_create_buiding_type(job_info["objektart"]).id
	    job.modemtyp_id = OtHelper.get_or_create_modem_type(job_info["modemtyp"]).id
	    job.netztopologie_id = OtHelper.get_or_create_network_type(job_info["netztopologie"]).id
	    job.transfer_point = get_or_create_transfer_point(job_info["uep_ot_id"], (job_info["mini_fibre_node"] == "Ja"), client)
	          
	    #for updating purpose
	    job.ot_id = id

	    #save record
	    puts "attempting to save job"
	    no_matching_job_record = Job.where(:ot_auftragsnummer => job.ot_auftragsnummer).blank?
	    if !no_matching_job_record then
	      puts "Job already exists!" 
	      puts "Saving anyway because of force_save!" if force_save 
	    end

	    if (no_matching_job_record || force_save) then     
	      puts "Saving job with OT-Number:"+job.ot_auftragsnummer
	      result = job.save :validate => false
	      puts "Result: #{result}"
	      if !job.errors.empty?
	      	puts "Validierungsfehler:"
		      job.errors.each {|e| puts e}
		    end
		    puts "RETURNING A JOB"
	      return job
	    else
	      return nil
	    end  
	  rescue Exception => e
	  	puts "ERROR SAVING"
	    puts e.message  
	    #puts e.backtrace.inspect
	  end

  end

  #Maier-Gerber, Patrick - 88239 Wangen im Allgäu Wangen - Braugasse 15 - Kd-Nr: 238082103

  def self.extract_kunde_nachname(kunde_string)
    kunde_array = kunde_string.split(" - ") #leerzeichen wegen doppelnamen mit bindestrich 
    kunde_name_array = kunde_array[0].split(",")
    nachname = kunde_name_array[0].blank? ? "" : kunde_name_array[0].strip
  end

  def self.extract_kunde_vorname(kunde_string)
    kunde_array = kunde_string.split(" - ")
    kunde_name_array = kunde_array[0].split(",")
    nachname = kunde_name_array[1].blank? ? "" : kunde_name_array[1].strip
  end

  def self.extract_kunde_plz(kunde_string)
    kunde_array = kunde_string.split(" - ")
    kunde_ort_array = kunde_array[1].split(" ")
    plz = kunde_ort_array[0].blank? ? "" : kunde_ort_array[0].strip
  end

  def self.extract_kunde_ort(kunde_string)
    kunde_array = kunde_string.split(" - ")
    kunde_ort_array = kunde_array[1].split(" ")
    ort = (kunde_ort_array - [kunde_ort_array.first]).join(" ")
  end

  def self.extract_kunde_strasse(kunde_string)
    kunde_array = kunde_string.split(" - ")
    kunde_strasse_array = kunde_array[2].split(" ")
    strasse = (kunde_strasse_array - [kunde_strasse_array.last]).join(" ")
  end

  def self.extract_kunde_hausnummer(kunde_string)
    kunde_array = kunde_string.split(" - ")
    kunde_strasse_array = kunde_array[2].split(" ")
    hausnummer = kunde_strasse_array.last
  end

  def self.extract_kunde_kundennummer(kunde_string)
    kunde_array = kunde_string.split(" - ")
    kunde_array.last.split(":").last.blank? ? "" : kunde_array.last.split(":").last.strip
  end

  def self.extract_eigentuemer_strasse(strasse_und_hausnummer)
    return "" if strasse_und_hausnummer.blank?
    a = strasse_und_hausnummer.split(" ")
    a = a - [a.last]
    return a.join(" ")
  end

  def self.extract_eigentuemer_hausnummer(strasse_und_hausnummer)
    return "" if strasse_und_hausnummer.blank?
    a = strasse_und_hausnummer.split(" ")
    return a.last
  end
  
  def self.extract_eigentuemer_plz(plz_und_ort)
    return "" if plz_und_ort.blank?
    a = plz_und_ort.split(" ")
    return a.first
  end
  
  def self.extract_eigentuemer_ort(plz_und_ort)
    return "" if plz_und_ort.blank?
    a = plz_und_ort.split(" ")
    a = a - [a.first]
    return a.join(" ")
  end

  def self.previous_jobs_in_same_building(job)
    Job.where(:kunde_plz => job.kunde_plz).where(:kunde_ort => job.kunde_ort).where(:kunde_strasse => job.kunde_strasse).where(:kunde_hausnummer => job.kunde_hausnummer)
  end


def self.add_service_job_from_ot(id, client, force_save = false)
    puts "getting service_job with OMNITRACKER-ID: #{id}"
    job_info = OtHelper.get_service_job_info(id, client)
    if job_info.blank? then
      return
    end
    job = ServiceJob.new

    #defaults
    job.status = 0
    job.client = client

=begin
    job_info.each_key do |key|
      puts "#{key}: #{job_info[key]}"
   
      job[key] = job_info[key]
    end
=end

    job.ot_auftragsnummer = job_info["ot_auftragsnummer"]
    job.wirktiefe = job_info["wirktiefe"]
    job.nachname_des_anrufers = job_info["nachname_des_anrufers"]
    job.telefonnummer_des_anrufers = job_info["telefonnummer_des_anrufers"]
    job.auftragsklasse = job_info["auftragsklasse"]
    job.zustand = job_info["zustand"]
    job.vorname_des_anrufers = job_info["vorname_des_anrufers"]
    job.email_des_anrufers = job_info["email_des_anrufers"]
    
    job.kunde_telefon_privat = job_info["kunde_telefon_privat"]
    job.kunde_telefon_mobil = job_info["kunde_telefon_mobil"]
    job.kunde_telefon_geschaeftlich = job_info["kunde_telefon_geschaeftlich"]
    job.kunde_email = job_info["kunde_email"]

    #extract customer data
    kunde_string = job_info["kunde_string"]
    puts "!!!!!!!!!!!!!!!!!!!!!kunde_string="+kunde_string
    if (!kunde_string.blank?)
      job.kunde_nachname = extract_kunde_nachname(kunde_string)
      job.kunde_vorname = extract_kunde_vorname(kunde_string)
      job.kunde_plz = extract_kunde_plz(kunde_string)
      job.kunde_ort = extract_kunde_ort(kunde_string)
      job.kunde_strasse = extract_kunde_strasse(kunde_string)
      job.kunde_hausnummer = extract_kunde_hausnummer(kunde_string)
      job.kunde_kundennummer = extract_kunde_kundennummer(kunde_string)
    end

    #job.kunde_string = job_info["kunde_string"]
    # job.kunde_plz = job_info["kunde_plz"]
    # job.kunde_strasse = job_info["kunde_strasse"]
    # job.kunde_hausnummer = job_info["kunde_hausnummer"]+job_info["kunde_hausnummer_zusatz"]
    # job.kunde_ort = job_info["kunde_ort"]
    
    job.uep_string = job_info["uep_string"]
    job.uep_zustand = job_info["uep_zustand"]
    job.uebk = job_info["uebk"]
    job.asb = job_info["asb"]
    job.onkz = job_info["onkz"]
    job.we_angeschlossen = job_info["we_angeschlossen"]
    job.we_max = job_info["we_max"]
    job.uebk = job_info["uebk"]

    job.uep_zustand_klartext = job_info["uep_zustand_klartext"]
    job.grussz = job_info["grussz"]
    job.vrp = job_info["vrp"]
    job.c_linie = job_info["c_linie"]
    job.versorgte_objekt = job_info["versorgte_objekt"]
    job.inbetriebnahme = job_info["inbetriebnahme"]

    job.ad_zeitfenster_fruehester_beginn = job_info["ad_zeitfenster_fruehester_beginn"]
    job.e_vsip_kundentermin = job_info["e_vsip_kundentermin"]
    job.vereinbarter_kundentermin = job_info["vereinbarter_kundentermin"]
    job.ad_zeitfenster_spaetester_beginn = job_info["ad_zeitfenster_spaetester_beginn"]
    job.techniker = job_info["techniker"]

    job.textbaustein = job_info["textbaustein"]
    job.beschreibung = job_info["beschreibung"]
    job.rx = job_info["rx"]
    job.tx = job_info["tx"]
    job.bandbreite_downstream = job_info["bandbreite_downstream"]
    job.rufnummer_1 = job_info["rufnummer_1"]
    job.modemtyp = job_info["modemtyp"]
    job.mahnlauf = job_info["mahnlauf"]

    job.mac_adresse = job_info["mac_adresse"]
    job.snr = job_info["snr"]
    job.bandbreite_upstream = job_info["bandbreite_upstream"]
    job.rufnummer_2 = job_info["rufnummer_2"]
    job.dienstmerkmalpaket = job_info["dienstmerkmalpaket"]
    #job.kommentare = job_info["kommentare"]
    #puts "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
    #puts job_info["kommentare"]
    #job.wartegrund = job_info["wartegrund"]
    job.wartegrund_beschreibung = job_info["wartegrund_beschreibung"]
    job.wiedervorlage = job_info["wiedervorlage"]
    job.netzebene = job_info["netzebene"]
    job.stoerungswirkbreite = job_info["stoerungswirkbreite"]
    job.abschlussgrund = job_info["abschlussgrund"]
    job.abschlussgrund_beschreibung = job_info["abschlussgrund_beschreibung"]
    job.stoerungsende_aus_kundensicht = job_info["stoerungsende_aus_kundensicht"]
    job.systemabschluss = job_info["systemabschluss"]
    job.leistungsnachweis_vorhanden = job_info["leistungsnachweis_vorhanden"]
    job.save

    


=begin
    #copy field values directly
    job.workorder = job_info["workorder"]
    job.ot_auftragsnummer = job_info["ot_auftragsnummer"]
    job.kunde_telefon_privat = job_info["kunde_telefon_privat"]
    job.kunde_telefon_mobil = job_info["kunde_telefon_mobil"]
    job.kunde_telefon_geschaeftlich = job_info["kunde_telefon_geschaeftlich"]
    job.kunde_email = job_info["kunde_email"]
    job.telefon_neu = job_info["telefon_neu_01"]
    job.eigentuemer_vorname = job_info["eigentuemer_vorname"]
    job.eigentuemer_nachname = job_info["eigentuemer_nachname"]
    job.eigentuemer_telefon_1 = job_info["eigentuemer_telefon_1"]
    job.eigentuemer_telefon_2 = job_info["eigentuemer_telefon_2"]
    job.eigentuemer_email = job_info["eigentuemer_email"]
    job.eigentuemer_ansprechpartner = job_info["eigentuemer_ansprechpartner"]
    job.eigentuemer_telefon_ansprechpartner = job_info["eigentuemer_telefon_ansprechpartner"]
    job.eigentuemer_hausnummer = job_info["eigentuemer_hausnummer"]
    job.rks_anlagennummer = job_info["rks_anlagennummer"]
    job.wohneinheiten = job_info["wohneinheiten"]
    job.aktive_kunden = job_info["aktive_kunden"]
    job.erstinstallation_erfolgt = job_info["erstinstallation_erfolgt"]
    job.modemaktivierung = job_info["modemaktivierung"]
    job.beauftragte_leistungen = job_info["servicecode_01"]+"\n"+job_info["servicecode_02"]+"\n"+job_info["servicecode_03"]+"\n"+job_info["servicecode_04"]+"\n"+job_info["servicecode_05"]+"\n"+job_info["servicecode_06"]+"\n"+job_info["servicecode_07"]+"\n"+job_info["servicecode_08"]+"\n"+job_info["servicecode_09"]+"\n"+job_info["servicecode_10"]
    job.verbindlicher_installationstermin = job_info["verbindlicher_installationstermin"]
    job.verbindliche_tageszeit = job_info["verbindliche_tageszeit"]
    
    #extract eigentuemer data
    job.eigentuemer_plz = OtHelper.extract_eigentuemer_plz(job_info["eigentuemer_plz_und_ort"])
    job.eigentuemer_ort = OtHelper.extract_eigentuemer_ort(job_info["eigentuemer_plz_und_ort"])
    job.eigentuemer_strasse = OtHelper.extract_eigentuemer_strasse(job_info["eigentuemer_strasse_und_hausnummer"])
    job.eigentuemer_hausnummer = OtHelper.extract_eigentuemer_hausnummer(job_info["eigentuemer_strasse_und_hausnummer"])
    
    #extract customer data
    kunde_string = job_info["kunde"]
    if (!kunde_string.blank?)
      job.kunde_nachname = extract_kunde_nachname(kunde_string)
      job.kunde_vorname = extract_kunde_vorname(kunde_string)
      job.kunde_plz = extract_kunde_plz(kunde_string)
      job.kunde_ort = extract_kunde_ort(kunde_string)
      job.kunde_strasse = extract_kunde_strasse(kunde_string)
      job.kunde_hausnummer = extract_kunde_hausnummer(kunde_string)
      job.kunde_kundennummer = extract_kunde_kundennummer(kunde_string)
    end

    #logik
    job.client_id = Client.where(:name => client.name).first.id
    job.kunde_is_eigentuemer = (job.eigentuemer_vorname.blank? && job.eigentuemer_nachname.blank?)
#    jobtype_short = OtHelper.previous_jobs_in_same_building(job).blank? ? "EA" : "FA"

    jobtype_short = "EA"

    if (job.aktive_kunden != nil) and (!job.aktive_kunden.blank?) then
      if (job.aktive_kunden > 0) then
        jobtype_short = "FA"
      end
    end
    job.auftragsart_id = JobType.where(:shortname => jobtype_short).first.id
    job.objektart_id = OtHelper.get_or_create_buiding_type(job_info["objektart"]).id
    job.modemtyp_id = OtHelper.get_or_create_modem_type(job_info["modemtyp"]).id
    job.netztopologie_id = OtHelper.get_or_create_network_type(job_info["netztopologie"]).id
    job.transfer_point = get_or_create_transfer_point(job_info["uep_ot_id"], (job_info["mini_fibre_node"] == "Ja"), client)
=end    
    #for updating purpose
    job.ot_id = id

    #save record
    no_matching_job_record = ServiceJob.where(:ot_id => job.ot_id).blank?
    if !no_matching_job_record then
      puts "Job already exists!" 
      puts "Saving anyway because of force_save!" if force_save 
    end

    if (no_matching_job_record || force_save) then     
      puts "Saving job with OT-Number:"+job.ot_id
      result = job.save :validate => false
      puts "Result: #{result}"
      job.errors.each {|e| puts e}
      return job
    else
      return nil
    end  
  end

  def self.get_modemaktivierung(job, a)
   
    url = Settings.OT_DETAILS_URL.gsub("##ID##",job.ot_id.to_s) # Test ot_id for modem activated: 39419934
    a.get(url)
 
    results = Hash.new
    mappings1 = {
    "modemaktivierung" => "rdo4568"
    }
    results.merge! get_data_from_tab(nil,mappings1,a)
    
    modem_aktiviert = false
    modem_aktiviert = true if results["modemaktivierung"] != ""
 
    #get back to job list to prevent locking the job
    do_postback('otBtnMainView','',a)
  
    #anzeigen
    # results.each_key do |key|
      # puts "#{key}: #{results[key]} #{results[key].length}"
    # end
    
    return modem_aktiviert  
  end
  
  def self.update_jobs
    a = Mechanize.new if a == nil
    # Windows specific
    a.agent.http.verify_mode = OpenSSL::SSL::VERIFY_NONE
    
    login_ot(a, Client.last)
        
    #Job.where(:ot_id => '39585557').each do |job| # TEST OT-ID where modem is activated
    Job.all.each do |job|
      modem_aktiviert = OtHelper.get_modemaktivierung(job, a)
      if modem_aktiviert
        job.status = 4 
        job.save
        puts "Status of job ID "+job.id.to_s+" changed to TA"
      else
        puts "Status of job ID "+job.id.to_s+" not changed"
      end
    end 
    
    logout_ot(a)
  end
  
  # CT zdE jobs werden anhand der Abschlusszeiten der disponierten Aufträge im OT von zdE auf TA gesetzt.
  def self.update_zdE_jobs
    ots = OtScraper.new
    rows = ots.get_job_list({:ot_login=> Client.last.ot_login, :ot_password=> Client.last.ot_password}, "Zustand di", "13798")
    ots.logout

    puts rows.length.to_s + " jobs found"
    blocked_jobs = Array.new
    existing_jobs = Array.new
    new_jobs = Array.new

    rows.each do |r|
      puts "processing job:"
      puts "Tatsaechl. Abschlusszeit of ID " + r["id"] +": "+r["Tatsächl. Abschlusszeit"]
      if r["Tatsächl. Abschlusszeit"] == ""
        Job.where(:ot_id => r["id"]).where(:status => 0).each do |job|
          job.status = 4 # status TA
          job.save
          puts "Status changed"          
        end
      end
    end  
    puts "done."
    Admin::LogsController.create("System updated job status") 
  end  

  def self.upload_file_test
  	a = Mechanize.new
    login_ot(a, Client.find(3))

    puts "get parent page"
    a.get("https://otweb.kabelbw.com/OTWG_NoRefresh/OTWGObjForm.aspx?popup=0&no_restore=false&id=37098148&save=1&original_props_id=-1&ShortCutBarID=0")
 		
 		puts "get upload page"
 		upload_page = a.get("https://otweb.kabelbw.com/OTWG_NoRefresh/OTWGAttachmentAdd.aspx?fieldid=4953&addfile=True&addlink=False&objectid=37098148&folderid=831")
 		
 		puts "add file"
 		upload_page.form_with(:method => 'POST') do |upload_form|
    	upload_form.file_uploads.first.file_name = "test.txt"
  	end#.submit

  	puts "click upload"
   	do_postback("btn1","",a)

   	puts "click ok"
   	do_postback("btnOK","",a)
   	
  	logout_ot(a)
  end

end