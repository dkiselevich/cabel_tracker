class TransferPoint < ActiveRecord::Base
	has_many :jobs, :foreign_key => "uep_id"

	def adresse
		"#{plz} #{ort}, #{strasse} #{hausnummer}"
	end

	def short_description
		"#{adresse} - ID: #{uid}"
	end
end
