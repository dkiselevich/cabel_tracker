class PostalCode < ActiveRecord::Base
	has_and_belongs_to_many :service_user_groups
	belongs_to :scheduling_user_group
end
