class JobType < ActiveRecord::Base
	validates :name, :presence => {:message => "^Geben Sie eine Bezeichnung an."}
  scope :active, where(:active => true)

  def self.options_for_select
    active.collect {|c| [ c.name, c.id ] }
  end

end
