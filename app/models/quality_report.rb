class QualityReport < ActiveRecord::Base
  belongs_to :user
  
  attr_accessible :detected_quality_problems, :identified_necessary_measures, :implemented_measures, :launch_plannings, :pdf_created, :planned_measures_next_two_month, :sustainability_control, :user_id
end
