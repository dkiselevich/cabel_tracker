class CancellationSweeper < ActionController::Caching::Sweeper
  observe Cancellation

  # If our sweeper detects that a Cancellation was created call this
  def after_create(cancellation)
    expire_cache_for(cancellation)
  end
 
  # If our sweeper detects that a Cancellation was updated call this
  def after_update(cancellation)
    expire_cache_for(cancellation)
  end
 
  # If our sweeper detects that a Cancellation was deleted call this
  def after_destroy(cancellation)
    expire_cache_for(cancellation)
  end
 
private
  def expire_cache_for(cancellation)
    begin
      SweeperHelper.expire_cancellation_views(cancellation, self)
      SweeperHelper.expire_job_views(cancellation.job, self)
    rescue Exception => ex
      puts "ERROR while clearing caches: #{ex}"
    end
  end
end