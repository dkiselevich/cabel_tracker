# encoding: utf-8
class Job < ActiveRecord::Base
  audited
	has_many :appointments, :dependent => :destroy
	has_many :cancellations, :dependent => :destroy, :through => :appointments
	has_many :completions, :dependent => :destroy, :through => :appointments
	has_many :appointment_requests, :dependent => :destroy, :through => :appointments
	has_many :comments, :dependent => :destroy
	belongs_to :client
	belongs_to :transfer_point, :foreign_key => "uep_id"
	belongs_to :job_type, :foreign_key => "auftragsart_id"
	belongs_to :building_type, :foreign_key => "objektart_id"
	belongs_to :network_type, :foreign_key => "netztopologie_id"
	belongs_to :waiting_cause, :foreign_key => "wartegrund_id"
	belongs_to :modem_type, :foreign_key => "modemtyp_id"
	belongs_to :scheduling_user_group

	attr_protected :id
	

  validates :client_id, :presence => {:message => "^Geben Sie einen Mandant an."}
  validates :auftragsart_id, :presence => {:message => "^Geben Sie eine Auftragsart an."}
  validates :workorder, :presence => {:message => "^Geben Sie eine Workorder an."}
  validates :kunde_kundennummer, :presence => {:message => "^Geben Sie eine Kundennummer an."}
  validates :ot_auftragsnummer, :presence => {:message => "^Geben Sie eine Auftragsnummer an."}
  validates :modemtyp_id, :presence => {:message => "^Geben Sie einen Modemtyp an."}
  	
  validates :kunde_plz, :presence => {:message => "^Geben Sie eine PLZ für den Kunden an."}
  validates :kunde_ort, :presence => {:message => "^Geben Sie einen Ort für den Kunden an."}
  validates :kunde_strasse, :presence => {:message => "^Geben Sie eine Strasse für den Kunden an."}
  validates :kunde_hausnummer, :presence => {:message => "^Geben Sie eine Hausnummer für den Kunden an."}
  validates :kunde_nachname, :presence => {:message => "^Geben Sie einen Nachnamen für den Kunden an."}
	
	#validates :eigentuemer_plz, :presence => {:message => "^Geben Sie eine PLZ für den Eigentümer an.", :unless => :kunde_is_eigentuemer?}
  #validates :eigentuemer_ort, :presence => {:message => "^Geben Sie einen Ort für den Eigentümer an.", :unless => :kunde_is_eigentuemer?}
  #validates :eigentuemer_strasse, :presence => {:message => "^Geben Sie eine Strasse für den Eigentümer an.", :unless => :kunde_is_eigentuemer?}
  #validates :eigentuemer_hausnummer, :presence => {:message => "^Geben Sie eine Hausnummer für den Eigentümer an.", :unless => :kunde_is_eigentuemer?}
  #validates :eigentuemer_nachname, :presence => {:message => "^Geben Sie einen Nachnamen für den Eigentümer an.", :unless => :kunde_is_eigentuemer?}

  	
  validates_with HasKundeTelefonValidator
  #validates_with HasEigentuemerTelefonValidator, :unless => :kunde_is_eigentuemer?

  validates :wohneinheiten, :presence => {:message => "^Geben Sie die Anzahl der Wohneinheiten im Objekt an."}
  validates :netztopologie_id, :presence => {:message => "^Geben Sie eine Netztopologie an."}

  	
  def kunde_is_eigentuemer?
    kunde_is_eigentuemer
  end

  def short
    return "#{ot_auftragsnummer} | #{kunde_plz} #{kunde_ort} | #{kunde_strasse}#{kunde_hausnummer} | #{kunde_nachname}"
  end

	def short_description
		return "#{ot_auftragsnummer} | #{kunde_plz} #{kunde_ort} | #{kunde_strasse}#{kunde_hausnummer} | #{kunde_nachname}, #{kunde_vorname} | #{kunde_telefon}"

=begin
    sd = Settings.JOB_SHORT_DESCRIPTION_FOMAT
    md = sd.scan(/\{(.*?)\}/).flatten

    md.each do |m|
      sd.gsub!("{"+m+"}", eval(m.downcase))
    end

    return sd
=end

	end


	def adresse
		"#{kunde_plz} | #{kunde_ort} | #{kunde_strasse}#{kunde_hausnummer} | #{kunde_nachname}, #{kunde_vorname} | #{kunde_telefon}"
	end

	def adresse_short
		"#{kunde_plz} #{kunde_ort}, #{kunde_strasse}#{kunde_hausnummer}"
	end

	def eigentuemer_daten
		s = "#{eigentuemer_nachname}, #{eigentuemer_vorname} | #{eigentuemer_plz} | #{eigentuemer_ort} | #{eigentuemer_strasse}#{eigentuemer_hausnummer} | #{eigentuemer_telefon_1} #{eigentuemer_telefon_2}"
		s.gsub("|  |","").gsub(",  ","")
	end

	def kunde_name
		"#{kunde_vorname} #{kunde_nachname}"
	end

	def kunde_strasse_full
		"#{kunde_strasse} #{kunde_hausnummer}"
	end 

	def short_info
		"#{job_type.shortname}, WE: #{wohneinheiten}, KD: #{aktive_kunden}, N: #{network_type.name}"
	end

  def kunde_telefon
     privat = kunde_telefon_privat.blank? ? nil : kunde_telefon_privat
     mobil = kunde_telefon_mobil.blank? ? nil : kunde_telefon_mobil
     geschaeftlich = kunde_telefon_geschaeftlich.blank? ? nil : kunde_telefon_geschaeftlich
     (privat || mobil || geschaeftlich).to_s
  end

  def status_name
    case status
      when 0 then "zu disponieren"
      when 1 then "disponiert"
      when 2 then "wartend"
      when 3 then "abgeschlossen"
      when 4 then "technisch abgeschlossen"
      when 5 then "storniert"
    end
  end

	def status_short
    case status
      when 0 then "ZDE"
      when 1 then "DI"
      when 2 then "WK"
      when 3 then "AG"
      when 4 then "TA"
      when 5 then "OM"
    end
  end

	def full_logs
		"erstellt am: #{created_at.strftime("%d.%m.%Y um %H:%M")}<br/>#{logs}"
	end

	def abgeschlossen?
		((status == 3) || (status == 4))
	end

	def completion 
		completions.last
	end
   
	def messwerte_photo_url
		 (completion.nil? || completion.messwerte_photo_file_name.blank?) ? "/assets/blank.gif" : "#{Settings.COMPLETION_IMAGES_BASE_URL}#{id}/messwerte_photo_file_name/#{completion.messwerte_photo_file_name}"
	end

	def bvt_photo_url
		(completion.nil? || completion.bvt_photo_file_name.blank?) ? "/assets/blank.gif" : "#{Settings.COMPLETION_IMAGES_BASE_URL}#{id}/bvt_photo_file_name/#{completion.bvt_photo_file_name}"
	end

	def uep_photo_url
		(completion.nil? || completion.uep_photo_file_name.blank?) ? "/assets/blank.gif" : "#{Settings.COMPLETION_IMAGES_BASE_URL}#{id}/uep_photo_file_name/#{completion.uep_photo_file_name}"
	end

	def pot_schiene_photo_url
		(completion.nil? || completion.pot_schiene_photo_file_name.blank?) ? "/assets/blank.gif" : "#{Settings.COMPLETION_IMAGES_BASE_URL}#{id}/pot_schiene_photo_file_name/#{completion.pot_schiene_photo_file_name}"
	end

	def pot_gas_photo_url
		(completion.nil? || completion.pot_gas_photo_file_name.blank?) ? "/assets/blank.gif" : "#{Settings.COMPLETION_IMAGES_BASE_URL}#{id}/pot_gas_photo_file_name/#{completion.pot_gas_photo_file_name}"
	end

	def pot_wasser_photo_url
		(completion.nil? || completion.pot_wasser_photo_file_name.blank?) ? "/assets/blank.gif" : "#{Settings.COMPLETION_IMAGES_BASE_URL}#{id}/pot_wasser_photo_file_name/#{completion.pot_wasser_photo_file_name}"
	end
	
	def pot_hak_photo_url
		(completion.nil? || completion.pot_hak_photo_file_name.blank?) ? "/assets/blank.gif" : "#{Settings.COMPLETION_IMAGES_BASE_URL}#{id}/pot_hak_photo_file_name/#{completion.pot_hak_photo_file_name}"
	end
	
	def pot_heizung_photo_url
		(completion.nil? || completion.pot_heizung_photo_file_name.blank?) ? "/assets/blank.gif" : "#{Settings.COMPLETION_IMAGES_BASE_URL}#{id}/pot_heizung_photo_file_name/#{completion.pot_heizung_photo_file_name}"
	end
	
	def pot_oel_photo_url
		(completion.nil? || completion.pot_oel_photo_file_name.blank?) ? "/assets/blank.gif" : "#{Settings.COMPLETION_IMAGES_BASE_URL}#{id}/pot_oel_photo_file_name/#{completion.pot_oel_photo_file_name}"
	end
	
	def mmd_unbundled_photo_url
		(completion.nil? || completion.mmd_unbundled_photo_file_name.blank?) ? "/assets/blank.gif" : "#{Settings.COMPLETION_IMAGES_BASE_URL}#{id}/mmd_unbundled_photo_file_name/#{completion.mmd_unbundled_photo_file_name}"
	end

	def self.belonging_to_user_client(user)
		self.where(:client_id => user.clients.collect{|c|c.id})
	end

	def self.belonging_to_scheduling_user_group(user)
		if user.scheduling_user_group then
			self.where(:scheduling_user_group_id => user.scheduling_user_group.id)
		else
			self.where(:scheduling_user_group_id => -1)
		end	
	end

	def scheduled_by
		self.appointments.last.scheduling_user.full_name if !self.appointments.blank?
	end

	def last_updated_by
		self.audits.last.user.full_name if self.audits.last && self.audits.last.user
	end

end
