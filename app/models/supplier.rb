class Supplier < ActiveRecord::Base
  attr_accessible :name, :contact_first_name, :contact_last_name, :street, :house_number, :postal_code, :city, :phone, :fax, :email, :order_portal
  has_many :article_availabilities
  has_many :articles, :through => :article_availabilities

	def contact_full_name
		"#{contact_first_name} #{contact_last_name}"
	end

	def full_address
		"#{postal_code} #{city}, #{street} #{house_number}"
	end

	def self.belonging_to_user_client(user)
		self.where(:client_id => user.clients.first) #purchase manager is upposed to habe only one client for now #user.clients.collect{|c|c.id})
	end
end
