class StatisticsHelper
	
	cattr_reader :PERIOD
	@@PERIOD = Hash.new
	@@PERIOD["FOREVER"] = {:start => Date.today-50.years, :end => Date.today+50.years, :label => "Gesamt"}
	@@PERIOD["TODAY"] = {:start => Date.today, :end => Date.today+1, :label => "Heute"}
	@@PERIOD["YESTERDAY"] = {:start => Date.today.yesterday, :end => Date.today, :label => "Gestern"}
	@@PERIOD["TOMORROW"] = {:start => Date.today.tomorrow, :end => Date.today.tomorrow+1.days, :label => "Morgen"}
	@@PERIOD["LAST_7_DAYS"] = {:start => Date.today - 7.days, :end => Date.today, :label => "Die letzten 7 Tage"}
	@@PERIOD["THIS_WEEK"] = {:start => Date.today.beginning_of_week, :end => Date.today.end_of_week, :label => "Diese Woche"}
	@@PERIOD["LAST_WEEK"] = {:start => Date.today.beginning_of_week - 7.days, :end => Date.today.end_of_week - 7.days, :label => "Letzte Woche"}
	@@PERIOD["THIS_MONTH"] = {:start => Date.today.beginning_of_month, :end => Date.today.end_of_month, :label => "Diesen Monat"}
	@@PERIOD["LAST_MONTH"] = {:start => Date.today.prev_month.beginning_of_month, :end => Date.today.prev_month.end_of_month, :label => "Letzten Monat"}
	@@PERIOD["THIS_YEAR"] = {:start => Date.today.beginning_of_year, :end => Date.today.end_of_year, :label => "Dieses Jahr"}
	@@PERIOD["LAST_YEAR"] = {:start => Date.today.prev_year.beginning_of_year, :end => Date.today.prev_year.end_of_year, :label => "Letztes Jahr"}

	def self.get_periods(hsh = {})
		exclude = hsh[:exclude]
		self.PERIOD.collect{|k,v| [v[:label], k] unless exclude.include?(k) }.delete_if {|x| x == nil}
	end

	def self.get_quotes_per_job_type(hsh = {})
		start_date = StatisticsHelper.PERIOD[hsh[:period]][:start] 
		end_date = StatisticsHelper.PERIOD[hsh[:period]][:end]  
		start_date ||= Date.today
		end_date ||= Date.today
		clients = hsh[:clients]
		client_ids = "("+clients.collect{|c| c.id}.join(",")+")"

		ea_completions = Completion.joins(:job_type).joins(:appointment => :job).where("jobs.client_id in #{client_ids}").where("completions.created_at >= '#{start_date}'").where("completions.created_at < '#{end_date}'").where("job_types.shortname" => "EA").length
		fa_completions = Completion.joins(:job_type).joins(:appointment => :job).where("jobs.client_id in #{client_ids}").where("completions.created_at >= '#{start_date}'").where("completions.created_at < '#{end_date}'").where("job_types.shortname" => "FA").length
		st_completions = ServiceCompletion.joins(:service_appointment => :service_job).where("service_jobs.client_id in #{client_ids}").where("service_completions.created_at >= '#{start_date}'").where("service_completions.created_at < '#{end_date}'").length

		#ea_cancellations = Cancellation.where("cancellations.created_at >= '#{start_date}'").where("cancellations.created_at < '#{end_date}'").joins(:appointment => [ :job => [:job_type]]).where("job_types.shortname" => "EA").length
		#fa_cancellations = Cancellation.where("cancellations.created_at >= '#{start_date}'").where("cancellations.created_at < '#{end_date}'").joins(:appointment => [ :job => [:job_type]]).where("job_types.shortname" => "FA").length
		
	    all_ea_appointments = Appointment.joins(:job).where("jobs.client_id in #{client_ids}").where("appointments.created_at >= '#{start_date}'").where("appointments.created_at < '#{end_date}'").joins(:job => [ :job_type]).where("job_types.shortname" => "EA").length
	    all_fa_appointments = Appointment.joins(:job).where("jobs.client_id in #{client_ids}").where("appointments.created_at >= '#{start_date}'").where("appointments.created_at < '#{end_date}'").joins(:job => [ :job_type]).where("job_types.shortname" => "FA").length
	    all_st_appointments = ServiceAppointment.joins(:service_job).where("service_jobs.client_id in #{client_ids}").where("service_appointments.created_at >= '#{start_date}'").where("service_appointments.created_at < '#{end_date}'").length

		result = Hash.new
        result[:succesfull] = 
        [
	     	ea_completions,
	        fa_completions,
	        st_completions
	    ]
        result[:all] = 
        [
	     	all_ea_appointments,
	        all_fa_appointments,
	        all_st_appointments
	   	]
	   	return result
	end

	def self.get_jobs_per_period(hsh = {})
		start_date = StatisticsHelper.PERIOD[hsh[:period]][:start] 
		end_date = StatisticsHelper.PERIOD[hsh[:period]][:end]  
		start_date ||= Date.today
		end_date ||= Date.today
		clients = hsh[:clients]
		client_ids = "("+clients.collect{|c| c.id}.join(",")+")"

		days = (start_date..end_date).collect { |d| d.strftime("'%d.%m.'")}

		ea_jobs_in_period = Job.joins(:job_type).where("jobs.client_id in #{client_ids}").where("jobs.created_at >= '#{start_date}'").where("jobs.created_at < '#{end_date}'").where("job_types.shortname" => "EA")
  		ea_jobs_per_day_hash = Hash.new
  		ea_jobs_in_period.group_by{|j| j.created_at.strftime("'%d.%m.'")}.each{|k ,v| ea_jobs_per_day_hash[k] = v.count }
   	   	
		fa_jobs_in_period = Job.joins(:job_type).where("jobs.client_id in #{client_ids}").where("jobs.created_at >= '#{start_date}'").where("jobs.created_at < '#{end_date}'").where("job_types.shortname" => "FA")
  		fa_jobs_per_day_hash = Hash.new
  		fa_jobs_in_period.group_by{|j| j.created_at.strftime("'%d.%m.'")}.each{|k ,v| fa_jobs_per_day_hash[k] = v.count }  		
  		
		service_jobs_in_period = ServiceJob.where("service_jobs.client_id in #{client_ids}").where("service_jobs.created_at >= '#{start_date}'").where("service_jobs.created_at < '#{end_date}'")
  		service_jobs_per_day_hash = Hash.new
  		service_jobs_in_period.group_by{|j| j.created_at.strftime("'%d.%m.'")}.each{|k ,v| service_jobs_per_day_hash[k] = v.count }  		
  		
  		result = Array.new
		days.each do |d| 
			ea_jobs = ea_jobs_per_day_hash[d]? ea_jobs_per_day_hash[d] : 0
			fa_jobs = fa_jobs_per_day_hash[d]? fa_jobs_per_day_hash[d] : 0
			service_jobs = service_jobs_per_day_hash[d]? service_jobs_per_day_hash[d] : 0
			result << {:date => d, :ea_jobs => ea_jobs, :fa_jobs => fa_jobs, :service_jobs => service_jobs}
		end
		return result
	end


	def self.get_appointments_per_period(hsh = {})
		start_date = StatisticsHelper.PERIOD[hsh[:period]][:start] 
		end_date = StatisticsHelper.PERIOD[hsh[:period]][:end]  
		technician = hsh[:technician] 
		start_date ||= Date.today
		end_date ||= Date.today
		clients = hsh[:clients]
		client_ids = "("+clients.collect{|c| c.id}.join(",")+")"
		#technicians = technicians.collect{|u| [u.id, u.full_name] }

		days = (start_date..end_date).collect { |d| d.strftime("'%d.%m.'")}

		ea_jobs_in_period = Appointment.joins(:job => :job_type).where("jobs.client_id in #{client_ids}").where(:user_id => technician).where("jobs.created_at >= '#{start_date}'").where("jobs.created_at < '#{end_date}'").where("job_types.shortname" => "EA")
  		ea_jobs_per_day_hash = Hash.new
  		ea_jobs_in_period.group_by{|j| j.job.created_at.strftime("'%d.%m.'")}.each{|k ,v| ea_jobs_per_day_hash[k] = v.count }
   	   	
		fa_jobs_in_period = Appointment.joins(:job => :job_type).where("jobs.client_id in #{client_ids}").where(:user_id => technician).where("jobs.created_at >= '#{start_date}'").where("jobs.created_at < '#{end_date}'").where("job_types.shortname" => "FA")
  		fa_jobs_per_day_hash = Hash.new
  		fa_jobs_in_period.group_by{|j| j.job.created_at.strftime("'%d.%m.'")}.each{|k ,v| fa_jobs_per_day_hash[k] = v.count }  		
  		
		result = Array.new
		days.each do |d| 
			ea_jobs = ea_jobs_per_day_hash[d]? ea_jobs_per_day_hash[d] : 0
			fa_jobs = fa_jobs_per_day_hash[d]? fa_jobs_per_day_hash[d] : 0
			result << {:date => d, :ea_jobs => ea_jobs, :fa_jobs => fa_jobs}
		end
		return result
	end

	def self.get_quotes_per_technician(hsh = {})
		start_date = StatisticsHelper.PERIOD[hsh[:period]][:start] 
		end_date = StatisticsHelper.PERIOD[hsh[:period]][:end]  
		technicians = hsh[:technicians] 
		start_date ||= Date.today
		end_date ||= Date.today
		clients = hsh[:clients]
		client_ids = "("+clients.collect{|c| c.id}.join(",")+")"
		technicians ||= User.where(:is_technician => true)
		technicians = technicians.collect{|u| [u.id, u.full_name] }

		technicians_hash = Hash.new
		technicians.each{|a| technicians_hash[a[0]] = a[1]}

		appointments_per_technician = Appointment.joins(:job).where("jobs.client_id in #{client_ids}").where("day >= '#{start_date}'").where("day < '#{end_date}'").group_by{|a| a.user_id }.collect{|k ,v| [k, v.count] }
		appointments_per_technician_hash = Hash.new
		appointments_per_technician.each{|a| appointments_per_technician_hash[a[0]] = a[1]}

		completions_per_technician = Completion.joins(:appointment => :job).where("jobs.client_id in #{client_ids}").where("completions.created_at >= '#{start_date}'").where("completions.created_at < '#{end_date}'").group_by{|a| a.technician_id }.collect{|k ,v| [k, v.count] }
		completions_per_technician_hash = Hash.new
		completions_per_technician.each{|a| completions_per_technician_hash[a[0]] = a[1]}

		cancellations_per_technician = Cancellation.joins(:appointment => :job).where("jobs.client_id in #{client_ids}").where("cancellations.created_at >= '#{start_date}'").where("cancellations.created_at < '#{end_date}'").group_by{|a| a.technician_id }.collect{|k ,v| [k, v.count] }
		cancellations_per_technician_hash = Hash.new
		cancellations_per_technician.each{|a| cancellations_per_technician_hash[a[0]] = a[1]}

		appointment_requests_per_technician = AppointmentRequest.joins(:appointment => :job).where("jobs.client_id in #{client_ids}").where("appointment_requests.created_at >= '#{start_date}'").where("appointment_requests.created_at < '#{end_date}'").group_by{|a| a.technician_id }.collect{|k ,v| [k, v.count] }
		appointment_requests_per_technician_hash = Hash.new
		appointment_requests_per_technician.each{|a| appointment_requests_per_technician_hash[a[0]] = a[1]}

		result = Array.new
		technicians_hash.each{|e| result << {:user_id => e[0], :user_name => e[1], :number_of_appointments => appointments_per_technician_hash[e[0]] || 0, :number_of_completions => completions_per_technician_hash[e[0]] || 0, :number_of_cancellations => cancellations_per_technician_hash[e[0]] || 0, :number_of_appointment_requests => appointment_requests_per_technician_hash[e[0]] || 0 } }
		return result
	end

	def self.get_quotes_per_controller(hsh = {})
		start_date = StatisticsHelper.PERIOD[hsh[:period]][:start] 
		end_date = StatisticsHelper.PERIOD[hsh[:period]][:end]  
		controllers = hsh[:controllers] 
		start_date ||= Date.today
		end_date ||= Date.today
		clients = hsh[:clients]
		client_ids = "("+clients.collect{|c| c.id}.join(",")+")"
		controllers ||= User.where(:is_controller => true)
		controllers = controllers.collect{|u| [u.id, u.full_name] }

		controllers_hash = Hash.new
		controllers.each{|a| controllers_hash[a[0]] = a[1]}

		appointments_per_controller = Appointment.joins(:job).where("jobs.client_id in #{client_ids}").where("appointments.created_at >= '#{start_date}'").where("appointments.created_at < '#{end_date}'").group_by{|a| a.scheduling_user_id }.collect{|k ,v| [k, v.count] }
		appointments_per_controller_hash = Hash.new
		appointments_per_controller.each{|a| appointments_per_controller_hash[a[0]] = a[1]}

		completions_per_controller = Completion.joins(:appointment => :job).where("jobs.client_id in #{client_ids}").where("completions.created_at >= '#{start_date}'").where("completions.created_at < '#{end_date}'").group_by{|a| a.appointment.scheduling_user_id }.collect{|k ,v| [k, v.count] }
		completions_per_controller_hash = Hash.new
		completions_per_controller.each{|a| completions_per_controller_hash[a[0]] = a[1]}

		cancellations_per_controller = Cancellation.joins(:appointment => :job).where("jobs.client_id in #{client_ids}").where("cancellations.created_at >= '#{start_date}'").where("cancellations.created_at < '#{end_date}'").group_by{|a| a.appointment.scheduling_user_id }.collect{|k ,v| [k, v.count] }
		cancellations_per_controller_hash = Hash.new
		cancellations_per_controller.each{|a| cancellations_per_controller_hash[a[0]] = a[1]}

		appointment_requests_per_controller = AppointmentRequest.joins(:appointment => :job).where("jobs.client_id in #{client_ids}").where("appointment_requests.created_at >= '#{start_date}'").where("appointment_requests.created_at < '#{end_date}'").group_by{|a| a.appointment.scheduling_user_id }.collect{|k ,v| [k, v.count] }
		appointment_requests_per_controller_hash = Hash.new
		appointment_requests_per_controller.each{|a| appointment_requests_per_controller_hash[a[0]] = a[1]}

		result = Array.new
		controllers_hash.each{|e| result << {:user_id => e[0], :user_name => e[1], :number_of_appointments => appointments_per_controller_hash[e[0]] || 0, :number_of_completions => completions_per_controller_hash[e[0]] || 0, :number_of_cancellations => cancellations_per_controller_hash[e[0]] || 0, :number_of_appointment_requests => appointment_requests_per_controller_hash[e[0]] || 0 } }
		return result
	end


	def self.get_volume_per_controller(hsh = {})
		start_date = StatisticsHelper.PERIOD[hsh[:period]][:start] 
		end_date = StatisticsHelper.PERIOD[hsh[:period]][:end]  
		controllers = hsh[:controllers] 
		start_date ||= Date.today
		end_date ||= Date.today
		clients = hsh[:clients]
		client_ids = "("+clients.collect{|c| c.id}.join(",")+")"
		controllers ||= User.where(:is_controller => true)
		controllers = controllers.collect{|u| [u.id, u.full_name] }

		controllers_hash = Hash.new
		controllers.each{|a| controllers_hash[a[0]] = a[1]}

		appointments_per_controller = Appointment.joins(:job).where("jobs.client_id in #{client_ids}").where("appointments.created_at >= '#{start_date}'").where("appointments.created_at < '#{end_date}'").group_by{|a| a.scheduling_user_id }.collect{|k ,v| [k, v.count] }
		appointments_per_controller_hash = Hash.new
		appointments_per_controller.each{|a| appointments_per_controller_hash[a[0]] = a[1]}

		completions_per_controller = Completion.joins(:appointment => :job).where("jobs.client_id in #{client_ids}").where("completions.created_at >= '#{start_date}'").where("completions.created_at < '#{end_date}'").group_by{|a| a.appointment.scheduling_user_id }.collect{|k ,v| [k, v.count] }
		completions_per_controller_hash = Hash.new
		completions_per_controller.each{|a| completions_per_controller_hash[a[0]] = a[1]}

		cancellations_per_controller = Cancellation.joins(:appointment => :job).where("jobs.client_id in #{client_ids}").where("cancellations.created_at >= '#{start_date}'").where("cancellations.created_at < '#{end_date}'").group_by{|a| a.appointment.scheduling_user_id }.collect{|k ,v| [k, v.count] }
		cancellations_per_controller_hash = Hash.new
		cancellations_per_controller.each{|a| cancellations_per_controller_hash[a[0]] = a[1]}

		appointment_requests_per_controller = AppointmentRequest.joins(:appointment => :job).where("jobs.client_id in #{client_ids}").where("appointment_requests.created_at >= '#{start_date}'").where("appointment_requests.created_at < '#{end_date}'").group_by{|a| a.appointment.scheduling_user_id }.collect{|k ,v| [k, v.count] }
		appointment_requests_per_controller_hash = Hash.new
		appointment_requests_per_controller.each{|a| appointment_requests_per_controller_hash[a[0]] = a[1]}

		result = Array.new
		controllers_hash.each{|e| result << {:user_id => e[0], :user_name => "#{e[1]}", :number_of_appointments => appointments_per_controller_hash[e[0]] || 0, :number_of_completions => completions_per_controller_hash[e[0]] || 0, :number_of_cancellations => cancellations_per_controller_hash[e[0]] || 0, :number_of_appointment_requests => appointment_requests_per_controller_hash[e[0]] || 0 } }
		return result
	end
end
