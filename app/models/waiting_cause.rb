class WaitingCause < ActiveRecord::Base
	validates :name, :presence => {:message => "^Geben Sie eine Bezeichnung an."}

  scope :active, where(:active => true)
  
	def full_name
		"#{code} #{name}"
	end

  def self.options_for_select
    active.collect {|c| [ c.full_name, c.id ] }
  end
  
end
