class CreateSpecialExcpenses < ActiveRecord::Migration
  def self.up
    create_table :special_expenses do |t|
    	t.integer :appointment_id
    	t.string :name
    	t.decimal :costs, :precision => 8, :scale => 2 
    	t.decimal :factor, :precision => 8, :scale => 2
      t.timestamps
    end
  end

  def self.down
    drop_table :special_expenses
  end
end
