class MoveMoreFieldsFromArticlesToArticleAvailabilities < ActiveRecord::Migration
  def up
	remove_column :articles, :delivery_time_in_days
	remove_column :articles, :supplier_id
	add_column :article_availabilities, :delivery_time_in_days, :integer, :default => 0 
  end

  def down
	remove_column :article_availabilities, :delivery_time_in_days
	add_column :articles, :supplier_id, :integer
	add_column :article, :delivery_time_in_days, :integer, :default => 0 
  end
end
