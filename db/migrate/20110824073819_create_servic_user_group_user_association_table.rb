class CreateServicUserGroupUserAssociationTable < ActiveRecord::Migration
  def self.up
    create_table :service_user_groups_users, :id => false do |t|
      t.integer :service_user_group_id
      t.integer :user_id
    end
  end

  def self.down
    drop_table :service_user_groups_users
  end
end
