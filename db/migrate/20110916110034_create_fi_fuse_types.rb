class CreateFiFuseTypes < ActiveRecord::Migration
  def self.up
    create_table :fi_fuse_types do |t|
    	t.string :name
    	t.boolean :active, :default => true
      	t.timestamps
    end
  end

  def self.down
    drop_table :fi_fuse_types
  end
end
