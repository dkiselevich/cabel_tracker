class AddFieldsToWaitingCauses < ActiveRecord::Migration
  def change
  	add_column :waiting_causes, :ot_id, :string
  	add_column :waiting_causes, :code, :string
  end
end
