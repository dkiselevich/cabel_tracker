class CreatePostalCodes < ActiveRecord::Migration
  def self.up
    create_table :postal_codes do |t|
      t.string :code
      t.timestamps
    end

    create_table :postal_codes_service_user_groups, :id => false do |t|
      t.integer :service_user_group_id
      t.integer :postal_code_id
    end
  end

  def self.down
    drop_table :postal_codes
    drop_table :postal_codes_service_user_groups
  end
end
