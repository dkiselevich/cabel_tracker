class CreateArticles < ActiveRecord::Migration
	def change
		create_table :articles do |t|

			t.string 	:name
			t.integer 	:article_group_id
			t.string 	:unit_id
			t.integer		:order_quantity
			t.integer 	:supply_to_technician_quantity
			t.string 	:description
			t.integer 	:supplier_id
			t.integer 	:delivery_time_in_days
			t.decimal 	:price1, :precision => 8, :scale => 2
			t.decimal 	:price2, :precision => 8, :scale => 2
			t.decimal 	:price3, :precision => 8, :scale => 2
			t.decimal 	:purchase_price, :precision => 8, :scale => 2
			t.integer 	:status_id
			t.integer 	:current_quantity_in_storage, :default => 0
			t.integer 	:minimum_quantity_in_storage, :default => 0
			t.integer 	:minimum_quantity_pro_technician, :default => 0
			t.timestamps
		end
	end
end
