class CreateArticleAvailabilities < ActiveRecord::Migration
  def change
    create_table :article_availabilities do |t|
    	t.integer 	:supplier_id
    	t.integer	:article_id
		t.decimal 	:purchase_price, :precision => 8, :scale => 2
    	t.timestamps
    end

    remove_column :articles, :purchase_price
  end
end
