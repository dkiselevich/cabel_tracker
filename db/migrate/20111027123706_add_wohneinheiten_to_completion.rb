class AddWohneinheitenToCompletion < ActiveRecord::Migration
  def self.up
  	add_column :completions, :wohneinheiten, :integer
  end

  def self.down
  	remove_column :completions, :wohneinheiten
  end
end
