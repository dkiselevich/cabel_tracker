class AddSchedulingUserGroupToJob < ActiveRecord::Migration
  def change
	add_column :jobs, :scheduling_user_group_id, :integer
	add_column :service_jobs, :scheduling_user_group_id, :integer
  end
end
