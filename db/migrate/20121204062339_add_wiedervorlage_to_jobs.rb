class AddWiedervorlageToJobs < ActiveRecord::Migration
  def change
    add_column :jobs, :wiedervorlage, :date
  end
end
