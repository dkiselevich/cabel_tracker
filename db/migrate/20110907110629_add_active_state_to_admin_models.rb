class AddActiveStateToAdminModels < ActiveRecord::Migration
  def self.up
  	add_column :users, :active, :boolean, :default => true
  	add_column :clients, :active, :boolean, :default => true
  	add_column :job_types, :active, :boolean, :default => true
  	add_column :building_types, :active, :boolean, :default => true
  	add_column :network_types, :active, :boolean, :default => true
  	add_column :waiting_causes, :active, :boolean, :default => true
  end

  def self.down
  	remove_column :users, :active
  	remove_column :clients, :active
  	remove_column :job_types, :active
  	remove_column :building_types, :active
  	remove_column :network_types, :active
  	remove_column :waiting_causes, :active
  end
end
