class AddTableIdToLogs < ActiveRecord::Migration
  def change
    add_column :logs, :table_id, :integer
  end
end
