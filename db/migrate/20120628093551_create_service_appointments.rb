class CreateServiceAppointments < ActiveRecord::Migration
	def change
		create_table 	:service_appointments do |t|
			t.integer 		:service_job_id
			t.timestamp 	:start_time
			t.timestamp 	:end_time
			t.date 			:day
			t.text 			:bemerkungen
			t.timestamps
		end
	end
end
