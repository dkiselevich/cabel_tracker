class CreateServiceJobs < ActiveRecord::Migration
  def change
    create_table :service_jobs do |t|
    	t.string 	:ot_id
    	t.integer 	:client_id

    	t.string 	:auftragsnummer
    	t.string 	:wirktiefe
    	t.string 	:nachname_des_anrufers
    	t.string 	:telefonnummer_des_anrufers
    	t.string 	:auftragsklasse
    	t.string 	:zustand
    	t.string 	:vorname_des_anrufers
    	t.string 	:email_des_anrufers
    
    	t.string 	:kunde_string
    	t.string 	:kunde_telefon_privat
    	t.string 	:kunde_telefon_mobil
    	t.string 	:kunde_telefon_geschaeftlich
    	t.string 	:kunde_email

    	t.string 	:kunde_plz
    	t.string 	:kunde_strasse
    	t.string 	:kunde_hausnummer_zusatz
    	t.string 	:kunde_ort
    	t.string 	:kunde_hausnummer
    
    	t.string 	:uep_string
    	t.string 	:uep_zustand
    	t.string 	:uebk
    	t.string 	:asb
    	t.string 	:onkz
    	t.string 	:we_angeschlossen
    	t.string 	:we_max
    	t.string 	:uebk

    	t.string 	:uep_zustand_klartext
    	t.string 	:grussz
    	t.string 	:vrp
    	t.string 	:c_linie
    	t.string 	:versorgte_objekt
    	t.string 	:inbetriebnahme

    	t.string 	:ad_zeitfenster_fruehester_beginn
    	t.string 	:e_vsip_kundentermin
    	t.string 	:vereinbarter_kundentermin
    	t.string 	:ad_zeitfenster_spaetester_beginn
    	t.string 	:techniker


    	t.string 	:textbaustein
    	t.string 	:beschreibung
    	t.string 	:rx
    	t.string 	:tx
    	t.string 	:bandbreite_downstream
    	t.string 	:rufnummer_1
    	t.string 	:modemtyp
    	t.string 	:mahnlauf

    	t.string 	:mac_adresse
    	t.string 	:snr
    	t.string 	:bandbreite_upstream
    	t.string 	:rufnummer_2
    	t.string 	:dienstmerkmalpaket

    	t.string 	:wartegrund_beschreibung
    	t.string 	:wiedervorlage
    	t.string 	:netzebene
    	t.string 	:stoerungswirkbreite    
    	t.string 	:abschlussgrund
    	t.string 	:abschlussgrund_beschreibung
    	t.string 	:stoerungsende_aus_kundensicht
    	t.string 	:systemabschluss
    	t.string 	:leistungsnachweis_vorhanden

	    t.timestamps
    end
  end
end
