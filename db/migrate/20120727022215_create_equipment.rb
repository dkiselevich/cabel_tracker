class CreateEquipment < ActiveRecord::Migration
  def change
    create_table :equipment do |t|
    	t.integer	:user_id
    	t.integer 	:article_id
    	t.integer	:quantity
      	t.timestamps
    end
  end
end
