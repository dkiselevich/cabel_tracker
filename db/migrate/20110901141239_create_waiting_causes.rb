class CreateWaitingCauses < ActiveRecord::Migration
  def self.up
    create_table :waiting_causes do |t|
    	t.string :name
      	t.timestamps
    end
    add_column :jobs, :wartegrund_id, :integer
  end

  def self.down
    drop_table :waiting_causes
    remove_column :jobs, :wartegrund_id
  end
end
