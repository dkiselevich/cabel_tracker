class AddMorePotFieldsToCompletion < ActiveRecord::Migration
  def up
  	add_column :completions, :no_access_to_pot_gas, :boolean, :default => false
  	add_column :completions, :no_access_to_pot_hak, :boolean, :default => false
  	add_column :completions, :no_access_to_pot_wasser, :boolean, :default => false
  	add_column :completions, :no_access_to_pot_heizung, :boolean, :default => false
  	add_column :completions, :no_access_to_pot_oel, :boolean, :default => false
  end
end
