class AddMoreCustomeDataToServiceJob < ActiveRecord::Migration
  def change
	  add_column :service_jobs, :kunde_nachname, :string
	  add_column :service_jobs, :kunde_vorname, :string
	  add_column :service_jobs, :kunde_kundennummer, :string
	  remove_column :service_jobs, :kunde_string
	  remove_column :service_jobs, :kunde_hausnummer_zusatz
  end
end
