class CreateServiceComments < ActiveRecord::Migration
  def change
    create_table :service_comments do |t|
      t.integer :service_job_id
      t.integer :user_id
      t.text	:body
      t.timestamps
    end
  end
end
