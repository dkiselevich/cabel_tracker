class AddVorverstaerkerbezeichnungToJobs < ActiveRecord::Migration
  def self.up
  	add_column :completions, :vorverstaerker_bezeichnung, :string
  	add_column :completions, :vorverstaerker_id, :integer
  	remove_column :completions, :vorverstaerker
  end

  def self.down
	remove_column :completions, :vorverstaerker_id
  	remove_column :completions, :vorverstaerker_bezeichnung, :string
  	add_column :completions, :vorverstaerker, :boolean
  end
end
