class AddPriorityToAppointment < ActiveRecord::Migration
  def change
  	add_column :appointments, :high_priority, :boolean, :default => false
  end
end
