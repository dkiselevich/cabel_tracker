class AddModemAktivierungToJob < ActiveRecord::Migration
  def self.up
  	add_column :jobs, :modemaktivierung, :date
  	remove_column :completions, :modemaktivierung
  end

  def self.down
 	add_column :completions, :modemaktivierung, :date
  	remove_column :jobs, :modemaktivierung
   end
end
