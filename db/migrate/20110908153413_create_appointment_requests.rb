class CreateAppointmentRequests < ActiveRecord::Migration
  def self.up
    create_table :appointment_requests do |t|
    	t.integer	:job_id
    	t.integer	:technician_id
    	
    	t.boolean 	:clever_pro
    	t.boolean 	:unbundled
    	t.timestamp :modemaktivierung
    	t.integer	:modemtyp_id
    	t.boolean	:umzug

  		t.integer	:installationsart_id
  		t.string	:installationsart_sonstiges
  		t.integer	:fi_sicherung_id
  		t.integer	:potentialausgleich_id
  		t.integer	:netztopologie_id

  		t.boolean	:vorverstaerker
  		t.boolean	:geschlossener_bvt
  		t.boolean	:klasse_4c
  		t.integer	:linienverstaerker_id

  		t.integer	:auftragsart_id
  		t.integer	:dosentausch
  		t.boolean	:kellerquerverkabelung
  		t.integer	:kellerquerverkabelung_laenge
  		t.integer	:potentialausgleich_id
  		
  		t.boolean	:mehraufwand

  		t.text		:hinderungsgrund

    	t.timestamp :termin_datum
    	t.time 		:zeitfenster_start
    	t.time 		:zeitfenster_ende
    	t.integer	:scheduling_user_id

    	t.timestamps
    end
  end

  def self.down
    drop_table :appointment_requests
  end
end
