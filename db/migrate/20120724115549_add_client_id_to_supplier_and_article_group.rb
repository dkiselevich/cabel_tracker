class AddClientIdToSupplierAndArticleGroup < ActiveRecord::Migration
  def change
  	add_column :suppliers, :client_id, :integer
  	#add_column :article_groups, :client_id, :integer #ooops - no article_groups yet
  end
end
