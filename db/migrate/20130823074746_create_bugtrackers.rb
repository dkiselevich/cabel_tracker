class CreateBugtrackers < ActiveRecord::Migration
  def change
    create_table :bugtrackers do |t|
      t.integer :user_id
      t.string :email
      t.string :bug
      t.string :location
      t.text :description
      t.boolean :bug_solved

      t.timestamps
    end
  end
end
