class AddSchedulingLeaderToUsers < ActiveRecord::Migration
  def change
  	add_column :users, :is_controller_leader, :boolean
  end
end
