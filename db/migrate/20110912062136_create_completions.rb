class CreateCompletions < ActiveRecord::Migration
  def self.up
    create_table :completions do |t|
    	t.integer	:job_id
    	t.integer	:technician_id
    	
    	t.boolean 	:clever_pro
    	t.boolean 	:unbundled
    	t.timestamp :modemaktivierung
    	t.integer	:modemtyp_id
    	t.boolean	:umzug

  		t.integer	:installationsart_id
  		t.string	:installationsart_sonstiges
  		t.integer	:fi_sicherung_id
  		t.integer	:potentialausgleich_id
  		t.integer	:netztopologie_id

  		t.boolean	:vorverstaerker
  		t.boolean	:geschlossener_bvt
  		t.boolean	:klasse_4c
  		t.integer	:linienverstaerker_id
  		t.boolean	:mehraufwand

      t.integer :auftragsart_id
      t.integer :dosentausch
      t.boolean :kellerquerverkabelung
      t.integer :kellerquerverkabelung_laenge
      t.boolean :potentialausgleich

  		t.decimal 	:D130, :precision => 5, :scale => 2
  		t.decimal 	:K5, :precision => 5, :scale => 2
  		t.decimal 	:K29, :precision => 5, :scale => 2
  		t.decimal 	:D802, :precision => 5, :scale => 2

  		t.string	:HFC_MAC
    	t.boolean	:adresspruefung

    	t.text		:messwerte
    	t.text		:besonderheiten

    	t.boolean	:uebrige_modems_im_gebaeude_per_SBC_portal_geprueft
    	t.boolean	:tv_empfang_beim_kunden_erfolgreich_geprueft
    	t.boolean	:internetzugang_mit_montagelaptop_geprueft
    	t.boolean	:telefon_abgehend_erfolgreich_getestet
    	t.boolean	:telefon_ankommend_erfolgreich_getestet

    	t.text		:beauftragte_leistungen

      	t.timestamps
    end
  end

  def self.down
    drop_table :completions
  end
end
