class AddIsPurchaseManagerToUser < ActiveRecord::Migration
  def change
  	add_column :users, :is_purchase_manager, :boolean, :default => false
  end
end
