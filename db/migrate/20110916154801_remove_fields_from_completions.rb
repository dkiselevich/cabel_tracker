class RemoveFieldsFromCompletions < ActiveRecord::Migration
  def self.up
  	remove_column :completions, :messwerte
  	remove_column :completions, :potentialausgleich
  	#remove_column :completions, :modemaktivierung
  end

  def self.down
  	add_column :completions, :messwerte, :text
  	add_column :completions, :potentialausgleich, :boolean
  	#add_column :completions, :modemaktivierung, :timestamp
  end
end
