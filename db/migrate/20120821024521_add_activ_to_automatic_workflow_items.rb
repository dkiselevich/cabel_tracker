class AddActivToAutomaticWorkflowItems < ActiveRecord::Migration
  def change
  	add_column :automatic_workflow_items, :active, :boolean, :default => true
  end
end
