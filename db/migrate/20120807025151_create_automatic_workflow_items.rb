class CreateAutomaticWorkflowItems < ActiveRecord::Migration
  def change
    create_table :automatic_workflow_items do |t|
    	t.integer 	:client_id
    	t.integer 	:workflow_action_id
    	t.time 		:start_time	
    	t.datetime	:last_run_at
    	t.timestamps
    end
  end
end
