class AddAppointmentIdToJobAssociatedTables < ActiveRecord::Migration
  def self.up
  	add_column :cancellations, :appointment_id, :integer
  	add_column :completions, :appointment_id, :integer
  	add_column :appointment_requests, :appointment_id, :integer

  	remove_column :cancellations, :job_id
  	remove_column :completions, :job_id
  	remove_column :appointment_requests, :job_id
  end

  def self.down
  	remove_column :cancellations, :appointment_id
  	remove_column :completions, :appointment_id
  	remove_column :appointment_requests, :appointment_id

  	add_column :cancellations, :job_id, :integer
  	add_column :completions, :job_id, :integer
  	add_column :appointment_requests, :job_id, :integer
  end
end
