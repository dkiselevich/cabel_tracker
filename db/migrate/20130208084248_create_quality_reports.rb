class CreateQualityReports < ActiveRecord::Migration
  def change
    create_table :quality_reports do |t|
      t.text :planned_measures_next_two_month
      t.text :detected_quality_problems
      t.text :identified_necessary_measures
      t.text :launch_plannings
      t.text :implemented_measures
      t.text :sustainability_control
      t.date :pdf_created
      t.integer :user_id

      t.timestamps
    end
  end
end
