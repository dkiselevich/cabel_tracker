class FixArticleTableWrongTypeForUnitId < ActiveRecord::Migration
  def up
  	remove_column :articles, :unit_id
  	add_column :articles, :unit_id, :integer, :default => 1
  end

  def down
  	remove_column :articles, :unit_id
  	add_column :articles, :unit_id, :string
  end
end
