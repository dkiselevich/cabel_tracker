class AddAttachmentsFieldsToServiceCompletions < ActiveRecord::Migration
  def change
  	add_column :service_completions, :additional_attachment_1_file_name, :string
  	add_column :service_completions, :additional_attachment_2_file_name, :string
  	add_column :service_completions, :additional_attachment_3_file_name, :string
  	add_column :service_completions, :additional_attachment_4_file_name, :string
  	add_column :service_completions, :additional_attachment_5_file_name, :string
  	add_column :service_completions, :additional_attachment_6_file_name, :string
  	add_column :service_completions, :additional_attachment_7_file_name, :string
  	add_column :service_completions, :additional_attachment_8_file_name, :string
  	add_column :service_completions, :additional_attachment_9_file_name, :string
  	add_column :service_completions, :additional_attachment_10_file_name, :string

  	add_column :service_completions, :additional_attachment_1_description, :string
  	add_column :service_completions, :additional_attachment_2_description, :string
  	add_column :service_completions, :additional_attachment_3_description, :string
  	add_column :service_completions, :additional_attachment_4_description, :string
  	add_column :service_completions, :additional_attachment_5_description, :string
  	add_column :service_completions, :additional_attachment_6_description, :string
  	add_column :service_completions, :additional_attachment_7_description, :string
  	add_column :service_completions, :additional_attachment_8_description, :string
  	add_column :service_completions, :additional_attachment_9_description, :string
  	add_column :service_completions, :additional_attachment_10_description, :string
  end
end
