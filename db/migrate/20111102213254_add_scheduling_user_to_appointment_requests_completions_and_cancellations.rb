class AddSchedulingUserToAppointmentRequestsCompletionsAndCancellations < ActiveRecord::Migration
  	def self.up
		#add_column :appointment_requests, :scheduling_user_id, :integer
		add_column :cancellations, :scheduling_user_id, :integer
		add_column :completions, :scheduling_user_id, :integer

	  	c = User.where(:is_controller => true)		
  		AppointmentRequest.all.each{|a| a.scheduling_user = c[rand(c.size)]; a.save}
  		Completion.all.each{|a| a.scheduling_user = c[rand(c.size)]; a.save}
  		Cancellation.all.each{|a| a.scheduling_user = c[rand(c.size)]; a.save}
  	end

  	def self.down
		#remove_column :appointment_requests, :scheduling_user_id, :integer
		remove_column :cancellations, :scheduling_user_id, :integer
		remove_column :completions, :scheduling_user_id, :integer
  	end
end
