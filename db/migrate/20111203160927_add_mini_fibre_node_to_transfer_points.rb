class AddMiniFibreNodeToTransferPoints < ActiveRecord::Migration
  def change
  	add_column :transfer_points, :mini_fibre_node, :boolean
  end
end
