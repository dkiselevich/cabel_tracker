class AlterAppointments < ActiveRecord::Migration
  def self.up
  	add_column :appointments, :day, :date
  	add_column :appointments, :start_time, :time
  	add_column :appointments, :end_time, :time
  	remove_column :appointments, :start
  	remove_column :appointments, :ende
  end

  def self.down
  	remove_column :appointments, :day
  	remove_column :appointments, :start_time
  	remove_column :appointments, :end_time
  	add_column :appointments, :start, :datetime
  	add_column :appointments, :ende, :datetime
  end
end
