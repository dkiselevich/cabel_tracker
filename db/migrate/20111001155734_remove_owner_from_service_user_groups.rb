class RemoveOwnerFromServiceUserGroups < ActiveRecord::Migration
  def self.up
  	remove_column :service_user_groups, :owner_id
  end

  def self.down
  	add_column :service_user_groups, :owner_id, :integer
  end
end
