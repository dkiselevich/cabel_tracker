class AddOtDetailsToClient < ActiveRecord::Migration
  def change
  	add_column :clients, :ot_login, :string
  	add_column :clients, :ot_password, :string
  end
end
