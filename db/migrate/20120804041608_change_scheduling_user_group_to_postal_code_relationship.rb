class ChangeSchedulingUserGroupToPostalCodeRelationship < ActiveRecord::Migration
  def up
  	add_column :postal_codes, :scheduling_user_group_id, :integer
    drop_table :postal_codes_scheduling_user_groups
  end

  def down
	remove_column :postal_codes, :scheduling_user_group_id
	create_table :postal_codes_scheduling_user_groups, :id => false do |t|
		t.integer :scheduling_user_group_id
		t.integer :postal_code_id
	end
  end
end
