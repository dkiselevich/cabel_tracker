class AddProvisionierungToCompletion < ActiveRecord::Migration
  def change
  	add_column :completions, :provisionierung, :boolean
  end
end
