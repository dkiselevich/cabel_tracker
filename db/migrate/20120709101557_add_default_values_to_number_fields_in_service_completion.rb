class AddDefaultValuesToNumberFieldsInServiceCompletion < ActiveRecord::Migration
  def change
	remove_column :service_completions,	:call_out_cost_per_unit
	remove_column :service_completions,	:service_time_cost_per_unit
	remove_column :service_completions,	:additional_cost_per_unit
	remove_column :service_completions,	:transfer_point_cost_per_unit
	remove_column :service_completions,	:amplifier_cost_per_unit
	remove_column :service_completions,	:mmd_cost_per_unit
	remove_column :service_completions,	:material_quantity_1_cost_per_unit
	remove_column :service_completions,	:material_quantity_2_cost_per_unit
	remove_column :service_completions,	:material_quantity_3_cost_per_unit
	remove_column :service_completions,	:material_quantity_4_cost_per_unit
	remove_column :service_completions,	:material_quantity_5_cost_per_unit

	add_column :service_completions, :call_out_cost_per_unit, :decimal, :precision => 8, :scale => 2, :default => 0
	add_column :service_completions, :service_time_cost_per_unit, :decimal, :precision => 8, :scale => 2, :default => 0
	add_column :service_completions, :additional_cost_per_unit, :decimal, :precision => 8, :scale => 2, :default => 0
	add_column :service_completions, :transfer_point_cost_per_unit, :decimal, :precision => 8, :scale => 2, :default => 0
	add_column :service_completions, :amplifier_cost_per_unit, :decimal, :precision => 8, :scale => 2, :default => 0
	add_column :service_completions, :mmd_cost_per_unit, :decimal, :precision => 8, :scale => 2, :default => 0
	add_column :service_completions, :material_quantity_1_cost_per_unit, :decimal, :precision => 8, :scale => 2, :default => 0
	add_column :service_completions, :material_quantity_2_cost_per_unit, :decimal, :precision => 8, :scale => 2, :default => 0
	add_column :service_completions, :material_quantity_3_cost_per_unit, :decimal, :precision => 8, :scale => 2, :default => 0
	add_column :service_completions, :material_quantity_4_cost_per_unit, :decimal, :precision => 8, :scale => 2, :default => 0
	add_column :service_completions, :material_quantity_5_cost_per_unit, :decimal, :precision => 8, :scale => 2, :default => 0


	remove_column :service_completions, :call_out_cost_quantity
	remove_column :service_completions, :service_time_cost_quantity
	remove_column :service_completions, :additional_cost_quantity

	add_column :service_completions, :call_out_cost_quantity, :decimal, :precision => 8, :scale => 2, :default => 0
	add_column :service_completions, :service_time_cost_quantity, :decimal, :precision => 8, :scale => 2, :default => 0
	add_column :service_completions, :additional_cost_quantity, :decimal, :precision => 8, :scale => 2, :default => 0

	remove_column :service_completions, :material_quantity_1
	remove_column :service_completions, :material_quantity_2
	remove_column :service_completions, :material_quantity_3
	remove_column :service_completions, :material_quantity_4
	remove_column :service_completions, :material_quantity_5
	
	add_column :service_completions, :material_quantity_1, :decimal, :precision => 8, :scale => 2, :default => 0
	add_column :service_completions, :material_quantity_2, :decimal, :precision => 8, :scale => 2, :default => 0
	add_column :service_completions, :material_quantity_3, :decimal, :precision => 8, :scale => 2, :default => 0
	add_column :service_completions, :material_quantity_4, :decimal, :precision => 8, :scale => 2, :default => 0
	add_column :service_completions, :material_quantity_5, :decimal, :precision => 8, :scale => 2, :default => 0



  end
end
