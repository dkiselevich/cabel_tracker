class AddZuHoherAufwandId < ActiveRecord::Migration
  def self.up
  	add_column :cancellations, :zu_hoher_aufwand_id, :int
  	remove_column :cancellations, :zu_hoher_aufwand
  end

  def self.down
  	remove_column :cancellations, :zu_hoher_aufwand_id
  	add_column :cancellations, :zu_hoher_aufwand, :boolean
  end
end
