class AddAdminUser < ActiveRecord::Migration
  def up
  	admin = User.new
  	admin.email = "admin@cabletracker.de"
  	admin.password = "n8igall"
  	admin.password_confirmation = "n8igall"
  	admin.vorname = "Frank"
  	admin.nachname = "Duffner"
  	admin.is_admin = true
  	admin.save
  end

  def down
  	User.where(:email => "admin@cabletracker.de")[0].delete
  end
end
