class AddRolesToUser < ActiveRecord::Migration
  def self.up
  	add_column :users, :is_boss, :boolean
  	add_column :users, :is_controller, :boolean
  	add_column :users, :is_technician, :boolean
  	add_column :users, :is_accountant, :boolean
  	add_column :users, :is_admin, :boolean
  end

  def self.down
  end
end
