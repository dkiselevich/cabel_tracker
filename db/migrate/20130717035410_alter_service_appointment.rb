class AlterServiceAppointment < ActiveRecord::Migration
  def up
    remove_column :service_appointments, :start_time
    remove_column :service_appointments, :end_time    
    add_column :service_appointments, :start_time, :time
    add_column :service_appointments, :end_time, :time   
  end

  def down
    remove_column :service_appointments, :start_time
    remove_column :service_appointments, :end_time
    add_column :service_appointments, :start_time, :timestamp
    add_column :service_appointments, :end_time, :timestamp   
  end
end

