class CreateServiceUserGroups < ActiveRecord::Migration
  def self.up
    create_table :service_user_groups do |t|
      t.string :name
      t.integer :owner_id

      t.timestamps
    end
  end

  def self.down
    drop_table :service_user_groups
  end
end
