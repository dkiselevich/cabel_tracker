class ChangeAuftragsnummerToOtAuftragsnummerOnServiceJobs < ActiveRecord::Migration
  def up
  	rename_column :service_jobs, :auftragsnummer, :ot_auftragsnummer
  end

  def down
  end
end
