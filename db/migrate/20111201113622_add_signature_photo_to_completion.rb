class AddSignaturePhotoToCompletion < ActiveRecord::Migration
  def change
    add_column :completions, :signature_photo_file_name, :string 
    add_column :completions, :signature_photo_content_type, :string
    add_column :completions, :signature_photo_file_size, :integer
    add_column :completions, :signature_photo_updated_at, :datetime
  end
end
