class CreateAppointments < ActiveRecord::Migration
  def self.up
    create_table :appointments do |t|
      t.integer :job_id
      t.timestamp :start
      t.timestamp :ende
      t.text :bemerkungen

      t.timestamps
    end
  end

  def self.down
    drop_table :appointments
  end
end
