class AddCancellationCause < ActiveRecord::Migration
  def self.up
  	add_column :cancellations, :cause_id, :integer
  	remove_column :cancellations, :keine_installation_moeglich
  	remove_column :cancellations, :genehmigung_notwendig
  end

  def self.down
  	remove_column :cancellations, :cause_id
  	remove_column :cancellations, :keine_installation_moeglich, :boolean
  	remove_column :cancellations, :genehmigung_notwendig, :boolean
  end
end
