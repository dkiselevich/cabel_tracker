class AddVerbindlicherTerminToJob < ActiveRecord::Migration
  def self.up
  	add_column :jobs, :verbindlicher_installationstermin, :date
  end

  def self.down
  	remove_column :jobs, :verbindlicher_installationstermin
  end
end
