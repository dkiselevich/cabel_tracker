class CreateTransferPoints < ActiveRecord::Migration
  def change
    create_table :transfer_points do |t|
      t.string :uid
      t.string :ot_id
      t.string :plz
      t.string :ort
      t.string :strasse
      t.string :hausnummer
      t.string :zustand
      t.string :typ
      t.string :dienstverfuegbarkeit
      t.integer :we_am_uep
      t.integer :aktive_kunden_am_uep
      t.date :inbetriebnahme

      t.timestamps
    end

    add_column :jobs, :uep_id, :integer
  end
end
