class AddPrivisionierungToCompletion < ActiveRecord::Migration
  def self.up
  	add_column :completions, :provisionierung_id, :integer
  end

  def self.down
  	remove_column :completions, :provisionierung_id, :integer
  end
end
