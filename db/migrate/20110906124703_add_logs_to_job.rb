class AddLogsToJob < ActiveRecord::Migration
  def self.up
  	add_column :jobs, :logs, :text, :default => ""
  end

  def self.down
  	remove_column :jobs, :logs
  end
end
