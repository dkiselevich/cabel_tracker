class ChangeUserIdColumnInBugtrackersTable < ActiveRecord::Migration
  def change
  	rename_column :bugtrackers, :user_id, :user
  	change_column :bugtrackers, :user, :string
  end
end
