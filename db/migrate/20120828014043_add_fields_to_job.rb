class AddFieldsToJob < ActiveRecord::Migration
  def change
    add_column :jobs, :clerver_pro, :boolean
    add_column :jobs, :unbunled, :boolean
  end
end
