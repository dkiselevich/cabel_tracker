class AddNoAccessFieldsToCompletion < ActiveRecord::Migration
  def change
  	add_column :completions, :no_access_to_bvt, :boolean, :default => false
  	add_column :completions, :no_access_to_uep, :boolean, :default => false
  	add_column :completions, :no_access_to_pot_schiene, :boolean, :default => false
  end
end
