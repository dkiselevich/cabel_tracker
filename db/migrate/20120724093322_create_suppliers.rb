class CreateSuppliers < ActiveRecord::Migration
  def change
    create_table :suppliers do |t|
      t.string :name
      t.string :contact_first_name
      t.string :contact_last_name
      t.string :street
      t.string :house_number
      t.string :postal_code
      t.string :city
      t.string :phone
      t.string :fax
      t.string :email
      t.string :order_portal

      t.timestamps
    end
  end
end
