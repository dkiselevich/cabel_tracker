class RenameColumnsInDaysOfAbsence < ActiveRecord::Migration
  def change
    rename_column :days_of_absence, :start, :starting_point
    rename_column :days_of_absence, :end, :end_point
  end
end
