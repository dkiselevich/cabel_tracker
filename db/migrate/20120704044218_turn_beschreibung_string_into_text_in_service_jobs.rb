class TurnBeschreibungStringIntoTextInServiceJobs < ActiveRecord::Migration
  def up
  	remove_column :service_jobs, :beschreibung
  	add_column :service_jobs, :beschreibung, :text
  end

  def down
  	remove_column :service_jobs, :beschreibung
  end
end
