class AddSchedulingUserToAppointment < ActiveRecord::Migration
  def self.up
  	add_column :appointments, :scheduling_user_id, :integer

  	c = User.where(:is_controller => true)
  	Appointment.all.each{|a| a.scheduling_user = c[rand(c.size)]}
  end

  def self.down
  	remove_column :appointments, :scheduling_user_id
  end
end
