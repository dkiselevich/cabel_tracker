class CreateModemTypes < ActiveRecord::Migration
  def self.up
    create_table :modem_types do |t|
      t.string :name
      t.boolean :active, :default => true

      t.timestamps
    end
  end

  def self.down
    drop_table :modem_types
  end
end
