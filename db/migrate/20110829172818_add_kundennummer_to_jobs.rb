class AddKundennummerToJobs < ActiveRecord::Migration
  def self.up
  	add_column :jobs, :kunde_kundennummer, :string
  end

  def self.down
  	remove_column :jobs, :kunde_kundennummer
  end
end
