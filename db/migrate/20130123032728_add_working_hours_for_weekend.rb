class AddWorkingHoursForWeekend < ActiveRecord::Migration
  def change
  	add_column :users, :saturday_work_start, :time
  	add_column :users, :saturday_lunch_break_start, :time
  	add_column :users, :saturday_lunch_break_end, :time
  	add_column :users, :saturday_work_end, :time
  	
  	add_column :users, :sunday_work_start, :time
  	add_column :users, :sunday_lunch_break_start, :time
  	add_column :users, :sunday_lunch_break_end, :time
  	add_column :users, :sunday_work_end, :time
  end
end
