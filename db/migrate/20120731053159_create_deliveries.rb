class CreateDeliveries < ActiveRecord::Migration
  def change
    create_table :deliveries do |t|
    	t.integer :type_id
    	t.integer :technician_id
    	t.integer :supplier_id
    	t.integer :status_id, :default => 0
    	t.integer :client_id
      	t.timestamps
    end
  end
end
