class AddWorkingHoursToUser < ActiveRecord::Migration
  def change
  	add_column :users, :monday_work_start, :time
  	add_column :users, :monday_lunch_break_start, :time
  	add_column :users, :monday_lunch_break_end, :time
  	add_column :users, :monday_work_end, :time
  	
  	add_column :users, :tuesday_work_start, :time
  	add_column :users, :tuesday_lunch_break_start, :time
  	add_column :users, :tuesday_lunch_break_end, :time
  	add_column :users, :tuesday_work_end, :time
  	
  	add_column :users, :wednesday_work_start, :time
  	add_column :users, :wednesday_lunch_break_start, :time
  	add_column :users, :wednesday_lunch_break_end, :time
  	add_column :users, :wednesday_work_end, :time
  	
  	add_column :users, :thursday_work_start, :time
  	add_column :users, :thursday_lunch_break_start, :time
  	add_column :users, :thursday_lunch_break_end, :time
  	add_column :users, :thursday_work_end, :time
  	
  	add_column :users, :friday_work_start, :time
  	add_column :users, :friday_lunch_break_start, :time
  	add_column :users, :friday_lunch_break_end, :time
  	add_column :users, :friday_work_end, :time
  end
end
