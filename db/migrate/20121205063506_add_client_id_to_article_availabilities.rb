class AddClientIdToArticleAvailabilities < ActiveRecord::Migration
  def change
    add_column :article_availabilities, :client_id, :integer
  end
end
