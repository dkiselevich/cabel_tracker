class CreateCancellations < ActiveRecord::Migration
  def self.up
    create_table :cancellations do |t|
    	t.integer	:job_id
    	t.integer	:technician_id

    	t.integer	:anzahl_der_etagen
    	t.integer 	:maximale_anzahl_HH_auf_etage
    	t.integer	:netztopologie_id
    	t.boolean	:mitversorgung
    	t.integer	:mitversorgung_art_id

    	t.boolean	:keine_installation_moeglich
    	t.boolean	:genehmigung_notwendig

    	t.boolean	:up_rohre_vorhanden_und_geeignet
    	t.integer	:up_rohre_tausch_hinderungsgrund_id
    	t.integer	:ap_verlegung_option_id
    	t.boolean	:stromeinspeisung_per_dach_moeglich
    	t.boolean	:pot_erstellung_moeglich

    	t.integer	:installation_verweigerer_id
    	t.string 	:verweigerer_nachname
    	t.string 	:verweigerer_vorname
    	t.string 	:verweigerer_telefon
    	
    	t.boolean	:eigentuemer_im_haus
    	t.string 	:eigentuemer_nachname
    	t.string 	:eigentuemer_vorname
    	t.string 	:eigentuemer_telefon

    	t.integer 	:verweigerungsgrund_id
    	t.text	 	:verweigerungsgrund_sonstiges_beschreibung

    	t.boolean 	:zu_hoher_aufwand
    	t.text	 	:zu_hoher_aufwand_beschreibung

    	t.text		:installationsbeschreibung
    			
      	t.timestamps
    end
  end

  def self.down
    drop_table :cancellations
  end
end
