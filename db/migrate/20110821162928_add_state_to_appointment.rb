class AddStateToAppointment < ActiveRecord::Migration
  def self.up
  	add_column :appointments, :status, :integer, :default => 0
  end

  def self.down
  	remove_column :appointments, :status
  end
end
