class CreateArticleGroups < ActiveRecord::Migration
  def change
    create_table :article_groups do |t|
    	t.string 	:name
    	t.integer 	:type_id
    	t.boolean	:active
    	t.integer	:client_id
	    t.timestamps
    end
  end
end
