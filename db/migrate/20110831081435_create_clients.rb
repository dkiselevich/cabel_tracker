class CreateClients < ActiveRecord::Migration
  def self.up
    create_table :clients do |t|
    	t.string :name
    	t.timestamps
    end
    rename_column :jobs, :mandant_id, :client_id
  end

  def self.down
  	drop_table :clients
    rename_column :jobs, :client_id, :mandant_id
  end
end
