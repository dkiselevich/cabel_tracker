class AddPhotosToCompletions < ActiveRecord::Migration
  def self.up
    add_column :completions, :bvt_photo_file_name, :string 
    add_column :completions, :bvt_photo_content_type, :string
    add_column :completions, :bvt_photo_file_size, :integer
    add_column :completions, :bvt_photo_updated_at, :datetime
    
    add_column :completions, :uep_photo_file_name, :string 
    add_column :completions, :uep_photo_content_type, :string
    add_column :completions, :uep_photo_file_size, :integer
    add_column :completions, :uep_photo_updated_at, :datetime
    
    add_column :completions, :pot_schiene_photo_file_name, :string 
    add_column :completions, :pot_schiene_photo_content_type, :string
    add_column :completions, :pot_schiene_photo_file_size, :integer
    add_column :completions, :pot_schiene_photo_updated_at, :datetime
    
    add_column :completions, :pot_gas_photo_file_name, :string 
    add_column :completions, :pot_gas_photo_content_type, :string
    add_column :completions, :pot_gas_photo_file_size, :integer
    add_column :completions, :pot_gas_photo_updated_at, :datetime
    
    add_column :completions, :pot_wasser_photo_file_name, :string 
    add_column :completions, :pot_wasser_photo_content_type, :string
    add_column :completions, :pot_wasser_photo_file_size, :integer
    add_column :completions, :pot_wasser_photo_updated_at, :datetime
    
    add_column :completions, :pot_hak_photo_file_name, :string 
    add_column :completions, :pot_hak_photo_content_type, :string
    add_column :completions, :pot_hak_photo_file_size, :integer
    add_column :completions, :pot_hak_photo_updated_at, :datetime
    
    add_column :completions, :pot_heizung_photo_file_name, :string 
    add_column :completions, :pot_heizung_photo_content_type, :string
    add_column :completions, :pot_heizung_photo_file_size, :integer
    add_column :completions, :pot_heizung_photo_updated_at, :datetime
    
    add_column :completions, :pot_oel_photo_file_name, :string 
    add_column :completions, :pot_oel_photo_content_type, :string
    add_column :completions, :pot_oel_photo_file_size, :integer
    add_column :completions, :pot_oel_photo_updated_at, :datetime
    
    add_column :completions, :mmd_unbundled_photo_file_name, :string 
    add_column :completions, :mmd_unbundled_photo_content_type, :string
    add_column :completions, :mmd_unbundled_photo_file_size, :integer
    add_column :completions, :mmd_unbundled_photo_updated_at, :datetime

    add_column :completions, :messwerte_photo_file_name, :string 
    add_column :completions, :messwerte_photo_content_type, :string
    add_column :completions, :messwerte_photo_file_size, :integer
    add_column :completions, :messwerte_photo_updated_at, :datetime
  end

  def self.down
    remove_column :completions, :bvt_photo_file_name
    remove_column :completions, :bvt_photo_content_type
    remove_column :completions, :bvt_photo_file_size
    remove_column :completions, :bvt_photo_updated_at
    
    remove_column :completions, :uep_photo_file_name
    remove_column :completions, :uep_photo_content_type
    remove_column :completions, :uep_photo_file_size
    remove_column :completions, :uep_photo_updated_at
    
    remove_column :completions, :pot_schiene_photo_file
    remove_column :completions, :pot_schiene_photo_content
    remove_column :completions, :pot_schiene_photo_file
    remove_column :completions, :pot_schiene_photo_updated
    
    remove_column :completions, :pot_gas_photo_file
    remove_column :completions, :pot_gas_photo_content
    remove_column :completions, :pot_gas_photo_file
    remove_column :completions, :pot_gas_photo_updated
    
    remove_column :completions, :pot_wasser_photo_file
    remove_column :completions, :pot_wasser_photo_content
    remove_column :completions, :pot_wasser_photo_file
    remove_column :completions, :pot_wasser_photo_updated
    
    remove_column :completions, :pot_hak_photo_file
    remove_column :completions, :pot_hak_photo_content
    remove_column :completions, :pot_hak_photo_file
    remove_column :completions, :pot_hak_photo_updated
    
    remove_column :completions, :pot_heizung_photo_file
    remove_column :completions, :pot_heizung_photo_content
    remove_column :completions, :pot_heizung_photo_file
    remove_column :completions, :pot_heizung_photo_updated
    
    remove_column :completions, :pot_oel_photo_file
    remove_column :completions, :pot_oel_photo_content
    remove_column :completions, :pot_oel_photo_file
    remove_column :completions, :pot_oel_photo_updated
    
    remove_column :completions, :mmd_unbundled_photo_file
    remove_column :completions, :mmd_unbundled_photo_content
    remove_column :completions, :mmd_unbundled_photo_file
    remove_column :completions, :mmd_unbundled_photo_updated

    remove_column :completions, :messwerte_photo_file
    remove_column :completions, :messwerte_photo_content
    remove_column :completions, :messwerte_photo_file
    remove_column :completions, :messwerte_photo_updated
  end
end
