class RemoveStuffFromAppointmentRequests < ActiveRecord::Migration
  def self.up
	remove_column :appointment_requests, :clever_pro
	remove_column :appointment_requests, :unbundled
	remove_column :appointment_requests, :modemaktivierung
	remove_column :appointment_requests, :modemtyp_id
	remove_column :appointment_requests, :umzug

	remove_column :appointment_requests, :installationsart_id
	remove_column :appointment_requests, :installationsart_sonstiges
	remove_column :appointment_requests, :fi_sicherung_id
	remove_column :appointment_requests, :potentialausgleich_id
	remove_column :appointment_requests, :netztopologie_id

	remove_column :appointment_requests, :vorverstaerker
	remove_column :appointment_requests, :geschlossener_bvt
	remove_column :appointment_requests, :klasse_4c
	remove_column :appointment_requests, :linienverstaerker_id

	remove_column :appointment_requests, :auftragsart_id
	remove_column :appointment_requests, :dosentausch
	remove_column :appointment_requests, :kellerquerverkabelung
	remove_column :appointment_requests, :kellerquerverkabelung_laenge
#	remove_column :appointment_requests, :potentialausgleich_id
	
	remove_column :appointment_requests, :mehraufwand
  end

  def self.down
	add_column :appointment_requests, :clever_pro ,:boolean 	
	add_column :appointment_requests, :unbundled ,:boolean 	
	add_column :appointment_requests, :modemaktivierung ,:timestamp 
	add_column :appointment_requests, :modemtyp_id ,:integer	
	add_column :appointment_requests, :umzug ,:boolean	

	add_column :appointment_requests, :installationsart_id ,:integer	
	add_column :appointment_requests, :installationsart_sonstiges ,:string	
	add_column :appointment_requests, :fi_sicherung_id ,:integer	
	add_column :appointment_requests, :potentialausgleich_id ,:integer	
	add_column :appointment_requests, :netztopologie_id ,:integer	

	add_column :appointment_requests, :vorverstaerker ,:boolean	
	add_column :appointment_requests, :geschlossener_bvt ,:boolean	
	add_column :appointment_requests, :klasse_4c ,:boolean	
	add_column :appointment_requests, :linienverstaerker_id ,:integer	

	add_column :appointment_requests, :auftragsart_id ,:integer	
	add_column :appointment_requests, :dosentausch ,:integer	
	add_column :appointment_requests, :kellerquerverkabelung ,:boolean	
	add_column :appointment_requests, :kellerquerverkabelung_laenge ,:integer	
#	add_column :appointment_requests, :potentialausgleich_id ,:integer	
	
	add_column :appointment_requests, :mehraufwand ,:boolean	
  end
end
