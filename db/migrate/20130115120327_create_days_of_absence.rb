class CreateDaysOfAbsence < ActiveRecord::Migration
  def change
    create_table :days_of_absence do |t|
			t.integer :user_id
      t.timestamp :start
      t.timestamp :end
      t.integer :cause_id
      t.timestamps
    end
  end
end
