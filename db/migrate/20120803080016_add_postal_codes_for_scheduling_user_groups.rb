class AddPostalCodesForSchedulingUserGroups < ActiveRecord::Migration
  def up
    create_table :postal_codes_scheduling_user_groups, :id => false do |t|
      t.integer :scheduling_user_group_id
      t.integer :postal_code_id
    end
  end

  def down
    drop_table :postal_codes_scheduling_user_groups
  end
end
