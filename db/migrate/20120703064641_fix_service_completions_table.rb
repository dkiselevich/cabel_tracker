class FixServiceCompletionsTable < ActiveRecord::Migration
  def up
	remove_column :service_completions, :call_out_cost_quantity
	remove_column :service_completions, :service_time_cost_quantity
	remove_column :service_completions, :additional_cost_quantity

	add_column :service_completions, :call_out_cost_quantity, :decimal, :precision => 8, :scale => 2
	add_column :service_completions, :service_time_cost_quantity, :decimal, :precision => 8, :scale => 2
	add_column :service_completions, :additional_cost_quantity, :decimal, :precision => 8, :scale => 2

	remove_column :service_completions, :material_quantity_1
	remove_column :service_completions, :material_quantity_2
	remove_column :service_completions, :material_quantity_3
	remove_column :service_completions, :material_quantity_4
	remove_column :service_completions, :material_quantity_5
	
	add_column :service_completions, :material_quantity_1, :decimal, :precision => 8, :scale => 2
	add_column :service_completions, :material_quantity_2, :decimal, :precision => 8, :scale => 2
	add_column :service_completions, :material_quantity_3, :decimal, :precision => 8, :scale => 2
	add_column :service_completions, :material_quantity_4, :decimal, :precision => 8, :scale => 2
	add_column :service_completions, :material_quantity_5, :decimal, :precision => 8, :scale => 2
  end

  def down
  end
end
