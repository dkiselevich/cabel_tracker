class AddCancellationIdToPhotos < ActiveRecord::Migration
  def self.up
  	add_column :photos, :cancellation_id, :integer
  end

  def self.down
  	remove_column :photos, :cancellation_id
  end
end
