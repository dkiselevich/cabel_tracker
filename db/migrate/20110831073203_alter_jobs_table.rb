class AlterJobsTable < ActiveRecord::Migration
  def self.up
      change_column :jobs, :kundennummer, :string
      change_column :jobs,  :workorder, :string
      remove_column :jobs, :kundennummer
  end

  def self.down
      change_column :jobs, :kundennummer, :integer
      change_column :jobs,  :workorder, :integer
      add_column :jobs, :kundennummer, :string
  end
end
