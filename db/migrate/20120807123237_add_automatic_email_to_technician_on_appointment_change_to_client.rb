class AddAutomaticEmailToTechnicianOnAppointmentChangeToClient < ActiveRecord::Migration
  def change
  	add_column :clients, :automatic_email_to_technician_on_appointment_change_to_client, :boolean, :default => false
  end
end
