class CreateServiceCompletions < ActiveRecord::Migration
  def change
    create_table :service_completions do |t|
    	t.integer 	:service_type_id, :nil => false
    	t.integer 	:invoice_option_id, :nil => false

    	t.string 	:debitor_sap_number

    	t.integer 	:customer_type_id
    	t.string 	:customer_first_name
    	t.string 	:customer_last_name
    	t.string 	:customer_street_name
    	t.string 	:customer_house_number
    	t.string 	:customer_postal_code
    	t.string 	:customer_city

    	t.integer 	:invoice_recipient_type_id
    	t.string 	:invoice_recipient_first_name
    	t.string 	:invoice_recipient_last_name
    	t.string 	:invoice_recipient_street_name
    	t.string 	:invoice_recipient_house_number
    	t.string 	:invoice_recipient_postal_code
    	t.string 	:invoice_recipient_city

    	t.date 		:date
    	t.string	:order_number
    	t.string	:customer_number
    	t.string	:sk_order_number
    	t.string	:group_name

    	t.string 	:service
    	t.string 	:appartment_number
    	t.time 		:start_time
    	t.time 		:end_time

    	t.date		:call_out_cost_date 
    	t.date		:call_out_cost_quantity
    	t.decimal	:call_out_cost_per_unit, :precision => 8, :scale => 2
    	t.date		:service_time_cost_date 
    	t.date		:service_time_cost_quantity
    	t.decimal	:service_time_cost_per_unit, :precision => 8, :scale => 2
    	t.date		:additional_cost_date 
    	t.date		:additional_cost_quantity 
    	t.decimal	:additional_cost_per_unit, :precision => 8, :scale => 2

    	t.string	:transfer_point_reading
    	t.string	:transfer_point_type
    	t.boolean	:transfer_point_changed
    	t.string	:transfer_point_new_type
		t.decimal	:transfer_point_cost_per_unit, :precision => 8, :scale => 2

    	t.string	:amplifier_reading
    	t.string	:amplifier_type
    	t.boolean	:amplifier_changed
    	t.string	:amplifier_new_type
		t.decimal	:amplifier_cost_per_unit, :precision => 8, :scale => 2

    	t.string	:mmd_reading
    	t.string	:mmd_type
    	t.boolean	:mmd_changed
    	t.string	:mmd_new_type
		t.decimal	:mmd_cost_per_unit, :precision => 8, :scale => 2

		t.string 	:material_description_1
		t.string 	:material_quantity_1
		t.decimal	:material_quantity_1_cost_per_unit, :precision => 8, :scale => 2
		
		t.string 	:material_description_2
		t.string 	:material_quantity_2
		t.decimal	:material_quantity_2_cost_per_unit, :precision => 8, :scale => 2

		t.string 	:material_description_3
		t.string 	:material_quantity_3
		t.decimal	:material_quantity_3_cost_per_unit, :precision => 8, :scale => 2

		t.string 	:material_description_4
		t.string 	:material_quantity_4
		t.decimal	:material_quantity_4_cost_per_unit, :precision => 8, :scale => 2

		t.string 	:material_description_5
		t.string 	:material_quantity_5
		t.decimal	:material_quantity_5_cost_per_unit, :precision => 8, :scale => 2

    	t.text		:additional_description
    	t.text		:signature
    	
    	t.string	:vp_number
    	t.string	:sap_order_number

    	t.integer	:client_id, :nil => false
        t.integer   :service_appointment_id
        t.integer   :service_job_id
      	t.timestamps
    end
  end
end
