class CreateSchedulingUserGroups < ActiveRecord::Migration
  def change
    create_table :scheduling_user_groups do |t|
		t.string 	:name
      	t.string	:sender_email
      	t.string	:bcc_email_receiver

	    t.timestamps
    end

    add_column :users, :scheduling_user_group_id, :integer
  end
end
