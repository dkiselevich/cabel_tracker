class AddTechnicianIdToServiceCompletions < ActiveRecord::Migration
  def change
  	add_column :service_completions, :technician_id, :integer
  end
end
