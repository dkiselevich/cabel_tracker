class AddModemtypToJob < ActiveRecord::Migration
  def self.up
    add_column :jobs, :modemtyp_id, :integer
  end

  def self.down
    remove_column :jobs, :modemtyp_id
  end
end
