class AddStatusToServiceJob < ActiveRecord::Migration
  def change
  	add_column :service_jobs, :status, :integer, :default => 0
  end
end
