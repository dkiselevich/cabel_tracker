class AddBeauftragteLeistungenToJobs < ActiveRecord::Migration
  def self.up
  	add_column :jobs, :beauftragte_leistungen, :text
  end

  def self.down
  	remove_column :jobs, :beauftragte_leistungen
  end
end
