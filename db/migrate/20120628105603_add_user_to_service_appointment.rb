class AddUserToServiceAppointment < ActiveRecord::Migration
  def change
  	add_column :service_appointments, :scheduling_user_id, :integer
  	add_column :service_appointments, :user_id, :integer
  	add_column :service_appointments, :high_priority, :boolean
  end
end
