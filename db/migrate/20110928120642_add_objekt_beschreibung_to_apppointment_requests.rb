class AddObjektBeschreibungToApppointmentRequests < ActiveRecord::Migration
  def self.up
  	add_column :appointment_requests, :objekt_beschreibung, :text
  end

  def self.down
  	remove_column :appointment_requests, :objekt_beschreibung
  end
end
