class AddSignatureToUser < ActiveRecord::Migration
  def change
  	add_column :users, :signature, :text
    add_column :users, :signature_photo_file_name, :string 
    add_column :users, :signature_photo_content_type, :string
    add_column :users, :signature_photo_file_size, :integer
    add_column :users, :signature_photo_updated_at, :datetime
  end
end
