class CreateDeliveryItems < ActiveRecord::Migration
  def change
    create_table :delivery_items do |t|
    	t.integer :delivery_id
    	t.integer :article_id
    	t.integer :quantity
    end
  end
end
