class CreateAssignments < ActiveRecord::Migration
  def change
    create_table :assignments do |t|
    	t.integer :user_id, :null => false
    	t.integer :client_id, :null => false

      	t.timestamps
    end
  end
end
