class AddEigentuemerHausnummerAndKundeIsEigentuemer < ActiveRecord::Migration
  def self.up
  	add_column :jobs, :eigentuemer_hausnummer, :string
  	add_column :jobs, :kunde_is_eigentuemer, :boolean
  end

  def self.down
  	remove_column :jobs, :eigentuemer_hausnummer
  	remove_column :jobs, :kunde_is_eigentuemer
  end
end
