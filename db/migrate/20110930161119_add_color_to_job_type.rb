class AddColorToJobType < ActiveRecord::Migration
  def self.up
  	add_column :job_types, :color ,:string
  end

  def self.down
  	remove_column :job_types, :color
  end
end
