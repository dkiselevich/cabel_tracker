class AddNewFieldsToCompletion < ActiveRecord::Migration
  def up
  	add_column :completions, :telefon_ok, :boolean, :default => false
  	add_column :completions, :internet_ok, :boolean, :default => false
  	add_column :completions, :fernsehempfang_ok, :boolean, :default => false
  	add_column :completions, :installationsort_sauber_verlassen, :boolean, :default => false

  	add_column :completions, :sbc_meldung_erfolgt, :boolean, :default => false
  	add_column :completions, :bemerkungen_kunde, :text
  end
  def down
  	remove_column :completions, :telefon_ok
  	remove_column :completions, :internet_ok
  	remove_column :completions, :fernsehempfang_ok
  	remove_column :completions, :installationsort_sauber_verlassen

  	remove_column :completions, :sbc_meldung_erfolgt
  	remove_column :completions, :bemerkungen_kunde
  end
end
