class CreateJobs < ActiveRecord::Migration
  def self.up
    create_table :jobs do |t|
      t.integer :mandant_id
      t.integer :auftragsart_id
      t.integer :workorder
      t.integer :kundennummer
      t.string :ot_auftragsnummer
      t.string :kunde_plz
      t.string :kunde_ort
      t.string :kunde_strasse
      t.string :kunde_hausnummer
      t.string :kunde_nachname
      t.string :kunde_vorname
      t.string :kunde_telefon_privat
      t.string :kunde_telefon_mobil
      t.string :kunde_telefon_geschaeftlich
      t.string :kunde_email
      t.string :telefon_neu
      t.string :eigentuemer_vorname
      t.string :eigentuemer_nachname
      t.string :eigentuemer_plz
      t.string :eigentuemer_ort
      t.string :eigentuemer_strasse
      t.string :eigentuemer_telefon_1
      t.string :eigentuemer_telefon_2
      t.string :eigentuemer_email
      t.string :eigentuemer_ansprechpartner
      t.string :eigentuemer_telefon_ansprechpartner
      t.string :rks_anlagennummer
      t.integer :objektart_id
      t.integer :wohneinheiten
      t.integer :aktive_kunden
      t.integer :netztopologie_id
      t.boolean :erstinstallation_erfolgt

      t.timestamps
    end
  end

  def self.down
    drop_table :jobs
  end
end
