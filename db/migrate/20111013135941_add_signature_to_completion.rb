class AddSignatureToCompletion < ActiveRecord::Migration
  def self.up
  	add_column :completions, :signature, :text
  end

  def self.down
  	remove_column :completions, :signature
  end
end
