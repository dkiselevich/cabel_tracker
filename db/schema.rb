# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20130823090219) do

  create_table "appointment_requests", :force => true do |t|
    t.integer  "technician_id"
    t.text     "hinderungsgrund"
    t.datetime "termin_datum"
    t.time     "zeitfenster_start"
    t.time     "zeitfenster_ende"
    t.integer  "scheduling_user_id"
    t.datetime "created_at",          :null => false
    t.datetime "updated_at",          :null => false
    t.integer  "appointment_id"
    t.text     "objekt_beschreibung"
  end

  create_table "appointments", :force => true do |t|
    t.integer  "job_id"
    t.text     "bemerkungen"
    t.datetime "created_at",                            :null => false
    t.datetime "updated_at",                            :null => false
    t.integer  "user_id"
    t.integer  "status",             :default => 0
    t.date     "day"
    t.time     "start_time"
    t.time     "end_time"
    t.integer  "scheduling_user_id"
    t.boolean  "high_priority",      :default => false
  end

  create_table "article_availabilities", :force => true do |t|
    t.integer  "supplier_id"
    t.integer  "article_id"
    t.decimal  "purchase_price",        :precision => 8, :scale => 2
    t.datetime "created_at",                                                         :null => false
    t.datetime "updated_at",                                                         :null => false
    t.integer  "delivery_time_in_days",                               :default => 0
    t.integer  "client_id"
  end

  create_table "article_groups", :force => true do |t|
    t.string   "name"
    t.integer  "type_id"
    t.boolean  "active"
    t.integer  "client_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "articles", :force => true do |t|
    t.string   "name"
    t.integer  "article_group_id"
    t.integer  "order_quantity"
    t.integer  "supply_to_technician_quantity"
    t.string   "description"
    t.decimal  "price1",                          :precision => 8, :scale => 2
    t.decimal  "price2",                          :precision => 8, :scale => 2
    t.decimal  "price3",                          :precision => 8, :scale => 2
    t.integer  "status_id"
    t.integer  "current_quantity_in_storage",                                   :default => 0
    t.integer  "minimum_quantity_in_storage",                                   :default => 0
    t.integer  "minimum_quantity_pro_technician",                               :default => 0
    t.datetime "created_at",                                                                   :null => false
    t.datetime "updated_at",                                                                   :null => false
    t.integer  "client_id"
    t.integer  "unit_id",                                                       :default => 1
  end

  create_table "assignments", :force => true do |t|
    t.integer  "user_id",    :null => false
    t.integer  "client_id",  :null => false
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "audits", :force => true do |t|
    t.integer  "auditable_id"
    t.string   "auditable_type"
    t.integer  "associated_id"
    t.string   "associated_type"
    t.integer  "user_id"
    t.string   "user_type"
    t.string   "username"
    t.string   "action"
    t.text     "audited_changes"
    t.integer  "version",         :default => 0
    t.string   "comment"
    t.string   "remote_address"
    t.datetime "created_at"
  end

  add_index "audits", ["associated_id", "associated_type"], :name => "associated_index"
  add_index "audits", ["auditable_id", "auditable_type"], :name => "auditable_index"
  add_index "audits", ["created_at"], :name => "index_audits_on_created_at"
  add_index "audits", ["user_id", "user_type"], :name => "user_index"

  create_table "automatic_workflow_items", :force => true do |t|
    t.integer  "client_id"
    t.integer  "workflow_action_id"
    t.time     "start_time"
    t.datetime "last_run_at"
    t.datetime "created_at",                           :null => false
    t.datetime "updated_at",                           :null => false
    t.boolean  "active",             :default => true
  end

  create_table "bugtrackers", :force => true do |t|
    t.string   "user"
    t.string   "email"
    t.string   "bug"
    t.string   "location"
    t.text     "description"
    t.boolean  "bug_solved"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "building_types", :force => true do |t|
    t.string   "name"
    t.datetime "created_at",                   :null => false
    t.datetime "updated_at",                   :null => false
    t.boolean  "active",     :default => true
  end

  create_table "cancellations", :force => true do |t|
    t.integer  "technician_id"
    t.integer  "anzahl_der_etagen"
    t.integer  "maximale_anzahl_HH_auf_etage"
    t.integer  "netztopologie_id"
    t.boolean  "mitversorgung"
    t.integer  "mitversorgung_art_id"
    t.boolean  "up_rohre_vorhanden_und_geeignet"
    t.integer  "up_rohre_tausch_hinderungsgrund_id"
    t.integer  "ap_verlegung_option_id"
    t.boolean  "stromeinspeisung_per_dach_moeglich"
    t.boolean  "pot_erstellung_moeglich"
    t.integer  "installation_verweigerer_id"
    t.string   "verweigerer_nachname"
    t.string   "verweigerer_vorname"
    t.string   "verweigerer_telefon"
    t.boolean  "eigentuemer_im_haus"
    t.string   "eigentuemer_nachname"
    t.string   "eigentuemer_vorname"
    t.string   "eigentuemer_telefon"
    t.integer  "verweigerungsgrund_id"
    t.text     "verweigerungsgrund_sonstiges_beschreibung"
    t.text     "zu_hoher_aufwand_beschreibung"
    t.text     "installationsbeschreibung"
    t.datetime "created_at",                                :null => false
    t.datetime "updated_at",                                :null => false
    t.integer  "appointment_id"
    t.integer  "cause_id"
    t.integer  "zu_hoher_aufwand_id"
    t.integer  "scheduling_user_id"
  end

  create_table "clients", :force => true do |t|
    t.string   "name"
    t.datetime "created_at",                                                                       :null => false
    t.datetime "updated_at",                                                                       :null => false
    t.boolean  "active",                                                        :default => true
    t.string   "ot_login"
    t.string   "ot_password"
    t.boolean  "automatic_email_to_technician_on_appointment_change_to_client", :default => false
    t.string   "logo_img_file_name"
    t.string   "logo_img_content_type"
    t.integer  "logo_img_file_size"
    t.datetime "logo_img_updated_at"
  end

  create_table "comments", :force => true do |t|
    t.integer  "job_id"
    t.integer  "user_id"
    t.text     "body"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "completion_photos", :force => true do |t|
    t.string   "photo_file_name"
    t.string   "photo_content_type"
    t.integer  "photo_file_size"
    t.datetime "photo_updated_at"
    t.datetime "created_at",         :null => false
    t.datetime "updated_at",         :null => false
  end

  create_table "completions", :force => true do |t|
    t.integer  "technician_id"
    t.boolean  "clever_pro"
    t.boolean  "unbundled"
    t.integer  "modemtyp_id"
    t.boolean  "umzug"
    t.integer  "installationsart_id"
    t.string   "installationsart_sonstiges"
    t.integer  "fi_sicherung_id"
    t.integer  "potentialausgleich_id"
    t.integer  "netztopologie_id"
    t.boolean  "geschlossener_bvt"
    t.boolean  "klasse_4c"
    t.integer  "linienverstaerker_id"
    t.boolean  "mehraufwand"
    t.integer  "auftragsart_id"
    t.integer  "dosentausch"
    t.boolean  "kellerquerverkabelung"
    t.integer  "kellerquerverkabelung_laenge"
    t.decimal  "D130",                                               :precision => 5, :scale => 2
    t.decimal  "K5",                                                 :precision => 5, :scale => 2
    t.decimal  "K29",                                                :precision => 5, :scale => 2
    t.decimal  "D802",                                               :precision => 5, :scale => 2
    t.string   "HFC_MAC"
    t.boolean  "adresspruefung"
    t.text     "besonderheiten"
    t.boolean  "uebrige_modems_im_gebaeude_per_SBC_portal_geprueft"
    t.boolean  "tv_empfang_beim_kunden_erfolgreich_geprueft"
    t.boolean  "internetzugang_mit_montagelaptop_geprueft"
    t.boolean  "telefon_abgehend_erfolgreich_getestet"
    t.boolean  "telefon_ankommend_erfolgreich_getestet"
    t.text     "beauftragte_leistungen"
    t.datetime "created_at",                                                                                          :null => false
    t.datetime "updated_at",                                                                                          :null => false
    t.integer  "appointment_id"
    t.string   "vorverstaerker_bezeichnung"
    t.integer  "vorverstaerker_id"
    t.string   "bvt_photo_file_name"
    t.string   "bvt_photo_content_type"
    t.integer  "bvt_photo_file_size"
    t.datetime "bvt_photo_updated_at"
    t.string   "uep_photo_file_name"
    t.string   "uep_photo_content_type"
    t.integer  "uep_photo_file_size"
    t.datetime "uep_photo_updated_at"
    t.string   "pot_schiene_photo_file_name"
    t.string   "pot_schiene_photo_content_type"
    t.integer  "pot_schiene_photo_file_size"
    t.datetime "pot_schiene_photo_updated_at"
    t.string   "pot_gas_photo_file_name"
    t.string   "pot_gas_photo_content_type"
    t.integer  "pot_gas_photo_file_size"
    t.datetime "pot_gas_photo_updated_at"
    t.string   "pot_wasser_photo_file_name"
    t.string   "pot_wasser_photo_content_type"
    t.integer  "pot_wasser_photo_file_size"
    t.datetime "pot_wasser_photo_updated_at"
    t.string   "pot_hak_photo_file_name"
    t.string   "pot_hak_photo_content_type"
    t.integer  "pot_hak_photo_file_size"
    t.datetime "pot_hak_photo_updated_at"
    t.string   "pot_heizung_photo_file_name"
    t.string   "pot_heizung_photo_content_type"
    t.integer  "pot_heizung_photo_file_size"
    t.datetime "pot_heizung_photo_updated_at"
    t.string   "pot_oel_photo_file_name"
    t.string   "pot_oel_photo_content_type"
    t.integer  "pot_oel_photo_file_size"
    t.datetime "pot_oel_photo_updated_at"
    t.string   "mmd_unbundled_photo_file_name"
    t.string   "mmd_unbundled_photo_content_type"
    t.integer  "mmd_unbundled_photo_file_size"
    t.datetime "mmd_unbundled_photo_updated_at"
    t.string   "messwerte_photo_file_name"
    t.string   "messwerte_photo_content_type"
    t.integer  "messwerte_photo_file_size"
    t.datetime "messwerte_photo_updated_at"
    t.text     "signature"
    t.integer  "provisionierung_id"
    t.integer  "wohneinheiten"
    t.integer  "scheduling_user_id"
    t.string   "signature_photo_file_name"
    t.string   "signature_photo_content_type"
    t.integer  "signature_photo_file_size"
    t.datetime "signature_photo_updated_at"
    t.boolean  "provisionierung"
    t.boolean  "no_access_to_bvt",                                                                 :default => false
    t.boolean  "no_access_to_uep",                                                                 :default => false
    t.boolean  "no_access_to_pot_schiene",                                                         :default => false
    t.string   "additional_attachment_1_file_name"
    t.string   "additional_attachment_2_file_name"
    t.string   "additional_attachment_3_file_name"
    t.string   "additional_attachment_4_file_name"
    t.string   "additional_attachment_5_file_name"
    t.string   "additional_attachment_6_file_name"
    t.string   "additional_attachment_7_file_name"
    t.string   "additional_attachment_8_file_name"
    t.string   "additional_attachment_9_file_name"
    t.string   "additional_attachment_10_file_name"
    t.string   "additional_attachment_1_description"
    t.string   "additional_attachment_2_description"
    t.string   "additional_attachment_3_description"
    t.string   "additional_attachment_4_description"
    t.string   "additional_attachment_5_description"
    t.string   "additional_attachment_6_description"
    t.string   "additional_attachment_7_description"
    t.string   "additional_attachment_8_description"
    t.string   "additional_attachment_9_description"
    t.string   "additional_attachment_10_description"
    t.boolean  "no_access_to_pot_gas",                                                             :default => false
    t.boolean  "no_access_to_pot_hak",                                                             :default => false
    t.boolean  "no_access_to_pot_wasser",                                                          :default => false
    t.boolean  "no_access_to_pot_heizung",                                                         :default => false
    t.boolean  "no_access_to_pot_oel",                                                             :default => false
    t.boolean  "telefon_ok",                                                                       :default => false
    t.boolean  "internet_ok",                                                                      :default => false
    t.boolean  "fernsehempfang_ok",                                                                :default => false
    t.boolean  "installationsort_sauber_verlassen",                                                :default => false
    t.boolean  "sbc_meldung_erfolgt",                                                              :default => false
    t.text     "bemerkungen_kunde"
  end

  create_table "days_of_absence", :force => true do |t|
    t.integer  "user_id"
    t.datetime "starting_point"
    t.datetime "end_point"
    t.integer  "cause_id"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
  end

  create_table "delayed_jobs", :force => true do |t|
    t.integer  "priority",   :default => 0
    t.integer  "attempts",   :default => 0
    t.text     "handler"
    t.text     "last_error"
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string   "locked_by"
    t.string   "queue"
    t.datetime "created_at",                :null => false
    t.datetime "updated_at",                :null => false
  end

  add_index "delayed_jobs", ["priority", "run_at"], :name => "delayed_jobs_priority"

  create_table "deliveries", :force => true do |t|
    t.integer  "type_id"
    t.integer  "technician_id"
    t.integer  "supplier_id"
    t.integer  "status_id",     :default => 0
    t.integer  "client_id"
    t.datetime "created_at",                   :null => false
    t.datetime "updated_at",                   :null => false
  end

  create_table "delivery_items", :force => true do |t|
    t.integer "delivery_id"
    t.integer "article_id"
    t.integer "quantity"
  end

  create_table "equipment", :force => true do |t|
    t.integer  "user_id"
    t.integer  "article_id"
    t.integer  "quantity"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "fi_fuse_types", :force => true do |t|
    t.string   "name"
    t.boolean  "active",     :default => true
    t.datetime "created_at",                   :null => false
    t.datetime "updated_at",                   :null => false
  end

  create_table "job_types", :force => true do |t|
    t.string   "name"
    t.string   "shortname"
    t.datetime "created_at",                   :null => false
    t.datetime "updated_at",                   :null => false
    t.boolean  "active",     :default => true
    t.string   "color"
  end

  create_table "jobs", :force => true do |t|
    t.integer  "client_id"
    t.integer  "auftragsart_id"
    t.string   "workorder"
    t.string   "ot_auftragsnummer"
    t.string   "kunde_plz"
    t.string   "kunde_ort"
    t.string   "kunde_strasse"
    t.string   "kunde_hausnummer"
    t.string   "kunde_nachname"
    t.string   "kunde_vorname"
    t.string   "kunde_telefon_privat"
    t.string   "kunde_telefon_mobil"
    t.string   "kunde_telefon_geschaeftlich"
    t.string   "kunde_email"
    t.string   "telefon_neu"
    t.string   "eigentuemer_vorname"
    t.string   "eigentuemer_nachname"
    t.string   "eigentuemer_plz"
    t.string   "eigentuemer_ort"
    t.string   "eigentuemer_strasse"
    t.string   "eigentuemer_telefon_1"
    t.string   "eigentuemer_telefon_2"
    t.string   "eigentuemer_email"
    t.string   "eigentuemer_ansprechpartner"
    t.string   "eigentuemer_telefon_ansprechpartner"
    t.string   "rks_anlagennummer"
    t.integer  "objektart_id"
    t.integer  "wohneinheiten"
    t.integer  "aktive_kunden"
    t.integer  "netztopologie_id"
    t.boolean  "erstinstallation_erfolgt"
    t.datetime "created_at",                                             :null => false
    t.datetime "updated_at",                                             :null => false
    t.integer  "status",                              :default => 0
    t.string   "kunde_kundennummer"
    t.integer  "wartegrund_id"
    t.text     "logs",                                :default => ""
    t.string   "eigentuemer_hausnummer"
    t.boolean  "kunde_is_eigentuemer"
    t.integer  "modemtyp_id"
    t.date     "modemaktivierung"
    t.text     "beauftragte_leistungen"
    t.date     "verbindlicher_installationstermin"
    t.string   "ot_id"
    t.string   "verbindliche_tageszeit"
    t.integer  "uep_id"
    t.boolean  "closed",                              :default => false
    t.integer  "scheduling_user_group_id"
    t.boolean  "clerver_pro"
    t.boolean  "unbunled"
    t.date     "wiedervorlage"
    t.text     "ot_kommentare"
  end

  create_table "logs", :force => true do |t|
    t.text     "text"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.integer  "table_id"
    t.string   "tbl"
  end

  create_table "modem_types", :force => true do |t|
    t.string   "name"
    t.boolean  "active",     :default => true
    t.datetime "created_at",                   :null => false
    t.datetime "updated_at",                   :null => false
  end

  create_table "network_types", :force => true do |t|
    t.string   "name"
    t.datetime "created_at",                   :null => false
    t.datetime "updated_at",                   :null => false
    t.boolean  "active",     :default => true
  end

  create_table "photos", :force => true do |t|
    t.string   "photo_file_name"
    t.string   "photo_content_type"
    t.integer  "photo_file_size"
    t.datetime "photo_updated_at"
    t.datetime "created_at",         :null => false
    t.datetime "updated_at",         :null => false
    t.integer  "cancellation_id"
  end

  create_table "postal_codes", :force => true do |t|
    t.string   "code"
    t.datetime "created_at",               :null => false
    t.datetime "updated_at",               :null => false
    t.integer  "scheduling_user_group_id"
  end

  create_table "postal_codes_service_user_groups", :id => false, :force => true do |t|
    t.integer "service_user_group_id"
    t.integer "postal_code_id"
  end

  create_table "purchase_storages", :force => true do |t|
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "quality_reports", :force => true do |t|
    t.text     "planned_measures_next_two_month"
    t.text     "detected_quality_problems"
    t.text     "identified_necessary_measures"
    t.text     "launch_plannings"
    t.text     "implemented_measures"
    t.text     "sustainability_control"
    t.date     "pdf_created"
    t.integer  "user_id"
    t.datetime "created_at",                      :null => false
    t.datetime "updated_at",                      :null => false
  end

  create_table "scheduling_user_groups", :force => true do |t|
    t.string   "name"
    t.string   "sender_email"
    t.string   "bcc_email_receiver"
    t.datetime "created_at",         :null => false
    t.datetime "updated_at",         :null => false
    t.integer  "client_id"
  end

  create_table "service_appointments", :force => true do |t|
    t.integer  "service_job_id"
    t.date     "day"
    t.text     "bemerkungen"
    t.datetime "created_at",         :null => false
    t.datetime "updated_at",         :null => false
    t.integer  "scheduling_user_id"
    t.integer  "user_id"
    t.boolean  "high_priority"
    t.time     "start_time"
    t.time     "end_time"
  end

  create_table "service_comments", :force => true do |t|
    t.integer  "service_job_id"
    t.integer  "user_id"
    t.text     "body"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
  end

  create_table "service_completions", :force => true do |t|
    t.integer  "service_type_id"
    t.integer  "invoice_option_id"
    t.string   "debitor_sap_number"
    t.integer  "customer_type_id"
    t.string   "customer_first_name"
    t.string   "customer_last_name"
    t.string   "customer_street_name"
    t.string   "customer_house_number"
    t.string   "customer_postal_code"
    t.string   "customer_city"
    t.integer  "invoice_recipient_type_id"
    t.string   "invoice_recipient_first_name"
    t.string   "invoice_recipient_last_name"
    t.string   "invoice_recipient_street_name"
    t.string   "invoice_recipient_house_number"
    t.string   "invoice_recipient_postal_code"
    t.string   "invoice_recipient_city"
    t.date     "date"
    t.string   "order_number"
    t.string   "customer_number"
    t.string   "sk_order_number"
    t.string   "group_name"
    t.string   "service"
    t.string   "appartment_number"
    t.time     "start_time"
    t.time     "end_time"
    t.date     "call_out_cost_date"
    t.date     "service_time_cost_date"
    t.date     "additional_cost_date"
    t.string   "transfer_point_reading"
    t.string   "transfer_point_type"
    t.boolean  "transfer_point_changed"
    t.string   "transfer_point_new_type"
    t.string   "amplifier_reading"
    t.string   "amplifier_type"
    t.boolean  "amplifier_changed"
    t.string   "amplifier_new_type"
    t.string   "mmd_reading"
    t.string   "mmd_type"
    t.boolean  "mmd_changed"
    t.string   "mmd_new_type"
    t.string   "material_description_1"
    t.string   "material_description_2"
    t.string   "material_description_3"
    t.string   "material_description_4"
    t.string   "material_description_5"
    t.text     "additional_description"
    t.text     "signature"
    t.string   "vp_number"
    t.string   "sap_order_number"
    t.integer  "client_id"
    t.integer  "service_appointment_id"
    t.integer  "service_job_id"
    t.datetime "created_at",                                                                          :null => false
    t.datetime "updated_at",                                                                          :null => false
    t.integer  "technician_id"
    t.decimal  "call_out_cost_per_unit",               :precision => 8, :scale => 2, :default => 0.0
    t.decimal  "service_time_cost_per_unit",           :precision => 8, :scale => 2, :default => 0.0
    t.decimal  "additional_cost_per_unit",             :precision => 8, :scale => 2, :default => 0.0
    t.decimal  "transfer_point_cost_per_unit",         :precision => 8, :scale => 2, :default => 0.0
    t.decimal  "amplifier_cost_per_unit",              :precision => 8, :scale => 2, :default => 0.0
    t.decimal  "mmd_cost_per_unit",                    :precision => 8, :scale => 2, :default => 0.0
    t.decimal  "material_quantity_1_cost_per_unit",    :precision => 8, :scale => 2, :default => 0.0
    t.decimal  "material_quantity_2_cost_per_unit",    :precision => 8, :scale => 2, :default => 0.0
    t.decimal  "material_quantity_3_cost_per_unit",    :precision => 8, :scale => 2, :default => 0.0
    t.decimal  "material_quantity_4_cost_per_unit",    :precision => 8, :scale => 2, :default => 0.0
    t.decimal  "material_quantity_5_cost_per_unit",    :precision => 8, :scale => 2, :default => 0.0
    t.decimal  "call_out_cost_quantity",               :precision => 8, :scale => 2, :default => 0.0
    t.decimal  "service_time_cost_quantity",           :precision => 8, :scale => 2, :default => 0.0
    t.decimal  "additional_cost_quantity",             :precision => 8, :scale => 2, :default => 0.0
    t.decimal  "material_quantity_1",                  :precision => 8, :scale => 2, :default => 0.0
    t.decimal  "material_quantity_2",                  :precision => 8, :scale => 2, :default => 0.0
    t.decimal  "material_quantity_3",                  :precision => 8, :scale => 2, :default => 0.0
    t.decimal  "material_quantity_4",                  :precision => 8, :scale => 2, :default => 0.0
    t.decimal  "material_quantity_5",                  :precision => 8, :scale => 2, :default => 0.0
    t.string   "additional_attachment_1_file_name"
    t.string   "additional_attachment_2_file_name"
    t.string   "additional_attachment_3_file_name"
    t.string   "additional_attachment_4_file_name"
    t.string   "additional_attachment_5_file_name"
    t.string   "additional_attachment_6_file_name"
    t.string   "additional_attachment_7_file_name"
    t.string   "additional_attachment_8_file_name"
    t.string   "additional_attachment_9_file_name"
    t.string   "additional_attachment_10_file_name"
    t.string   "additional_attachment_1_description"
    t.string   "additional_attachment_2_description"
    t.string   "additional_attachment_3_description"
    t.string   "additional_attachment_4_description"
    t.string   "additional_attachment_5_description"
    t.string   "additional_attachment_6_description"
    t.string   "additional_attachment_7_description"
    t.string   "additional_attachment_8_description"
    t.string   "additional_attachment_9_description"
    t.string   "additional_attachment_10_description"
  end

  create_table "service_jobs", :force => true do |t|
    t.string   "ot_id"
    t.integer  "client_id"
    t.string   "ot_auftragsnummer"
    t.string   "wirktiefe"
    t.string   "nachname_des_anrufers"
    t.string   "telefonnummer_des_anrufers"
    t.string   "auftragsklasse"
    t.string   "zustand"
    t.string   "vorname_des_anrufers"
    t.string   "email_des_anrufers"
    t.string   "kunde_telefon_privat"
    t.string   "kunde_telefon_mobil"
    t.string   "kunde_telefon_geschaeftlich"
    t.string   "kunde_email"
    t.string   "kunde_plz"
    t.string   "kunde_strasse"
    t.string   "kunde_ort"
    t.string   "kunde_hausnummer"
    t.string   "uep_string"
    t.string   "uep_zustand"
    t.string   "uebk"
    t.string   "asb"
    t.string   "onkz"
    t.string   "we_angeschlossen"
    t.string   "we_max"
    t.string   "uep_zustand_klartext"
    t.string   "grussz"
    t.string   "vrp"
    t.string   "c_linie"
    t.string   "versorgte_objekt"
    t.string   "inbetriebnahme"
    t.string   "ad_zeitfenster_fruehester_beginn"
    t.string   "e_vsip_kundentermin"
    t.string   "vereinbarter_kundentermin"
    t.string   "ad_zeitfenster_spaetester_beginn"
    t.string   "techniker"
    t.string   "textbaustein"
    t.string   "rx"
    t.string   "tx"
    t.string   "bandbreite_downstream"
    t.string   "rufnummer_1"
    t.string   "modemtyp"
    t.string   "mahnlauf"
    t.string   "mac_adresse"
    t.string   "snr"
    t.string   "bandbreite_upstream"
    t.string   "rufnummer_2"
    t.string   "dienstmerkmalpaket"
    t.string   "wartegrund_beschreibung"
    t.string   "wiedervorlage"
    t.string   "netzebene"
    t.string   "stoerungswirkbreite"
    t.string   "abschlussgrund"
    t.string   "abschlussgrund_beschreibung"
    t.string   "stoerungsende_aus_kundensicht"
    t.string   "systemabschluss"
    t.string   "leistungsnachweis_vorhanden"
    t.datetime "created_at",                                      :null => false
    t.datetime "updated_at",                                      :null => false
    t.integer  "technician_id"
    t.integer  "status",                           :default => 0
    t.text     "beschreibung"
    t.string   "kunde_nachname"
    t.string   "kunde_vorname"
    t.string   "kunde_kundennummer"
    t.integer  "scheduling_user_group_id"
  end

  create_table "service_user_groups", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.integer  "client_id"
  end

  create_table "service_user_groups_users", :id => false, :force => true do |t|
    t.integer "service_user_group_id"
    t.integer "user_id"
  end

  create_table "special_expenses", :force => true do |t|
    t.integer  "appointment_id"
    t.string   "name"
    t.decimal  "costs",          :precision => 8, :scale => 2
    t.decimal  "factor",         :precision => 8, :scale => 2
    t.datetime "created_at",                                   :null => false
    t.datetime "updated_at",                                   :null => false
  end

  create_table "suppliers", :force => true do |t|
    t.string   "name"
    t.string   "contact_first_name"
    t.string   "contact_last_name"
    t.string   "street"
    t.string   "house_number"
    t.string   "postal_code"
    t.string   "city"
    t.string   "phone"
    t.string   "fax"
    t.string   "email"
    t.string   "order_portal"
    t.datetime "created_at",         :null => false
    t.datetime "updated_at",         :null => false
    t.integer  "client_id"
  end

  create_table "transfer_points", :force => true do |t|
    t.string   "uid"
    t.string   "ot_id"
    t.string   "plz"
    t.string   "ort"
    t.string   "strasse"
    t.string   "hausnummer"
    t.string   "zustand"
    t.string   "typ"
    t.string   "dienstverfuegbarkeit"
    t.integer  "we_am_uep"
    t.integer  "aktive_kunden_am_uep"
    t.date     "inbetriebnahme"
    t.datetime "created_at",           :null => false
    t.datetime "updated_at",           :null => false
    t.boolean  "mini_fibre_node"
  end

  create_table "users", :force => true do |t|
    t.string   "email",                                        :default => "",    :null => false
    t.string   "encrypted_password",            :limit => 128, :default => "",    :null => false
    t.string   "password_salt",                                :default => "",    :null => false
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "reset_password_token"
    t.string   "remember_token"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                                :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "vorname"
    t.string   "nachname"
    t.string   "plz"
    t.string   "ort"
    t.string   "strasse"
    t.string   "hausnummer"
    t.string   "telefon"
    t.string   "telefon_mobil"
    t.datetime "created_at",                                                      :null => false
    t.datetime "updated_at",                                                      :null => false
    t.boolean  "is_boss"
    t.boolean  "is_controller"
    t.boolean  "is_technician"
    t.boolean  "is_accountant"
    t.boolean  "is_admin"
    t.boolean  "active",                                       :default => true
    t.text     "signature"
    t.string   "signature_photo_file_name"
    t.string   "signature_photo_content_type"
    t.integer  "signature_photo_file_size"
    t.datetime "signature_photo_updated_at"
    t.boolean  "is_controller_leader"
    t.boolean  "is_purchase_manager",                          :default => false
    t.integer  "scheduling_user_group_id"
    t.string   "tabledibkom_zertifikat_nummer"
    t.time     "monday_work_start"
    t.time     "monday_lunch_break_start"
    t.time     "monday_lunch_break_end"
    t.time     "monday_work_end"
    t.time     "tuesday_work_start"
    t.time     "tuesday_lunch_break_start"
    t.time     "tuesday_lunch_break_end"
    t.time     "tuesday_work_end"
    t.time     "wednesday_work_start"
    t.time     "wednesday_lunch_break_start"
    t.time     "wednesday_lunch_break_end"
    t.time     "wednesday_work_end"
    t.time     "thursday_work_start"
    t.time     "thursday_lunch_break_start"
    t.time     "thursday_lunch_break_end"
    t.time     "thursday_work_end"
    t.time     "friday_work_start"
    t.time     "friday_lunch_break_start"
    t.time     "friday_lunch_break_end"
    t.time     "friday_work_end"
    t.time     "saturday_work_start"
    t.time     "saturday_lunch_break_start"
    t.time     "saturday_lunch_break_end"
    t.time     "saturday_work_end"
    t.time     "sunday_work_start"
    t.time     "sunday_lunch_break_start"
    t.time     "sunday_lunch_break_end"
    t.time     "sunday_work_end"
    t.string   "authentication_token"
  end

  add_index "users", ["email"], :name => "index_users_on_email", :unique => true
  add_index "users", ["reset_password_token"], :name => "index_users_on_reset_password_token", :unique => true

  create_table "waiting_causes", :force => true do |t|
    t.string   "name"
    t.datetime "created_at",                   :null => false
    t.datetime "updated_at",                   :null => false
    t.boolean  "active",     :default => true
    t.string   "ot_id"
    t.string   "code"
  end

end
