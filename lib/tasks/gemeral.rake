# encoding: utf-8

desc "create necessary database entries"
task :init_db => :environment do
	puts "attempting to create necessary database entries"
	DatabaseHelper.reset_hard
	puts "done."
end
