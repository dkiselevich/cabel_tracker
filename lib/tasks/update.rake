# encoding: utf-8
desc "This task is called by the Heroku cron add-on"
task :cron => :environment do
	#if Time.now.day % 7 == 0 # run every week
	OtHelper.get_jobs(Settings.MAX_NUMBER_OF_JOBS_TO_GET)
	#end
end

task [:set_job_to_di, :id, :dt, :note] => :environment do |t, args|
	id = args[:id]
	dt = args[:dt]
	note = args[:note]
  	if id.blank? ||  dt.blank? || note.blank? then
  		puts "ungenügende Parameter"
  	else
		OtHelper.set_state_to_di(id, DateTime.parse(dt), note)
	end
end

task [:set_job_to_wk, :id, :dt, :note] => :environment do |t, args|
	id = args[:id]
	dt = args[:dt]
	note = args[:note]
  	if id.blank? ||  dt.blank? || note.blank? then
  		puts "ungenügende Parameter"
  	else
		OtHelper.set_state_to_wk(id, DateTime.parse(dt), note)
	end
end

task [:get_jobs] => :environment do
	OtHelper.get_jobs(Settings.MAX_NUMBER_OF_JOBS_TO_GET)
end

task [:get_job_info, :id] => :environment do |t, args|
	id = args[:id]
	if id.blank? then
		puts "Keine ID angegeben :("
	else
  		OtHelper.get_job_info(id)
  	end
end

task [:get_uep_info, :id] => :environment do |t, args|
	id = args[:id]
	if id.blank? then
		puts "Keine ID angegeben :("
	else
  		OtHelper.get_uep_info(id)
  	end
end

task [:add_job_from_ot, :id, :force_save] => :environment do |t, args|
	id = args[:id]
	force_save = args[:force_save] || false
	
	if id.blank? then
		puts "Keine ID angegeben :("
	else
  		OtHelper.add_job_from_ot(id,force_save)
  	end
end

task [:add_uep_from_ot, :id, :force_save] => :environment do |t, args|
	id = args[:id]
	force_save = args[:force_save] || false
	
	if id.blank? then
		puts "Keine ID angegeben :("
	else
  		OtHelper.add_uep_from_ot(id)
  	end
end

task [:fad_test, :id] => :environment do |t, args|
	id = args[:id]
	if id.blank? then
		puts "Keine ID angegeben :("
	else
 		puts id.to_s
 	end
end
 

desc "Import xml data for jobs table"
task :add_job, [:xml_name, :client_id] => :environment do |t, args|
  client_id = args[:client_id]
  xml_name = args[:xml_name]
  if client_id.blank? || client_id.nil?
    puts "Input the client_id"
  else
    ImportHelper.add_job(xml_name, client_id)
  end
end

desc "Import some xml files data for jobs table"
task :add_xml_files_to_job, [:client_id] => :environment do |t, args|
  client_id = args[:client_id]
  if client_id.blank? || client_id.nil?
    puts "Input the client_id"
  else
    ImportHelper.add_xml_files_to_job(client_id)
  end
end
