# encoding: utf-8

desc "import Jobs from OT"
task :import_from_ot => :environment do
	puts "attempting to import new jobs and update existing jobs..."
	client = Client.where(:name => "3kabel")
	scheduling_user = User.where(:is_controller => true)[0]
	puts "for client: #{client.name}, scheduling_user: #{scheduling_user.full_name}"

	begin
		puts "attempting to update TA 'Installationsaufträge'"
		OtHelper.get_ta_jobs(client, scheduling_user)
		puts "done."
	rescue Exception => e  
	  puts e.message  
	  puts e.backtrace.inspect  
	end  

	begin
		puts "attempting to import 'Installationsaufträge'"
		OtHelper.get_zde_jobs(client, scheduling_user)
		puts "done."
	rescue Exception => e  
	  puts e.message  
	  puts e.backtrace.inspect  
	end  

	begin
		puts "attempting to import 'Entstörungsaufträge'"
		OtHelper.get_zde_service_jobs(client, scheduling_user)
		puts "done."
	rescue Exception => e  
	  puts e.message  
	  puts e.backtrace.inspect  
	end  
	
	puts "all imports and updates done."
end


desc "update signatures - not sure if currently needed because of hirefireapp"
task :update_signature_images => :environment do
	puts "updating signatures..."
	Job.where(:status => 3).each{|j|j.completion.create_signature(j.completion.signature, j.ot_auftragsnummer)}
	puts "done."
end

desc "check for automated task and run them"
task :process_automatic_workflow_items => :environment do
	puts "checking for tasks..."
	AutomaticWorkflowItem.all.each do |item|
		if (item.active) then
		if (item.start_time < Time.now) then
			if (item.last_run_at == nil) || (item.last_run_at.to_date < Date.today.to_date) then
				puts item.start_time
				puts Time.now
				
				if item.workflow_action_id == 1 then
					puts "SENDING MAIL FOR NEWEST APPOINTMENT"
					TaskHelper.send_test_email()
				end

				if item.workflow_action_id == 2 then
					puts "Email mit Terminen an alle Techniker" 	
					TaskHelper.send_pdf_with_appointments_for_tomorrow_to_all_technicians(item.client)
				end

				if item.workflow_action_id == 3 then
					puts "Emails pro Termin mit Auftragsabschlussformular an alle Techniker" 
					TaskHelper.send_emails_with_blank_completion_pdf_for_each_appointment_tomorrow_to_all_technicians(item.client)
				end

				if item.workflow_action_id == 4 then
					item.client.automatic_email_to_technician_on_appointment_change = true
					item.client.save
				end

				if item.workflow_action_id == 5 then
					item.client.automatic_email_to_technician_on_appointment_change = false
					item.client.save
				end

				item.last_run_at = Time.now
				item.save
			end
		end
	end
end
	puts "done."
end


desc "send all emails to technicians of all clients for tomorrow"
task :send_all_emails_for_tomorrow => :environment do
	Client.all.each do |c|
		TaskHelper.send_emails_with_blank_completion_pdf_for_each_appointment_tomorrow_to_all_technicians(c)
	end
end