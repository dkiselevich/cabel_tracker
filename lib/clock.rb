# encoding: utf-8
require File.expand_path('../../config/boot',        __FILE__)
require File.expand_path('../../config/environment', __FILE__)
require 'clockwork'

include Clockwork

# handler do |job|
  # puts "Running #{job}"
# end


every(20.minutes, 'Neue Installationsauftraege werden importiert ...'){
  Delayed::Job.enqueue(
    OtHelper.get_zde_jobs(Client.last, User.find_by_email("james@arkavis.com"))   
  ) 
}

every(20.minutes, 'Neue Serviceauftraege werden importiert ...'){
  Delayed::Job.enqueue(
    OtHelper.get_zde_service_jobs(Client.last, User.find_by_email("james@arkavis.com")) 
  )  
}

every(20.minutes, 'Job Status werden upgedated ...'){
  Delayed::Job.enqueue(
    OtHelper.update_zdE_jobs
  )
}


# every(1.day, 'PDF mit allen Terminen + vorausgefüllte Auftragsabschluss PDF pro Termin wird an die jeweiligen Techniker zugesendet.', :at => '17:30', :tz => 'UTC+07:00'){
  # TaskHelper.send_pdf_with_appointments_for_tomorrow_to_all_technicians(Client.last)
  # TaskHelper.send_emails_with_blank_completion_pdf_for_each_appointment_tomorrow_to_all_technicians(Client.last)  
# }

#every(1.day, 'TESTEN.', :at => ['9:00', '4:50', '5:00', '5:10', '5:20', '5:30']){
every(1.day, 'PDF mit allen Terminen wird an die jeweiligen Techniker zugesendet.', :at => '14:00'){ # 16:00 deutsche Zeit
  Delayed::Job.enqueue(
    TaskHelper.send_emails_with_blank_completion_pdf_for_each_appointment_tomorrow_to_all_technicians(Client.last)  
  )
}

every(1.day, 'Vorausgefuellte PDF Auftragsabschlüsse wird an die jeweiligen Techniker zugesendet.', :at => '14:02'){ # 16:02 deutsche Zeit
  Delayed::Job.enqueue(
    TaskHelper.send_pdf_with_appointments_for_tomorrow_to_all_technicians(Client.last)
  )  
}

# every(2.minutes, 'Queueing scheduled job'){  
  # User.where(:id => 2).each do |technician|
    # user_appointments = Appointment.where(:user_id => technician)     
    # Delayed::Job.enqueue(AppointmentsMailer.tomorrow_appointments_forms(technician, technician, user_appointments))  
  # end
# }

# every(1.day, 'Queueing scheduled job', :at => '11:00'){
  # AppointmentsMailer.test_mail.deliver
# }
