#!/usr/bin/env sh

echo "starting dev server"
term.sh -t rails s

echo "starting dev console"
term.sh -t rails c

echo "starting memcached"
term.sh -t memcached -vv

echo "starting sublime2"
subl .
