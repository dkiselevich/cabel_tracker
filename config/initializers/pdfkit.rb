# encoding: utf-8
# PDFKit.configure do |config|
# config.wkhtmltopdf = '/usr/local/Cellar/wkhtmltopdf/0.9.9/bin/wkhtmltopdf'
# config.wkhtmltopdf = Rails.root.join('bin', 'wkhtmltopdf-amd64').to_s if Rails.env.production?
#
# config.default_options = {
# :page_size => 'A4',
# :print_media_type => true
# }
# # config.root_url = "http://localhost" # Use only if your external hostname is unavailable on the server.
# end

PDFKit.configure do |config|
   config.wkhtmltopdf = "/usr/local/bin/wkhtmltopdf"
   #config.wkhtmltopdf = "C:/Program Files (x86)/wkhtmltopdf/wkhtmltopdf.exe" if RUBY_PLATFORM == "win32"
   config.wkhtmltopdf = "C:/wkhtmltopdf/wkhtmltopdf.exe" if RUBY_PLATFORM == "win32"
   config.wkhtmltopdf = "C:/wkhtmltopdf/wkhtmltopdf.exe" if RUBY_PLATFORM == "i386-mingw32"
   config.wkhtmltopdf = Rails.root.join('bin', 'wkhtmltopdf-amd64').to_s if Rails.env.production?
   config.default_options = {
     :encoding=>"UTF-8",
     :footer_font_size => '9',
     :footer_line => true,
     :footer_left => "© #{Date.today.year} 3Kabel GmbH",
     :footer_right => "Erstellt am: [date], [time] Uhr",
     :footer_center => "Seite [frompage] von [topage]",
     :page_size => 'A4',
     :print_media_type => true,
    
   } 
   config.root_url = "http://localhost" # Use only if your external hostname is unavailable on the server.
end
