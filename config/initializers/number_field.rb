ActionView::Helpers::FormBuilder.class_eval do

  def number_field(field, options = {})
  	if (!object.blank?) then
	    value = object.send(field).to_s                   
    	value.gsub!('.', ',') unless value.blank?  
    	options[:value] = value
    	options[:class] = "#{options[:class]} number_field"
    end
    text_field(field, options)
  end

end