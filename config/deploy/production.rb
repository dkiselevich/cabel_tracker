role :app, "78.109.59.250", :primary => true
role :app, "78.109.59.251"
role :worker, "78.109.59.253"
role :db, "78.109.59.250", :primary => true

role :web, "78.109.59.250", :primary => true
role :web, "78.109.59.251"

set :rails_env, "production"
set :branch, "master"
