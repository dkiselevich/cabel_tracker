##### Requirement's #####
require 'bundler/capistrano'
require 'capistrano/ext/multistage'
require 'delayed/recipes'

rails_v = (Proc.new {
    file = File.new("Gemfile", "r")
      while (line = file.gets)
            if line =~ /gem\s+[\"\']rails[\"\'].+/
                    version = line.scan(/(\d+)\.(\d+)\.(\d+)/).first.map {|x| x.to_i}
                  end
              end
        file.close
          version
}).call

#### Use the asset-pipeline

if rails_v[0] >= 3 && rails_v[1] >= 1
  load 'deploy/assets'
end

##### Stages #####
set :stages, %w(production staging)

##### Constant variables #####
set :application, "cabletracker"
set :deploy_to,   "/var/www/#{application}"
set :user, "deploy"
set :use_sudo, false

##### worker ######
set :delayed_job_server_role, :worker 

##### Default variables #####
set :keep_releases, 10

##### Repository Settings #####
set :scm,        :git
set :repository, "git@github.com:fad/ct-dev.git"

##### Additional Settings #####
#set :deploy_via, :remote_cache
set :ssh_options, { :forward_agent => true }

#### Roles #####
# See Stages

##### Overwritten and changed default capistrano tasks #####
namespace :deploy do

  # Restart Application
  desc "Restart RailsApp"
  task :restart, :roles => :app, :except => { :no_release => true } do
    run "touch #{File.join(current_path,'tmp','restart.txt')}"
  end

  desc "Additional Symlinks"
  task :additional_symlink, :roles => [ :app, :worker ] do
    run "ln -nfs #{shared_path}/config/database.yml #{release_path}/config/database.yml"
  end

  desc "Addtional Rake Tasks"
  task :additional_rake, :roles => :app, :only => {:primary => true} do

  end
end
  
##### After and Before Tasks #####
if rails_v[0] >= 3 && rails_v[1] >= 1
  before "deploy:assets:precompile", "deploy:additional_symlink"
  after "deploy", "deploy:additional_rake"
else
  after "deploy:create_symlink", "deploy:additional_symlink", "deploy:additional_rake"
end

after "deploy:restart", "delayed_job:restart"


