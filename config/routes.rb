Dispo::Application.routes.draw do

  namespace :android do
    resources :tokens, :only => [:create, :destroy]
    resources :http_requests do
      collection do
        get 'getTermine'
        post 'getTermine'
        get 'postAuftragsabschluss'
        post 'postAuftragsabschluss'
      end
    end
  end

  namespace :purchase do resources :storages end

  resources :s3_uploads

  #root
  devise_for :users, :path_prefix=>"d", :controllers=> {:sessions=>"sessions"}

  resources :bugtrackers, :only => [:new, :create]
  resources :users, :except => [:destroy, :create, :index]
  resources :plz
  resources :cities
  resources :streets
  resources :street_numbers

  match '/purchase', :controller => 'purchase/base', :action => 'index'
  match '/scheduling', :controller => 'scheduling/base', :action => 'index'
  match '/scheduling_leader', :controller => 'scheduling_leader/base', :action => 'index'
  match '/management', :controller => 'management/base', :action => 'index'
  match '/management/reports', :controller => 'management/reports', :action => 'index'
  match '/management/reports/overview_performance_appointments', :controller => 'management/reports', :action => 'overview_performance_appointments'
  match '/management/reports/overview_performance_success', :controller => 'management/reports', :action => 'overview_performance_success'
  match '/management/reports/overview_dispositions', :controller => 'management/reports', :action => 'overview_dispositions'

  match '/management/reports/crp', :controller => 'management/reports', :action => 'capacity_requirements_planning'
  match '/service', :controller => 'service/base', :action => 'index'
  match '/accounting', :controller => 'accounting/base', :action => 'index'
  match '/admin', :controller => 'admin/base', :action => 'index'
  match '/admin/delayed_jobs', :controller => 'admin/delayed_jobs', :action => 'index'
  match '/mobile', :controller => 'mobile/base', :action => 'index'
  match '/test', :controller => 'test/base', :action => 'index'
  match '/test/sig1', :controller => 'test/base', :action => 'sig1'
  match '/test/sig2', :controller => 'test/base', :action => 'sig2'
  match '/test/sig3', :controller => 'test/base', :action => 'sig3'
  match '/test/sig4', :controller => 'test/base', :action => 'sig4'
  match '/test/sig5', :controller => 'test/base', :action => 'sig5'
  match '/test/sig6', :controller => 'test/base', :action => 'sig6'
  match '/test/sig7', :controller => 'test/base', :action => 'sig7'
  match '/test/pdf', :controller => 'test/base', :action => 'pdf'

  match '/no_right', :controller => 'base', :action => 'no_right'
  match '/issue_form', :controller => 'base', :action => 'issue_form'

  root :to => "base#index"

  #admin
  namespace :admin do
    resources :bugtrackers do
      collection do
        get 'reload_index'
      end
    end
    resources :logs
    resources :service_completions
    resources :transfer_points
    resources :service_jobs do
      resources :service_appointments
      resources :service_completions
      resources :service_comments
    end
    resources :service_appointments
    resources :service_completions
    resources :photos
    resources :special_expenses
    resources :jobs do
      resources :appointments
      resources :cancellations
      resources :appointment_requests
      resources :completions
    end
    resources :appointments do
      member do
        get 'special_expenses_pop_up'
      end
      resources :special_expenses
    end
    resources :users do
      collection do
        get 'new_shortcut'
        post 'create_shortcut'
        get 'advanced_search'
        post 'advanced_search'
      end
    end
    resources :service_users
    resources :service_user_groups do
      member do
        get 'appointments'
        get 'appointments_data'
      end
    end
    resources :scheduling_users
    resources :clients
    resources :job_types
    resources :fi_fuse_types
    resources :building_types
    resources :network_types
    resources :modem_types
    resources :waiting_causes
    resources :cancellations
    resources :appointment_requests
    resources :completions do
      member do
        get 'pdf'
        get 'get_zip'
      end
    end
  end
  match '/admin/import_jobs', :controller => 'admin/base', :action => 'import_jobs'
  match '/admin/import_jobs_from_text', :controller => 'admin/base', :action => 'import_jobs_from_text'
  match '/admin/create_jobs_from_text', :controller => 'admin/base', :action => 'create_jobs_from_text'

  match '/admin/import_service_jobs_from_text', :controller => 'admin/base', :action => 'import_service_jobs_from_text'
  match '/admin/create_service_jobs_from_text', :controller => 'admin/base', :action => 'create_service_jobs_from_text'

  match '/admin/import_job', :controller => 'admin/base', :action => 'import_job'
  match '/admin/create_job', :controller => 'admin/base', :action => 'create_job'

  #accounting
  namespace :accounting do
  end

  #scheduling_leader
  namespace :scheduling_leader do
    resources :automatic_workflow_items
    resources :transfer_points
    resources :clients
    resources :scheduling_user_groups
    resources :service_user_groups do
      member do
        get 'appointments'
        get 'appointments_pop_up'
        get 'appointments_data'
      end
    end
    resources :jobs, :only => [:show, :index] do
      resources :comments
      resources :appointments, :except => [:destroy]
    end
    resources :completions
    resources :appointments, :except => [:destroy] do
      collection do
        get 'open'
        get 'calendar'
        get 'pop_up'
        get 'data'
        get 'latest_update'
      end
    end
    resources :appointment_requests
    resources :cancellations
    resources :users, :controller => 'service_users', :only => [:show]
    resources :scheduling_users, :except => [:destroy]
    resources :days_of_absence
    resources :service_users, :except => [:destroy] do
      resources :days_of_absence
      member do
        get 'create_pdf'
        get 'appointments'
        get 'appointments_pop_up'
        get 'appointments_data'
        get 'get_days_of_absence'
      end
      collection do
        get 'show_send_emails'
      end
    end

    #redundant user/days_of_absence route needed in view
    resources :user do
      resources :days_of_absence do
      end
    end
  end
  match '/scheduling_leader/service_user_groups/:id/appointments/pop_up', :controller => 'scheduling_leader/service_user_groups', :action => 'appointments_pop_up'
  match '/scheduling_leader/import_job', :controller => 'scheduling_leader/base', :action => 'import_job'
  match '/scheduling_leader/create_job', :controller => 'scheduling_leader/base', :action => 'create_job'
  match '/scheduling_leader/service_users/send_emails', :controller => 'scheduling_leader/service_users', :action => 'send_emails'
  match '/scheduling_leader/import_jobs_from_text', :controller => 'scheduling_leader/base', :action => 'import_jobs_from_text'
  match '/scheduling_leader/create_jobs_from_text', :controller => 'scheduling_leader/base', :action => 'create_jobs_from_text'
  match '/scheduling_leader/import_service_jobs_from_text', :controller => 'scheduling_leader/base', :action => 'import_service_jobs_from_text'
  match '/scheduling_leader/create_service_jobs_from_text', :controller => 'scheduling_leader/base', :action => 'create_service_jobs_from_text'
  match '/scheduling_leader/quotes_per_controller', :controller => 'scheduling_leader/base', :action => 'quotes_per_controller'
  match '/scheduling_leader/volume_per_controller', :controller => 'scheduling_leader/base', :action => 'volume_per_controller'

  #scheduling
  namespace :scheduling do
    resources :service_completions, :except => [:destroy] do
      member do
        get 'pdf'
        get 'get_zip'
      end
    end
    resources :service_jobs do
      resources :service_appointments
      resources :service_completions
      resources :service_comments
      collection do
        get 'all'
        get 'closed'
      end
      member do
        get 'close'
      end
    end
    resources :service_appointments do
      member do
        get 'create_pdf'
        get 'mail_pdf'
      end
    end
    resources :transfer_points
    resources :special_expenses
    resources :cancellations
    resources :completions, :except => [:destroy] do
      member do
        get 'technician_signature'
        get 'pdf'
        get 'get_zip'
      end
    end
    resources :appointment_requests, :except => [:destroy]
    resources :jobs, :except => [:destroy] do
      resources :comments
      resources :appointments, :except => [:destroy]
      collection do
        get 'all'
        get 'closed'
        get 'all_closed'
      end
      member do
        get 'close'
      end
    end
    resources :appointments do
      collection do
        get 'open'
        get 'calendar'
        get 'calendar_data'
        post 'calendar_data'
        get 'pop_up'
        get 'data'
        get 'latest_update'
        post 'save_calendar_position'
        post 'save_new_calendar_task'
        post 'get_job_id'
      end
      resources :special_expenses
      member do
        get 'create_pdf'
        get 'mail_pdf'
        get 'has_special_expenses'
        get 'update_from_js'
      end
    end
    resources :users, :controller => 'service_users'
    resources :service_users, :except => [:destroy, :edit, :new] do
      member do
        get 'mail_appointments_list'
        get 'mail_tomorrow_appointments_forms'
        get 'create_pdf'
        get 'appointments'
        get 'appointments_pop_up'
        get 'appointments_data'
      end
    end
    #redundant user/days_of_absence route needed in view
    resources :user do
      resources :days_of_absence do
      end
    end

    resources :service_user_groups do
      member do
        get 'appointments'
        get 'appointments_pop_up'
        get 'appointments_data'
      end
    end
  end
  match '/scheduling/service_users/:id/appointments/pop_up', :controller => 'scheduling/service_users', :action => 'appointments_pop_up'
  match '/scheduling/service_user_groups/:id/appointments/pop_up', :controller => 'scheduling/service_user_groups', :action => 'appointments_pop_up'
  match '/scheduling/quotes_per_controller', :controller => 'scheduling/base', :action => 'quotes_per_controller'
  match '/scheduling/volume_per_controller', :controller => 'scheduling/base', :action => 'volume_per_controller'

  match '/scheduling/import_job', :controller => 'scheduling/base', :action => 'import_job'
  match '/scheduling/create_job', :controller => 'scheduling/base', :action => 'create_job'
  match '/scheduling/all', :controller => 'scheduling/base', :action => 'all'


  match '/scheduling/import_zde_jobs_form', :controller => 'scheduling/base', :action => 'import_zde_jobs_form'
  match '/scheduling/import_zde_jobs', :controller => 'scheduling/base', :action => 'import_zde_jobs'

  match '/scheduling/import_zde_service_jobs_form', :controller => 'scheduling/base', :action => 'import_zde_service_jobs_form'
  match '/scheduling/import_zde_service_jobs', :controller => 'scheduling/base', :action => 'import_zde_service_jobs'


  match '/scheduling/import_jobs_from_text', :controller => 'scheduling/base', :action => 'import_jobs_from_text'
  match '/scheduling/create_jobs_from_text', :controller => 'scheduling/base', :action => 'create_jobs_from_text'
  match '/scheduling/import_service_jobs_from_text', :controller => 'scheduling/base', :action => 'import_service_jobs_from_text'
  match '/scheduling/create_service_jobs_from_text', :controller => 'scheduling/base', :action => 'create_service_jobs_from_text'

  match '/scheduling/route_planer', :controller => 'scheduling/route_planer', :action => 'index'

  match '/scheduling/test_ot', :controller => 'scheduling/base', :action => 'test_ot'

  #management
  namespace :management do
    resources :transfer_points
    resources :users, :controller => 'service_users', :only => [:show]
    resources :jobs do
      resources :appointments
    end
    resources :appointments
    resources :service_users, :except => [:destroy, :edit, :new] do
      member do
        get 'appointments'
        get 'appointments_pop_up'
        get 'appointments_data'
      end
    end
    resources :scheduling_users
    resources :users, :controller => 'service_users'
    resources :service_appointments do
      member do
        get 'create_pdf'
        get 'mail_pdf'
      end
    end
    namespace :reports do
      resources :quality_reports do
        member do
          get "show_as_pdf"
          get "show_as_html"
        end
      end
      resources :crp_reports do
        collection do
          get 'index'
          get 'capacity_requirements_planning'
        end
      end
      resources :test_reports do
        collection do
          get 'test'
        end
      end
      resources :total_evaluation_reports do
        collection do
          get 'index'
          get 'total_evaluation'
        end
      end
      resources :installation_reports do
        collection do
          get 'number_of_jobs'
          get 'number_of_dispositions'
          get 'successful_installations'
          get 'performance'
          get 'performance_success_rate'
        end
      end
    end
  end

  match '/management/jobs_per_period', :controller => 'management/base', :action => 'jobs_per_period'
  match '/management/appointments_per_period', :controller => 'management/base', :action => 'appointments_per_period'
  match '/management/quotes_per_job_type', :controller => 'management/base', :action => 'quotes_per_job_type'
  match '/management/quotes_per_technician', :controller => 'management/base', :action => 'quotes_per_technician'
  match '/management/quotes_per_controller', :controller => 'management/base', :action => 'quotes_per_controller'
  match '/management/volume_per_controller', :controller => 'management/base', :action => 'volume_per_controller'
  match '/management/reports/test', :controller => 'management/reports/test_reports', :action => 'test'


  namespace :purchase do
    resources :suppliers, :except => [:destroy] do
      member do
        get 'get_article_availabilities'
      end
      resources :article_availabilities, :controller => "supplier_article_availabilities"
    end
    resources :articles do
      member do
        get 'get_article_availabilities'
      end
      resources :article_availabilities, :controller => "article_article_availabilities"
    end
    resources :article_groups
    resources :article_availabilities
    resources :technicians
    resources :deliveries do
      member do
        get 'process_delivery'
        get 'order'
      end
      resources :delivery_items
    end
    resources :delivery_items
  end

  #service
  namespace :service do
    resources :service_jobs, :except => [:destroy, :edit, :new] do
      resources :service_appointments, :except => [:destroy, :edit, :new]
      collection do
        get 'closed'
      end
    end
    resources :service_completions, :except => [:destroy]do
      member do
        get 'get_zip'
      end
    end
    resources :service_appointments do
      resources :service_completions
      member do
        get 'preview'
      end
    end
    resources :transfer_points
    resources :special_expenses
    resources :users, :controller => 'service_users'
    resources :jobs, :except => [:destroy, :edit, :new] do
      resources :appointments, :except => [:destroy, :edit, :new]
      collection do
        get 'closed'
      end
    end
    resources :cancellations do
      resources :photos
    end
    resources :completions, :except => [:destroy]do
      member do
        get 'get_zip'
      end
    end
    resources :appointment_requests, :except => [:destroy]
    resources :appointments, :except => [:destroy, :edit, :new] do
      collection do
        get 'latest_update'
      end
      member do
        get 'preview'
        get 'has_special_expenses'
      #        get 'special_expenses_pop_up'
      end
      resources :special_expenses
      resources :cancellations, :except => [:destroy]
      resources :completions, :except => [:destroy]
      resources :appointment_requests, :except => [:destroy]
      collection do
        get 'pop_up'
      end
      member do
        get 'update_from_js'
      end
    end
  end

  match '/service/quotes_per_technician', :controller => 'service/base', :action => 'quotes_per_technician'
  match '/service/appointments_per_period', :controller => 'service/base', :action => 'appointments_per_period'
  match '/service/appointments_data', :controller => 'service/base', :action => 'appointments_data'
  match '/service/equipment', :controller => 'service/base', :action => 'equipment'
  match '/service/create_pdf', :controller => 'service/base', :action => 'create_pdf'

  #mobile
  namespace :mobile do
    resources :appointments, :except => [:destroy, :edit, :new] do
      resources :special_expenses
      member do
        get 'map'
      end
      collection do
        get 'next_open_appointment'
        get 'calendar'
      end
    end
  end
  match '/mobile', :controller => 'mobile/base', :action => 'index', :mobile => 1 #param mobile nötig um in sessions_controller zu checkn das mobile
  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
  # root :to => "jobs#index"

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id(.:format)))'

  match '*a', :to => 'errors#routing'
end
