Dispo::Application.configure do
  # Settings specified here will take precedence over those in config/application.rb

  # In the development environment your application's code is reloaded on
  # every request.  This slows down response time but is perfect for development
  # since you don't have to restart the webserver when you make code changes.
  config.cache_classes = false

  # Log error messages when you accidentally call methods on nil.
  config.whiny_nils = true

  # Show full error reports and disable caching
  config.consider_all_requests_local       = true
  #config.action_view.debug_rjs             = true
  config.action_controller.perform_caching = false

  # Don't care if the mailer can't send
  config.action_mailer.raise_delivery_errors = false

  # Print deprecation notices to the Rails logger
  config.active_support.deprecation = :log

  # Only use best-standards-support built into browsers
  config.action_dispatch.best_standards_support = :builtin
  config.action_mailer.default_url_options = { :host => 'localhost:3000' }

  # Do not compress assets
  config.assets.compress = false

  # Expands the lines which load the assets
  config.assets.debug = true

  #config.cache_store = :dalli_store

  #config.consider_all_requests_local = false

=begin
  config.middleware.use ExceptionNotifier,
  :sender_address => "noreply@cabletracker.de",
  :exception_recipients => "frank.duffner@gmail.com"
  config.action_mailer.delivery_method = :smtp
  config.action_mailer.smtp_settings = {
    :address              => "smtp.gmail.com",
    :port                 => 587,
    :domain               => 'gmail.com',
    :user_name            => 'cabletracker.info@gmail.com',
    :password             => 'cabletracker',
    :authentication       => 'plain',
    :enable_starttls_auto => true  }


  config.action_mailer.delivery_method = :smtp
  config.action_mailer.smtp_settings = {
      :address              => "developer.seesys.de",
      :port                 => 25,
      #:domain               => '3kabel',
      :user_name            => 'versand@cabletracker.de',
      :password             => 'FD9999!!',
      :authentication       => 'login',
      :enable_starttls_auto => true  
  }
=end

=begin
  config.action_mailer.delivery_method = :smtp
  config.action_mailer.smtp_settings = {
      :address              => "mail.3kabel.de",
      :port                 => 99,
      :domain               => '3kabel', 
      :user_name            => 'smtp',
      :password             => '3Kabel9999!',
      :authentication       => 'login',
      :enable_starttls_auto => true  
  }
=end

=begin
  config.action_mailer.delivery_method = :smtp
  config.action_mailer.smtp_settings = {
      :address              => "smtp.gmail.com",
      :port                 => 587,
      :domain               => 'gmail.com',
      :user_name            => 'frank.duffner@gmail.com',
      :password             => 'iscmpmeitsmy1',
      :authentication       => 'plain',
      :enable_starttls_auto => true  
  }
=end    
    
  config.action_mailer.delivery_method = :smtp
  config.action_mailer.smtp_settings = {
      :address              => "mail.3kabel.de",
      :port                 => 99,
      :domain               => '3kabel', 
      :user_name            => 'smtp',
      :password             => '3Kabel9999!',
      :authentication       => 'login',
      :enable_starttls_auto => true
  }
  
  # config.action_mailer.delivery_method = :smtp
  # config.action_mailer.smtp_settings = {
    # :address              => "smtp.gmail.com",
    # :port                 => 587,
    # :domain               => 'gmail.com',
    # :user_name            => 'cabletracker.info@gmail.com',
    # :password             => 'cabletracker',
    # :authentication       => 'plain',
    # :enable_starttls_auto => true  
  # }
    
     
end





silence_warnings do
  require "pry"
  IRB = Pry
end