
Rails.application.routes.draw do |map|
  #mobile
  namespace :mobile do
    resources :appointments, :except => [:destroy, :edit, :new] do
      resources :special_expenses
      member do
        get 'map'
      end
      collection do
        get 'next_open_appointment'
        get 'calendar'
      end
    end
  end
  match '/mobile', :controller => 'mobile/base', :action => 'index', :mobile => 1 #param mobile nötig um in sessions_controller zu checkn das mobile
end