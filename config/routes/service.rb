#service
Rails.application.routes.draw do |map|
  namespace :service do
    resources :special_expenses
    resources :users, :controller => 'service_users'
    resources :jobs, :except => [:destroy, :edit, :new] do
      resources :appointments, :except => [:destroy, :edit, :new]
      collection do
        get 'closed'
      end
    end
    resources :cancellations do
      resources :photos
    end
    resources :completions, :except => [:destroy]do
      member do
        get 'get_zip'
      end
    end
    resources :appointment_requests, :except => [:destroy]
    resources :appointments, :except => [:destroy, :edit, :new] do
      member do
        get 'preview'
        get 'has_special_expenses'
  #        get 'special_expenses_pop_up'
      end
      resources :special_expenses
      resources :cancellations, :except => [:destroy]
      resources :completions, :except => [:destroy]
      resources :appointment_requests, :except => [:destroy]
      collection do
        get 'pop_up'
      end
      member do
        get 'update_from_js'
      end
    end
  end
end