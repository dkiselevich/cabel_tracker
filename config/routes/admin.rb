
Rails.application.routes.draw do |map|
  #admin
  namespace :admin do
    resources :photos
    resources :special_expenses
    resources :jobs do
      resources :appointments
      resources :cancellations
      resources :appointment_requests
      resources :completions
    end
    resources :appointments do
      member do
        get 'special_expenses_pop_up'
      end
      resources :special_expenses 
    end
    resources :users
    resources :service_users
    resources :service_user_groups do
      member do
        get 'appointments'
        get 'appointments_data'
      end
    end
    resources :scheduling_users
    resources :clients
    resources :job_types
    resources :fi_fuse_types
    resources :building_types
    resources :network_types
    resources :modem_types
    resources :waiting_causes
    resources :cancellations
    resources :appointment_requests
    resources :completions
  end
end