
Rails.application.routes.draw do |map|
  #scheduling
  namespace :scheduling do
    resources :special_expenses
    resources :cancellations
    resources :completions, :except => [:destroy] do
      member do
        get 'pdf'
        get 'get_zip'
      end
    end
    resources :appointment_requests, :except => [:destroy]
    resources :jobs, :except => [:destroy] do
      resources :appointments, :except => [:destroy]
      collection do
        get 'closed'
      end
    end
    resources :appointments, :except => [:destroy] do
      resources :special_expenses
      member do
        get 'update_from_js'
      end
    end
    resources :users, :controller => 'service_users'
    resources :service_users, :except => [:destroy, :edit, :new] do
      member do
        get 'appointments'
        get 'appointments_pop_up'
        get 'appointments_data'
      end
    end
    resources :service_user_groups do
      member do
        get 'appointments'
        get 'appointments_pop_up'
        get 'appointments_data'
      end
    end
  end
  match '/scheduling/service_users/:id/appointments/pop_up', :controller => 'scheduling/service_users', :action => 'appointments_pop_up'
  match '/scheduling/service_user_groups/:id/appointments/pop_up', :controller => 'scheduling/service_user_groups', :action => 'appointments_pop_up'
end