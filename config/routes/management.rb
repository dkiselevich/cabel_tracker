Rails.application.routes.draw do |map|
	#management
	namespace :management do
		resources :jobs do
		  resources :appointments
		end
		resources :appointments
		resources :service_users, :except => [:destroy, :edit, :new] do
	      member do
	        get 'appointments'
	        get 'appointments_pop_up'
	        get 'appointments_data'
	      end
	    end
		resources :scheduling_users
	end


  	match '/management/jobs_per_period', :controller => 'management/base', :action => 'jobs_per_period'
  	match '/management/quotes_per_job_type', :controller => 'management/base', :action => 'quotes_per_job_type'
  	match '/management/quotes_per_technician', :controller => 'management/base', :action => 'quotes_per_technician'
  	match '/management/quotes_per_controller', :controller => 'management/base', :action => 'quotes_per_controller'
  	match '/management/volume_per_controller', :controller => 'management/base', :action => 'volume_per_controller'
end