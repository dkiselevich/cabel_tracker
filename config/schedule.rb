# Use this file to easily define all of your cron jobs.
#
# It's helpful, but not entirely necessary to understand cron before proceeding.
# http://en.wikipedia.org/wiki/Cron

set :environment, :production
set :output, {
    :error    => "log/error.log",
    :standard => "log/cron.log"
}

job_type :runner, "source $HOME/.rvm/scripts/rvm && rvm use 1.9.2@ct && cd :path && script/rails runner -e :environment ':task' :output"

every :hour do
  client_id = 3 # will update when have info for client
  rake "add_xml_files_to_job[#{client_id}]"
end
