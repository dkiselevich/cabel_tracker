# encoding: utf-8
require 'spec_helper'
describe Appointment do
  let(:appointment) {create :appointment}
  subject {appointment}
  describe "attributes" do
    ["id",
    "job_id",
    "bemerkungen",
    "created_at",
    "updated_at",
    "user_id",
    "status",
    "day",
    "start_time",
    "end_time",
    "scheduling_user_id",
    "high_priority"].each do |field|
      it {should have_attribute(field)}
    end
  end

  describe "associations" do
    it { should belong_to(:job)}
    it { should belong_to(:user)}
    it { should belong_to(:scheduling_user).class_name("User")}
    it { should have_one(:cancellation)}
    it { should have_one(:completion)}
    it { should have_one(:appointment_request)}
    it { should have_many(:special_expenses)}
  end
  
  describe "validations" do
    it { should validate_presence_of(:day).with_message("^Geben Sie ein Datum an.")}
    #it { should validate_presence_of(:start_time).with_message("^Geben Sie einen Startzeitpunkt an.")}
    #it { should validate_presence_of(:end_time).with_message("^Geben Sie einen Endzeitpunkt an.")}
    it { should validate_presence_of(:job_id).with_message("^Geben Sie einen Auftrag an.")}
    it { should validate_presence_of(:user).with_message("^Wählen Sie einen Techniker.")}
  end

  describe "#short_description" do
    subject {appointment.short_description}
    context "built from job ot_auftragsnummer; appointment day and start_time, user full_name" do
      let(:result) {"#{appointment.job.ot_auftragsnummer} | #{appointment.day.strftime('%d.%m.%Y')} #{appointment.start_time.strftime('%H:%M')} | #{appointment.user.full_name}"}
      it {should eq(result)}
    end
  end
  

  describe "#processed_already?" do
    
    context "given all completion, appointment_request, cancellation = nil" do
      subject {appointment.processed_already?}
      before do
        appointment.should_receive(:completion).and_return(nil)
        appointment.should_receive(:appointment_request).and_return(nil)
        appointment.should_receive(:cancellation).and_return(nil)
      end
      it{should be_false}
    end

    context "given vaild completion" do
      subject {appointment.processed_already?}
      before do
        appointment.should_receive(:completion).and_return(mock("completion"))
      end
      it{should be_true}
    end
    
    context "given vaild appointment_request" do
      subject {appointment.processed_already?}
      before do
        appointment.should_receive(:appointment_request).and_return(mock("appointment_request"))
      end
      it{should be_true}
    end

    context "given vaild cancellation" do
      subject {appointment.processed_already?}
      before do
        appointment.should_receive(:cancellation).and_return(mock("cancellation"))
      end
      it{should be_true}
    end
  end
  
  describe ".belonging_to_user_client" do
    let(:user) {create :user}
    subject {Appointment.belonging_to_user_client(user)}
    context "given the same client info between appointment job and user" do
      before do
        client = create :client
        user.should_receive(:clients).and_return([client])
        job = create :job, :client => client
        appointment.job = job
        appointment.save
      end
      it{should eq([appointment])}
    end

    context "given has no the same client info between appointment job and user" do
      before do
        client = create :client
        another_client = create :client
        user.should_receive(:clients).and_return([client])
        job = create :job, :client => another_client
        appointment.job = job
        appointment.save
      end
      it{should eq([])}
    end

    context "given appointment job has no client" do
      before do
        client = create :client
        user.should_receive(:clients).and_return([client])
        job = create :job
        appointment.job = job
        appointment.save
      end
      subject {Appointment.belonging_to_user_client(user)}
      it{should eq([])}
    end
  end

  describe "#start_is_before_ende" do
    subject {appointment.send(:start_is_before_ende)}

    context "given start_time > end_time" do
      before do
        start_time = Time.now
        end_time = Time.now - 1.days
        appointment.should_receive(:start_time).and_return(start_time)
        appointment.should_receive(:end_time).and_return(end_time)
      end
      context "errors" do
        it{subject.first.should include("^Terminanfang muss vor Terminende liegen!")}
      end
    end

    context "given start_time < end_time" do
      before do
        start_time = Time.now
        end_time = Time.now + 1.days
        appointment.should_receive(:start_time).and_return(start_time)
        appointment.should_receive(:end_time).and_return(end_time)
      end
      context "errors" do
        it{should be_nil}
      end
    end
  end
end 