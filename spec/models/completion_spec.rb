# encoding: utf-8
require 'spec_helper'
describe Completion do
  let(:completion) {create :completion}
  describe "attributes" do
    ["id",
    "technician_id",
    "clever_pro",
    "unbundled",
    "modemtyp_id",
    "umzug",
    "installationsart_id",
    "installationsart_sonstiges",
    "fi_sicherung_id",
    "potentialausgleich_id",
    "netztopologie_id",
    "geschlossener_bvt",
    "klasse_4c",
    "linienverstaerker_id",
    "mehraufwand",
    "auftragsart_id",
    "dosentausch",
    "kellerquerverkabelung",
    "kellerquerverkabelung_laenge",
    "D130",
    "K5",
    "K29",
    "D802",
    "HFC_MAC",
    "adresspruefung",
    "besonderheiten",
    "uebrige_modems_im_gebaeude_per_SBC_portal_geprueft",
    "tv_empfang_beim_kunden_erfolgreich_geprueft",
    "internetzugang_mit_montagelaptop_geprueft",
    "telefon_abgehend_erfolgreich_getestet",
    "telefon_ankommend_erfolgreich_getestet",
    "beauftragte_leistungen",
    "created_at",
    "updated_at",
    "appointment_id",
    "vorverstaerker_bezeichnung",
    "vorverstaerker_id",
    "bvt_photo_file_name",
    "bvt_photo_content_type",
    "bvt_photo_file_size",
    "bvt_photo_updated_at",
    "uep_photo_file_name",
    "uep_photo_content_type",
    "uep_photo_file_size",
    "uep_photo_updated_at",
    "pot_schiene_photo_file_name",
    "pot_schiene_photo_content_type",
    "pot_schiene_photo_file_size",
    "pot_schiene_photo_updated_at",
    "pot_gas_photo_file_name",
    "pot_gas_photo_content_type",
    "pot_gas_photo_file_size",
    "pot_gas_photo_updated_at",
    "pot_wasser_photo_file_name",
    "pot_wasser_photo_content_type",
    "pot_wasser_photo_file_size",
    "pot_wasser_photo_updated_at",
    "pot_hak_photo_file_name",
    "pot_hak_photo_content_type",
    "pot_hak_photo_file_size",
    "pot_hak_photo_updated_at",
    "pot_heizung_photo_file_name",
    "pot_heizung_photo_content_type",
    "pot_heizung_photo_file_size",
    "pot_heizung_photo_updated_at",
    "pot_oel_photo_file_name",
    "pot_oel_photo_content_type",
    "pot_oel_photo_file_size",
    "pot_oel_photo_updated_at",
    "mmd_unbundled_photo_file_name",
    "mmd_unbundled_photo_content_type",
    "mmd_unbundled_photo_file_size",
    "mmd_unbundled_photo_updated_at",
    "messwerte_photo_file_name",
    "messwerte_photo_content_type",
    "messwerte_photo_file_size",
    "messwerte_photo_updated_at",
    "signature",
    "provisionierung_id",
    "wohneinheiten",
    "scheduling_user_id",
    "signature_photo_file_name",
    "signature_photo_content_type",
    "signature_photo_file_size",
    "signature_photo_updated_at",
    "provisionierung",
    "no_access_to_bvt",
    "no_access_to_uep",
    "no_access_to_pot_schiene",
    "additional_attachment_1_file_name",
    "additional_attachment_2_file_name",
    "additional_attachment_3_file_name",
    "additional_attachment_4_file_name",
    "additional_attachment_5_file_name",
    "additional_attachment_6_file_name",
    "additional_attachment_7_file_name",
    "additional_attachment_8_file_name",
    "additional_attachment_9_file_name",
    "additional_attachment_10_file_name",
    "additional_attachment_1_description",
    "additional_attachment_2_description",
    "additional_attachment_3_description",
    "additional_attachment_4_description",
    "additional_attachment_5_description",
    "additional_attachment_6_description",
    "additional_attachment_7_description",
    "additional_attachment_8_description",
    "additional_attachment_9_description",
    "additional_attachment_10_description",
    "no_access_to_pot_gas",
    "no_access_to_pot_hak",
    "no_access_to_pot_wasser",
    "no_access_to_pot_heizung",
    "no_access_to_pot_oel"].each do |field|
      it { should have_attribute(field) }
    end
  end

  describe "associations" do
    it { should belong_to(:scheduling_user).class_name("User")}
    it { should belong_to(:appointment)}
    it {should have_one(:job).through(:appointment)}
    it { should belong_to(:technician).class_name("User") }
    it { should belong_to(:job_type)}
    it { should belong_to(:network_type)}
  end

  describe "validations" do
    it { should validate_presence_of(:appointment).with_message("^Abschluss muss zu einem Termin gehören.") }
    it { should validate_presence_of(:technician).with_message("^Abschluss muss zu einem Techniker gehören.") }

    it { should validate_presence_of(:modemtyp_id).with_message("^Geben Sie einen Modemtyp an.") }

    it { should validate_presence_of(:installationsart_id).with_message("^Geben Sie eine 'Installationsart' an.") }
    #it { should validate_presence_of(:installationsart_sonstiges).with_message("^Geben Sie eine 'Installationsart' an.") }
    it { should validate_presence_of(:fi_sicherung_id).with_message("^Geben Sie eine 'FI Sicherung' an.") }
    it { should validate_presence_of(:potentialausgleich_id).with_message("^Geben Sie einen 'Potentialausgleich' an.") }
    it { should validate_presence_of(:netztopologie_id).with_message("^Geben Sie eine 'Netztopologie' an.") }

    it { should validate_presence_of(:vorverstaerker_id).with_message("^Geben Sie einen 'Vorverstärker' an.") }
    #it { should validate_presence_of(:vorverstaerker_bezeichnung).with_message("^Geben Sie eine 'Vorverstärkerbezeichnung' an.") }

    it { should validate_presence_of(:auftragsart_id).with_message("^Geben Sie eine 'Auftragsart' an.") }
    #it { should validate_presence_of(:kellerquerverkabelung_laenge).with_message("^Geben Sie die 'Länge der Kellerquerverkabelung' an.") }

    it { should validate_presence_of(:wohneinheiten).with_message("^Geben Sie die 'Anzahl der WE im Objekt' an.") }
    
    it { should validate_presence_of(:D130).with_message("^Füllen Sie das Feld 'D130' aus.") }
    it { should validate_presence_of(:K5).with_message("^Füllen Sie das Feld 'K5' aus." ) }
    it { should validate_presence_of(:K29).with_message("^Füllen Sie das Feld 'K29' aus.") }
    it { should validate_presence_of(:D802).with_message("^Füllen Sie das Feld 'D802' aus.") }

    it { should validate_presence_of(:HFC_MAC).with_message("^Geben Sie die 'MAC-Adresse' an.") }

    it { should validate_presence_of(:messwerte_photo_file_name).with_message("^Fügen Sie ein Foto 'Messwerte' hinzu.") }

    it { should validate_presence_of(:bvt_photo_file_name).with_message("^Fügen Sie ein Foto 'BVT' hinzu.") }
    it { should validate_presence_of(:uep_photo_file_name).with_message("^Fügen Sie ein Foto 'ÜP' hinzu.") }
    it { should validate_presence_of(:pot_schiene_photo_file_name).with_message("^Fügen Sie ein Foto 'POT-Schiene' hinzu.") }

    it { should validate_presence_of(:pot_gas_photo_file_name).with_message("^Fügen Sie ein Foto 'POT-Gas' hinzu.") }
    it { should validate_presence_of(:pot_wasser_photo_file_name).with_message("^Fügen Sie ein Foto 'POT-Wasser' hinzu.") }
    it { should validate_presence_of(:pot_heizung_photo_file_name).with_message("^Fügen Sie ein Foto 'POT-Heizung' hinzu.") }
    it { should validate_presence_of(:pot_hak_photo_file_name).with_message("^Fügen Sie ein Foto 'POT-Hak' hinzu.") }
    it { should validate_presence_of(:pot_oel_photo_file_name).with_message("^Fügen Sie ein Foto 'POT-Öl' hinzu.") }
    #TODO add spec to validate_inclusion_of and correct validate_presence_of with :if constrain
  end
  
  describe "#no_access_to_pot_gas_required" do
    subject {completion.no_access_to_pot_gas_required}
    context "given no_access_to_pot_gas = true" do
      before do 
        completion.should_receive(:no_access_to_pot_gas).and_return(true)
      end
      it{should be_true}
    end

    context "given no_access_to_pot_gas = false and potentialausgleich_id = 2" do
      before do
        completion.should_receive(:no_access_to_pot_gas).and_return(false)
        completion.should_receive(:potentialausgleich_id).and_return(2) 
      end
      it{should be_true}
    end

    context "given no_access_to_pot_gas = false and potentialausgleich_id = 3" do
      before do
        completion.should_receive(:no_access_to_pot_gas).and_return(false)
        completion.should_receive(:potentialausgleich_id).and_return(3) 
      end
      it{should_not be_true}
    end
  end
  
  describe "#no_access_to_pot_wasser_required" do
    subject {completion.no_access_to_pot_wasser_required}
    context "given no_access_to_pot_wasser = true" do
      before do 
        completion.should_receive(:no_access_to_pot_wasser).and_return(true)
      end
      it{should be_true}
    end

    context "given no_access_to_pot_wasser = false and potentialausgleich_id = 2" do
      before do
        completion.should_receive(:no_access_to_pot_wasser).and_return(false)
        completion.should_receive(:potentialausgleich_id).and_return(2) 
      end
      it{should be_true}
    end

    context "given no_access_to_pot_wasser = false and potentialausgleich_id = 3" do
      before do
        completion.should_receive(:no_access_to_pot_wasser).and_return(false)
        completion.should_receive(:potentialausgleich_id).and_return(3) 
      end
      it{should_not be_true}
    end
  end
  
  describe "#no_access_to_pot_hak_required" do
    subject {completion.no_access_to_pot_hak_required}
    context "given no_access_to_pot_hak = true" do
      before do 
        completion.should_receive(:no_access_to_pot_hak).and_return(true)
      end
      it{should be_true}
    end

    context "given no_access_to_pot_hak = false and potentialausgleich_id = 2" do
      before do
        completion.should_receive(:no_access_to_pot_hak).and_return(false)
        completion.should_receive(:potentialausgleich_id).and_return(2) 
      end
      it{should be_true}
    end

    context "given no_access_to_pot_hak = false and potentialausgleich_id = 3" do
      before do
        completion.should_receive(:no_access_to_pot_hak).and_return(false)
        completion.should_receive(:potentialausgleich_id).and_return(3) 
      end
      it{should_not be_true}
    end
  end

  describe "#no_access_to_pot_heizung_required" do
    subject {completion.no_access_to_pot_heizung_required}
    context "given no_access_to_pot_heizung = true" do
      before do 
        completion.should_receive(:no_access_to_pot_heizung).and_return(true)
      end
      it{should be_true}
    end

    context "given no_access_to_pot_heizung = false and potentialausgleich_id = 2" do
      before do
        completion.should_receive(:no_access_to_pot_heizung).and_return(false)
        completion.should_receive(:potentialausgleich_id).and_return(2) 
      end
      it{should be_true}
    end

    context "given no_access_to_pot_heizung = false and potentialausgleich_id = 3" do
      before do
        completion.should_receive(:no_access_to_pot_heizung).and_return(false)
        completion.should_receive(:potentialausgleich_id).and_return(3) 
      end
      it{should_not be_true}
    end
  end

  describe "#no_access_to_pot_oel_required" do
    subject {completion.no_access_to_pot_oel_required}
    context "given no_access_to_pot_oel = true" do
      before do 
        completion.should_receive(:no_access_to_pot_oel).and_return(true)
      end
      it{should be_true}
    end

    context "given no_access_to_pot_oel = false and potentialausgleich_id = 2" do
      before do
        completion.should_receive(:no_access_to_pot_oel).and_return(false)
        completion.should_receive(:potentialausgleich_id).and_return(2) 
      end
      it{should be_true}
    end

    context "given no_access_to_pot_oel = false and potentialausgleich_id = 3" do
      before do
        completion.should_receive(:no_access_to_pot_oel).and_return(false)
        completion.should_receive(:potentialausgleich_id).and_return(3) 
      end
      it{should_not be_true}
    end
  end

  describe "#installationsart_is_other?" do
    subject {completion.installationsart_is_other?}
    context "given installationsart_id = 1" do
      before do 
        completion.should_receive(:installationsart_id).and_return(1)
      end
      it{should be_true}
    end

    context "given installationsart_id <> 1" do
      before do
        completion.should_receive(:installationsart_id).and_return(2)
      end
      it{should_not be_true}
    end
  end
  
  describe "#is_folgeauftrag?" do
    subject {completion.is_folgeauftrag?}
    context "given short nane of job type = FA" do
      before do 
        completion.should_receive(:job_type).and_return(job_type = mock("job type object"))
        job_type.should_receive(:shortname).and_return("FA")
      end
      it{should be_true}
    end

    context "given short nane of job type <> FA" do
      before do 
        completion.should_receive(:job_type).and_return(job_type = mock("job type object"))
        job_type.should_receive(:shortname).and_return("FAA")
      end
      it{should_not be_true}
    end
  end
  
  describe "#kellerquerverkabelung?" do
    subject {completion.kellerquerverkabelung?}
    context "given kellerquerverkabelung = 1" do
      before do 
        completion.should_receive(:kellerquerverkabelung).and_return(1)
      end
      it{should be_true}
    end

    context "kellerquerverkabelung <> 1" do
      before do 
        completion.should_receive(:kellerquerverkabelung).and_return(2)
      end
      it{should_not be_true}
    end
  end
  

  describe "#vorverstaerker?" do
    subject {completion.vorverstaerker?}
    context "given vorverstaerker_id = 1" do
      before do 
        completion.should_receive(:vorverstaerker_id).and_return(1)
      end
      it{should be_true}
    end

    context "given vorverstaerker_id = 2" do
      before do
        completion.should_receive(:vorverstaerker_id).and_return(2)
        completion.should_receive(:vorverstaerker_id).and_return(2) 
      end
      it{should be_true}
    end

    context "given vorverstaerker_id <> 1 and <> 2" do
      before do
        completion.should_receive(:vorverstaerker_id).twice.and_return(3) 
      end
      it{should_not be_true}
    end
  end
  

  describe "#herstellung_potentialausgleich?" do
    subject {completion.herstellung_potentialausgleich?}
    context "given potentialausgleich_id = 1" do
      before do 
        completion.should_receive(:potentialausgleich_id).and_return(1)
      end
      it{should be_true}
    end

    context "potentialausgleich_id <> 1" do
      before do 
        completion.should_receive(:potentialausgleich_id).and_return(2)
      end
      it{should_not be_true}
    end
  end
  
  describe "#herstellung_potentialausgleich_and_heizung?" do
    subject {completion.herstellung_potentialausgleich_and_heizung?}
    context "given potentialausgleich_id = 1 and valid pot_heizung_photo" do
      before do 
        completion.should_receive(:potentialausgleich_id).and_return(1)
        completion.should_receive(:pot_heizung_photo).and_return(mock("pot_heizung_photo")) 
      end
      it{should be_true}
    end

    context "given potentialausgleich_id <> 1" do
      before do
        completion.should_receive(:potentialausgleich_id).and_return(2) 
      end
      it{should_not be_true}
    end

    context "given potentialausgleich_id = 1 and nil pot_heizung_photo" do
      before do
        completion.should_receive(:potentialausgleich_id).and_return(1)
        completion.should_receive(:pot_heizung_photo).and_return(nil) 
      end
      it{should_not be_true}
    end
  end
  
  describe "#short_description" do
    created_at = Time.now
    convert_created_at = created_at.strftime(Settings.TIME_FORMAT)
    full_name = "My full name"
    subject {completion.short_description}
    
    context "created_at = #{created_at} and technician full_name = #{full_name}" do
      before do
        completion.should_receive(:created_at).and_return(created_at)
        created_at.should_receive(:strftime).exactly(1).times.with(Settings.TIME_FORMAT).and_return(convert_created_at)
        completion.should_receive(:technician).and_return(technician = mock("technician"))
        technician.should_receive(:full_name).and_return(full_name)
      end
      let(:result) {["erstellt am", convert_created_at, "durch", full_name].join(" ")}
      it {should eq(result)}
    end
  end
  
  describe ".belonging_to_user_client" do
    let(:user) {create :user}
    subject {Completion.belonging_to_user_client(user)}
    context "given the same client info between completion job and user" do
      before do
        client = create :client
        user.should_receive(:clients).and_return([client])
        completion.job.client = client
        completion.save!
      end
      it{should eq([completion])}
    end

    context "given has no the same client info between completion job and user" do
      before do
        client = create :client
        another_client = create :client
        user.should_receive(:clients).and_return([client])
        completion.job.client = another_client
        completion.save
      end
      it{should eq([])}
    end

    context "given completion job has no client" do
      before do
        client = create :client
        user.should_receive(:clients).and_return([client])
      end
      it{should eq([])}
    end
  end

  def self.belonging_to_user_client(user)
    self.includes(:job).where("jobs.client_id in (#{user.clients.collect{|c| c.id}.join(',')})")
  end

  1.upto(10).each do |n|
    describe "#additional_attachment_#{n}_url" do
      subject {completion.send("additional_attachment_#{n}_url")}
      context "given nil additional_attachment_#{n}_url" do
        before {completion.should_receive("additional_attachment_#{n}_file_name").and_return(nil)}
        it{should eq('/assets/blank.gif')}
      end

      context "given valid additional_attachment_#{n}" do
        let(:job) {create :job}
        before do
          completion.should_receive(:job).and_return(job)
          completion.should_receive("additional_attachment_#{n}_file_name").exactly(3).times.and_return("additional_attachment_#{n}_file_name")
        end
        let(:url){"#{Settings.COMPLETION_IMAGES_BASE_URL}#{job.id}/additional_attachment_#{n}_file_name/#{completion.send("additional_attachment_#{n}_file_name")}"}
        it{should eq(url)}
      end
    end
  end
end



