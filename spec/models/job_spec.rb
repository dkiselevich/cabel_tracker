# encoding: utf-8
require 'spec_helper'
describe Job do
  let(:job) {create :job}

  describe "attributes" do
    it { should have_attribute(:auftragsart_id) }
    it { should have_attribute(:workorder) }
    it { should have_attribute(:ot_auftragsnummer) }
    it { should have_attribute(:kunde_plz) }
    it { should have_attribute(:kunde_ort) }
    it { should have_attribute(:kunde_strasse) }
    it { should have_attribute(:kunde_hausnummer) }
    it { should have_attribute(:kunde_nachname) }
    it { should have_attribute(:kunde_vorname) }
    it { should have_attribute(:kunde_telefon_privat) }
    it { should have_attribute(:kunde_telefon_mobil) }
    it { should have_attribute(:kunde_telefon_geschaeftlich) }
    it { should have_attribute(:kunde_email) }
    it { should have_attribute(:telefon_neu) }
    it { should have_attribute(:eigentuemer_vorname) }
    it { should have_attribute(:eigentuemer_nachname) }
    it { should have_attribute(:eigentuemer_plz) }
    it { should have_attribute(:eigentuemer_ort) }
    it { should have_attribute(:eigentuemer_strasse) }
    it { should have_attribute(:eigentuemer_telefon_1) }
    it { should have_attribute(:eigentuemer_telefon_2) }
    it { should have_attribute(:eigentuemer_email) }
    it { should have_attribute(:eigentuemer_ansprechpartner) }
    it { should have_attribute(:eigentuemer_telefon_ansprechpartner) }
    it { should have_attribute(:rks_anlagennummer) }
    it { should have_attribute(:objektart_id) }
    it { should have_attribute(:wohneinheiten) }
    it { should have_attribute(:aktive_kunden) }
    it { should have_attribute(:netztopologie_id) }
    it { should have_attribute(:erstinstallation_erfolgt) }
  end

  describe 'associations' do
    it { should have_many(:appointments).dependent(:destroy) }
    it { should have_many(:cancellations).dependent(:destroy).through(:appointments)}
    it { should have_many(:completions).dependent(:destroy).through(:appointments)}
    it { should have_many(:appointment_requests).dependent(:destroy).through(:appointments)}
    it { should have_many(:comments).dependent(:destroy)}
    it { should belong_to(:client)}
    it { should belong_to(:transfer_point)}
    it { should belong_to(:job_type)}
    it { should belong_to(:building_type)}
    it { should belong_to(:network_type)}
    it { should belong_to(:waiting_cause)}
    it { should belong_to(:modem_type)}
  end


  describe "protected_attributes" do
    it(:id) { subject.class.protected_attributes include(:id)}
  end

  describe "validations" do
    it { should validate_presence_of(:client_id).with_message("^Geben Sie einen Mandant an.") }
    it { should validate_presence_of(:client_id).with_message("^Geben Sie einen Mandant an.") }
    it { should validate_presence_of(:client_id).with_message("^Geben Sie einen Mandant an.") }
    it { should validate_presence_of(:client_id).with_message("^Geben Sie einen Mandant an.") }
    it { should validate_presence_of(:client_id).with_message("^Geben Sie einen Mandant an.") }
    it { should validate_presence_of(:client_id).with_message("^Geben Sie einen Mandant an.") }

    it { should validate_presence_of(:client_id).with_message("^Geben Sie einen Mandant an.")}
    it { should validate_presence_of(:auftragsart_id).with_message("^Geben Sie eine Auftragsart an.")}
    it { should validate_presence_of(:workorder).with_message("^Geben Sie eine Workorder an.")}
    it { should validate_presence_of(:kunde_kundennummer).with_message("^Geben Sie eine Kundennummer an.")}
    it { should validate_presence_of(:ot_auftragsnummer).with_message("^Geben Sie eine Auftragsnummer an.")}
    it { should validate_presence_of(:modemtyp_id).with_message("^Geben Sie einen Modemtyp an.")}

    it { should validate_presence_of(:kunde_plz).with_message("^Geben Sie eine PLZ für den Kunden an.")}
    it { should validate_presence_of(:kunde_ort).with_message("^Geben Sie einen Ort für den Kunden an.")}
    it { should validate_presence_of(:kunde_strasse).with_message("^Geben Sie eine Strasse für den Kunden an.")}
    it { should validate_presence_of(:kunde_hausnummer).with_message("^Geben Sie eine Hausnummer für den Kunden an.")}
    it { should validate_presence_of(:kunde_nachname).with_message("^Geben Sie einen Nachnamen für den Kunden an.")}

    it { should validate_presence_of(:eigentuemer_plz).with_message("^Geben Sie eine PLZ für den Eigentümer an.")}
    it { should validate_presence_of(:eigentuemer_ort).with_message("^Geben Sie einen Ort für den Eigentümer an.")}
    it { should validate_presence_of(:eigentuemer_strasse).with_message("^Geben Sie eine Strasse für den Eigentümer an.")}
    it { should validate_presence_of(:eigentuemer_hausnummer).with_message("^Geben Sie eine Hausnummer für den Eigentümer an.")}
    it { should validate_presence_of(:eigentuemer_nachname).with_message("^Geben Sie einen Nachnamen für den Eigentümer an.")}

    it { should validate_presence_of(:wohneinheiten).with_message("^Geben Sie die Anzahl der Wohneinheiten im Objekt an.")}
    it { should validate_presence_of(:netztopologie_id).with_message("^Geben Sie eine Netztopologie an.")}
  end
  
  describe "#kunde_is_eigentuemer?" do
    kunde_is_eigentuemer = [Faker::Name.name, nil, ""]
    kunde_is_eigentuemer.each do |k|
      context "given kunde_is_eigentuemer = #{k.inspect}" do
        before{ job.should_receive(:kunde_is_eigentuemer).and_return(k)}
        subject {job.kunde_is_eigentuemer?}
        it{should equal(k)}
      end
    end
  end  

  describe "#short" do

    list = [:ot_auftragsnummer, :kunde_plz, :kunde_ort, :kunde_strasse, :kunde_hausnumme, :kunde_nachname]
    attributes = FactoryGirl.create(:job).attributes.select!{|attr| list.include?(attr.to_sym)}
    context "given valid params = #{attributes}" do
      before do
        job.should_receive(:ot_auftragsnummer).and_return(attributes["ot_auftragsnummer"])
        job.should_receive(:kunde_plz).and_return(attributes["kunde_plz"])
        job.should_receive(:kunde_ort).and_return(attributes["kunde_ort"])
        job.should_receive(:kunde_strasse).and_return(attributes["kunde_strasse"])
        job.should_receive(:kunde_hausnummer).and_return(attributes["kunde_hausnummer"])
        job.should_receive(:kunde_nachname).and_return(attributes["kunde_nachname"])
      end
      subject {job.short}
      it{should eq("#{attributes["ot_auftragsnummer"]} | #{attributes["kunde_plz"]} #{attributes["kunde_ort"]} | #{attributes["kunde_strasse"]}#{attributes["kunde_hausnummer"]} | #{attributes["kunde_nachname"]}")}
    end
  end

  describe "#short_description" do
    list = [:ot_auftragsnummer, :kunde_plz, :kunde_ort, :kunde_strasse, :kunde_hausnumme, :kunde_nachname, :kunde_vorname, :kunde_telefon]
    attributes = FactoryGirl.create(:job).attributes.select!{|attr| list.include?(attr.to_sym)}
    context "given valid params = #{attributes}" do
      before do
        job.should_receive(:ot_auftragsnummer).and_return(attributes["ot_auftragsnummer"])
        job.should_receive(:kunde_plz).and_return(attributes["kunde_plz"])
        job.should_receive(:kunde_ort).and_return(attributes["kunde_ort"])
        job.should_receive(:kunde_strasse).and_return(attributes["kunde_strasse"])
        job.should_receive(:kunde_hausnummer).and_return(attributes["kunde_hausnummer"])
        job.should_receive(:kunde_nachname).and_return(attributes["kunde_nachname"])
        job.should_receive(:kunde_vorname).and_return(attributes["kunde_vorname"])
        job.should_receive(:kunde_telefon).and_return(attributes["kunde_telefon"])
      end
      subject {job.short_description}
      it{should eq("#{attributes["ot_auftragsnummer"]} | #{attributes["kunde_plz"]} #{attributes["kunde_ort"]} | #{attributes["kunde_strasse"]}#{attributes["kunde_hausnummer"]} | #{attributes["kunde_nachname"]}, #{attributes["kunde_vorname"]} | #{attributes["kunde_telefon"]}")}
    end
  end
  
  describe "#adresse" do
    list = [:kunde_plz, :kunde_plz, :kunde_ort, :kunde_strasse, :kunde_hausnummer, :kunde_nachname, :kunde_vorname, :kunde_telefon]
    attributes = FactoryGirl.create(:job).attributes.select!{|attr| list.include?(attr.to_sym)}
    context "given valid params = #{attributes}" do
      before do
        job.should_receive(:kunde_plz).and_return(attributes["kunde_plz"])
        job.should_receive(:kunde_ort).and_return(attributes["kunde_ort"])
        job.should_receive(:kunde_strasse).and_return(attributes["kunde_strasse"])
        job.should_receive(:kunde_hausnummer).and_return(attributes["kunde_hausnummer"])
        job.should_receive(:kunde_nachname).and_return(attributes["kunde_nachname"])
        job.should_receive(:kunde_vorname).and_return(attributes["kunde_vorname"])
        job.should_receive(:kunde_telefon).and_return(attributes["kunde_telefon"])
      end
      subject {job.adresse}
      it{should eq("#{attributes["kunde_plz"]} | #{attributes["kunde_ort"]} | #{attributes["kunde_strasse"]}#{attributes["kunde_hausnummer"]} | #{attributes["kunde_nachname"]}, #{attributes["kunde_vorname"]} | #{attributes["kunde_telefon"]}")}
    end
  end
  
  describe "#adresse_short" do
    list = [:kunde_plz, :kunde_ort, :kunde_strasse, :kunde_hausnummer]
    attributes = FactoryGirl.create(:job).attributes.select!{|attr| list.include?(attr.to_sym)}
    context "given valid params = #{attributes}" do
      before do
        job.should_receive(:kunde_plz).and_return(attributes["kunde_plz"])
        job.should_receive(:kunde_ort).and_return(attributes["kunde_ort"])
        job.should_receive(:kunde_strasse).and_return(attributes["kunde_strasse"])
        job.should_receive(:kunde_hausnummer).and_return(attributes["kunde_hausnummer"])
      end
      subject {job.adresse_short}
      it{should eq("#{attributes["kunde_plz"]} #{attributes["kunde_ort"]}, #{attributes["kunde_strasse"]}#{attributes["kunde_hausnummer"]}")}
    end
  end
  
  describe "#eigentuemer_daten" do
    list = [:eigentuemer_nachname, :eigentuemer_vorname, :eigentuemer_plz, :eigentuemer_ort, :eigentuemer_strasse, :eigentuemer_hausnummer, :eigentuemer_telefon_1, :eigentuemer_telefon_2]
    attributes = FactoryGirl.create(:job).attributes.select!{|attr| list.include?(attr.to_sym)}
    context "given valid params = #{attributes}" do
      before do
        job.should_receive(:eigentuemer_nachname).and_return(attributes["eigentuemer_nachname"])
        job.should_receive(:eigentuemer_vorname).and_return(attributes["eigentuemer_vorname"])
        job.should_receive(:eigentuemer_plz).and_return(attributes["eigentuemer_plz"])
        job.should_receive(:eigentuemer_ort).and_return(attributes["eigentuemer_ort"])
        job.should_receive(:eigentuemer_strasse).and_return(attributes["eigentuemer_strasse"])
        job.should_receive(:eigentuemer_hausnummer).and_return(attributes["eigentuemer_hausnummer"])
        job.should_receive(:eigentuemer_telefon_1).and_return(attributes["eigentuemer_telefon_1"])
        job.should_receive(:eigentuemer_telefon_2).and_return(attributes["eigentuemer_telefon_2"])
      end
      subject {job.eigentuemer_daten}
      let(:data) do
        s = "#{attributes["eigentuemer_nachname"]}, #{attributes["eigentuemer_vorname"]} | #{attributes["eigentuemer_plz"]} | #{attributes["eigentuemer_ort"]} | #{attributes["eigentuemer_strasse"]}#{attributes["eigentuemer_hausnummer"]} | #{attributes["eigentuemer_telefon_1"]} #{attributes["eigentuemer_telefon_2"]}"
        s.gsub("|  |","").gsub(",  ","")
      end
      it{should eq(data)}
    end
  end

  describe "#kunde_name" do
    list = [:kunde_vorname, :kunde_nachname]
    attributes = FactoryGirl.create(:job).attributes.select!{|attr| list.include?(attr.to_sym)}
    context "given valid params = #{attributes}" do
      before do
        job.should_receive(:kunde_vorname).and_return(attributes["kunde_vorname"])
        job.should_receive(:kunde_nachname).and_return(attributes["kunde_nachname"])
      end
      subject {job.kunde_name}
      it{should eq("#{attributes["kunde_vorname"]} #{attributes["kunde_nachname"]}")}
    end
  end
  
  describe "#kunde_strasse_full" do
    list = [:kunde_strasse, :kunde_hausnummer]
    attributes = FactoryGirl.create(:job).attributes.select!{|attr| list.include?(attr.to_sym)}

    context "given valid params = #{attributes}" do
      before do
        job.should_receive(:kunde_strasse).and_return(attributes["kunde_strasse"])
        job.should_receive(:kunde_hausnummer).and_return(attributes["kunde_hausnummer"])
      end
      subject {job.kunde_strasse_full}
      it{should eq("#{attributes["kunde_strasse"]} #{attributes["kunde_hausnummer"]}")}
    end
  end

  describe "#kunde_telefon" do
    subject {job.kunde_telefon}
    
    describe do
      attributes = {:kunde_telefon_privat => Time.now.to_i.to_s, 
                  :kunde_telefon_mobil => Time.now.to_i.to_s, 
                  :kunde_telefon_geschaeftlich => Time.now.to_i.to_s}
      context "given params = #{attributes}" do
        before do
          job.should_receive(:kunde_telefon_privat).twice.and_return(attributes[:kunde_telefon_privat])
          job.should_receive(:kunde_telefon_mobil).twice.and_return(attributes[:kunde_telefon_mobil])
          job.should_receive(:kunde_telefon_geschaeftlich).twice.and_return(attributes[:kunde_telefon_geschaeftlich])
        end
        it{should eq(attributes["kunde_telefon_privat"] || attributes["kunde_telefon_mobil"] || attributes[:kunde_telefon_geschaeftlich])}
      end
    end
    describe do
      attributes = {:kunde_telefon_privat => "", 
                    :kunde_telefon_mobil => Time.now.to_i.to_s, 
                    :kunde_telefon_geschaeftlich => Time.now.to_i.to_s}
      context "given some invalid attributes = #{attributes}" do
        before do
          job.should_receive(:kunde_telefon_privat).and_return(attributes[:kunde_telefon_privat])
          job.should_receive(:kunde_telefon_mobil).twice.and_return(attributes[:kunde_telefon_mobil])
          job.should_receive(:kunde_telefon_geschaeftlich).twice.and_return(attributes[:kunde_telefon_geschaeftlich])
        end
        it{should eq(attributes["kunde_telefon_privat"] || attributes["kunde_telefon_mobil"] || attributes[:kunde_telefon_geschaeftlich])}
      end
    end
    describe do
      attributes = {:kunde_telefon_privat => "", 
                    :kunde_telefon_mobil => "", 
                    :kunde_telefon_geschaeftlich => Time.now.to_i.to_s}
      context "given some invalid attributes = #{attributes}" do
        before do
          job.should_receive(:kunde_telefon_privat).and_return(attributes[:kunde_telefon_privat])
          job.should_receive(:kunde_telefon_mobil).and_return(attributes[:kunde_telefon_mobil])
          job.should_receive(:kunde_telefon_geschaeftlich).twice.and_return(attributes[:kunde_telefon_geschaeftlich])
        end
        it{should eq(attributes["kunde_telefon_privat"] || attributes["kunde_telefon_mobil"] || attributes[:kunde_telefon_geschaeftlich])}
      end
    end
    describe do
      attributes = {:kunde_telefon_privat => Time.now.to_i.to_s, 
                    :kunde_telefon_mobil => "", 
                    :kunde_telefon_geschaeftlich => ""}
      context "given some invalid attributes = #{attributes}" do
        before do
          job.should_receive(:kunde_telefon_privat).twice.and_return(attributes[:kunde_telefon_privat])
          job.should_receive(:kunde_telefon_mobil).and_return(attributes[:kunde_telefon_mobil])
          job.should_receive(:kunde_telefon_geschaeftlich).and_return(attributes[:kunde_telefon_geschaeftlich])
        end
        it{should eq(attributes[:kunde_telefon_privat] || attributes[:kunde_telefon_mobil] || attributes[:kunde_telefon_geschaeftlich])}
      end
    end
  end

  describe "#status_name" do
   
    status = {0 => "zu disponieren",
              1 => "disponiert",
              2 => "wartend",
              3 => "abgeschlossen",
              4 => "technisch abgeschlossen",
              5 => "storniert",
              :another => nil}
    status.keys.each_with_index do |s, index|
      context "given status = #{s}" do
        before {job.should_receive(:status).and_return(s)}
        subject {job.status_name}
        it{should eq(status[s])}
      end
    end
    
  end

  describe "#status_short" do
   
    status = {0 => "ZDE",
              1 => "DI",
              2 => "WK",
              3 => "AG",
              4 => "TA",
              5 => "OM",
              :another => nil}
    status.keys.each_with_index do |s, index|
      context "given status = #{s}" do
        before {job.should_receive(:status).and_return(s)}
        subject {job.status_short}
        it{should eq(status[s])}
      end
    end
    
  end
  
  describe "#full_logs" do
    params = {:created_at => Time.now, :blogs => Faker::Name.name}
    context "given params = #{params}" do
      before do 
        job.should_receive(:created_at).and_return(params[:created_at])
        job.should_receive(:logs).and_return(params[:blogs])
      end
      subject {job.full_logs}
      it{should eq("erstellt am: #{params[:created_at].strftime("%d.%m.%Y um %H:%M")}<br/>#{params[:blogs]}")}
    end

    params = {:created_at => Time.now, :blogs => nil}
    context "given params = #{params}" do
      before do 
        job.should_receive(:created_at).and_return(params[:created_at])
        job.should_receive(:logs).and_return(params[:blogs])
      end
      subject {job.full_logs}
      it{should eq("erstellt am: #{params[:created_at].strftime("%d.%m.%Y um %H:%M")}<br/>#{params[:blogs]}")}
    end
  end
  
  describe "#abgeschlossen?" do
    list = {3 => true, 4 => true, :another => false}
    list.keys.each_with_index do |s|
      context "given status = #{s}" do
        time = (s == 3) ? 1 : 2
        before {job.should_receive(:status).exactly(time).times.and_return(s)}
        subject {job.abgeschlossen?}
        it{should eq(list[s])}
      end
    end
  end
  
  describe "#completion" do
    current_completion = RSpec::Mocks::Mock.new("completion object")
    subject {job.completion}
    [current_completion, nil].each do |completion|
      context "given vaild completion #{current_completion}" do
        before {job.should_receive(:completions).and_return([completion])}
        it{should eq(completion)}
      end
    end
  end
  
  describe "#messwerte_photo_url" do
    subject {job.messwerte_photo_url}
    context "given nil completion" do
      before {job.should_receive(:completion).and_return(nil)}
      it{should eq('/assets/blank.gif')}
    end

    context "given valid completion with nil messwerte_photo" do
      let(:completion) {mock("completion object")}
      before do
        job.should_receive(:completion).twice.and_return(completion)
        completion.should_receive(:messwerte_photo_file_name).and_return(nil)
      end
      it{should eq('/assets/blank.gif')}
    end

    context "given valid completion with valid messwerte_photo" do
      let(:completion) {mock("completion object")}
      before do
        job.should_receive(:completion).exactly(3).times.and_return(completion)
        completion.should_receive(:messwerte_photo_file_name).exactly(3).times.and_return("messwerte photo")
      end
      let(:url){"#{Settings.COMPLETION_IMAGES_BASE_URL}#{job.id}/messwerte_photo_file_name/#{completion.messwerte_photo_file_name}"}
      it{should eq(url)}
    end
  end

  describe "#bvt_photo_url" do
    subject {job.bvt_photo_url}
    context "given nil completion" do
      before {job.should_receive(:completion).and_return(nil)}
      it{should eq('/assets/blank.gif')}
    end

    context "given valid completion with nil bvt_photo" do
      let(:completion) {mock("completion object")}
      before do
        job.should_receive(:completion).twice.and_return(completion)
        completion.should_receive(:bvt_photo_file_name).and_return(nil)
      end
      it{should eq('/assets/blank.gif')}
    end

    context "given valid completion with valid bvt_photo" do
      let(:completion) {mock("completion object")}
      before do
        job.should_receive(:completion).exactly(3).times.and_return(completion)
        completion.should_receive(:bvt_photo_file_name).exactly(3).times.and_return("bvt_photo")
      end
      let(:url){"#{Settings.COMPLETION_IMAGES_BASE_URL}#{job.id}/bvt_photo_file_name/#{completion.bvt_photo_file_name}"}
      it{should eq(url)}
    end
  end
  
  describe "#uep_photo_url" do
    subject {job.uep_photo_url}
    context "given nil completion" do
      before {job.should_receive(:completion).and_return(nil)}
      it{should eq('/assets/blank.gif')}
    end

    context "given valid completion with nil uep_photo" do
      let(:completion) {mock("completion object")}
      before do
        job.should_receive(:completion).twice.and_return(completion)
        completion.should_receive(:uep_photo_file_name).and_return(nil)
      end
      it{should eq('/assets/blank.gif')}
    end

    context "given valid completion with valid uep_photo" do
      let(:completion) {mock("completion object")}
      before do
        job.should_receive(:completion).exactly(3).times.and_return(completion)
        completion.should_receive(:uep_photo_file_name).exactly(3).times.and_return("uep_photo")
      end
      let(:url){"#{Settings.COMPLETION_IMAGES_BASE_URL}#{job.id}/uep_photo_file_name/#{completion.uep_photo_file_name}"}
      it{should eq(url)}
    end
  end
  
  describe "#pot_schiene_photo_url" do
    subject {job.pot_schiene_photo_url}
    context "given nil completion" do
      before {job.should_receive(:completion).and_return(nil)}
      it{should eq('/assets/blank.gif')}
    end

    context "given valid completion with nil pot_schiene_photo" do
      let(:completion) {mock("completion object")}
      before do
        job.should_receive(:completion).twice.and_return(completion)
        completion.should_receive(:pot_schiene_photo_file_name).and_return(nil)
      end
      it{should eq('/assets/blank.gif')}
    end

    context "given valid completion with valid pot_schiene_photo" do
      let(:completion) {mock("completion object")}
      before do
        job.should_receive(:completion).exactly(3).times.and_return(completion)
        completion.should_receive(:pot_schiene_photo_file_name).exactly(3).times.and_return("pot_schiene_photo")
      end
      let(:url){"#{Settings.COMPLETION_IMAGES_BASE_URL}#{job.id}/pot_schiene_photo_file_name/#{completion.pot_schiene_photo_file_name}"}
      it{should eq(url)}
    end
  end

  describe "#pot_gas_photo_url" do
    subject {job.pot_gas_photo_url}
    context "given nil completion" do
      before {job.should_receive(:completion).and_return(nil)}
      it{should eq('/assets/blank.gif')}
    end

    context "given valid completion with nil pot_gas_photo" do
      let(:completion) {mock("completion object")}
      before do
        job.should_receive(:completion).twice.and_return(completion)
        completion.should_receive(:pot_gas_photo_file_name).and_return(nil)
      end
      it{should eq('/assets/blank.gif')}
    end

    context "given valid completion with valid pot_gas_photo" do
      let(:completion) {mock("completion object")}
      before do
        job.should_receive(:completion).exactly(3).times.and_return(completion)
        completion.should_receive(:pot_gas_photo_file_name).exactly(3).times.and_return("pot_gas_photo")
      end
      let(:url){"#{Settings.COMPLETION_IMAGES_BASE_URL}#{job.id}/pot_gas_photo_file_name/#{completion.pot_gas_photo_file_name}"}
      it{should eq(url)}
    end
  end
  
  describe "#pot_wasser_photo_url" do
    subject {job.pot_wasser_photo_url}
    context "given nil completion" do
      before {job.should_receive(:completion).and_return(nil)}
      it{should eq('/assets/blank.gif')}
    end

    context "given valid completion with nil pot_wasser_photo" do
      let(:completion) {mock("completion object")}
      before do
        job.should_receive(:completion).twice.and_return(completion)
        completion.should_receive(:pot_wasser_photo_file_name).and_return(nil)
      end
      it{should eq('/assets/blank.gif')}
    end

    context "given valid completion with valid pot_wasser_photo" do
      let(:completion) {mock("completion object")}
      before do
        job.should_receive(:completion).exactly(3).times.and_return(completion)
        completion.should_receive(:pot_wasser_photo_file_name).exactly(3).times.and_return("pot_wasser_photo")
      end
      let(:url){"#{Settings.COMPLETION_IMAGES_BASE_URL}#{job.id}/pot_wasser_photo_file_name/#{completion.pot_wasser_photo_file_name}"}
      it{should eq(url)}
    end
  end
  
  describe "#pot_hak_photo_url" do
    subject {job.pot_hak_photo_url}
    context "given nil completion" do
      before {job.should_receive(:completion).and_return(nil)}
      it{should eq('/assets/blank.gif')}
    end

    context "given valid completion with nil pot_hak_photo" do
      let(:completion) {mock("completion object")}
      before do
        job.should_receive(:completion).twice.and_return(completion)
        completion.should_receive(:pot_hak_photo_file_name).and_return(nil)
      end
      it{should eq('/assets/blank.gif')}
    end

    context "given valid completion with valid pot_hak_photo" do
      let(:completion) {mock("completion object")}
      before do
        job.should_receive(:completion).exactly(3).times.and_return(completion)
        completion.should_receive(:pot_hak_photo_file_name).exactly(3).times.and_return("pot_hak_photo")
      end
      let(:url){"#{Settings.COMPLETION_IMAGES_BASE_URL}#{job.id}/pot_hak_photo_file_name/#{completion.pot_hak_photo_file_name}"}
      it{should eq(url)}
    end
  end

  describe "#pot_heizung_photo_url" do
    subject {job.pot_heizung_photo_url}
    context "given nil completion" do
      before {job.should_receive(:completion).and_return(nil)}
      it{should eq('/assets/blank.gif')}
    end

    context "given valid completion with nil pot_heizung_photo" do
      let(:completion) {mock("completion object")}
      before do
        job.should_receive(:completion).twice.and_return(completion)
        completion.should_receive(:pot_heizung_photo_file_name).and_return(nil)
      end
      it{should eq('/assets/blank.gif')}
    end

    context "given valid completion with valid pot_heizung_photo" do
      let(:completion) {mock("completion object")}
      before do
        job.should_receive(:completion).exactly(3).times.and_return(completion)
        completion.should_receive(:pot_heizung_photo_file_name).exactly(3).times.and_return("pot_heizung_photo")
      end
      let(:url){"#{Settings.COMPLETION_IMAGES_BASE_URL}#{job.id}/pot_heizung_photo_file_name/#{completion.pot_heizung_photo_file_name}"}
      it{should eq(url)}
    end
  end
  
  describe "#pot_oel_photo_url" do
    subject {job.pot_oel_photo_url}
    context "given nil completion" do
      before {job.should_receive(:completion).and_return(nil)}
      it{should eq('/assets/blank.gif')}
    end

    context "given valid completion with nil pot_oel_photo" do
      let(:completion) {mock("completion object")}
      before do
        job.should_receive(:completion).twice.and_return(completion)
        completion.should_receive(:pot_oel_photo_file_name).and_return(nil)
      end
      it{should eq('/assets/blank.gif')}
    end

    context "given valid completion with valid pot_oel_photo" do
      let(:completion) {mock("completion object")}
      before do
        job.should_receive(:completion).exactly(3).times.and_return(completion)
        completion.should_receive(:pot_oel_photo_file_name).exactly(3).times.and_return("pot_oel_photo")
      end
      let(:url){"#{Settings.COMPLETION_IMAGES_BASE_URL}#{job.id}/pot_oel_photo_file_name/#{completion.pot_oel_photo_file_name}"}
      it{should eq(url)}
    end
  end
  
  describe "#mmd_unbundled_photo_url" do
    subject {job.mmd_unbundled_photo_url}
    context "given nil completion" do
      before {job.should_receive(:completion).and_return(nil)}
      it{should eq('/assets/blank.gif')}
    end

    context "given valid completion with nil mmd_unbundled_photo" do
      let(:completion) {mock("completion object")}
      before do
        job.should_receive(:completion).twice.and_return(completion)
        completion.should_receive(:mmd_unbundled_photo_file_name).and_return(nil)
      end
      it{should eq('/assets/blank.gif')}
    end

    context "given valid completion with valid mmd_unbundled_photo" do
      let(:completion) {mock("completion object")}
      before do
        job.should_receive(:completion).exactly(3).times.and_return(completion)
        completion.should_receive(:mmd_unbundled_photo_file_name).exactly(3).times.and_return("mmd_unbundled_photo")
      end
      let(:url){"#{Settings.COMPLETION_IMAGES_BASE_URL}#{job.id}/mmd_unbundled_photo_file_name/#{completion.mmd_unbundled_photo_file_name}"}
      it{should eq(url)}
    end
  end
  
  describe "#belonging_to_user_client" do
    let(:user) {mock_model(User)}
    client = RSpec::Mocks::Mock.new("client object")
    subject{Job.belonging_to_user_client(user)}
    
    context "user has no any client" do
      before do
        user.should_receive(:clients).twice.and_return([])
        Job.should_receive(:where).with(:client_id => user.clients.collect{|c|c.id}).and_return([])
      end
      it{should eq([])}
    end

    context "given user has client but not the same client of job" do
      before do
        user.should_receive(:clients).twice.and_return([client])
        client.should_receive(:id).twice.and_return(1)
        Job.should_receive(:where).with(:client_id => user.clients.collect{|c|c.id}).and_return([])
      end
      it{should eq([])}
    end

    context "given user has client and the same client of job" do
      before do
        job.stub!(:client_id).and_return(1)
        user.should_receive(:clients).twice.and_return([client])
        client.should_receive(:id).twice.and_return(1)
        Job.should_receive(:where).with(:client_id => user.clients.collect{|c|c.id}).and_return([job])
      end
      it{should eq([job])}
    end
  end

  describe ".belonging_to_scheduling_user_group" do
    let(:user) {create:user}
    subject {Job.belonging_to_scheduling_user_group(user)}
    context "job and user is the same scheduling_user_group" do
      before do
        scheduling_user_group = create :scheduling_user_group
        scheduling_user_group.should_receive(:id).and_return(1)
        user.should_receive(:scheduling_user_group).twice.and_return(scheduling_user_group)
        job.scheduling_user_group_id = 1
        job.save
      end
      it{should eq([job])}
    end

    context "job and user is the not same scheduling_user_group" do
      before do
        scheduling_user_group = create :scheduling_user_group
        scheduling_user_group.should_receive(:id).and_return(1)
        user.should_receive(:scheduling_user_group).twice.and_return(scheduling_user_group)
      end
      it{should eq([])}
    end

    context "user has not scheduling_user_group" do
      before do
        user.should_receive(:scheduling_user_group).and_return(nil)
      end
      it{should eq([])}
    end
  end
end