require 'spec_helper'

describe User do
  
  subject {create :user}

  describe "attributes" do
    it { should have_attribute(:email) }
    it { should have_attribute(:vorname) }
    it { should have_attribute(:nachname) }
    it { should have_attribute(:encrypted_password) }
    it { should have_attribute(:reset_password_token) }
  end

  describe 'associations' do
    it { should have_and_belong_to_many(:service_user_groups) }
    it { should have_many(:appointments)}
    it { should have_many(:service_appointments)}
    it { should have_many(:completions).through(:appointments)}
    it { should have_many(:cancellations).through(:appointments)}
    it { should have_many(:appointment_requests).through(:appointments)}
    it { should have_many(:assignments)}
    it { should have_many(:clients).through(:assignments)}
  end
  
  describe 'validations' do
    it { should validate_presence_of(:email).with_message("^Geben Sie eine Email an.") }
    it { should validate_uniqueness_of(:email).with_message("^Die angegebene Email wird bereits verwendet Email muss einmalig sein..") }
    it { should validate_presence_of(:vorname).with_message("^Geben Sie einen Vornamen an.") }
    it { should validate_presence_of(:nachname).with_message("^Geben Sie einen Nachnamen an.") }
  end

  describe "mass_assignment" do
    it { should allow_mass_assignment_of(:email) }
    it { should allow_mass_assignment_of(:password) }
    it { should allow_mass_assignment_of(:password_confirmation) }
    it { should allow_mass_assignment_of(:vorname) }
    it { should allow_mass_assignment_of(:nachname) }
    it { should allow_mass_assignment_of(:plz) }
    it { should allow_mass_assignment_of(:ort) }
    it { should allow_mass_assignment_of(:strasse) }
    it { should allow_mass_assignment_of(:hausnummer) }
    it { should allow_mass_assignment_of(:telefon) }
    it { should allow_mass_assignment_of(:telefon_mobil) }
    it { should allow_mass_assignment_of(:signature) }
    it { should allow_mass_assignment_of(:signature_photo) }
    it { should allow_mass_assignment_of(:is_boss) }
    it { should allow_mass_assignment_of(:is_controller) }
    it { should allow_mass_assignment_of(:is_controller_leader) }
    it { should allow_mass_assignment_of(:is_technician) }
    it { should allow_mass_assignment_of(:is_accountant) }
    it { should allow_mass_assignment_of(:is_admin) }
    it { should allow_mass_assignment_of(:active) }
    it { should allow_mass_assignment_of(:client_ids) }
  end

  describe "#before_save" do
    let(:email) {"Email@email.email"}
    let(:user) {build :user, :email => email}
    context "create a user with upcase email adresss" do
      subject do
        user.save
        user
      end
      its(:email){should eq(email.downcase())}
    end
  end
  
  describe ".find_for_authentication" do
    upcase_email = "UPCASE@email.com"
    another_email = "ANOTHER@email.com"
    context "given existed a user with email address as #{upcase_email.downcase()}" do
      let(:user) {create :user, :email => upcase_email}
      context "pass upcase of email address from existed user with #{upcase_email}" do
        subject (:current_user) do
          user.reload
          User.find_for_authentication({:email => upcase_email})
        end
        it "should eq this user" do current_user.should eq(user) end
      end

      context "pass upcase of email address not related with existed user with #{another_email}" do
        subject (:current_user) do
          user.reload
          User.find_for_authentication({:email => another_email})
        end
        it{current_user.should be_nil}
      end
    end

    context "given not existed any user" do
      context "pass upcase of email address with #{upcase_email}" do
        subject (:current_user) do
          User.find_for_authentication({:email => upcase_email})
        end
        it{current_user.should be_nil}
      end
    end
  end
  
  describe "#full_name" do
    vorname = "Vor"
    nachname = "Nach"
    context "given existed user with vorname = #{vorname} and nachname = #{nachname}" do
      let(:user) {create :user}
      before do
        user.should_receive(:vorname).and_return(vorname)
        user.should_receive(:nachname).and_return(nachname)
      end
      subject {user.full_name}
      it {subject.should eq([vorname, nachname].join(" "))}
    end
  end
  
  describe "#client_names" do
    first_name = "Client 1"
    second_name = "Client 2"
    let(:user) {create :user}
    context "given current user has 2 client names is [#{first_name},#{second_name}]" do
      let(:first_client) {create :client, :name => first_name}
      let(:second_client) {create :client, :name => second_name}
      before do
        user.should_receive(:clients).and_return([first_client, second_client])
      end
      subject{ user.client_names }
      it{subject.should eq([first_name, second_name].join(", "))}
    end

    context "given current user has no any client" do
      before do
        user.should_receive(:clients).and_return([])
      end
      subject{ user.client_names }
      it{subject.should be_empty}
    end
  end

  describe "#full_name_and_companies" do
    let(:user) {create :user}
    full_name = "Full name"
    client_names = "Client 1"
    context "given user has at least a client name with '#{client_names}'" do
      before do
        user.should_receive(:full_name).and_return(full_name)
        user.should_receive(:client_names).and_return(client_names)
      end
      subject {user.full_name_and_companies}
      it {subject.should eq([full_name,"(#{client_names})"].join(" "))}
    end

    context "given user has no any client -> This is bad data please review this case" do
      before do
        user.should_receive(:full_name).and_return(full_name)
        user.should_receive(:client_names).and_return("")
      end
      subject {user.full_name_and_companies}
      it {subject.should eq([full_name,"()"].join(" "))}
    end
  end

  describe "#assigned_to_same_company" do
    name = "Client 1"
    let(:client) {create :client, :name => name}
    context "given it has a user belongs to client name = '#{name}'" do
      let(:user) {create :user, :clients => [client]}
      subject { User.assigned_to_same_company(user)}
      it "should inclue this user" do subject.include?(user).should be_true end
    end

    context "given it has 2 users belongs to client name = '#{name}'" do
      let(:user) {create :user, :clients => [client]}
      let(:other_user) {create :user, :clients => [client]}
      subject { User.assigned_to_same_company(user)}
      it "should inclue first user" do subject.include?(user).should be_true end
      it "should inclue second user" do subject.include?(other_user).should be_true end
    end
  end

  describe "#assigned_to_company" do
    name = "Client 1"
    let(:client) {create :client, :name => name}
    context "given it has a user belongs to client name = '#{name}'" do
      let(:user) {create :user, :clients => [client]}
      subject { User.assigned_to_company(client)}
      it "should inclue this user" do subject.include?(user).should be_true end
    end

    context "given it has 2 users belongs to client name = '#{name}'" do
      let(:user) {create :user, :clients => [client]}
      let(:other_user) {create :user, :clients => [client]}
      subject { User.assigned_to_company(client)}
      it "should inclue first user" do subject.include?(user).should be_true end
      it "should inclue second user" do subject.include?(other_user).should be_true end
    end
  end
  
  describe "#create_signature" do
    let(:user) {create :user}
    signature = "signature"
    ot_auftragsnummer = 'ot_auftragsnummer'
    before {PdfHelper.should_receive(:create_signature).with(signature, ot_auftragsnummer).and_return(true)}
    subject{user.create_signature(signature, ot_auftragsnummer)}
    it"always call successfully" do should be_true end
  end
end