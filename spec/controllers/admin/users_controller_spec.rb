require 'spec_helper'

describe Admin::UsersController do
  let(:user) { create(:user)}
  before do
    controller.stub!(:authenticate_user!).and_return(true)
    controller.stub!(:check_rights_for_view).and_return(true)
    controller.stub!(:current_user).and_return(user)
  end

  describe "GET Index - admin/users" do
    before {get :index}
    
    # assignments
    context "assigns" do
      subject {assigns}
      it {should include(:users)}

      context "users" do
        subject {assigns[:users]}
        it {should_not be_blank}
        it {subject.size.should <= Settings.DEFAULT_ITEMS_PER_TABLE}
        it "should include current user" do subject.should include(user) end
      end
    end
    
    # response
    context "response" do
      subject {response}
      it {should render_template('index')}
    end
  end

  describe "PUT Update - admin/users/:id" do
    let(:edit_user) {create :user}
    valid_params = {:vorname => "Another vorname", :nachname => "Another nachname"}
    
    # successfully updated with valid parameters
    context "given valid parameters with #{valid_params}" do
      before {put :update, :id => edit_user.id, :user => valid_params}

      # assignments
      context "assigns" do
        subject {assigns}
        it {should include(:user)}
        context "user" do
          subject {assigns[:user]}
          its(:vorname) {should eq(valid_params[:vorname])}
          its(:nachname) {should eq(valid_params[:nachname])}
        end
      end

      # response
      context "response" do
        subject {response}
        it {should be_redirect}
        it {should redirect_to(admin_users_path)}
        context "flash" do
          subject do 
            edit_user.reload
            flash 
          end
          its(:notice) {should_not be_blank}
          let(:string) do
            edit_user.reload
            'Benutzer upgedated: '+controller.class.helpers.link_to(edit_user.full_name, admin_user_path(edit_user))
          end
          its(:notice){should eq(string)}
        end
      end
    end
    
    # unsuccessfully updated with invalid parameters
    # case: vorname and nachname are blank
    invalid_params = {:vorname => "", :nachname => ""}
    context "given invalid parameters with #{invalid_params}" do
      before {put :update, :id => edit_user.id, :user => invalid_params}
      
      # assignments
      context "assigns" do
        subject {assigns}
        it {should include(:user)}
        context "user[errors]" do
          subject {assigns[:user].errors}
          it {should include(:vorname)}
          it {should include(:nachname)}
          context "vorname" do
            it {subject[:vorname].first.should eq("^Geben Sie einen Vornamen an.")}
          end
          context "nachname" do
            it {subject[:nachname].first.should eq("^Geben Sie einen Nachnamen an.")}
          end
        end
      end

      # response
      context "response" do
        subject {response}
        it{ should be_success}
        it {should render_template "admin/users/edit"} 
      end
    end
    
    # unsuccessfully updated with invalid parameters
    # case: email existed
    context "given invalid parameters with email existed" do
      before do
        user = create :user
        put :update, :id => edit_user.id, :user => {:email => user.email}
      end

      # assignments
      context "assigns" do
        subject {assigns}
        it {should include(:user)}
        context "user[errors]" do
          subject {assigns[:user].errors}
          it {should include(:email)}
          context "email" do
            it {subject[:email].first.should eq("^Die angegebene Email wird bereits verwendet Email muss einmalig sein..")}
          end
        end
      end

      # response
      context "response" do
        subject {response}
        it{ should be_success}
        it {should render_template "admin/users/edit"} 
      end
    end
  end

  describe "POST Create - admin/users" do
    valid_params = {:vorname => "Another vorname", 
                    :nachname => "Another nachname", 
                    :email => "new@email.com", 
                    :password => "123456"
                   }
    
    # successfully created with valid parameters
    context "given valid parameters with #{valid_params}" do
      before {post :create, :user => valid_params}
      
      # assignments
      context "assigns" do
        subject {assigns}
        it {should include(:user)}
        context "user" do
          subject {assigns[:user]}
          its(:vorname) {should eq(valid_params[:vorname])}
          its(:nachname) {should eq(valid_params[:nachname])}
          its(:email) {should eq(valid_params[:email])}
        end
      end

      # response
      context "response" do
        subject {response}
        it {should be_redirect}
        it {should redirect_to(admin_users_path)}
        context "flash" do
          subject {flash} 
          its(:notice) {should_not be_blank}
          let(:string) do
            'Benutzer erstellt: '+controller.class.helpers.link_to(assigns[:user].full_name, admin_user_path(assigns[:user]))
          end
          its(:notice){should eq(string)}
        end
      end
    end
    
    # unsuccessfully created with invalid parameters
    # case: vorname, nachname and email are blank
    invalid_params = {:vorname => "", :nachname => "", :email => ""}
    context "given invalid parameters with #{invalid_params}" do
      before do
        post :create, :user => invalid_params
      end

      # assignments
      context "assigns" do
        subject {assigns}
        it {should include(:user)}
        context "user[errors]" do
          subject {assigns[:user].errors}
          it {should include(:vorname)}
          it {should include(:nachname)}
          it {should include(:email)}
          context "vorname" do
            it {subject[:vorname].first.should eq("^Geben Sie einen Vornamen an.")}
          end
          context "nachname" do
            it {subject[:nachname].first.should eq("^Geben Sie einen Nachnamen an.")}
          end
          context "email" do
            it {subject[:email].first.should eq("^Geben Sie eine Email an.")}
          end
        end
      end

      # response
      context "response" do
        subject {response}
        it{ should be_success}
        it {should render_template "admin/users/new"} 
      end
    end
    
    # unsuccessfully created with invalid parameters
    # case: email existed
    context "given invalid parameters with email existed" do
      before do
        user = create :user
        post :create, :user => user.attributes
      end

      # assignments
      context "assigns" do
        subject {assigns}
        it {should include(:user)}
        context "user[errors]" do
          subject {assigns[:user].errors}
          it {should include(:email)}
          context "email" do
            it {subject[:email].first.should eq("^Die angegebene Email wird bereits verwendet Email muss einmalig sein..")}
          end
        end
      end

      # response
      context "response" do
        subject {response}
        it{ should be_success}
        it {should render_template "admin/users/new"} 
      end
    end
  end

  describe "GET new_shortcut - admin/users/new_shortcut" do
    before do
      get :new_shortcut 
    end
    
    # assignments
    context "assigns" do
      subject {assigns}
      it {should include(:user)}

      context "users" do
        subject {assigns[:user]}
        it {should_not be_blank}
      end
    end
    
    # response
    context "response" do
      subject {response}
      it {should render_template('new_shortcut')}
    end
  end

  describe "POST create_shortcut - admin/users/create_shortcut" do
    valid_params = {:email => "sebas.tien@email.com"}
    
    # successfully created with valid parameters
    context "given valid parameters with #{valid_params}" do
      before {post :create_shortcut, :user => valid_params}
      
      # assignments
      context "assigns" do
        subject {assigns}
        it {should include(:user)}
        context "user" do
          subject {assigns[:user]}
          its(:vorname) {should eq(valid_params[:email].split("@")[0].split(".")[0])}
          its(:nachname) {should eq(valid_params[:email].split("@")[0].split(".")[1])}
          its(:email) {should eq(valid_params[:email])}
        end
      end

      # response
      context "response" do
        subject {response}
        it {should be_redirect}
        it {should redirect_to(admin_users_path)}
        context "flash" do
          subject {flash} 
          its(:notice) {should_not be_blank}
          let(:string) do
            'Benutzer erstellt: '+controller.class.helpers.link_to(assigns[:user].full_name, admin_user_path(assigns[:user]))
          end
          its(:notice){should eq(string)}
        end
      end
    end

    # unsuccessfully created with valid parameters
    invalid_params = {:email => "sebas@email.com"}
    context "given invalid parameters with #{invalid_params}" do
      before {post :create_shortcut, :user => invalid_params}
      
      # assignments
      context "assigns" do
        subject {assigns}
        it {should include(:user)}
        context "user[errors]" do
          subject {assigns[:user].errors}
          it {should include(:nachname)}
          context "email" do
            it {subject[:nachname].first.should eq("^Geben Sie einen Nachnamen an.")}
          end
        end
      end

      # response
      context "response" do
        subject {response}
        it{ should be_success}
        it {should render_template "admin/users/new"} 
      end
    end

    # unsuccessfully created with valid parameters
    invalid_params = {:email => ""}
    context "given invalid parameters with #{invalid_params}" do
      before {post :create_shortcut, :user => invalid_params}
      
      # assignments
      context "assigns" do
        subject {assigns}
        it {should include(:user)}
        context "user[errors]" do
          subject {assigns[:user].errors}
          it {should include(:vorname)}
          it {should include(:nachname)}
          it {should include(:email)}
          context "vorname" do
            it {subject[:vorname].first.should eq("^Geben Sie einen Vornamen an.")}
          end
          context "nachname" do
            it {subject[:nachname].first.should eq("^Geben Sie einen Nachnamen an.")}
          end
          context "email" do
            it {subject[:email].first.should eq("^Geben Sie eine Email an.")}
          end
        end
      end

      # response
      context "response" do
        subject {response}
        it{ should be_success}
        it {should render_template "admin/users/new"} 
      end
    end
  end
  
  describe "DELETE Destroy - admin/users/:id" do
    let(:destroy_user) {create :user}
    
    # successfully created with valid parameters
    context "should destroy successfully" do
      before do
        User.should_receive(:find).with(destroy_user.id.to_s).and_return(destroy_user)
        #destroy_user.should_receive(:save).and_return(true)
        delete :destroy, :id => destroy_user.id.to_s
      end
      
      # assignments
      context "assigns" do
        subject {assigns}
        it {should include(:user)}
        context "user" do
          subject do
            assigns[:user] 
          end
          #it{should be_nil}
        end
      end

      # response
      context "response" do
        subject {response}
        it {should be_redirect}
        it {should redirect_to(admin_users_path)}
      end

    end
  end
end