# encoding: utf-8
require 'spec_helper'

describe Admin::JobsController do
  let(:user) { create(:user)}
  let(:job) {create(:job)}
  before do
    controller.stub!(:authenticate_user!).and_return(true)
    controller.stub!(:check_rights_for_view).and_return(true)
    controller.stub!(:current_user).and_return(user)
  end
  describe "Index - admin/jobs" do
    context "given found a job" do   
      before do
        params = {:q => "search term", :page => "1"}
        search = mock("job search")
        Job.should_receive(:search).with(params[:q]).and_return(search)
        search.should_receive(:result).and_return(Job)
        Job.should_receive(:belonging_to_user_client).with(user).and_return(jobs = [job])
        jobs.should_receive(:paginate).with(:per_page => Settings.DEFAULT_ITEMS_PER_TABLE, :page => params[:page]).and_return(jobs)
        get :index, :q => params[:q], :page => params[:page]
      end

      # assignments
      context "assigns" do
        subject {assigns}
        it {should include(:search)}
        it {should include(:jobs)}
        context "jobs" do
          subject {assigns[:jobs]}
          it{should eq([job])}
        end
      end

      # response
      context "response" do
        subject {response}
        it {should render_template('application')}
      end
    end

    context "given not found a job" do   
      before do
        params = {:q => "search term", :page => "1"}
        search = mock("job search")
        Job.should_receive(:search).with(params[:q]).and_return(search)
        search.should_receive(:result).and_return(Job)
        Job.should_receive(:belonging_to_user_client).with(user).and_return(jobs = [])
        jobs.should_receive(:paginate).with(:per_page => Settings.DEFAULT_ITEMS_PER_TABLE, :page => params[:page]).and_return(jobs)
        get :index, :q => params[:q], :page => params[:page]
      end

      # assignments
      context "assigns" do
        subject {assigns}
        it {should include(:search)}
        it {should include(:jobs)}
        context "jobs" do
          subject {assigns[:jobs]}
          it{should eq([])}
        end
      end

      # response
      context "response" do
        subject {response}
        it {should render_template('application')}
      end
    end  
  end
  
  describe "Show - admin/jobs/:job_id" do
    context "given found current job and other_jobs_in_object" do
      let(:another_job) {create :job}
      before do
        Job.should_receive(:find).with(job.id.to_s).and_return(job)
        controller.should_receive(:get_other_jobs_in_object).with(job).and_return([another_job])
        get :show, :id => job.id.to_s
      end
      # assignments
      context "assigns" do
        subject {assigns}
        it {should include(:job)}
        it {should include(:other_jobs_in_object)}
        context "jobs" do
          subject {assigns[:job]}
          it{should eq(job)}
        end

        context "other_jobs_in_object" do
          subject {assigns[:other_jobs_in_object]}
          it{should eq([another_job])}
        end
      end
    end

    context "given not found job with job_id from params" do
      before do
        lambda {Job.find("job_id")}.should raise_error(Exception)
        get :show, :id => job.id.to_s
      end
      it "" do end
    end
  end  
  
  describe "Edit - admin/jobs/:job_id" do
    context "given found current job and other_jobs_in_object" do
      let(:another_job) {create :job}
      before do
        Job.should_receive(:find).with(job.id.to_s).and_return(job)
        controller.should_receive(:get_other_jobs_in_object).with(job).and_return([another_job])
        get :edit, :id => job.id.to_s
      end
      # assignments
      context "assigns" do
        subject {assigns}
        it {should include(:job)}
        it {should include(:other_jobs_in_object)}
        context "jobs" do
          subject {assigns[:job]}
          it{should eq(job)}
        end

        context "other_jobs_in_object" do
          subject {assigns[:other_jobs_in_object]}
          it{should eq([another_job])}
        end
      end
    end

    context "given not found job with job_id from params" do
      before do
        lambda {Job.find("job_id")}.should raise_error(Exception)
        get :edit, :id => job.id.to_s
      end
      it "" do end
    end
  end
  describe "Create - Post admin/jobs" do
    context "given valid params" do
      before do
        params = {:job => "job detail"}
        job.should_receive(:short_description).and_return(@short_description = Faker::Name.name)
        Job.should_receive(:new).with(params[:job]).and_return(job)
        job.should_receive(:save).and_return(true)
        controller.should_receive(:current_view).twice.and_return(@current_view = "admin")
        post :create, params
      end

      # assignments
      context "assigns" do
        subject {assigns}
        it {should include(:job)}
      end

      # response
      context "response" do
        subject {response}
        it {should redirect_to([@current_view, :jobs])}
      end

      context "flash" do
        subject do 
          job.reload
          flash 
        end
        its(:notice) {should_not be_blank}
        let(:string) do
          'Auftrag erstellt: '+ Admin::JobsController.helpers.link_to(@short_description, controller.url_for([@current_view, job]))
        end
        its(:notice){should eq(string)}
      end
    end

    context "given invalid params" do
      before do
        params = {:job => {:client => mock("client")}}
        Job.should_receive(:new).and_return(new_job = mock("job"))
        new_job.should_receive(:save).and_return(false)
        post :create, params
      end

      # assignments
      context "assigns" do
        subject {assigns}
        it {should include(:job)}
      end

      #response
      context "response" do
        subject {response}
        it {should render_template("new")}
      end
    end
  end
  
  describe "Update - Put admin/jobs/:job_id" do
    context "given valid params" do
      before do
        params = {:job => "job detail", :id => "id"}
        job.should_receive(:short_description).and_return(@short_description = Faker::Name.name)
        Job.should_receive(:find).with(params[:id]).and_return(job)
        job.should_receive(:update_attributes).and_return(true)
        controller.should_receive(:current_view).twice.and_return(@current_view = "admin")
        put :update, params
      end

      # assignments
      context "assigns" do
        subject {assigns}
        it {should include(:job)}
      end

      # response
      context "response" do
        subject {response}
        it {should redirect_to([@current_view, job])}
      end

      context "flash" do
        subject do 
          flash 
        end
        its(:notice) {should_not be_blank}
        let(:string) do
          'Auftrag upgedated: '+ Admin::JobsController.helpers.link_to(@short_description, controller.url_for([@current_view, job]))
        end
        its(:notice){should eq(string)}
      end
    end

    context "given invalid params" do
      before do
        params = {:job => "job detail", :id => "id"}
        Job.should_receive(:find).and_return(new_job = mock("job"))
        new_job.should_receive(:update_attributes).and_return(false)
        put :update, params
      end

      # assignments
      context "assigns" do
        subject {assigns}
        it {should include(:job)}
      end

      #response
      context "response" do
        subject {response}
        it {should render_template("edit")}
      end
    end
  end
  describe "Destroy - Delete admin/jobs/:job_id" do
    context "given detroy successfully" do
      before do
        params = {:job => "job detail", :id => "id"}
        Job.should_receive(:find).and_return(new_job = mock("job"))
        new_job.should_receive(:destroy).and_return(true)
        controller.should_receive(:current_view).and_return(@current_view = "admin")
        delete :destroy, params
      end

      # assignments
      context "assigns" do
        subject {assigns}
        it {should include(:job)}
      end

      #response
      context "response" do
        subject {response}
        it {should redirect_to([@current_view, :jobs])}
      end
      
      context "flash" do
        subject do 
          flash 
        end
        its(:notice) {should_not be_blank}
        its(:notice){should eq("Auftrag gelöscht.")}
      end
    end

    context "given detroy successfully" do
      before do
        params = {:job => "job detail", :id => "id"}
        Job.should_receive(:find).and_return(new_job = mock("job"))
        new_job.should_receive(:destroy).and_return(false)
        controller.should_receive(:current_view).and_return(@current_view = "admin")
        delete :destroy, params
      end

      # assignments
      context "assigns" do
        subject {assigns}
        it {should include(:job)}
      end

      #response
      context "response" do
        subject {response}
        it {should redirect_to([@current_view, :jobs])}
      end
      
      context "flash" do
        subject do 
          flash 
        end
        its(:notice) {should_not be_blank}
        its(:notice){should eq("Unsuccessfully destroyed.")}
      end
    end
  end
end