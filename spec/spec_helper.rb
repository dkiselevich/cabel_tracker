require 'rubygems'
require 'spork'
require 'faker'

Spork.prefork do
  # Loading more in this block will cause your tests to run faster. However,
  # if you change any configuration or code from libraries loaded here, you'll
  # need to restart spork for it take effect.

  ENV['RAILS_ENV'] ||= 'test'
  require File.expand_path("../../config/environment", __FILE__)
  require 'rspec/rails'
  require 'rspec/autorun'
  require 'simplecov'
  require 'simplecov-rcov'
  ActiveSupport::Deprecation.silenced = true
  
  # Requires supporting ruby files with custom matchers and macros, etc,
  # in spec/support/ and its subdirectories.
  Dir[Rails.root.join('spec/support/**/*.rb')].each { |f| require f }

  RSpec.configure do |config|
    # Use symbols as metadata
    config.treat_symbols_as_metadata_keys_with_true_values = true

    config.mock_with :rspec
    config.fixture_path = "#{Rails.root}/spec/fixtures"
    config.use_transactional_fixtures = true
    # Support FactoryGirl short syntax without FactoryGirl prefix
    config.include FactoryGirl::Syntax::Methods
    ActiveSupport::Dependencies.clear
  end

  SimpleCov.start 'rails'
  class SimpleCov::Formatter::MergedFormatter
    def format(result)
      SimpleCov::Formatter::HTMLFormatter.new.format(result)
      SimpleCov::Formatter::RcovFormatter.new.format(result)
    end
  end

  SimpleCov.formatter = SimpleCov::Formatter::MergedFormatter
  SimpleCov.coverage_dir('reports/coverage')
end

Spork.each_run do
  # This code will be run each time you run your specs.

  # Requires supporting ruby files with custom matchers and macros, etc,
  # in spec/support/ and its subdirectories.
  #Dir[Rails.root.join('spec/support/**/*.rb')].each { |f| require f }
  Dir["#{Rails.root}/app/**/*.rb"].each {|f| load f}

  FactoryGirl.reload
end