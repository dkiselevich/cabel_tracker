require "spec_helper"

describe QualityReportsController do
  describe "routing" do

    it "routes to #index" do
      get("/quality_reports").should route_to("quality_reports#index")
    end

    it "routes to #new" do
      get("/quality_reports/new").should route_to("quality_reports#new")
    end

    it "routes to #show" do
      get("/quality_reports/1").should route_to("quality_reports#show", :id => "1")
    end

    it "routes to #edit" do
      get("/quality_reports/1/edit").should route_to("quality_reports#edit", :id => "1")
    end

    it "routes to #create" do
      post("/quality_reports").should route_to("quality_reports#create")
    end

    it "routes to #update" do
      put("/quality_reports/1").should route_to("quality_reports#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/quality_reports/1").should route_to("quality_reports#destroy", :id => "1")
    end

  end
end
