require "spec_helper"

describe Management::QualityReportsController do
  describe "routing" do

    it "routes to #index" do
      get("/management/quality_reports").should route_to("management/quality_reports#index")
    end

    it "routes to #new" do
      get("/management/quality_reports/new").should route_to("management/quality_reports#new")
    end

    it "routes to #show" do
      get("/management/quality_reports/1").should route_to("management/quality_reports#show", :id => "1")
    end

    it "routes to #edit" do
      get("/management/quality_reports/1/edit").should route_to("management/quality_reports#edit", :id => "1")
    end

    it "routes to #create" do
      post("/management/quality_reports").should route_to("management/quality_reports#create")
    end

    it "routes to #update" do
      put("/management/quality_reports/1").should route_to("management/quality_reports#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/management/quality_reports/1").should route_to("management/quality_reports#destroy", :id => "1")
    end

  end
end
