require "spec_helper"

describe BugtrackersController do
  describe "routing" do

    it "routes to #index" do
      get("/bugtrackers").should route_to("bugtrackers#index")
    end

    it "routes to #new" do
      get("/bugtrackers/new").should route_to("bugtrackers#new")
    end

    it "routes to #show" do
      get("/bugtrackers/1").should route_to("bugtrackers#show", :id => "1")
    end

    it "routes to #edit" do
      get("/bugtrackers/1/edit").should route_to("bugtrackers#edit", :id => "1")
    end

    it "routes to #create" do
      post("/bugtrackers").should route_to("bugtrackers#create")
    end

    it "routes to #update" do
      put("/bugtrackers/1").should route_to("bugtrackers#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/bugtrackers/1").should route_to("bugtrackers#destroy", :id => "1")
    end

  end
end
