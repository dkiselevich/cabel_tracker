require "spec_helper"

describe Purchase::StoragesController do
  describe "routing" do

    it "routes to #index" do
      get("/purchase/storages").should route_to("purchase/storages#index")
    end

    it "routes to #new" do
      get("/purchase/storages/new").should route_to("purchase/storages#new")
    end

    it "routes to #show" do
      get("/purchase/storages/1").should route_to("purchase/storages#show", :id => "1")
    end

    it "routes to #edit" do
      get("/purchase/storages/1/edit").should route_to("purchase/storages#edit", :id => "1")
    end

    it "routes to #create" do
      post("/purchase/storages").should route_to("purchase/storages#create")
    end

    it "routes to #update" do
      put("/purchase/storages/1").should route_to("purchase/storages#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/purchase/storages/1").should route_to("purchase/storages#destroy", :id => "1")
    end

  end
end
