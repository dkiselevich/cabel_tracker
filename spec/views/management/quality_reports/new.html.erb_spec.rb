require 'spec_helper'

describe "management/quality_reports/new" do
  before(:each) do
    assign(:management_quality_report, stub_model(Management::QualityReport,
      :planned_measures_next_two_month => "MyText",
      :detected_quality_problems => "MyText",
      :identified_necessary_measures => "MyText",
      :launch_plannings => "MyText",
      :implemented_measures => "MyString",
      :sustainability_control => "MyText",
      :user_id => 1
    ).as_new_record)
  end

  it "renders new management_quality_report form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => management_quality_reports_path, :method => "post" do
      assert_select "textarea#management_quality_report_planned_measures_next_two_month", :name => "management_quality_report[planned_measures_next_two_month]"
      assert_select "textarea#management_quality_report_detected_quality_problems", :name => "management_quality_report[detected_quality_problems]"
      assert_select "textarea#management_quality_report_identified_necessary_measures", :name => "management_quality_report[identified_necessary_measures]"
      assert_select "textarea#management_quality_report_launch_plannings", :name => "management_quality_report[launch_plannings]"
      assert_select "input#management_quality_report_implemented_measures", :name => "management_quality_report[implemented_measures]"
      assert_select "textarea#management_quality_report_sustainability_control", :name => "management_quality_report[sustainability_control]"
      assert_select "input#management_quality_report_user_id", :name => "management_quality_report[user_id]"
    end
  end
end
