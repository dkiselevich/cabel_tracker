require 'spec_helper'

describe "management/quality_reports/index" do
  before(:each) do
    assign(:management_quality_reports, [
      stub_model(Management::QualityReport,
        :planned_measures_next_two_month => "MyText",
        :detected_quality_problems => "MyText",
        :identified_necessary_measures => "MyText",
        :launch_plannings => "MyText",
        :implemented_measures => "Implemented Measures",
        :sustainability_control => "MyText",
        :user_id => 1
      ),
      stub_model(Management::QualityReport,
        :planned_measures_next_two_month => "MyText",
        :detected_quality_problems => "MyText",
        :identified_necessary_measures => "MyText",
        :launch_plannings => "MyText",
        :implemented_measures => "Implemented Measures",
        :sustainability_control => "MyText",
        :user_id => 1
      )
    ])
  end

  it "renders a list of management/quality_reports" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => "Implemented Measures".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => 1.to_s, :count => 2
  end
end
