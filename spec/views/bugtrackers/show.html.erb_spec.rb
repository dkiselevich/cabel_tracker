require 'spec_helper'

describe "bugtrackers/show" do
  before(:each) do
    @bugtracker = assign(:bugtracker, stub_model(Bugtracker,
      :user_id => 1,
      :email => "Email",
      :bug => "Bug",
      :location => "Location",
      :description => "MyText",
      :bug_solved => false
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/1/)
    rendered.should match(/Email/)
    rendered.should match(/Bug/)
    rendered.should match(/Location/)
    rendered.should match(/MyText/)
    rendered.should match(/false/)
  end
end
