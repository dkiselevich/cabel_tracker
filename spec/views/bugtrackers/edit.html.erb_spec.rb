require 'spec_helper'

describe "bugtrackers/edit" do
  before(:each) do
    @bugtracker = assign(:bugtracker, stub_model(Bugtracker,
      :user_id => 1,
      :email => "MyString",
      :bug => "MyString",
      :location => "MyString",
      :description => "MyText",
      :bug_solved => false
    ))
  end

  it "renders the edit bugtracker form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", bugtracker_path(@bugtracker), "post" do
      assert_select "input#bugtracker_user_id[name=?]", "bugtracker[user_id]"
      assert_select "input#bugtracker_email[name=?]", "bugtracker[email]"
      assert_select "input#bugtracker_bug[name=?]", "bugtracker[bug]"
      assert_select "input#bugtracker_location[name=?]", "bugtracker[location]"
      assert_select "textarea#bugtracker_description[name=?]", "bugtracker[description]"
      assert_select "input#bugtracker_bug_solved[name=?]", "bugtracker[bug_solved]"
    end
  end
end
