require 'spec_helper'

describe "bugtrackers/index" do
  before(:each) do
    assign(:bugtrackers, [
      stub_model(Bugtracker,
        :user_id => 1,
        :email => "Email",
        :bug => "Bug",
        :location => "Location",
        :description => "MyText",
        :bug_solved => false
      ),
      stub_model(Bugtracker,
        :user_id => 1,
        :email => "Email",
        :bug => "Bug",
        :location => "Location",
        :description => "MyText",
        :bug_solved => false
      )
    ])
  end

  it "renders a list of bugtrackers" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => "Email".to_s, :count => 2
    assert_select "tr>td", :text => "Bug".to_s, :count => 2
    assert_select "tr>td", :text => "Location".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
  end
end
