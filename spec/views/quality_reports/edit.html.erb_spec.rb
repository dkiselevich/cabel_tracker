require 'spec_helper'

describe "quality_reports/edit" do
  before(:each) do
    @quality_report = assign(:quality_report, stub_model(QualityReport,
      :planned_measures_next_two_month => "MyText",
      :detected_quality_problems => "MyText",
      :identified_necessary_measures => "MyText",
      :launch_plannings => "MyText",
      :implemented_measures => "MyString",
      :sustainability_control => "MyText",
      :user_id => 1
    ))
  end

  it "renders the edit quality_report form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => quality_reports_path(@quality_report), :method => "post" do
      assert_select "textarea#quality_report_planned_measures_next_two_month", :name => "quality_report[planned_measures_next_two_month]"
      assert_select "textarea#quality_report_detected_quality_problems", :name => "quality_report[detected_quality_problems]"
      assert_select "textarea#quality_report_identified_necessary_measures", :name => "quality_report[identified_necessary_measures]"
      assert_select "textarea#quality_report_launch_plannings", :name => "quality_report[launch_plannings]"
      assert_select "input#quality_report_implemented_measures", :name => "quality_report[implemented_measures]"
      assert_select "textarea#quality_report_sustainability_control", :name => "quality_report[sustainability_control]"
      assert_select "input#quality_report_user_id", :name => "quality_report[user_id]"
    end
  end
end
