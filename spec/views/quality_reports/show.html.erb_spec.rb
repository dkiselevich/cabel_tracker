require 'spec_helper'

describe "quality_reports/show" do
  before(:each) do
    @quality_report = assign(:quality_report, stub_model(QualityReport,
      :planned_measures_next_two_month => "MyText",
      :detected_quality_problems => "MyText",
      :identified_necessary_measures => "MyText",
      :launch_plannings => "MyText",
      :implemented_measures => "Implemented Measures",
      :sustainability_control => "MyText",
      :user_id => 1
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/MyText/)
    rendered.should match(/MyText/)
    rendered.should match(/MyText/)
    rendered.should match(/MyText/)
    rendered.should match(/Implemented Measures/)
    rendered.should match(/MyText/)
    rendered.should match(/1/)
  end
end
