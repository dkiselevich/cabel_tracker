require 'spec_helper'

describe "purchase/storages/index" do
  before(:each) do
    assign(:purchase_storages, [
      stub_model(Purchase::Storage),
      stub_model(Purchase::Storage)
    ])
  end

  it "renders a list of purchase/storages" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
  end
end
