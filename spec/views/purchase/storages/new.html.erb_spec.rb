require 'spec_helper'

describe "purchase/storages/new" do
  before(:each) do
    assign(:purchase_storage, stub_model(Purchase::Storage).as_new_record)
  end

  it "renders new purchase_storage form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => purchase_storages_path, :method => "post" do
    end
  end
end
