require 'spec_helper'

describe "purchase/storages/edit" do
  before(:each) do
    @purchase_storage = assign(:purchase_storage, stub_model(Purchase::Storage))
  end

  it "renders the edit purchase_storage form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => purchase_storages_path(@purchase_storage), :method => "post" do
    end
  end
end
