FactoryGirl.define do
  factory :modem_type do
    sequence(:name) { |n| "Modem type #{n}" }
  end
end