FactoryGirl.define do
  factory :scheduling_user_group do
    sequence(:name) { |n| "scheduling user group #{n}" }
    client
    sender_email { |n| "sender_email#{n}@email.com"}
  end
end