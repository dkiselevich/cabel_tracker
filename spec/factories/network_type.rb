FactoryGirl.define do
  factory :network_type do
    sequence(:name) { |n| "Network type #{n}" }
  end
end