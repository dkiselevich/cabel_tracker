FactoryGirl.define do
  factory :user do
    sequence(:email) { |n| "testuser#{n}@gmail.com" }
    password 'password'
    password_confirmation { password }
    vorname Faker::Name.name
    nachname Faker::Name.name
    active true
  end

  factory :admin_user, :parent => :user do
  	is_admin true
  end

  factory :technician_user, :parent => :user do
    is_technician true
  end

  factory :accountant_user, :parent => :user do
    is_accountant true
  end

  factory :boss_user, :parent => :user do
    is_accountant true
  end

  factory :controller_user, :parent => :user do
    is_accountant true
  end
end
