FactoryGirl.define do
  factory :job do
    client
    job_type
    workorder Time.now.to_i.to_s[0..6]
    kunde_kundennummer Time.now.to_i.to_s[0..9]
    ot_auftragsnummer "OT- #{Time.now.to_i.to_s[0..8]}"
    modem_type
    kunde_plz Time.now.to_i.to_s[0..5]
    kunde_ort Faker::Name.name
    kunde_strasse Faker::Name.name
    sequence(:kunde_hausnummer) { |n| n + 10 }
    kunde_nachname Faker::Name.name
    eigentuemer_plz Time.now.to_i.to_s[0..5]
    eigentuemer_ort Faker::Name.name
    eigentuemer_strasse Faker::Name.name 
    sequence(:eigentuemer_hausnummer) { |n| n + 20 }
    eigentuemer_nachname Faker::Name.name
    sequence(:wohneinheiten) { |n| n + 20 }
    kunde_telefon_mobil Time.now.to_i
    network_type
  end
end