FactoryGirl.define do
  factory :appointment do
    day Time.now.strftime("%Y-%m-%d")
    start_time Time.now
    end_time Time.now + 1.days
    job
    user
  end
end