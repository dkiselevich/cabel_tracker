FactoryGirl.define do
  factory :completion do
    appointment
    technician :factory => :technician_user
    clever_pro true
    unbundled true
    modemtyp_id Random.rand(100)
    umzug true
    installationsart_id 1
    installationsart_sonstiges Faker::Name.name
    fi_sicherung_id Random.rand(100)
    potentialausgleich_id Random.rand(100)
    netztopologie_id Random.rand(100)
    vorverstaerker_id Random.rand(100)
    vorverstaerker_bezeichnung "BKD35"
    geschlossener_bvt true
    auftragsart_id Random.rand(100)
    kellerquerverkabelung true
    kellerquerverkabelung_laenge Random.rand(100)
    wohneinheiten Random.rand(100)
    D130 Random.rand(100)
    K5 Random.rand(100)
    K29 Random.rand(100)
    D802 Random.rand(100)
    HFC_MAC "C0:25:06:CC:0E:68"
    adresspruefung true
    uebrige_modems_im_gebaeude_per_SBC_portal_geprueft true
    tv_empfang_beim_kunden_erfolgreich_geprueft true
    internetzugang_mit_montagelaptop_geprueft true
    telefon_abgehend_erfolgreich_getestet true
    telefon_ankommend_erfolgreich_getestet true
    messwerte_photo_file_name Faker::Name.name
    bvt_photo_file_name Faker::Name.name
    uep_photo_file_name Faker::Name.name
    pot_schiene_photo_file_name Faker::Name.name
    pot_gas_photo_file_name Faker::Name.name
    pot_wasser_photo_file_name Faker::Name.name
    pot_heizung_photo_file_name Faker::Name.name
    pot_hak_photo_file_name Faker::Name.name
    pot_oel_photo_file_name Faker::Name.name
    signature Faker::Name.name

  end
end
  
