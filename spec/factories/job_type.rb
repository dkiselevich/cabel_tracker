FactoryGirl.define do
  factory :job_type do
    sequence(:name) { |n| "Job type #{n}" }
  end
end