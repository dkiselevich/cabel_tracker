When /^I accept the confirm dialog$/ do
  #page.driver.browser.switch_to.alert.accept
  page.execute_script 'window.confirm = function () { return true }'
end

When /^I am waiting for (\d+) seconds$/ do |number|
  sleep 5
end

Then /^I should go to the login page$/ do
  step %Q(I should see "Anmeldung")
end
