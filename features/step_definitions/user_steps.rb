Then /^I should see (\d+) users in list$/ do |number|
  User.all.size.should eq(number.to_i)
  User.all.each do |user|
    step %Q{I should see "#{user.nachname}" within ".inner"} 
  end
end

When /^I click on delete button of "(.*?)"$/ do |email|
  user = User.find_by_email(email)
  within '.inner' do
  	within "table" do
      tr = page.find("tr.user-#{user.id}")
      tr.find("a[data-method='delete']").click()
  	end
  end if user
end

When /^I click on show button of "(.*?)"$/ do |email|
  user = User.find_by_email(email)
  within '.inner' do
    within "table" do
      tr = page.find("tr.user-#{user.id}")
      tr.find("a[href='/admin/users/#{user.id}']").click()
    end
  end if user
end

When /^I click on edit button of "(.*?)"$/ do |email|
  user = User.find_by_email(email)
  within '.inner' do
    within "table" do
      tr = page.find("tr.user-#{user.id}")
      tr.find("a[href='/admin/users/#{user.id}/edit']").click()
    end
  end if user
end

Then /^I should go to the show user page$/ do
  step %Q(I should see "Benutzerdaten")
end

Then /^I should see detail information of "(.*?)"$/ do |email|
  user = User.find_by_email(email)
  if user
    step %(the "user_vorname" field should contain "#{user.vorname}")
    step %(the "user_nachname" field should contain "#{user.nachname}")
    step %(the "user_email" field should contain "#{user.email}")
  end
end

When /^I fill in some valid information(?: from "([^"]*)")?$/ do |page|
  new_info = FactoryGirl.build(:user)
  step %Q(I fill in "user_vorname" with "#{new_info.vorname}")
  step %Q(I fill in "user_nachname" with "#{new_info.nachname}")
  step %Q(I fill in "user_email" with "#{new_info.email}")
  if page && page == "new"
    step %Q(I fill in "user_password" with "password")
    step %Q(I fill in "user_password_confirmation" with "password")
  end
end

When /^I fill in some invalid information$/ do
  step %Q(I fill in "user_vorname" with "")
  step %Q(I fill in "user_nachname" with "")
  step %Q(I fill in "user_email" with "")
end

When /^I click on submit button$/ do
  within "#wrapper" do
    page.find("div.navform button.button").click()
  end
end

Then /^I should go to the list users page$/ do
  step %Q(I should see "Alle Benutzer")
end

Then /^I should see successful notice of "(.*?)"$/ do |email|
  user = User.find_by_email(email)
  if user 
    notice = 'Benutzer upgedated: '+ Admin::UsersController.helpers.link_to(user.full_name, admin_user_path(user))
    step %Q(I should see "#{notice}")
  end
end

Then /^I should see successful notice after creating user$/ do
  user = User.last
  if user
    notice = 'Benutzer erstellt: '+ Admin::UsersController.helpers.link_to(user.full_name, admin_user_path(user))
    puts notice.inspect
    step %Q(I should see "#{notice}")
  end
end

Then /^I should see error messages at the top page$/ do
  within "#error_explanation" do
    step %Q(I should see "Bitte korrigieren Sie die Angaben:")
    step %Q(I should see "Geben Sie einen Vornamen an.")
    step %Q(I should see "Geben Sie einen Nachnamen an.")
    step %Q(I should see "Geben Sie eine Email an.")
  end
end

Then /^I have permissions on "(.*?)" any user$/ do |arg1|
  # for new
  step %Q{I should see "Neuen Benutzer anlegen"}
  User.all.each do |user|
    within '.inner' do
      within "table" do
        within "tr.user-#{user.id}" do
          page.all("a").length.should eq(arg1.split(",").size - 1)
        end
      end
    end
  end
end

