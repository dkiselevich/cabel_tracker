Given /^I am an authenticated user$/ do
  step %Q{I exist in the application}
  step %Q{I am signed in}
end

Given /^I exist in the application as "(.*?)"$/ do |role|
  @current_user = FactoryGirl.create(:admin_user)
end

When /^I am signed in$/ do
  login_as @current_user
end

Given /^I am an authenticated admin user$/ do
  step %Q{I exist in the application as "admin"}
  step %Q{I am signed in}
end

Given /^I signed in with email "(.*?)"$/ do |email|
  login_with(email)
end