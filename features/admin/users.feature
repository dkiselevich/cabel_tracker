@ok
Feature:  Users Management
  In order users management
  As a logged user with generally admin
  I should be able to show, edit, update, delete anything
  As a other roles mostly just create, show, edit but not delete
  
  Background:
    Given a admin user exists with nachname: "Admin Norname 1", email: "admin@ct.com"
      And a user exists with nachname: "Norname 1", email: "normal@ct.com"
      And I signed in with email "admin@ct.com"
      And I go to the admin page
      And I follow "Benutzer"
   
  Scenario: Show list of users
    Then I should see "Alle Benutzer"
      And I should see 2 users in list
      And I have permissions on "create,show,edit,delete" any user

  @javascript
  Scenario: Destroy another user
    Then I should see "Norname 1" within ".inner"
    When I click on delete button of "normal@ct.com"
      And I accept the confirm dialog
    Then I am waiting for 5 seconds
    And I should see 1 users in list
  
  @javascript
  Scenario: Destroy myself
    When I click on delete button of "admin@ct.com"
      And I accept the confirm dialog
    Then I am waiting for 5 seconds
      Then I should go to the login page

  
  Scenario: Show a user
    Then I should see "Norname 1" within ".inner"
    When I click on show button of "normal@ct.com"
    Then I should go to the show user page
      And I should see detail information of "normal@ct.com"

  Scenario: Edit another user with valid params
    Then I should see "Norname 1" within ".inner"
    When I click on edit button of "normal@ct.com"
    When I fill in some valid information
      And I click on submit button
    Then I should go to the list users page

  Scenario: Edit another user with invalid params: vorname, nachname, email
    Then I should see "Norname 1" within ".inner"
    When I click on edit button of "normal@ct.com"
    When I fill in some invalid information
      And I click on submit button
    Then I should see error messages at the top page
  
  Scenario: Create a new user with vaild params
    Then I should see "Neuen Benutzer anlegen"
    When I follow "Neuen Benutzer anlegen"
      And I fill in some valid information from "new"
    And I click on submit button
    Then I should go to the list users page
  
  Scenario: Create a new user with invaild params: vorname, nachname, email, password
    Then I should see "Neuen Benutzer anlegen"
    When I follow "Neuen Benutzer anlegen"
      And I fill in some invalid information
    And I click on submit button
    Then I should see error messages at the top page