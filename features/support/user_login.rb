module UserLogin
  def login_as(user)
    login_with(user.email, user.password)
  end

  def login_with(email, password = 'password')
    visit root_path
    within 'form#new_user' do
      fill_in 'user_email', with: email
      fill_in 'user_password', with: password

      click_button 'anmelden'
    end
  end

  def logout
    visit root_path
    click_link('Sign out')
  end
end
World(UserLogin)